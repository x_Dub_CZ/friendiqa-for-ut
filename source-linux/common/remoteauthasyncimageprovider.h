//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef REMOTEAUTHIMAGEPROVIDER_H
#define REMOTEAUTHIMAGEPROVIDER_H

#include <QObject>
#include <QQuickAsyncImageProvider>
#include <QImage>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>


class AsyncImageResponse : public QQuickImageResponse
{
    Q_OBJECT
public:
    explicit AsyncImageResponse(QNetworkRequest req, QSize requestedSize);
    QQuickTextureFactory *textureFactory() const;

public slots:
    void onResponseFinished();

protected:
    QNetworkAccessManager m_imageLoader;
    QNetworkReply* m_reply;
    QSize m_requestedSize;
    QImage m_resultImage;
    int m_index;
    QString m_id;
    QImage m_image;
};

class RemoteAuthAsyncImageProvider : public QObject, public QQuickAsyncImageProvider
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString contacturl READ contacturl WRITE setContacturl NOTIFY contacturlChanged)
    Q_PROPERTY(QString login READ login WRITE setLogin NOTIFY loginChanged)

public:
    explicit RemoteAuthAsyncImageProvider();
    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override;
    QString url() const;
    QString contacturl() const;
    QString login() const;

signals:
    void contacturlChanged();
    void urlChanged();
    void loginChanged();

public slots:
    void setContacturl(QString contacturl);
    void setUrl(QString url);
    void setLogin(QString login);

private:
    QByteArray buffer;
    QString m_url;
    QString m_contacturl;
    QString m_login;
    QString bufferToString();
};

#endif // REMOTEAUTHIMAGEPROVIDER_H
