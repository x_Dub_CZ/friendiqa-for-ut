//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// message.qml
// message with buttons
import QtQuick 2.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/smiley.js" as Smileyjs
import "qrc:/js/news.js" as Newsjs
import "qrc:/qml/genericqml"
import "qrc:/qml/newsqml"

Rectangle{
    color:Material.dialogColor
    width:parent.width
    height: messageColumn.height+2*mm
    id:messageSend
    visible:conversation || (newstab.newstabstatus!="Search")?true:false
    property string parentId: ""
    property bool textfocus: false
    property bool conversation: false
    property string reply_to_user:""
    property alias bodyMessage: bodyField.text
    property var media_ids:[]
    property var contacts: []
    property var groups: []
    property var contact_allow:login.hasOwnProperty("permissions")?login.permissions[0]:[]
    property var contact_deny:login.hasOwnProperty("permissions")?login.permissions[1]:[]
    property var group_allow:login.hasOwnProperty("permissions")?login.permissions[2]:[]
    property var group_deny:login.hasOwnProperty("permissions")?login.permissions[3]:[]

    function directmessagePrepare(friend){
        messageSend.state="active";
        reply_to_user=friend.screen_name;
        receiverLabel.text=friend.screen_name;
    }

    function sendUrls(urls){
            attachImage(urls);
            messageSend.state="active";
    }

    function sendtext(text){
        if(text){
            if (text.subject=="undefined"){text.subject=""}
            if(text.plaintext.lastIndexOf(".jpg")>-1 || text.plaintext.lastIndexOf(".jpeg")>-1 || text.plaintext.lastIndexOf(".png")>-1 || text.plaintext.lastIndexOf(".gif")>-1){
                text.plaintext="<a href="+text.plaintext+"><img src="+text.plaintext+"></a>"}
            bodyField.text=text.subject+"\n"+text.plaintext;
            messageSend.state="active";
        }
    }

    function attachImage(url){
        if(url!=""){
            imageUploadDialog.visible=true;
            imageUploadDialog.attachImage(url)
        }
    }

    function statusUpdate(title,status,in_reply_to_status_id) {
        //xhr.url= login.server + "/api/statuses/update.json";
        try{newsBusy.running=true;conversationBusy.running=true}catch(e){}
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setUrl(login.server);
        xhr.setApi("/api/statuses/update");
        xhr.clearParams();
        xhr.setParam("source", "Friendiqa");
        xhr.setParam("htmlstatus", status);
        if (parentId!="") {xhr.setParam("in_reply_to_status_id", parentId)};
        if (title!=="") {xhr.setParam("title", title)};
        if (group_allow.length>0) {xhr.setParam("group_allow", Helperjs.cleanArray(group_allow))};
        if (group_deny.length>0) {xhr.setParam("group_deny",  Helperjs.cleanArray(group_deny))};
        if (contact_allow.length>0) {xhr.setParam("contact_allow", Helperjs.cleanArray(contact_allow))};
        if (contact_deny.length>0) {xhr.setParam("contact_deny",  Helperjs.cleanArray(contact_deny))};
        if (media_ids.length>0) {
            xhr.setParam("media_ids", media_ids.join());
        }
        xhr.post();
        Newsjs.storeHashtags(login,db,status,root)
    }

    function dmUpdate(title,text,replyto,screen_name) {
        newsBusy.running=true;
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setUrl(login.server);
        xhr.setApi("/api/direct_messages/new");
        xhr.clearParams();
        xhr.setParam("text", text);
        xhr.setParam("screen_name", screen_name);
        if (parentId!="") {xhr.setParam("replyto", replyto)};
        xhr.post();
    }

    function setParent(newsitemobject){
        if (newsitemobject!=""){
            messageSend.state="conversation"
            messageSend.reply_to_user=newsitemobject.user.screen_name;
            messageSend.parentId=newsitemobject.id
        } else {
            messageSend.state=null;
            messageSend.reply_to_user="";
            messageSend.parentId="";
            bodyField.text="";
        }
    }

    function contactmenu(letter){
        Newsjs.listFriends(login,db,function(contacts){
            contactModel.clear();
            for (var i=0;i<contacts.length;i++){
                if(contacts[i].description!="" &&contacts[i].description!=null){
                    contacts[i].description=Qt.atob(contacts[i].description)
                }
                contactModel.append({"contact":contacts[i]})
            }
            contactSelector.visible=true
        },letter);
    }

    function hashtagmenu(){
        Newsjs.listHashtags(login,db,function(tags){
            tagModel.clear();
            for (var i=0;i<tags.length;i++){
                tagModel.append({"tag":tags[i]})
            }
            tagSelector.visible=true
        });
    }
    //    Flickable{
    //        anchors.fill: parent
    //        contentHeight: messageColumn.height
    //        boundsBehavior: Flickable.StopAtBounds
    DropArea{
        anchors.fill: parent
        onDropped: {
            if (messageSend.state==""){messageSend.state="active"}
            if (drop.keys.includes('text/uri-list')){
                var droptext = drop.text.replace(/(\r\n|\n|\r)/gm, ",");
                imageUploadDialog.visible=true;
                 attachImage(droptext)}
            else if (drop.keys.includes('text/html')){
                bodyField.append(drop.html)}
            else if (drop.keys.includes('text/plain')){
                bodyField.append(drop.text)
            }
        }
    }
    Column {
        y:0.5*mm
        id:messageColumn
        spacing: 0.5*mm
        width: parent.width
        height: 2.6*root.fontFactor*osSettings.bigFontSize+stackTypeDescription.height
        Label{id:stackTypeDescription
            width: parent.width
            horizontalAlignment:Text.AlignHCenter
            text: !conversation &&newsSwipeview.stacktype?qsTr(newsSwipeview.stacktype):""
            font.pointSize: osSettings.bigFontSize

            BlueButton{
                width: root.fontFactor*osSettings.bigFontSize
                height:stackTypeDescription.height
                anchors.left: stackTypeDescription.left
                anchors.leftMargin: 2*root.fontFactor*osSettings.bigFontSize
                visible: newsSwipeview.currentIndex!=0
                text:"\uf053"
                fontColor: Material.hintTextColor
                border.color: "transparent"
                color:"transparent"
                radius:0
                onClicked: {newsSwipeview.currentIndex=newsSwipeview.currentIndex-1}
            }

            BlueButton{
                width: root.fontFactor*osSettings.bigFontSize
                height:stackTypeDescription.height
                anchors.right: stackTypeDescription.right
                anchors.rightMargin: 2*root.fontFactor*osSettings.bigFontSize
                visible: newsSwipeview.currentIndex!=newsSwipeview.length-1
                text:"\uf054"
                fontColor: Material.hintTextColor
                border.color: "transparent"
                color:"transparent"
                radius:0
                onClicked: {newsSwipeview.currentIndex=newsSwipeview.currentIndex+1}
            }
    }
        TextArea{
            id:receiverLabel
            width: messageColumn.width
            font.pointSize: osSettings.bigFontSize
            placeholderText:qsTr("to:")
            text: ""
            visible:false
            onTextChanged: {
                if (text!=""){contactmenu(text)} else {var receiver=getText(0,cursorPosition);contactmenu(receiver+preeditText)}}
        }

        TextField {
            id: titleField
            x: 0.5*mm
            width: parent.width-mm
            font.pointSize: osSettings.systemFontSize
            placeholderText: qsTr("Title (optional)")
            visible: false
            onVisibleChanged: if ((visible==true)&&(conversation==true)){
                                  conversationView.contentY=conversationView.contentY+titleField.height
                              }
        }

        Rectangle{
            color: Material.backgroundColor
            radius: 0.5*mm
            visible:(conversation || (newsSwipeview.stacktype!="Notifications"))
            x:mm
            width: parent.width-2*mm
            height:Math.max(bodyField.contentHeight+4*mm,2.5*root.fontFactor*osSettings.bigFontSize)

            TextArea {
                id: bodyField
                property string contactprefix:""
                property string placeholder: osSettings.osType=="Linux"? qsTr(" Drop your Content here."):""
                anchors.fill: parent
                font.pointSize: osSettings.systemFontSize
                font.family: "Noto Sans"
                wrapMode: Text.Wrap
                selectByMouse: true
                placeholderText: conversation?"": (qsTr("What's on your mind?")+placeholder)
                textFormat: TextEdit.RichText
                onLineCountChanged: {
                    if (messageSend.ListView.view==null){
                        if (newsitem.ListView.view==null){}
                        else {newsitem.ListView.view.contentY=newsitem.ListView.view.contentY+root.fontFactor*osSettings.systemFontSize}
                    }
                    if(conversation==true){
                        conversationView.contentY=conversationView.contentY+root.fontFactor*osSettings.systemFontSize
                    }
                    else{
                        messageSend.ListView.view.contentY=messageSend.ListView.view.contentY+root.fontFactor*osSettings.systemFontSize
                    }
                }
                onLinkActivated:{Qt.openUrlExternally(link)}
                onActiveFocusChanged:{
                    if (activeFocus==true){
                        if (messageSend.ListView.view==null){
                            if ((typeof newsitem == 'undefined') || (newsitem.ListView.view==null)){}
                            else {
                                newsitem.ListView.view.contentY=newsitem.ListView.view.contentY+newsitem.height/2}
                        }
                        else if (conversation==true){
                            if(parentId==""){setParent(conversationModel.get(0).newsitemobject);}
                            messageSend.state="conversation";
                            try{conversationView.contentY=conversationView.contentY+20*mm}catch(e){}
                        } else  if (textfocus==false){
                            messageSend.state="active";
                            messageSend.ListView.view.positionViewAtBeginning();
                        }
                        else{
                            messageSend.ListView.view.contentY=messageSend.ListView.view.contentY+8*mm
                        };
                    }
                }
                onTextChanged:{
                    if (text!=""){
                        var plaintext=getText(0,cursorPosition)
                        var regex1 = /@[a-z]+/;var regex2 = /\![a-z]+/;var regex3 = /\s/;
                        if (regex1.test(getText(plaintext.lastIndexOf('@',cursorPosition),cursorPosition)+preeditText) && !regex3.test(getText(plaintext.lastIndexOf('@',cursorPosition),cursorPosition)+preeditText)){
                            var letter=(getText(plaintext.lastIndexOf('@',cursorPosition),cursorPosition)).match(/[a-z]+/);
                            contactprefix="@";
                            contactmenu(letter.toString())
                        } else if( regex2.test(getText(plaintext.lastIndexOf('!',cursorPosition),cursorPosition)+preeditText) && !regex3.test(getText(plaintext.lastIndexOf('!',cursorPosition),cursorPosition)+preeditText) ){
                            var letter=(getText(plaintext.lastIndexOf('!',cursorPosition),cursorPosition)).match(/[a-z]+/);
                            contactprefix="!";
                            contactmenu(letter.toString())
                        }else {contactSelector.visible=false}
                    }else{contactSelector.visible=false}
                }
            }
        }

        ListView{
            id:contactSelector
            visible: false
            z:3
            x:2*root.fontFactor*osSettings.bigFontSize
            width: parent.width-2.2*root.fontFactor*osSettings.bigFontSize
            height: messageSend.height/2
            model:contactModel
            function processContactSelection(contact){
                if(Helperjs.getCount(db,login,"contacts","screen_name",contact.screen_name)>1){
                    contact.screen_name=contact.screen_name+"+"+contacts.cid
                }
                if (newsSwipeview.stacktype=='DirectMessages'){
                    receiverLabel.text=contact.screen_name;
                    reply_to_user=contact.screen_name
                } else {
                    bodyField.remove(bodyField.getText(0,bodyField.cursorPosition).lastIndexOf(bodyField.contactprefix,bodyField.cursorPosition),bodyField.cursorPosition);
                    bodyField.insert(bodyField.cursorPosition, bodyField.contactprefix+contact.screen_name+" ");
                    bodyField.cursorPosition=bodyField.cursorPosition+contact.screen_name.length+1
                }
                //receiverLabel.text=contact.screen_name;
                contactSelector.visible=false
            }
            delegate: ContactComponent { }
        }
        ListModel{id:contactModel}

        ListView{
            id: tagSelector
            visible: false
            z:3
            x:2*root.fontFactor*osSettings.bigFontSize
            width: parent.width-2.2*root.fontFactor*osSettings.bigFontSize
            height: messageSend.height/2
            model:tagModel
            clip: true
            spacing: 0
            function processTagSelection(hashtag){
                bodyField.insert(bodyField.cursorPosition, hashtag+" ");
                bodyField.cursorPosition=bodyField.cursorPosition+hashtag.length+1
                tagSelector.visible=false
            }
            delegate: MButton {text:tag;onClicked: tagSelector.processTagSelection(tag)}
        }
        ListModel{id:tagModel}

        Row{
            id:buttonRow
            visible:false
            spacing: mm
            height: 2.5*root.fontFactor*osSettings.bigFontSize
            x: 0.5*mm

//            MButton{id:permButton //Permissions not working in Friendica 02/2022
//                visible: !conversation && (newsSwipeview.stacktype!=="DirectMessages")
//                height: 2*root.fontFactor*osSettings.bigFontSize
//                width: 2*root.fontFactor*osSettings.bigFontSize
//                text: ((contact_allow.length==0)&&(contact_deny.length==0)&&(group_allow.length==0)&&(group_deny.length==0))?"\uf09c":"\uf023"
//                onClicked: { if (permissionDialog.visible==false){permissionDialog.visible=true} else{permissionDialog.visible=false}}
//            }

            MButton{
                id:smileyButton
                text: "\uf118"
                height: 2*root.fontFactor*osSettings.bigFontSize
                width: 2*root.fontFactor*osSettings.bigFontSize
                onClicked: {if (smileyDialog.visible==false){smileyDialog.visible=true} else{smileyDialog.visible=false}}
            }

            MButton{
                id:hastagButton
                text: "\uf292"
                height: 2*root.fontFactor*osSettings.bigFontSize
                width: 2*root.fontFactor*osSettings.bigFontSize
                onClicked: {if (tagSelector.visible==false){hashtagmenu()} else{tagSelector.visible=false}}
            }
            MButton{
                id:imagesButton
                visible:(newsSwipeview.stacktype!="DirectMessages")
                text: "\uf03e"
                height: 2*root.fontFactor*osSettings.bigFontSize
                width: 2*root.fontFactor*osSettings.bigFontSize
                onClicked: {
                    if (imageUploadDialog.visible==false){
                        imageUploadDialog.visible=true;
                        imageUploadDialog.attach()
                    }
                    else{imageUploadDialog.visible=false}}
            }

            MButton {
                id: cancelButton
                height: 2*root.fontFactor*osSettings.bigFontSize
                width: 2*root.fontFactor*osSettings.bigFontSize
                text: "\uf057"
                onClicked: {
                    if (textfocus==true){messageSend.destroy()}
                    else{
                        bodyField.text="";
                        messageSend.state="";
                        permissionDialog.visible=false;
                        smileyDialog.visible=false;
                        imageUploadDialog.visible=false;
                        receiverLabel.visible=false;
                        reply_to_user="";
                        media_ids=[]
                    }
                }
            }
            MButton {
                id: sendButton
                height: 2*root.fontFactor*osSettings.bigFontSize
                width: 2*root.fontFactor*osSettings.bigFontSize
                text: "\uf1d9"
                onClicked: {
                    var title=titleField.text.replace("\"","\'");
                    var body=bodyField.getFormattedText(0,bodyField.length);
                    var dmbody=bodyField.getText(0,bodyField.length);
                    if (conversation || newsSwipeview.stacktype!=="DirectMessages"){
                        if (parentId!=""){
                            statusUpdate(title,dmbody,parentId)
                        }else{
                            statusUpdate(title,body,parentId)}
                    }else {
                        if (reply_to_user!=""){dmUpdate(title,dmbody,parentId,reply_to_user)}
                        else{Helperjs.showMessage(qsTr("Error"),qsTr("No receiver supplied!"),root)}
                    }
                    if (conversation==true){
                        newstab.newstabstatus=root.globaloptions.newsViewType; rootstackView.pop(null)
                    }
                }
            }
        }
        PermissionDialog{id:permissionDialog;x:mm;visible: false}
        SmileyDialog{id:smileyDialog;x:mm;visible: false}
        MessageImageUploadDialog{id:imageUploadDialog;visible: false}
    }
    Component.onCompleted:{
        root.replySignal.connect(setParent);
        root.directmessageSignal.connect(directmessagePrepare);
        root.uploadSignal.connect(sendUrls);
        root.sendtextSignal.connect(sendtext);
        if (textfocus==true){bodyField.forceActiveFocus()}
    }

    states: [
        State {
            name: "active"
            PropertyChanges {
                target: messageColumn; height: implicitHeight
            }
            PropertyChanges {
                target: buttonRow; visible:true
            }
            PropertyChanges {
                target: titleField; visible:(newsSwipeview.stacktype!="DirectMessages")
            }
            PropertyChanges {
                target: receiverLabel; visible:(newsSwipeview.stacktype=="DirectMessages");
            }
        },
        State {
            name: "conversation"
            PropertyChanges {
                target: messageColumn; height: implicitHeight
            }
            PropertyChanges {
                target: buttonRow; visible:true
            }
            PropertyChanges {
                target: titleField; visible:(!conversation&&newsSwipeview.stacktype!="DirectMessages")
            }
        },

        State {
            name: "reply"
            PropertyChanges {
                target: messageColumn; height: implicitHeight
            }
            PropertyChanges {
                target: buttonRow; visible:true
            }
            PropertyChanges {
                target: titleField; visible:false
            }
            PropertyChanges {
                target: bodyField; placeholderText:"";focus:true
            }
            PropertyChanges {
                target: stackTypeDescription; visible:false
            }
        }
    ]
}
