//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// List of people
import QtQuick 2.0
import QtQuick.Controls 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Rectangle {
    id:contactlistRectangle
    property var contacts:[]
    property var possibleUsers: []
    //y:8*mm
    color: "white"
    border.color: "light grey"
    radius:0.5*mm
    width:groupListView.width
    height:groupListView.height

    ListView {
        id: contactView
        x:mm
        y:6*mm
        width: contactlistRectangle.width-2*mm
        height: contactlistRectangle.height-10*mm
        clip: true
        spacing: 0
        model: contactModel
        delegate: listContact
    }

    ListModel{id: contactModel}

    Component {  id:listContact
        Rectangle{
            border.color: "#EEEEEE"
            border.width: 1
            radius:0.5*mm
            width:contactView.width
            height:6*mm
            Image {
                id: contactImage
                x:1
                y:1
                width: 5*mm
                height:5*mm
                source:(contact.profile_image!="")? "file://"+contact.profile_image : contact.profile_image_url
                onStatusChanged: if (contactImage.status == Image.Error) {source="qrc:/images/defaultcontact.jpg"}
            }
            Text{
                font.pointSize: osSettings.bigFontSize
                anchors.left: contactImage.right
                anchors.margins: 1*mm
                text:Qt.atob(contact.name)
            }
            Text {
                id:selected
                anchors.right:parent.right
                visible: contactlist.indexOf(contact)>-1
                z:4
                text: "\u2713"
                width: 5*mm
                anchors.top: parent.top
                color: "green"
                font.pointSize: osSettings.bigFontSize
            }

            MouseArea{
                anchors.fill: parent
                onClicked:{
                    if(selected.visible==true){
                        contacts.splice(Helperjs.inArray(contacts,"id",contact.id),1);
                        selected.visible=false
                    }
                    else{
                        contacts.push(contact);
                        selected.visible=true;
                    }
                }
            }
        }
    }

    MButton {
        id: closeButton
        anchors.top: parent.top
        anchors.topMargin: 1*mm
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        //color:"white"
        text: "\uf057"
        onClicked: {
            groupModelAppend(contacts,function(){
                contactlistRectangle.destroy()
            });
        }
    }

    Component.onCompleted: {
        for (var user in possibleUsers){
            contactModel.append({"contact":possibleUsers[user]})
        }
    }
}
