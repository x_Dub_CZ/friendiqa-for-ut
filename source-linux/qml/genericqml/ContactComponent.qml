//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/qml/genericqml"

Item {
    id: contactComponent
    height: 5*root.fontFactor*osSettings.systemFontSize
    width: contactComponent.ListView.view.width
    property var createdAtDate: new Date(contact.created_at)
    property string connectUrl: (contact.network!=="dfrn")||(contact.isFriend==1)?"":( "<a href='"+contact.url.replace("profile","dfrn_request") +"'>"+qsTr("Connect")+"</a><br>")

    Rectangle {
        id: wrapper
        width:parent.width
        height: parent.height
        radius: 0.5*mm
        border.color: Material.backgroundDimColor
        color: Material.backgroundColor
        Image {
            id: photoImage
            x:0.5*mm
            y:0.5*mm
            width: 4*root.fontFactor*osSettings.systemFontSize
            height:4*root.fontFactor*osSettings.systemFontSize
            source:((contact.profile_image!="") && (typeof(contact.profile_image)=="string"))? "file://"+contact.profile_image : contact.profile_image_url
            onStatusChanged: {if (photoImage.status == Image.Error) {source="qrc:/images/defaultcontact.jpg"}}
            }

        Flow{
            width: wrapper.width-4*root.fontFactor*osSettings.systemFontSize
            height: wrapper.height-mm
            anchors.left: photoImage.right
            anchors.margins: 0.5*mm
            clip: true
            Label {
                id: namelabel
                width: Math.min(wrapper.width-(photoImage.width+mm),contentWidth)
                height: 1.1*root.fontFactor*osSettings.bigFontSize
                text: contact.name
                elide: contentWidth>wrapper.width-4*osSettings.systemFontSize?Text.ElideRight:Text.ElideNone
                color: Material.secondaryTextColor
                font.pointSize: osSettings.bigFontSize
                }
            Label {
                id: screennamelabel
                width: Math.min(wrapper.width-4*root.fontFactor*osSettings.systemFontSize,contentWidth)
                height: 1.1*root.fontFactor*osSettings.bigFontSize
                text: "(@"+contact.screen_name+")"
                elide: contentWidth>wrapper.width-4*root.fontFactor*osSettings.systemFontSize?Text.ElideRight:Text.ElideNone
                color: Material.secondaryTextColor
                font.pointSize: osSettings.bigFontSize
            }
            Label {
                id: descriptionlabel
                width: wrapper.width-5*root.fontFactor*osSettings.systemFontSize
                height: wrapper.height-mm-1.1*root.fontFactor*osSettings.bigFontSize
                maximumLineCount:2
                text: Qt.atob(contact.description)!=""?contact.description:""
                elide:Text.ElideRight
                color: Material.secondaryTextColor
                font.pointSize: osSettings.systemFontSize
            }
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                contactComponent.ListView.view.processContactSelection(contact)
            }
        }
    }
}
