//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <QtQml/QQmlEngine>
#include <QAndroidService>
#include <QtQuick>
#include "xhr.h"
#include "updatenews.h"
#include "filesystem.h"
#include "remoteauthasyncimageprovider.h"
#include "AndroidNative/systemdispatcher.h"


#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void*) {
    Q_UNUSED(vm);
 qDebug("NativeInterface::JNI_OnLoad()"); // It must call this function within JNI_OnLoad to enable System Dispatcher

 AndroidNative::SystemDispatcher::registerNatives();
 return JNI_VERSION_1_6;
 }
#endif


Q_DECL_EXPORT int main(int argc, char *argv[]) {
    //if (argc>1){qDebug()<< "argc Friendiqa"<< argc <<" argv1" <<argv[1];}
    if ((argc>1) && (qstrcmp(argv[1],"-service")==0)){
        //qDebug()<<"FriendiqaMain Service";
        QAndroidService app(argc, argv);
        UPDATENEWS* updatenews= UPDATENEWS::instance();
        updatenews->setDatabase();
        updatenews->login();
        updatenews->setSyncAll(true);
        updatenews->startsync();
        app.connect (updatenews,SIGNAL(quitapp()),&app,SLOT(quit()));
        return app.exec();
    }
    else{
    QApplication app(argc, argv);
    QQmlApplicationEngine view;
    //qDebug()<<"FriendiqaMain started";
    QTranslator qtTranslator;
    qtTranslator.load("friendiqa-" + QLocale::system().name(),":/translations");
    app.installTranslator(&qtTranslator);
    RemoteAuthAsyncImageProvider *imageProvider = new RemoteAuthAsyncImageProvider;
    view.addImageProvider("remoteauthimage",imageProvider);
    view.rootContext()->setContextProperty("remoteauth", imageProvider);
    XHR* xhr = XHR::instance();
    view.rootContext()->setContextProperty("xhr", xhr);
    FILESYSTEM* filesystem = FILESYSTEM::instance();
    view.rootContext()->setContextProperty("filesystem", filesystem);
    ALARM* alarm = ALARM::instance();
    view.rootContext()->setContextProperty("alarm", alarm);
    UPDATENEWS* updatenews = UPDATENEWS::instance();
    view.rootContext()->setContextProperty("updatenews", updatenews);
    view.load(QUrl("qrc:/qml/friendiqa.qml"));
    view.connect(view.rootContext()->engine(), SIGNAL(quit()), &app, SLOT(quit()));

    return app.exec();
    }
}

