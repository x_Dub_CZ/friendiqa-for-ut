//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

.pragma library
.import QtQuick.LocalStorage 2.0 as Sql
.import "qrc:/js/helper.js" as Helperjs

function requestFriends(login,database,rootwindow,callback){
    //    return array of friends
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        var result = tx.executeSql('UPDATE contacts SET isFriend=0 where username="'+login.username+'"'); // clean old friends
        var result2 = tx.executeSql('DELETE from groups where username="'+login.username+'"'); // clean old groups
    })
    // /api/statuses/friends not working in Friendica 2/2022 , switching to api/v1/lists and download of all list members
    //    Helperjs.friendicaRequest(login,"/api/statuses/friends?count=9999", rootwindow,function (obj){
    var allfriends=[];
    Helperjs.friendicaRequest(login,"/api/v1/lists",rootwindow,function(listsobj){
        var lists=JSON.parse(listsobj)
        for (var list in lists){
            Helperjs.friendicaRequest(login,"/api/v1/lists/"+lists[list].id+"/accounts?limit=0", rootwindow,function (obj){
                var friends=JSON.parse(obj);
                var memberarray=[];
                for (var i=0;i<friends.length;i++){
                    if (friends[i].note!=null){
                        friends[i].description=friends[i].note;}
                    else{friends[i].description=""}
                    friends[i].name=friends[i].display_name;
                    friends[i].screen_name=friends[i].acct;
                    friends[i].location="";
                    friends[i].profile_image=friends[i].avatar_static;
                    friends[i].profile_image_url=friends[i].avatar;
                    friends[i].protected=false;
                    friends[i].friends_count=friends[i].following_count;
                    friends[i].created_at=Date.parse(friends[i].created_at);
                    friends[i].favorites_count=0;
                    friends[i].utc_offset=0;
                    friends[i].isFriend=1
                    friends[i].cid=0
                    friends[i].following=true
                    memberarray.push(parseInt(friends[i].id))
                }
                //requestGroups() not working with Friendica 02/2022
                db.transaction( function(tx) {
                    var result3 = tx.executeSql('INSERT INTO groups VALUES (?,?,?,?)', [login.username,lists[list].title,lists[list].id,JSON.stringify(memberarray)])
                })
                callback(friends)
            })};
    });
}


function requestGroups(login,database,rootwindow,callback){
    // retrieve, save and return groups. Other features currently not implemented
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    Helperjs.friendicaRequest(login,"/api/friendica/group_show",rootwindow, function (obj){
        var groups=JSON.parse(obj);
        db.transaction( function(tx) {
            var result = tx.executeSql('DELETE from groups where username="'+login.username+'"'); // clean old groups
            for (var i=0;i<groups.length;i++){
                var memberarray=[]; for (var user in groups[i].user){memberarray.push(parseInt(groups[i].user[user].id))}
                //print("Members: "+groups[i].user)
                var result2 = tx.executeSql('INSERT INTO groups VALUES (?,?,?,?)', [login.username,groups[i].name,groups[i].gid,JSON.stringify(memberarray)])}
            callback()
        });
    })}

function listFriends(login,database,callback,filter,isFriend=0){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    var filtertext='';
    if (filter!=null){var filtertext=new RegExp(".*"+filter.toLowerCase()+".*")}else{var filtertext=new RegExp(".*")}
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT * from contacts WHERE username="'+login.username+'" AND isFriend>'+isFriend+' ORDER BY screen_name');
        // check for friends
        var contactlist=[];
        for (var i=0;i<result.rows.length;i++){
            var contact=result.rows.item(i)
            contact.name=Qt.atob(contact.name);
            if (contact.screen_name==null){contact.screen_name=""}
            if(filtertext.test(contact.name.toLowerCase())|| filtertext.test(contact.screen_name.toLowerCase())){
                contactlist.push(contact)}
        }
        callback(contactlist)
    });
}
function listBlocked(login,database,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT * from contacts WHERE username="'+login.username+'" AND statusnet_blocking=1 ORDER BY screen_name');
        // check for friends
        var contactlist=[];
        for (var i=0;i<result.rows.length;i++){
            var contact=result.rows.item(i)
            contact.name=Qt.atob(contact.name);
            if (contact.screen_name==null){contact.screen_name=""}
            contactlist.push(contact)
        }
        callback(contactlist)
    });
}

function listHashtags(login,database,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT * from hashtags WHERE username="'+login.username+'" ORDER BY date DESC LIMIT 50');
        // check for friends
        var taglist=[];
        for (var i=0;i<result.rows.length;i++){
            var tag=result.rows.item(i).tag;
            tag=Qt.atob(tag);
            taglist.push(tag)}
        callback(taglist)
    });
}

function storeHashtags(login,database,newstext,rootwindow){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    var curDate= Date.now();
    var hashtags=[];
    hashtags=findTags(newstext);
    for (var tag in hashtags){
        db.transaction( function(tx) {
            var result = tx.executeSql('SELECT * from hashtags where username="'+login.username+'" AND tag = "'+Qt.btoa(tag)+'"'); // check for tag
            if(result.rows.length > 0) {// use update
                result = tx.executeSql('UPDATE hashtags SET tag="'+'", date='+curDate+', ownership=0 where username="'+login.username+'" AND tag="'+Qt.btoa(hashtags[tag])+'"');
            } else {// use insert
                result = tx.executeSql('INSERT INTO hashtags (username,tag,date,statuses,ownership) VALUES (?,?,?,?,?)', [login.username,Qt.btoa(hashtags[tag]),curDate,"[]",0])
            }
        })
    }
}

function deleteGroup(login,database,rootwindow,group, callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    Helperjs.friendicaPostRequest(login,"/api/friendica/group_delete?gid="+group.gid+"&name="+group.groupname,"","POST",rootwindow, function (obj){
        var deletereturn=JSON.parse(obj);
        if(deletereturn.success){
            db.transaction( function(tx) {
                var result = tx.executeSql('DELETE from groups where username="'+login.username+'" AND groupname="'+group.name+'"'); // delete group
                callback()
            });
        }})}

function getLastNews(login,database,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    var lastnewsid=0;
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT status_id from news WHERE username="'+login.username+'" AND messagetype=0 ORDER BY status_id  DESC LIMIT 1');
        try{lastnewsid=result.rows.item(0).status_id;}catch(e){lastnewsid=0};
        callback(lastnewsid)
    })
}



//function getFriendsTimeline(login,database,contacts,onlynew,rootwindow,callback){
//    // retrieve and return timeline since last news, return contacts which are not friends and older than 2 days for update (friends can be updated in Contactstab)
//    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
//    var parameter = "?count=50";
//    if(onlynew){db.transaction( function(tx) {
//        var result = tx.executeSql('SELECT status_id from news WHERE username="'+login.username+'" AND messagetype=0 ORDER BY status_id  DESC LIMIT 1'); // check for last news id
//        try{parameter=parameter+"&since_id="+result.rows.item(0).status_id;}catch(e){};})}
//        var newContacts=[];
//        Helperjs.friendicaRequest(login,"/api/statuses/friends_timeline"+parameter, rootwindow,function (obj){
//            var news=JSON.parse(obj);
//            if (news.hasOwnProperty('status')){
//                Helperjs.showMessage(qsTr("Error"),"API:\n" +login.server+"/api/statuses/friends_timeline"+parameter+"\n Return: \n"+obj,rootwindow)
//            }
//            var newContacts=findNewContacts(news,contacts);
//            callback(news,newContacts)
//})}

function getCurrentContacts(login,database,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    var contactlist=[];
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT url from contacts WHERE username="'+login.username+'" AND isFriend=1'); // check for friends
        for (var i=0;i<result.rows.length;i++){
            contactlist.push(result.rows.item(i).url )
            //print(result.rows.item(i).url)
        }
        var lastDate=Date.now()-604800000;//  7 days old
        //print('SELECT url from contacts WHERE username="'+login.username+'" AND isFriend=0 AND imageAge>'+lastDate);
        var result2 = tx.executeSql('SELECT url from contacts WHERE username="'+login.username+'" AND isFriend=0 AND imageAge > '+lastDate);
        for (var j=0;j<result2.rows.length;j++){
            contactlist.push(result2.rows.item(j).url )
        }
    })
    callback(contactlist)
}

function findNewContacts(news,contacts){
    var newContacts=[];
    for (var i=0;i<news.length;i++){
        var url=news[i].user.url;
        if(contacts.indexOf(url)==-1 &&  !(inArray(newContacts,"url",url))){
            news[i].user.isFriend=0;
            newContacts.push(news[i].user);
        }
        if (news[i].hasOwnProperty('friendica_activities') && news[i].friendica_activities.like.length>0){
            for (var j=0;j<news[i].friendica_activities.like.length;j++){
                var like_url=news[i].friendica_activities.like[j].url;
                if(contacts.indexOf(like_url)==-1 &&  !(inArray(newContacts,"url",like_url))){
                    news[i].friendica_activities.like[j].isFriend=0;
                    newContacts.push(news[i].friendica_activities.like[j]);
                }
            }
        }
        if (news[i].hasOwnProperty('friendica_activities') && news[i].friendica_activities.dislike.length>0){
            for (var k=0;j<news[k].friendica_activities.dislike.length;k++){
                var dislike_url=news[i].friendica_activities.dislike[k].url;
                if(contacts.indexOf(dislike_url)==-1 &&  !(inArray(newContacts,"url",dislike_url))){
                    news[i].friendica_activities.dislike[k].isFriend=0;
                    newContacts.push(news[i].friendica_activities.dislike[k]);
                }
            }
        }

        if(news[i].hasOwnProperty('friendica_author')){
            var owner_url=news[i].friendica_author.url;
            if(contacts.indexOf(owner_url)==-1 &&  !(inArray(newContacts,"url",owner_url))){
                news[i].friendica_author.isFriend=0;
                newContacts.push(news[i].friendica_author);
            }
        }
    }
    return newContacts
}

function storeNews(login,database,news,rootwindow){
    // save news after contacts download, call next function
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    for (var i=0;i<news.length;i++){
        //print('store news data for ' + login.username+news[i].messagetype+Qt.btoa(news[i].text)+news[i].created_at+ news[i].in_reply_to_status_id+ news[i].source+ news[i].id+news[i].in_reply_to_user_id+news[i].geo+news[i].favorited+ news[i].user.id+Qt.btoa(news[i].statusnet_html)+news[i].statusnet_conversation_id+ Qt.btoa(JSON.stringify(friendica_activities))+"[]"+attachments+news[i].friendica_author.url);
        //var ausdruck=news[i];
        var likearray=[];var dislikearray=[];var attendyesarray=[];var attendnoarray=[];var attendmaybearray=[];
        if(news[i].hasOwnProperty('friendica_activities')){
            for (var user in news[i].friendica_activities.like){likearray.push(news[i].friendica_activities.like[user].url)}
            for (var user in news[i].friendica_activities.dislike){dislikearray.push(news[i].friendica_activities.dislike[user].url)}
            for (var user in news[i].friendica_activities.attendyes){attendyesarray.push(news[i].friendica_activities.attendyes[user].url)}
            for (var user in news[i].friendica_activities.attendno){attendnoarray.push(news[i].friendica_activities.attendno[user].url)}
            for (var user in news[i].friendica_activities.attendmaybe){attendmaybearray.push(news[i].friendica_activities.attendmaybe[user].url)}
        }
        var friendica_activities=[likearray,dislikearray,attendyesarray,attendnoarray,attendmaybearray]
        var attachments="";if (news[i].attachments){attachments=Qt.btoa(JSON.stringify(news[i].attachments))}

        //if (news[i].friendica_title!="") {news[i].statusnet_html="<b>"+news[i].friendica_title +"</b><br><br>"+news[i].friendica_html;}
        //else{
        news[i].statusnet_html=news[i].friendica_html//}

        db.transaction( function(tx) {
            var result = tx.executeSql('SELECT * from news where username="'+login.username+'" AND status_id = "'+news[i].id+'" AND messagetype='+news[i].messagetype); // check for news id
            if(result.rows.length === 1) {// use update
                //print(news[i].id +' news exists, update it'+'UPDATE news SET username="'+login.username+'", messagetype=0, text="'+Qt.btoa(news[i].text)+'", created_at="'+Date.parse(cleanDate(news[i].created_at))+'", in_reply_to_status_id="'+news[i].in_reply_to_status_id+'", source="'+news[i].source+'", status_id="'+news[i].id+'", in_reply_to_user_id="'+news[i].in_reply_to_user_id+'", geo="'+news[i].geo+'", favorited="'+news[i].favorited+'", uid="'+news[i].user.id+'", statusnet_html="'+Qt.btoa(news[i].status_html)+'", statusnet_conversation_id="'+news[i].statusnet_conversation_id+'",friendica_activities="'+Qt.btoa(JSON.stringify(friendica_activities))+'",attachments="'+attachments+'",friendica_owner="'+news[i].friendica_owner.url+'" where username="'+login.username+'" AND status_id="'+news[i].status_id+'" AND messagetype=0')
                result = tx.executeSql('UPDATE news SET username="'+login.username+'", messagetype='+news[i].messagetype+', text="'+Qt.btoa(news[i].text)+'", created_at="'+news[i].created_at+'", in_reply_to_status_id="'+news[i].in_reply_to_status_id+'", source="'+news[i].source+'", status_id="'+news[i].id+'", in_reply_to_user_id="'+news[i].in_reply_to_user_id+'", geo="'+news[i].geo+'", favorited="'+news[i].favorited+'", uid="'+news[i].user.id+'", statusnet_html="'+Qt.btoa(news[i].statusnet_html)+'", statusnet_conversation_id="'+news[i].statusnet_conversation_id+'",friendica_activities="'+Qt.btoa(JSON.stringify(friendica_activities))+'",attachments="'+attachments+'",friendica_owner="'+news[i].friendica_author.url+'" where username="'+login.username+'" AND status_id="'+news[i].status_id+'" AND messagetype=0');
            } else {// use insert
                result = tx.executeSql('INSERT INTO news (username,messagetype,text,created_at,in_reply_to_status_id,source,status_id,in_reply_to_user_id,geo,favorited,uid,statusnet_html,statusnet_conversation_id,friendica_activities,friendica_activities_self,attachments,friendica_owner) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [login.username,news[i].messagetype,Qt.btoa(news[i].text),news[i].created_at, news[i].in_reply_to_status_id, news[i].source, news[i].id,news[i].in_reply_to_user_id,news[i].geo,news[i].favorited, news[i].user.id,Qt.btoa(news[i].statusnet_html),news[i].statusnet_conversation_id, Qt.btoa(JSON.stringify(friendica_activities)),"[]",attachments,news[i].friendica_author.url])}})
    }
}


function getActivitiesUserData(allcontacts,userUrlArray){//print(JSON.stringify(userUrlArray));
    var helpArray=[];
    for (var i=0;i<userUrlArray.length;i++){
        helpArray.push(objFromArray(allcontacts,"url",userUrlArray[i]));
    }
    return helpArray
}

function newsfromdb(database,login,messagetype,callback,contact,stop_time){
    // return news before stop_time (used by More button), in brackets of 20 entries, or by specified contact
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        var result = tx.executeSql('SELECT status_id from news WHERE username="'+login.username+'" AND messagetype=0 ORDER BY status_id  DESC LIMIT 1');
        try{var lastid=result.rows.item(0).status_id;}catch(e){var lastid=0};

        if (!stop_time){var stop="";
            try{var rs = tx.executeSql('select created_at from news WHERE username="'+login.username+'" AND messagetype="'+messagetype+'" ORDER BY created_at DESC LIMIT 1');
                stop="<="+rs.rows.item(0).created_at}catch(e){stop="<99999999999999"}}
        else{var stop="<"+stop_time}
        var contactfilter="";if(contact){contactfilter=" AND (uid='"+contact+"' OR friendica_owner='"+contact+"')"}
        if (messagetype=="0"){messagetype="0,5"}
        //print('select * from news WHERE username="'+login.username+'" AND messagetype IN ( '+messagetype+' ) AND created_at'+stop+contactfilter+' ORDER BY created_at DESC LIMIT 20');
        var newsrs=tx.executeSql('select * from news WHERE username="'+login.username+'" AND messagetype IN ( '+messagetype+' ) AND created_at'+stop+contactfilter+' ORDER BY created_at DESC LIMIT 20');
        var newsArray=[];
        var allcontacts=getAllContacts(database,login.username);

        for(var i = 0; i < newsrs.rows.length; i++) {
            newsArray.push(newsrs.rows.item(i));
            newsArray[i].statusnet_html=Qt.atob(newsArray[i].statusnet_html);
            newsArray[i].text=Qt.atob(newsArray[i].text);
            newsArray[i].id=newsArray[i].status_id;
            newsArray[i].friendica_author=objFromArray(allcontacts,"url",newsArray[i].friendica_owner)
            newsArray[i]=fetchUsersForNews(database,login.username,newsArray[i],allcontacts);
            if (newsArray[i].attachments!="" && newsArray[i].attachments!==null){newsArray[i].attachments=JSON.parse(Qt.atob(newsArray[i].attachments))};
        }
        callback(newsArray,lastid)});
}


function fetchUsersForNews(database,username,news,allcontacts){//print("fetchusers "+JSON.stringify(news))
    news.user=objFromArray(allcontacts,"id",news.uid);
    if(news.in_reply_to_user_id){news.reply_user=objFromArray(allcontacts,"id",news.in_reply_to_user_id)}
    //news.friendica_owner_object=objFromArray(allcontacts,"url",news.friendica_owner);
    news.friendica_author=objFromArray(allcontacts,"url",news.friendica_author);
    if (news.messagetype==0){
        var friendicaArray=JSON.parse(Qt.atob(news.friendica_activities));
        delete news.friendica_activities;
        news.friendica_activities={};
        //for(var j=0;j<friendicaArray.length;j++){
        news.friendica_activities.like=getActivitiesUserData(allcontacts,friendicaArray[0]);
        news.friendica_activities.dislike=getActivitiesUserData(allcontacts,friendicaArray[1]);
        news.friendica_activities.attendyes=getActivitiesUserData(allcontacts,friendicaArray[2]);
        news.friendica_activities.attendno=getActivitiesUserData(allcontacts,friendicaArray[3]);
        news.friendica_activities.attendmaybe=getActivitiesUserData(allcontacts,friendicaArray[4]);
        //}
    }
    return news
}

function deleteNews(login,database,newsid,messagetype,rootwindow,callback){
    var api="" ;
    if (messagetype==0){ api="/api/statuses/destroy?id="}
    else if (messagetype==1){ api="/api/direct_messages/destroy?id="}
    else if (messagetype==2){ api="/api/friendica/notifications/seen?id="}
    Helperjs.friendicaPostRequest(login,api+newsid,"","POST", rootwindow,function (obj){
        var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
        db.transaction( function(tx) {var result = tx.executeSql('DELETE from news where username="'+login.username+'" AND messagetype='+messagetype+' AND status_id ='+newsid); // delete news id
            callback(obj)
        });
    })}

function retweetNews(login,database,newsid,rootwindow,callback){
    Helperjs.friendicaPostRequest(login,"/api/v1/statuses/"+newsid+"/reblog","","POST", rootwindow,function (obj){
        var answer=JSON.parse(obj);
        if(answer.hasOwnProperty('status'))//('error' in answer.status)
        {Helperjs.showMessage("Repost",answer.status.code,rootwindow);}
        else{Helperjs.showMessage("Repost",answer.content,rootwindow)}
    })
}

function favorite(login,favorited,newsid,rootwindow){
    // toggle favorites
    if(favorited){  Helperjs.friendicaPostRequest(login,"/api/v1/statuses/"+newsid+"/bookmark","","POST", rootwindow,function (obj){
    })}
    else {Helperjs.friendicaPostRequest(login,"/api/v1/statuses/"+newsid+"/unbookmark","","POST",rootwindow,function (obj){
    })}
}

function  likerequest(login,database,verb,newsid,rootwindow){
    Helperjs.friendicaPostRequest(login,"/api/friendica/activity/"+verb+"?id="+newsid, "","POST",rootwindow,function (obj){
        if (obj=='"ok"'){
            var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
            db.transaction( function(tx) {
                var currentActivities_rs=tx.executeSql('select friendica_activities_self from news WHERE username="'+login.username+'" AND status_id='+newsid) ;
                var currentActivities=JSON.parse(currentActivities_rs.rows.item(0).friendica_activities_self);
                //print(verb+"currentActivities "+JSON.stringify(currentActivities));
                if ((verb=="like")&&(currentActivities.indexOf(1)==-1)){ currentActivities.push(1);
                    if (currentActivities.indexOf(2)!=-1){currentActivities.splice(currentActivities.indexOf(2),1)}
                }
                if ((verb=="dislike")&&(currentActivities.indexOf(2)==-1)){ currentActivities.push(2);
                    if (currentActivities.indexOf(1)!=-1){currentActivities.splice(currentActivities.indexOf(1),1)}
                }
                if (verb=="unlike"){ if (currentActivities.indexOf(1)!=-1){currentActivities.splice(currentActivities.indexOf(1),1)}}
                if (verb=="undislike"){ if (currentActivities.indexOf(2)!=-1){currentActivities.splice(currentActivities.indexOf(2),1)}}
                //print(JSON.stringify(currentActivities));
                var result = tx.executeSql('UPDATE news SET friendica_activities_self ="'+JSON.stringify(currentActivities)+'" where username="'+login.username+'" AND status_id ='+newsid);
            })}
        else{}})
}

function like(login,database,toggle,verb,newsid,rootwindow){
    if(verb=="like"&& toggle==1){likerequest(login,database,"like",newsid,rootwindow);
    }
    if(verb=="dislike"&& toggle==1){likerequest(login,database,"dislike",newsid,rootwindow);
    }
    if(toggle==0){
        likerequest(login,database,"un"+verb,newsid,rootwindow);}
}

function attend(login,database,attend,newsid,rootwindow,callback){
    Helperjs.friendicaPostRequest(login,"/api/friendica/activity/attend"+attend+"?id="+newsid, "","POST",rootwindow,function (obj){
        //print("attend: "+attend+obj);
        if (obj=='"ok"')
            var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
        db.transaction( function(tx) {
            var currentActivities_rs=tx.executeSql('select friendica_activities_self from news WHERE username="'+login.username+'" AND status_id='+newsid) ;
            var currentActivities=JSON.parse(currentActivities_rs.rows.item(0).friendica_activities_self);
            if ((attend=="yes")&&(currentActivities.indexOf(3)==-1)){
                currentActivities.push(3);
                if (currentActivities.indexOf(4)!=-1){currentActivities.splice(currentActivities.indexOf(4),1)}
                if (currentActivities.indexOf(5)!=-1){currentActivities.splice(currentActivities.indexOf(5),1)}
                //print(JSON.stringify(currentActivities));
            }
            if ((attend=="no")&&(currentActivities.indexOf(4)==-1)){
                currentActivities.push(4);
                if (currentActivities.indexOf(3)!=-1){currentActivities.splice(currentActivities.indexOf(3),1)}
                if (currentActivities.indexOf(5)!=-1){currentActivities.splice(currentActivities.indexOf(5),1)}
            }
            if ((attend=="maybe")&&(currentActivities.indexOf(5)==-1)){
                currentActivities.push(5);
                if (currentActivities.indexOf(3)!=-1){currentActivities.splice(currentActivities.indexOf(3),1)}
                if (currentActivities.indexOf(4)!=-1){currentActivities.splice(currentActivities.indexOf(4),1)}
            }

            var result = tx.executeSql('UPDATE news SET friendica_activities_self ="'+JSON.stringify(currentActivities)+'" where username="'+login.username+'" AND status_id ='+newsid);
            callback();
        })})}

function requestConversation(login,database,newsid,contacts,rootwindow,callback){
    Helperjs.friendicaRequest(login,"/api/conversation/show?id="+newsid,rootwindow, function (obj){
        var news=JSON.parse(obj);
        var newContacts=findNewContacts(news,contacts);
        // storeNews(login,database,news,rootwindow,callback)
        callback(news,newContacts)
    })}

//function conversationfromdb(database,user,conversationId,callback){
//    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
//    db.transaction( function(tx) {
//        var newsrs=tx.executeSql('select * from news WHERE username="'+user+'" AND statusnet_conversation_id="'+conversationId+'" ORDER BY created_at ASC');
//        var newsArray=[];
//        var allcontacts=getAllContacts(database,user);
//        for(var i = 0; i < newsrs.rows.length; i++) {
//            newsArray.push(newsrs.rows.item(i));
//            newsArray[i].statusnet_html=Qt.atob(newsArray[i].statusnet_html);
//            newsArray[i].text=Qt.atob(newsArray[i].text);
//            newsArray[i].id=newsArray[i].status_id;
//            newsArray[i]=fetchUsersForNews(database,user,newsArray[i],allcontacts);
//            if (helpernews.attachments!="" && newsArray[i].attachments!==null){newsArray[i].attachments=JSON.parse(Qt.atob(newsArray[i].attachments))};
//        }
//    callback(newsArray)})
//}

function requestFavorites(login,database,contacts,rootwindow,callback){
    Helperjs.friendicaRequest(login,"/api/favorites",rootwindow, function (obj){
        //print(obj+JSON.stringify(obj));
        var news=JSON.parse(obj);
        var newContacts=findNewContacts(news,contacts);
        // storeNews(login,database,news,rootwindow,callback)
        callback(news,newContacts)
    })}


function chatsfromdb(database,login,messagetype,currentconversations,callback,stop_id){

    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        if (!stop_id){var stop="";
            try{var rs = tx.executeSql('select status_id from news WHERE username="'+login.username+'" AND messagetype="'+messagetype+'" ORDER BY status_id DESC LIMIT 1');
                stop="<="+rs.rows.item(0).status_id}catch(e){stop="<99999999999999"}}
        else{var stop="<"+stop_id}
        if (messagetype=="1"){messagetype="1,5"}
        let conversationfilter="";
        if (currentconversations.length>0){conversationfilter="AND statusnet_conversation_id NOT IN ("+currentconversations.toString()+") "}

        var conversationsrs=tx.executeSql('select DISTINCT statusnet_conversation_id from news WHERE username="'+login.username+'" AND status_id'+stop+' AND messagetype IN ( "'+messagetype+'" ) '+ conversationfilter +'ORDER BY created_at DESC LIMIT 20'); //+' ORDER BY created_at DESC LIMIT 20');
        var result = tx.executeSql('SELECT status_id from news WHERE username="'+login.username+'" AND messagetype=0 ORDER BY status_id  DESC LIMIT 1');
        try{var lastid=result.rows.item(0).status_id;}catch(e){var lastid=0};
        var conversations=[];
        for(var i = 0; i < conversationsrs.rows.length; i++) {
            conversations.push(conversationsrs.rows.item(i).statusnet_conversation_id);
        }
        var newsArray=[];
        var allcontacts=getAllContacts(database,login.username);

        for(var j = 0; j< conversations.length; j++) {
            var newsrs=tx.executeSql('select * from news WHERE username="'+login.username+'"  AND statusnet_conversation_id="'+conversations[j] +'" AND messagetype="'+messagetype+'" ORDER BY created_at ASC');
            //print(JSON.stringify(newsrs.rows.item(0))+JSON.stringify(newsrs.rows.item(1)))
            var helpernews=newsrs.rows.item(0);
            helpernews=cleanhelpernews(database,login.username,helpernews,allcontacts)
            helpernews.currentconversation=[];
            for (var h = 0;h<newsrs.rows.length;h++){
                var helpernews2=newsrs.rows.item(h);
                if(helpernews.id!=helpernews2.status_id){
                    helpernews2.newscount=0;
                    helpernews2=cleanhelpernews(database,login.username,helpernews2,allcontacts)
                    helpernews.currentconversation.push(helpernews2)
                }
            }

            helpernews.newscount=newsrs.rows.length;
            newsArray.push(helpernews);
        }
        callback(newsArray,lastid);
    })}


function allchatsfromdb(database,user,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        //    if (!stop_time){var stop="";
        //        try{var rs = tx.executeSql('select created_at from news WHERE username="'+username+'" ORDER BY created_at DESC LIMIT 1');
        //        stop="<="+rs.rows.item(0).created_at}catch(e){stop="<99999999999999"}}
        //    else{var stop="<"+stop_time}
        var conversationsrs=tx.executeSql('select DISTINCT statusnet_conversation_id from news WHERE username="'+user+'" ORDER BY created_at DESC'); //+' ORDER BY created_at DESC LIMIT 20');
        var conversationIds=[];
        for(var i = 0; i < conversationsrs.rows.length; i++) {
            conversationIds.push(conversationsrs.rows.item(i).statusnet_conversation_id);
        }
        var newsArray=[];
        var countArray=[];
        var allcontacts=getAllContacts(database,user);
        for(var j = 0; j< conversationIds.length; j++) {
            var newsrs=tx.executeSql('select * from news WHERE username="'+user+'"  AND statusnet_conversation_id="'+conversationIds[j] +'" ORDER BY created_at ASC');
            var helpernews=newsrs.rows.item(0);
            var helpernews=cleanhelpernews(database,user,helpernews,allcontacts)
            helpernews.currentconversation=[];
            for (var h = 0;h<newsrs.rows.length;h++){
                var helpernews2=newsrs.rows.item(h);
                if(helpernews.id!=helpernews2.status_id){
                    helpernews2.newscount=0;
                    helpernews2=cleanhelpernews(database,user,helpernews2,allcontacts)
                    helpernews.currentconversation.push(helpernews2)
                }
            }

            newsArray.push(helpernews);
            countArray.push(newsrs.rows.length)
        }
        var conversationsobject=({});

        conversationsobject.conversationIds=conversationIds;
        conversationsobject.newsArray=newsArray;
        conversationsobject.countArray=countArray;
        callback(conversationsobject);
    })}

function oldchatfromdb(database,user,conversationId,lastpost,allcontacts,callback){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    db.transaction( function(tx) {
        //    var newsArray=[];
        //    var countArray=[];
        //var allcontacts=getAllContacts(database,user);
        var newsrs=tx.executeSql('select * from news WHERE username="'+user+'"  AND statusnet_conversation_id="'+conversationId +'" AND status_id<'+lastpost+' ORDER BY created_at ASC');
        let conversation=[];
        var helpernews={currentconversation:conversation}
        if(newsrs.rows.length>0){
            //helpernews=newsrs.rows.item(0);
            //helpernews=cleanhelpernews(database,user,helpernews,allcontacts)
            //helpernews.currentconversation=[];
            for (var h = 0;h<newsrs.rows.length;h++){
                var helpernews2=newsrs.rows.item(h);
                if(helpernews.id!=helpernews2.status_id){
                    helpernews2.newscount=0;
                    helpernews2=cleanhelpernews(database,user,helpernews2,allcontacts)
                    //print(" helpernews "+JSON.stringify(helpernews.currentconversation))
                    helpernews.currentconversation.push(helpernews2)
                }
            }
        }
        var newscount=newsrs.rows.length;
        callback(helpernews,newscount);
        //    var conversationobject={news:helpernews,newscount:newscount};
        //    return conversationobject;
    })
}

function cleanhelpernews(database,user,helpernews,allcontacts){
    helpernews=fetchUsersForNews(database,user,helpernews,allcontacts);
    helpernews.statusnet_html=Qt.atob(helpernews.statusnet_html);
    helpernews.text=Qt.atob(helpernews.text);
    helpernews.id=helpernews.status_id;
    try{let geoobj=JSON.parse(helpernews.geo); helpernews.external_url=geoobj.external_url}catch(e){}
    helpernews.friendica_author=objFromArray(allcontacts,"url",helpernews.friendica_owner);
    if (helpernews.attachments!="" && helpernews.attachments!==null){helpernews.attachments=JSON.parse(Qt.atob(helpernews.attachments))};
    return helpernews
}


function getAllContacts(database,user){
    var allcontacts=[];
    Helperjs.readData(database,"contacts",user,function(obj){
        allcontacts=obj;
        for (var n in allcontacts){
            allcontacts[n].name=Qt.atob(allcontacts[n].name);
            allcontacts[n].description=Qt.atob(allcontacts[n].description)
        }
    });
    return allcontacts;
}

function inArray(list, prop, val) {
    if (list.length > 0 ) {
        for (var i in list) {if (list[i][prop] == val) {
                return true;
            }
        }
    } return false;
}

function objFromArray(list, prop, val) {
    if (list.length > 0 ) {
        for (var i in list) {if (list[i][prop] == val) {
                return list[i];
            }
        }
    } return false;
}

function cleanDate(date){
    var cleanedDate= date.slice(0,3)+", "+date.slice(8,11)+date.slice(4,7)+date.slice(25,30)+date.slice(10,25);
    return cleanedDate
}

function findTags(fulltext){
    return fulltext.match(/\s+[#]+[A-Za-z0-9-_\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF]+/g)
}
