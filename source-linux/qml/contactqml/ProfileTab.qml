//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
//import QtQuick.Controls.Styles 1.4
//import QtQuick.Layouts 1.11
//import QtQuick.LocalStorage 2.0
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/news.js" as Newsjs
import "qrc:/js/service.js" as Service
import "qrc:/qml/contactqml"
import "qrc:/qml/genericqml"

Item{
    id:profileTab
    function showProfile(callback){
        var profile=({});
        Helperjs.readData(db,"profiles",login.username,function(profileobject){
            var profilearray=[];
            for (var i in profileobject){
                profilearray.push(JSON.parse(Qt.atob(profileobject[i].profiledata)));
            }
            profile.profiles=profilearray;
        });
        Helperjs.readData(db,"contacts",login.username,function(owner){
            profile.friendica_owner=owner[0];
        },"isFriend",2);
        callback(profile)
    }
    Component.onCompleted:{
          showProfile(function(profile){
              var component = Qt.createComponent("qrc:/qml/contactqml/ProfileComponent.qml");
              var profilecomp = component.createObject(profileTab,{"profile": profile});
          });
      }
}
