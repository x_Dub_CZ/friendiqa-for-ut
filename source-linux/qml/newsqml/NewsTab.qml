//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import "qrc:/qml/newsqml"
import "qrc:/qml/genericqml"
import "qrc:/js/news.js" as Newsjs
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/service.js" as Service

Rectangle{
    id:newstabitem
    width:rootstack.width
    height: rootstack.height-8*mm

    Timer {id:contacttimer; interval: 50; running: false; repeat: false
        onTriggered: {
            root.newContacts=Newsjs.findNewContacts(root.news,root.contactlist);
            root.onNewContactsChanged(root.newContacts);
            Newsjs.storeNews(login,db,root.news,root)
        }
    }

    function showConversation(conversationIndex,newsitemobject){
        if(newsitemobject.messagetype==0 || newsitemobject.messagetype==3){
            xhr.clearParams();
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setUrl(login.server);
            xhr.setApi("/api/conversation/show");
            xhr.setParam("id",newsitemobject.id)
            xhr.setParam("count","200")
            xhr.get();
        }
        else{
            xhr.clearParams();
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setUrl(login.server);
            xhr.setApi("/api/direct_messages/conversation");
            xhr.setParam("uri",newsitemobject.statusnet_conversation_id)
            xhr.get();
        }
    }

    function getDateDiffString (seconds){
        var timestring="";
        if (seconds<60) {timestring= Math.round(seconds) +  " " +qsTr("seconds");}
        else if (seconds<90){timestring= Math.round(seconds/60) +  " " +qsTr("minute") ;}
        else if (seconds<3600){timestring= Math.round(seconds/60) +  " " +qsTr("minutes");}
        else if (seconds<5400){timestring= Math.round(seconds/3600) +  " " +qsTr("hour");}
        else if (seconds<86400){timestring= Math.round(seconds/3600) +  " " +qsTr("hours");}
        else if (seconds<129600){timestring= Math.round(seconds/86400) +  " " +qsTr("day");}
        else if (seconds<3888000){timestring= Math.round(seconds/86400) +  " " +qsTr("days");}
        else if (seconds<5832000){timestring= Math.round(seconds/3888000) +  " " +qsTr("month");}
        else if (seconds<69984000){timestring= Math.round(seconds/3888000) +  " " +qsTr("months");}
        else {timestring= Math.round(seconds/46656000) +  " " + qsTr("years");}

        return timestring;
    }

    function getActivitiesView(newsitemobject){
        var likeText="";var dislikeText="";var attendyesText="";var attendnoText="";var attendmaybeText="";  var self={};
        try{if (newsitemobject.messagetype==0&&newsitemobject.hasOwnProperty('friendica_activities')){
           if (newsitemobject.friendica_activities.like.length>0){
              if (newsitemobject.friendica_activities.like.length==1){likeText= newsitemobject.friendica_activities.like[0].name+" "+ qsTr("likes this.")}
              else {likeText= newsitemobject.friendica_activities.like.length+" "+ qsTr("like this.")}
           }
           if (newsitemobject.friendica_activities.dislike.length>0){
              if (newsitemobject.friendica_activities.dislike.length==1){dislikeText= newsitemobject.friendica_activities.dislike[0].name+" "+ qsTr("doesn't like this.")}
              else {dislikeText= newsitemobject.friendica_activities.dislike.length+" "+ qsTr("don't like this.")}
           }
           if (newsitemobject.friendica_activities.attendyes.length>0){
              if (newsitemobject.friendica_activities.attendyes.length==1){attendyesText=newsitemobject.friendica_activities.attendyes[0].name+" "+ qsTr("will attend.")}
              else {attendyesText= newsitemobject.friendica_activities.attendyes.length+" "+ qsTr("persons will attend.")}
           }
           if (newsitemobject.friendica_activities.attendno.length>0){
              if (newsitemobject.friendica_activities.attendno.length==1){attendnoText= newsitemobject.friendica_activities.attendno[0].name+" "+ qsTr("will not attend.")}
              else {attendnoText= newsitemobject.friendica_activities.attendno.length+" "+ qsTr("persons will not attend.")}
           }
           if (newsitemobject.friendica_activities.attendmaybe.length>0){
              if (newsitemobject.friendica_activities.attendmaybe.length==1){attendmaybeText= newsitemobject.friendica_activities.attendmaybe[0].name+" "+ qsTr("may attend.")}
              else {attendmaybeText= newsitemobject.friendica_activities.attendmaybe.length+" "+ qsTr("persons may attend.")}
           }
           //var friendica_activities_self=JSON.parse(newsitemobject.friendica_activities_self);
            }} catch(e){print("Activities "+e+ " "+JSON.stringify(newsitemobject.friendica_activities))}
        return {likeText:likeText,dislikeText:dislikeText,attendyesText:attendyesText,attendnoText:attendnoText,attendmaybeText:attendmaybeText}
    }



//    CalendarTab{
//        visible: wideScreen&&rootstackView.depth<2
//        width: newstabitem.width/3
//        x: newsSwipeview.width
//        //anchors.left: newsSwipeview.right
//        //anchors.fill: null
//    }
//    Rectangle{
//        color: "#F8F8F8"
//        height: parent.height
//        width: 0.5*mm
//        anchors.left: newsSwipeview.right
//    }
    BlueButton{
        z:2
        visible: !wideScreen
        fontColor: "grey"
        border.color: "transparent"
        text: "\uf0c9"
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            leftDrawerAndroid.visible?leftDrawerAndroid.close():leftDrawerAndroid.open()}
    }

    LeftDrawerLinux{
        id:leftDrawer
        property var newstabstatus: newstab.newstabstatus
        visible: wideScreen&&rootstackView.depth<2
        width: visible?root.fontFactor*osSettings.systemFontSize*15:0
        height: root.height-bar.height
    }

    LeftDrawerAndroid{
        id: leftDrawerAndroid
    }

    SwipeView{
        id: newsSwipeview
        property string stacktype:"Home"
        currentIndex: 0
        width: wideScreen&&rootstackView.depth<2?newstabitem.width-leftDrawer.width:newstabitem.width//newstabitem.width/3*2:newstabitem.width
        height: newstabitem.height-4*mm
        x: leftDrawer.width
        function onDirectMessage(friend){currentIndex=2}

        onCurrentIndexChanged: {
            switch(currentIndex){
                case 0: stacktype="Home";break;
                case 1: stacktype="Replies";break;
                case 2: stacktype="DirectMessages";break;
                case 3: stacktype="Notifications";break;
               default: stacktype="Home";
            }
        }

        //anchors.fill: parent
        Loader{
            id: friendstimeline
            source:(newsSwipeview.currentIndex==0)? "qrc:/qml/newsqml/NewsStack.qml":""
            //onLoaded: newsSwipeview.stacktype="Home"
        }
        Loader{
            id: replies
            //property string stacktype:"Replies"
            source:(newsSwipeview.currentIndex==1)? "qrc:/qml/newsqml/NewsStack.qml":""
            //onLoaded: newsSwipeview.stacktype="Replies"
        }
        Loader{
            id: directmessages
            property var friend:({})
            source:(newsSwipeview.currentIndex==2)? "qrc:/qml/newsqml/NewsStack.qml":""
            //onLoaded: newsSwipeview.stacktype="DirectMessages"
        }
        Loader{
            id: notifications
            //property string stacktype:"Notifications"
            source:(newsSwipeview.currentIndex==3)? "qrc:/qml/newsqml/NewsStack.qml":""
            //onLoaded: newsSwipeview.stacktype="Notifications"
        }
    }
    PageIndicator {
        id: swipeIndicator
        z:5
        height: root.fontFactor*osSettings.bigFontSize
        font.pointSize:  osSettings.systemFontSize
        count: newsSwipeview.count
        currentIndex: newsSwipeview.currentIndex
        anchors.bottom: newsSwipeview.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Component.onCompleted: {root.directmessageSignal.connect(newsSwipeview.onDirectMessage)}
}
