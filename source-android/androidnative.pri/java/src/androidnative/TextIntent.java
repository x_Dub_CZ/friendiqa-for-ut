package androidnative;
import org.qtproject.qt5.android.QtNative;
import android.content.Intent;
import android.app.Activity;
import java.util.Map;
import java.util.HashMap;
import android.os.Environment;
import android.util.Log;

public class TextIntent {

    // Random
    public static final int TEXT_INTENT_ACTION = 0x245285a4;

    public static final String CHOSEN_MESSAGE = "androidnative.TextIntent.chosen";

    private static final String TAG = "androidnative.TextIntent";

    private static Boolean broadcast = false;

    static {
        SystemDispatcher.addListener(new SystemDispatcher.Listener() {
            public void onDispatched(String type , Map message) {
                if (type.equals(SystemDispatcher.ACTIVITY_RESULT_MESSAGE)) {
                    onActivityResult(message);
                }
            }
        });
    }

    static private void onActivityResult(Map message) {       
        int resultCode = (Integer) message.get("resultCode");
        if (resultCode != Activity.RESULT_OK)
            return;
        int requestCode = (Integer) message.get("requestCode");
        Intent data = (Intent) message.get("data");

        if (requestCode == TEXT_INTENT_ACTION) {
	    String extrasubject = (String) message.get("subject");
	    String extratext = (String) message.get("text");
            importText(data,extrasubject,extratext);
        }
    }

    static private void importText(Intent data, String extrasubject, String extratext) {

        String subject = data.getStringExtra(Intent.EXTRA_SUBJECT);
        String plaintext = data.getStringExtra(Intent.EXTRA_TEXT);
        String htmltext = data.getStringExtra(Intent.EXTRA_HTML_TEXT);
	if (subject==null){subject=extrasubject;};
	if (plaintext==null){plaintext=extratext;};
        Map reply = new HashMap();
        reply.put("subject",subject);
        reply.put("plaintext",plaintext);
        reply.put("htmltext",htmltext);
        SystemDispatcher.dispatch(CHOSEN_MESSAGE,reply);
     }
}

