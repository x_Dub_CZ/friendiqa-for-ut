//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UPDATENEWS_H
#define UPDATENEWS_H

#include <QObject>
#include <QJsonObject>
#include <QSqlDatabase>
#include "xhr.h"
#include "alarm.h"
//#include "AndroidNative/systemdispatcher.h"

class UPDATENEWS : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
//    Q_PROPERTY(QString login READ login NOTIFY loginChanged)
public:
    static UPDATENEWS *instance();

    explicit UPDATENEWS(QObject *parent = 0);
  
    QString url() const;
    //QString login() const;

signals:

    void urlChanged(QString url);
    void success(QString api);
    void error(QString api, QString content);
    void quitapp();

public slots:
    void setUrl(QString url);
    void setSyncAll(bool syncAll);
    void setDatabase();
    void login();
    void timeline();
    void replies();
    void startsync();
    void directmessages();
    void notifications();
    void friendrequests();
    void events();
    //void startservice(QString type,QVariantMap map);
    void startImagedownload(QString downloadtype);
    void updateImageLocation(QString downloadtype,QString imageurl, QString filename, int index);
    void store(QByteArray serverreply,QString apiname);
    void storeFriendrequests(QByteArray serverreply,QString apiname);
    void storeEvents(QByteArray serverreply,QString apiname);
    void showError(QString data, QString url,QString api, int code);

private:
    QString m_url;
    QString m_api;
    QString m_imagedir;
    QString m_login;
    QString username;
    bool m_syncAll;
    int syncindex;
    int usernameindex;
    int usernamelength;
    QSqlDatabase m_db;
    QList<QString> synclist;
    QList <QString> notifylist;
    QList<QJsonValue> findNewContacts(QJsonDocument news);
    QJsonObject findNotificationContact(QString imagelink);
    int m_updateInterval;
    //void timeline();
    //void store(QByteArray serverreply,QString apiname);
    void updateContacts(QList<QJsonValue> contacts);

    XHR xhr;
    ALARM alarm;
    QList<QString> newcontactimagelinks;
    QList<QString> newcontactnames;
};

#endif // UPDATENEWS_H
