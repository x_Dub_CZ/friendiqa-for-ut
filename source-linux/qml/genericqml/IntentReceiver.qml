import QtQuick 2.0
import AndroidNative 1.0

Item {
    id:intent
    /// The URL of the image chosen. If multiple images are picked, it will be equal to the first image.
    property string imageUrl: ""

    /// A list of images chosen
    property var imageUrls: []

    property string m_TEXT_MESSAGE: "androidnative.TextIntent.chosen";
    property string m_IMAGE_MESSAGE: "androidnative.ImagePicker.chosen";


    Connections {
        target: SystemDispatcher
        function onDispatched (type, message) {
            if ((type === m_IMAGE_MESSAGE)&& (root.imagePicking==false)) {
                var h=[];
                for (var n in message.imageUrls){
                    h.push("file://"+ decodeURIComponent(message.imageUrls[n]).substring(5))
                }
                imageUrls=h;
                rootstack.currentIndex=0;newstab.active=true;
                root.uploadSignal(imageUrls.join(","))
//                if(imageUrls.length==1){
//                    rootstack.currentIndex=0;newstab.active=true;
//                    root.uploadSignal(imageUrls)
//                } else{
//                       rootstack.currentIndex=2;fotostab.active=true;
//                       root.uploadSignal(imageUrls)
//                }
            } else if (type==m_TEXT_MESSAGE){  
                rootstack.currentIndex=0;newstab.active=true;
                root.sendtextSignal(message)
            }
        }
    }

    Component.onCompleted: {
        SystemDispatcher.setInitialized();
//        if (root.globaloptions.hasOwnProperty("syncinterval") && root.globaloptions.syncinterval !=null && root.globaloptions.syncinterval !=0){
//            alarm.setAlarm(root.globaloptions.syncinterval);
//        }
    }
}

