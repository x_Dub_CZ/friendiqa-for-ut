//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

WorkerScript.onMessage = function(msg) {
    if (msg.firstalbum==0){msg.model.clear();}
    var contact={}; try{contact=msg.friend}catch(e){print(e)}
   var limit=0; if (msg.albums.length-msg.firstalbum<20){limit=msg.albums.length} else{limit=msg.firstalbum+20}
    for (var j=msg.firstalbum;j<limit;j++){
    if (msg.albums[msg.firstalbum]) {
 //       print("album"+msg.albums[j].name);
        if(msg.foreignPicture){
            var albumname=msg.albums[j].name.trim();var albumlink=msg.albums[j].link
        }else{
            var albumname=msg.albums[j].toString();var albumlink=""}
        msg.model.append({"albumlink":albumlink,"foreignPicture":msg.foreignPicture,"albumname":albumname,"friend":contact});
    msg.model.sync()
    };
 }
}
