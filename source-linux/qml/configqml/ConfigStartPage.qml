//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12


Page{
    //anchors.fill: parent
    width:root.width
    height:root.height-6*root.fontFactor*osSettings.bigFontSize

    CheckBox{
        id: autostartCheckbox
        x: mm
        y: root.fontFactor*osSettings.bigFontSize
        width: 10*root.fontFactor*osSettings.bigFontSize
        checked:filesystem.isAutostart
               //style: CheckBoxStyle {
        text: qsTr("Autostart")
        font.pointSize: osSettings.bigFontSize
        onClicked: {
            toggle();
            if(autostartCheckbox.checked==true){
                filesystem.setAutostart(false);
                autostartCheckbox.checked=false;
            }
            else{
                filesystem.setAutostart(true);
                autostartCheckbox.checked=true;
            }
        }
    }
//    CheckBox{
//            id: minimizeCheckbox
//            x: mm
//            y: 3*root.fontFactor*osSettings.bigFontSize
//            width: 10*root.fontFactor*osSettings.bigFontSize
//            enabled: autostartCheckbox.checked==true
//            checked:(globaloptions["notify_"+adapter]==1)?true:false
//            text: qsTr("Start Minimized")
//            font.pointSize: osSettings.bigFontSize
//            onClicked: {
//                toggle();
//                if(notifyCheckbox.checked==true){
//                    Service.updateglobaloptions(root.db,"notify_"+adapter,0);notifyCheckbox.checked=false;
//                }
//                else{
//                    Service.updateglobaloptions(root.db,"notify_"+adapter,1);notifyCheckbox.checked=true;
//            }
//        }
//    }
}
