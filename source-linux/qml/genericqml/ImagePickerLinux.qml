//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import Qt.labs.folderlistmodel 2.12
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Item{
    id:imageDialog
    z:2
//    border.color: "grey"
//    color: Material.backgroundColor
//    width: parent.width-root.fontFactor*osSettings.bigFontSize
//    height: parent.height-3*root.fontFactor*osSettings.bigFontSize
//    x: 0.5*root.fontFactor*osSettings.bigFontSize
//    y: 2*root.fontFactor*osSettings.bigFontSize
    property string directory: ""
    property bool multiple: false
    property string imageUrl: ""
    //property var imageUrls: []
    signal ready();
    function pickImage() {imageFileDialog.open()}

//    Text{
//        id:directoryText
//        anchors.bottom: parent.bottom
////        x:0.5*mm
////        y:0.5*mm
//        width: imageDialog.width-3.5*root.fontFactor*osSettings.bigFontSize
//        height:contentHeight
//        font.pointSize: osSettings.bigFontSize
//        wrapMode: Text.Wrap
//        text: directory
//        color: Material.primaryTextColor
//    }
//    Button{
//        id:closeButton
//        height: 2*root.fontFactor*osSettings.bigFontSize
//        anchors.top: parent.top
//        anchors.topMargin: 0.5*mm
//        anchors.right: parent.right
//        anchors.rightMargin: 1*mm
//        text:   "\uf057"
//        onClicked:{
//            ready();imageDialog.destroy()
//        }
//    }
    FileDialog {
        id: imageFileDialog
        title: "Please choose a file"
        folder: shortcuts.pictures
        selectFolder: false
        selectMultiple: true
        onAccepted: {
            imageUrl=imageFileDialog.fileUrls.toString();
            ready();
        }
    }
//    ListView {
//        id: imageView
//        x:0.5*mm
//        y: 2.5*root.fontFactor*osSettings.bigFontSize//Math.max(directoryText.height, closeButton.height)+mm
//        width: parent.width-2*mm
//        height: parent.height-3*root.fontFactor*osSettings.bigFontSize
//        boundsBehavior:Flickable.StopAtBounds
//        clip: true
//        model: imageModel
//        delegate: imageItem
//        ScrollBar.vertical: ScrollBar { }
//    }

//    FolderListModel{
//        id: imageModel
//        nameFilters: ["*.png", "*.jpg",".jpeg","*.JPG","*.gif"]
//        sortField: FolderListModel.Time
//        sortReversed:false
//        showDotAndDotDot: true
//        showDirs: true
//        showDirsFirst: true
//        folder:directory
//    }

//    BusyIndicator{
//        id: imageBusy
//        anchors.horizontalCenter: imageView.horizontalCenter
//        anchors.top:imageView.top
//        anchors.topMargin: 2*mm
//        width: 2*root.fontFactor*osSettings.bigFontSize
//        height: 2*root.fontFactor*osSettings.bigFontSize
//        running:false
//    }

//    Component{
//        id:imageItem
//        Item{
//            width:imageView.width
//            height: Math.max(fileImage.height,1.5*root.fontFactor*osSettings.bigFontSize)+2*mm
//            Rectangle{
//                id:imagetextRectangle
//                color:"black"
//                x:mm
//                z:3
//                opacity: fileIsDir?0:0.5
//                width:imagetext.contentWidth
//                height: imagetext.contentHeight
//                anchors.bottom: fileImage.bottom
//            }
//            Text {
//                id:imagetext
//                x: mm//fileIsDir?11*mm:mm
//                z:4
//                text: fileIsDir?"\uf07b "+fileName:fileName
//                width: fileIsDir?parent.width - 2.5*root.fontFactor*osSettings.bigFontSize :imageView.width-mm
//                anchors.bottom: fileImage.bottom
//                color: fileIsDir?Material.primaryTextColor:"white"
//                font.pointSize: osSettings.bigFontSize
//                wrapMode:Text.Wrap
//            }
//            Text {
//                id:selected
//                anchors.right:parent.right
//                visible: attachImageURLs.indexOf(fileURL)>-1
//                z:4
//                text: "\u2713"
//                width: 2*root.fontFactor*osSettings.bigFontSize
//                anchors.top: fileImage.top
//                color: "green"
//                font.pointSize: 3*osSettings.bigFontSize
//            }

//            Image{id:fileImage
//                width: imageView.width-mm
//                fillMode:Image.PreserveAspectFit
//                source:fileIsDir?"":fileURL
//            }

//            MouseArea{
//                anchors.fill: parent
//                onClicked:{
//                    if (fileName==".."){
//                        imageModel.folder=imageModel.parentFolder;
//                        directory=imageModel.parentFolder
//                    }
//                    else if (fileIsDir){
//                        imageModel.folder=fileURL;
//                        directory=fileURL
//                    }
//                    else{
//                        if (multiple!=true){
//                            //attachImageURLs.push(fileURL);
//                            //attachImage(fileURL);
//                            imageUrls.push(fileURL);
//                            imageUrl=fileURL;
//                            ready();
//                            imageDialog.destroy()
//                        }
//                        else {
//                            if(selected.visible==true){
//                                imageUrls.splice(imageUrls.indexOf(fileURL,1))
//                                selected.visible=false
//                            }
//                            else{
//                                imageUrls.push(fileURL);
//                                selected.visible=true;

//                            }
//                            imageUrl=fileURL
//                        }
//                    }
//                }
//            }
//        }
//    }
//    Component.onCompleted: imageFileDialog.open()
}
