//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/qml/genericqml"

Page{
    Text{id:infoBoxText
        anchors.top:closeButton.bottom
        anchors.topMargin: mm
        textFormat: Text.RichText
        width: root.width-mm
        font.pointSize: osSettings.systemFontSize
        color:Material.primaryTextColor
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: "<b>Friendiqa v0.6.7 </b><br>Licensed under GPL 3 with the exception of OpenSSL <br> "+
              "Website <a href='https://friendiqa.ma-nic.de'>https://friendiqa.ma-nic.de</a><br>"+
              "Sourcecode: <a href='https://git.friendi.ca/LubuWest/Friendiqa'>https://git.friendi.ca/LubuWest/Friendiqa</a><br>"+
              "Privacy Policy: <a href='https://git.friendi.ca/lubuwest/Friendiqa/src/branch/master/PrivacyPolicy.md'>http://git.friendi.ca/lubuwest/Friendiqa/src/branch/master/PrivacyPolicy.md</a><br>"+
              "Code by <a href='https://freunde.ma-nic.de/profile/pankraz'>Marco</a><br>"+
              "Qt Framework <a href='https://www.qt.io'>www.qt.io</a><br>"+
              "Icons by <a href='http://fontawesome.io'>FontAwesome</a><br>"+
              "Folder Icon by <a href='https://github.com/KDE/breeze-icons'>KDE Breeze Icons</a><br>"+
              "AndroidNative by <a href='https://github.com/benlau/androidnative.pri'>Ben Lau</a><br>"+
              "This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (<a href='http://www.openssl.org/'>http://www.openssl.org/</a>)"
        onLinkActivated:{
            Qt.openUrlExternally(link)}
    }
    MButton{
        id:closeButton
        anchors.top: parent.top
        anchors.topMargin: root.fontFactor*osSettings.bigFontSize
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        text:   "\uf057"
        font.pointSize: osSettings.bigFontSize
        onClicked:{rootstackView.pop()}
    }
}
