//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/qml/configqml"
import "qrc:/qml/genericqml"
import "qrc:/js/service.js" as Service

Page{
    //color:"white"
    width:root.width
    height: root.height

    //height:root.height
    Label {
        text: qsTr("Sync Interval (0=None)")
        font.pointSize: osSettings.bigFontSize
        //visible: false
        x: root.fontFactor*osSettings.bigFontSize; y: root.fontFactor*osSettings.bigFontSize; //width:35*mm;wrapMode: Text.Wrap
    }

    Slider{ id: messageIntervalSlider
        x:8*root.fontFactor*osSettings.bigFontSize; y: 4*root.fontFactor*osSettings.bigFontSize;
        width: root.width-10*root.fontFactor*osSettings.bigFontSize;
        height:2*root.fontFactor*osSettings.bigFontSize
        value: globaloptions.hasOwnProperty("syncinterval")?globaloptions.syncinterval:0
        from: 0;to:120; stepSize: 15
    }
    Rectangle{
        x: root.fontFactor*osSettings.bigFontSize; y:4*root.fontFactor*osSettings.bigFontSize;
        width: 4*root.fontFactor*osSettings.bigFontSize; height: 2*root.fontFactor*osSettings.bigFontSize;
        color: Material.dialogColor
        radius: 0.5*mm
        TextEdit{
            id: messageIntervalField
            anchors.fill: parent
            font.pointSize: osSettings.bigFontSize
            verticalAlignment:TextEdit.AlignRight
            color: Material.primaryTextColor
            text:messageIntervalSlider.value
            focus: true
            selectByMouse: true
            onTextChanged: {
                Service.updateglobaloptions(root.db,"syncinterval",text);
                if(osSettings.osType=="Android"){
                    alarm.setAlarm(text);
                } else if(osSettings.osType=="Linux" && text !=0){
                    root.updateSyncinterval(parseInt(text))
                }
            }
        }
    }
    Label{x: 6*root.fontFactor*osSettings.bigFontSize; y: 4*root.fontFactor*osSettings.bigFontSize;
        width: 2*root.fontFactor*osSettings.bigFontSize; height: 1.5*root.fontFactor*osSettings.bigFontSize;
        font.pointSize: osSettings.bigFontSize
        text:qsTr("Min.")
    }

    ScrollView{
        width: root.width
        height: root.height - 10*root.fontFactor*osSettings.bigFontSize;
        y:7*root.fontFactor*osSettings.bigFontSize
        clip:true
        Column{
            width: parent.width
            spacing:mm
            SyncComponent{adapter:"Timeline"}
            SyncComponent{adapter:"Replies"}
            SyncComponent{ adapter:"DirectMessages"}
            SyncComponent{ adapter:"Notifications"}
            SyncComponent{ adapter: "Events"}
            SyncComponent{adapter: "FriendRequests"}
        }
    }

//    MButton{
//        id:closeButton
//        anchors.top: parent.top
//        anchors.topMargin: osSettings.bigFontSize
//        anchors.right: parent.right
//        anchors.rightMargin: 1*mm
//        width: 2*root.fontFactor*osSettings.bigFontSize;
//        text:   "\uf057"
//        font.pointSize: osSettings.bigFontSize
//        onClicked:{rootstackView.pop()}
//    }
}
