//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import QtQuick.Controls 2.12
import QtQml.Models 2.1
//import "qrc:/js/service.js" as Service
import "qrc:/js/image.js" as Imagejs
import "qrc:/js/helper.js" as Helperjs

Package {
    Item {
        Package.name: 'browser'
        GridView {
            id: photosGridView; model: visualModel.parts.grid; width: albumgridview.width; height: albumgridview.height;y:albumgridview.y
            cellWidth: 16.5*mm; cellHeight: 16.5*mm; interactive: false//anchors.margins:2*mm
            onCurrentIndexChanged: photosListView.positionViewAtIndex(currentIndex, ListView.Contain)
             }
         }

    Item {
        Package.name: 'fullscreen'
        ListView {
            id: photosListView; model: visualModel.parts.list; orientation: Qt.Horizontal
            width: albumgridview.width; height: albumgridview.height; //y:albumgridview.y
            //width: parent.width; height: parent.height;
            interactive: false
            onCurrentIndexChanged: photosGridView.positionViewAtIndex(currentIndex, GridView.Contain)
            highlightRangeMode: ListView.StrictlyEnforceRange; snapMode: ListView.SnapOneItem
            }
         }

    Item {
        Package.name: 'album'
        id: albumWrapper; width: 16.5*mm; height: 16.5*mm
        DelegateModel {
            id: visualModel; delegate: PhotoComponent { }
            model: photoModel
            }

         PathView {
            id: photosPathView;
            model: visualModel.parts.stack;
            pathItemCount: 1
            anchors.centerIn: parent;
            path: Path {
                PathAttribute { name: 'z'; value: 9999.0 }
                PathLine { x: 1; y: 1 }
                PathAttribute { name: 'z'; value: 0.0 }
                }
            }

         Rectangle{
           color:"black"
           opacity: 0.5
           width:albumtext.contentWidth
           height: albumtext.contentHeight
           anchors.bottom: albumWrapper.bottom
         }
         Text {
            id:albumtext
            text: albumname
            width:albumWrapper.width-1*mm
            height: albumtext.contentHeight
            wrapMode:Text.Wrap
            color: "white"
            font.family: "Monospace"
            font.pointSize: osSettings.systemFontSize
            anchors.bottom: albumWrapper.bottom
            }

        ListModel{
            id: photoModel
        }

        Component.onCompleted:{
            try {photoModel.clear()}catch (e){print(e)}
            if(foreignPicture){
                Imagejs.newRequestFriendsPictures(login,albumlink,friend,photoStack.remoteContact,remoteauth,root,function(obj){
                    if (obj) {
                        for (var k=0;k<obj.length;k++){
                            photoModel.append({"imageLocation": obj[k].thumb,"photoDescription":obj[k].name,"photoLink":obj[k].link})
                        }
                    }
            })}
            else{
                Helperjs.readData(db,"imageData",root.login.username,function(obj){
                    if (obj) {
                        for (var k=0;k<obj.length;k++){
                            if(typeof(obj[k].desc)=="string" && obj[k].desc!=""){var name=obj[k].desc}else{var name=obj[k].filename}
                            photoModel.append({"imageLocation": obj[k].location+obj[k].filename,"photoDescription":name,"photoLink":obj[k].location+obj[k].filename,"imageId":obj[k].id})
                        }
                    }
                },"album",albumname)}
        }

    MouseArea {
        anchors.fill: parent
        onPressAndHold:{
            var menuString="import QtQuick.Controls 2.12; Menu {MenuItem{text:qsTr('Delete on client and server'); onTriggered: {deletepics('album',albumname);photogroupModel.remove(index)}}}";
            var albummenuObject=Qt.createQmlObject(menuString,albumWrapper,"albummenuOutput")
            albummenuObject.popup()
        }
        onClicked: albumWrapper.state = 'inGrid'
     }

     states: [
            State {
                name: 'inGrid'
                 PropertyChanges { target: photosGridView; interactive: true }
                 PropertyChanges { target: photoBackground; opacity: 1 }
                 PropertyChanges { target: backButton; onClicked: albumWrapper.state = ''; y: 6 }
                 },
             State {
                name: 'fullscreen'; extend: 'inGrid'
                PropertyChanges { target: photosGridView; interactive: false }
                PropertyChanges { target: photosListView; interactive: true }
                PropertyChanges { target: photoBackground; opacity: 1 }
                PropertyChanges { target: backButton;onClicked:{
                    albumWrapper.state = 'inGrid'}
                }
              }
              ]

} //album Ende
} //Package Ende
 //item Ende
