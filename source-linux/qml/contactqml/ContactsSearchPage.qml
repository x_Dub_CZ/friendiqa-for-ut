//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/service.js" as Service
import "qrc:/js/news.js" as Newsjs
import "qrc:/qml/contactqml"
import "qrc:/qml/genericqml"

Page{
    id: contactsSearchPage


    function search(term){
        contactSearchBusy.running=true;
        try {contactsSearchModel.clear()} catch(e){};
        xhr.clearParams();
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setUrl(login.server);
        xhr.setApi("/api/v1/accounts/search");
        xhr.setParam("q",term);
        xhr.setParam("limit",99)
        xhr.get();
    }

    Connections{
        target:xhr
        function onError(data,url,api,code){
            if (data !="contactlist"){Helperjs.showMessage(qsTr("Network Error"),"API:\n" +login.server+api+"\n Return: \n"+data,root);}
            contactSearchBusy.running=false;
        }
        function onSuccess(data,api){
            if (api=="/api/v1/accounts/search" && data!=""){
               try{var searchlist = JSON.parse(data);}catch(e){print("Error "+e)}
               contactSearchBusy.running=false;
               if (Array.isArray(searchlist)){
                   searchlist.sort(function(a,b){
                       if (a.group > b.group) {
                           return -1;
                       }
                       if (a.group < b.group) {
                           return 1;
                       }
                       return 0
                   })
                    for (let i=0;i<searchlist.length;i++){
                        if (searchlist[i].id!="0"){
                            if (searchlist[i].note!=null){
                                 searchlist[i].description=searchlist[i].note;}
                            else{searchlist[i].description=""}
                            searchlist[i].name=(searchlist[i].display_name);
                            searchlist[i].screen_name=searchlist[i].username;
                            searchlist[i].location="";
                            searchlist[i].profile_image=""
                            searchlist[i].profile_image_url=searchlist[i].avatar;
                            searchlist[i].curIndex=contactsSearchModel.count;
                            let contactType="";
                            if (searchlist[i].group){contactType=qsTr("Forum")}
                            //else if (contactlist[i].bot){contactType=qsTr("Bot")}
                            else{contactType=qsTr("Person")}
                            contactsSearchModel.append({"contact":searchlist[i],"contactType":contactType});
                        }
                    }
                }
            }
        }
    }

    MButton {
        id: closeButton
        anchors.top: parent.top
        anchors.topMargin: 0.5*root.fontFactor*osSettings.bigFontSize
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        width: 2*root.fontFactor*osSettings.bigFontSize;
        text: "\uf057"
        onClicked: {
            rootstackView.pop()
        }
    }

    Search{
        y:0.5*root.fontFactor*osSettings.bigFontSize;
        x:1.5*root.fontFactor*osSettings.systemFontSize;
        width:root.width-(7*root.fontFactor*osSettings.systemFontSize+mm);
        height: 2.5*root.fontFactor*osSettings.systemFontSize;
        color:Material.dialogColor
        selfdestroying:false
    }

    BusyIndicator{
        id: contactSearchBusy
        anchors.centerIn:parent
        width:10*mm
        height: 10*mm
        running: false
    }

    Component {
        id: sectionHeading
        Rectangle {
            width: contactsSearchView.width
            height: childrenRect.height
            color: Material.backgroundColor
            required property string section
            Text {
                color: Material.secondaryTextColor
                text: parent.section
                font.bold: true
                font.pointSize: osSettings.bigFontSize
            }
        }
    }

    ListView {
        id: contactsSearchView
        x:mm
        y:4*root.fontFactor*osSettings.bigFontSize;
        width:root.width-2*mm
        height: root.height-7*root.fontFactor*osSettings.bigFontSize;
        spacing: 2
        clip: true
        function processContactSelection(contactobject){contactobject.searchContact=true;showContact(contactobject)}
        model: contactsSearchModel
        delegate: ContactComponent { }
        section.property: "contactType"
        section.criteria: ViewSection.FullString
        section.delegate: sectionHeading
    }

    ListModel{id: contactsSearchModel}
//    Component.onCompleted: {
//        friendsTabView.contactsSignal.connect(showContacts);
//        showContacts()
//    }
}
