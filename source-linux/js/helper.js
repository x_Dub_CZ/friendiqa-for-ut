//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

.pragma library
.import QtQuick.LocalStorage 2.0 as Sql

function friendicaRequest(login,api,rootwindow,callback) {
    var xhrequest= new XMLHttpRequest();
    xhrequest.onreadystatechange = function() {
        if(xhrequest.readyState === XMLHttpRequest.DONE) {
            try{
                if (xhrequest.status==200){
                    callback(xhrequest.responseText)
                }else{print("xhrequest.status "+xhrequest.status)
                    callback(xhrequest.responseText)
                    //showMessage("Error","API:\n" +login.server+api+"\n NO RESPONSE"+xhrequest.statusText,rootwindow);
                }
            }
            catch (e){
                showMessage("Error",  "API:\n" +login.server+api+"\n"+e+"\n Return: "+xhrequest.responseText,rootwindow)
            }
         }
    }
    xhrequest.open("GET", login.server+api,true,login.username,Qt.atob(login.password));
    xhrequest.send();
}

function friendicaPostRequest(login,api,data,method,rootwindow,callback) {
    var xhrequest= new XMLHttpRequest();
    xhrequest.onreadystatechange = function() {
        //print(api+JSON.stringify(login)+Qt.atob(login.password));
        if (xhrequest.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            } else if(xhrequest.readyState === XMLHttpRequest.DONE) {
                try{ if (xhrequest.responseText!=""){//print (xhrequest.responseText)
                        callback(xhrequest.responseText)
                        }else{//print("API:\n" +api+" NO RESPONSE");
                        //showMessage("Error","API:\n" +api+" NO RESPONSE",rootwindow)
                        callback(xhrequest.responseText)
                        }
                }
                catch (e){
                    print("API:\n" + api+" "+e+"\n Return:"+xhrequest.responseText);
                    //showMessage("Error", "API:\n" + api+" "+e+"\n Return:"+xhrequest.responseText,rootwindow)
                }
            }
        }
    xhrequest.open(method, login.server+api,true,login.username,Qt.atob(login.password));
    xhrequest.send(data);
}

function getCount(database,login,table,field,countvalue){
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
    var count=0;
    db.transaction( function(tx) {
        var countrs = tx.executeSql('SELECT COUNT(*) from '+table+' WHERE username= "'+ login.username +'" AND '+field+' = "'+countvalue+'"');
        count = parseInt(countrs.rows.item(0)["COUNT(*)"])
    })
    return count
}

function friendicaWebRequest(url,rootwindow,callback) {
    var xhrequest = new XMLHttpRequest();
    xhrequest.onreadystatechange = function() {
        if (xhrequest.readyState === XMLHttpRequest.HEADERS_RECEIVED) {}
        else if(xhrequest.readyState === XMLHttpRequest.DONE) {
            try{callback(xhrequest.responseText)}
            catch (e){showMessage("Error","API:\n" +url+" "+e+"\n Return: "+xhrequest.responseText, rootwindow)}
         }
    }
    xhrequest.open("GET", url,true);
    xhrequest.send();
}

function friendicaXmlRequest(url,rootwindow,callback) {
    var xhrequest = new XMLHttpRequest();
    xhrequest.onreadystatechange = function() {
        if (xhrequest.readyState === XMLHttpRequest.HEADERS_RECEIVED) {}
        else if(xhrequest.readyState === XMLHttpRequest.DONE) {
            try{callback(xhrequest.responseXML)}
            catch (e){showMessage("Error","API:\n" +url+" "+e+"\n Return: "+xhrequest.responseText, rootwindow)}
         }
    }
    xhrequest.open("GET", url);
    xhrequest.responseType ="document";
    xhrequest.send();
}

function friendicaRemoteAuthRequest(login,url,c_url,rootwindow,callback) {
    var xhrequest = new XMLHttpRequest();
    xhrequest.onreadystatechange = function() {
        if (xhrequest.readyState === XMLHttpRequest.HEADERS_RECEIVED) {}
        else if(xhrequest.readyState === XMLHttpRequest.DONE) {
            try{callback(xhrequest.responseText)}
            catch (e){showMessage("Error","Url:\n" +url+" "+e+"\n Return: "+xhrequest.responseText, rootwindow)}
         }
    }
    xhrequest.open("GET", login.server+"/api/friendica/remoteauth?c_url="+c_url+"&url="+url,true,login.username,Qt.atob(login.password));
    xhrequest.send();
}


function readData(database,table,username,callback,filter,filtervalue, sort) { // reads and applies data from DB
 if (filter){
    if (username){var where = " AND "+ filter +" = '" + filtervalue+"'";} else{
     var where = " WHERE "+ filter +" = '" + filtervalue+"'";}
 } else { var where="";}
 if (username){
    var user = ' where username= "'+ username +'"';
 } else { var user='';}

 if (sort){
 var sortparam = " ORDER BY "+ sort;
 } else { var sortparam="";}
 var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
 if(!db) { return; }
 db.transaction( function(tx) {
     //print('select * from '+table+user+where+sortparam);
     var rsArray=[];
     var rs = tx.executeSql('select * from '+table+user+where+sortparam);
     for(var i = 0; i < rs.rows.length; i++) {
         rsArray.push(rs.rows.item(i))
     }
     callback(rsArray);
 });
}

function readField(field,database,table, username, callback,filter,filtervalue) { // reads and applies data from DB
if (filter){
var where = " AND "+ filter +" = '" + filtervalue+"'";
} else { var where="";}
 var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
  if(!db) { return; }
 db.transaction( function(tx) {
    //print('... read from database ' + field)
    var rsArray=[];
    //print('select DISTINCT '+field+' from '+table+' WHERE username="'+username+'"'+where+' ORDER BY '+field+' ASC');
    var rs = tx.executeSql('select DISTINCT '+field+' from '+table+' WHERE username="'+username+'"'+where+' ORDER BY '+field+' ASC');
    for(var i = 0; i < rs.rows.length; i++) {
        rsArray.push(rs.rows.item(i)[field])
    }
    callback(rsArray);
 });
}


function deleteData(database,table, username, callback,filter,filtervalue) { // reads and applies data from DB
if (filter){
var where = " AND "+ filter +" = '" + filtervalue+"'";
} else { var where="";}
 var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
  if(!db) { return; }
 db.transaction( function(tx) {
    var rsArray=[];
    var rs = tx.executeSql('DELETE from '+table+' WHERE username="'+username+'"'+where);
    callback();
 });
}

function updateData(database,table, username, key, value, callback,filter,filtervalue) { // reads and applies data from DB
    if (filter){
        var where = " AND "+ filter +" = '" + filtervalue+"'";
    } else { var where="";}
    var db=Sql.LocalStorage.openDatabaseSync(database[0],database[1],database[2],database[3]);
        if(!db) { return; }
    db.transaction( function(tx) {
        var rsArray=[];
        var rs = tx.executeSql('UPDATE '+table+' SET '+key+' = '+value+' WHERE username="'+username+'"'+where);
        callback();
     });
}

function showMessage(header,message,rootwindow){//print(message);
    var cleanmessage=message.replace(/"/g,"-");
    if(cleanmessage.length>200){cleanmessage=cleanmessage.slice(0,200)+'...'}
    var messageString='import QtQuick 2.0; import QtQuick.Controls 2.15; import QtQuick.Controls.Material 2.12; Dialog{ visible: true; title:"'+header+'";standardButtons: Dialog.Ok;anchors.centerIn: parent;Label{text:" '+cleanmessage+'"}}';
    var messageObject=Qt.createQmlObject(messageString,rootwindow,"messageOutput");
}

function inArray(list, prop, val) {
    if (list.length > 0 ) {
        for (var i in list) { if (list[i][prop] == val) {
                return i;
            }
        }
    } return -1;
}
function cleanArray(array) {
var arraystring=JSON.stringify(array);
    arraystring=arraystring.replace(/[\[\]]/g , '');
return arraystring;
}

function cleanDate(date){
var cleanedDate= date.slice(0,3)+", "+date.slice(8,11)+date.slice(4,7)+date.slice(25,30)+date.slice(10,25);
return cleanedDate
}
