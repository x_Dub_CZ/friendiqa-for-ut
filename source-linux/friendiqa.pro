# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TEMPLATE = app
TARGET = friendiqa
CONFIG += release
QT += qml quick gui widgets sql webview dbus webengine

SOURCES += common/friendiqa.cpp \
	common/uploadableimage.cpp \
	common/xhr.cpp \
        common/filesystem.cpp \
        common/remoteauthasyncimageprovider.cpp \
        common/updatenews.cpp \
        common/alarmlinux.cpp

RESOURCES     = application.qrc

OTHER_FILES += qml/friendiqa.qml \
    translations/*.ts \
    qml/*.qml
    qml/newsqml/*.qml
    qml/contactqml/*.qml
    qml/photoqml/*.qml
    qml/configqml/*.qml
    js/*.js

TRANSLATIONS += translations/friendiqa-de.ts \
                translations/friendiqa-es.ts \
                translations/friendiqa-it.ts
HEADERS += \
    common/uploadableimage.h \
    common/xhr.h \
    common/filesystem.h \   
    common/remoteauthasyncimageprovider.h \
    common/updatenews.h \
    common/alarm.h
    
DISTFILES += \
    qml/calendarqml/*.qml \
    translations/*.ts \
    translations/*.qm \
    qml/*.qml \
    qml/newsqml/*.qml \
    qml/contactqml/*.qml \
    qml/photoqml/*.qml \
    qml/configqml/*.qml \
    js/*.js \


target.path=/usr/bin
desktop.path = /usr/share/applications
desktop.files = images/de.ma-nic.Friendiqa.desktop
icon.path = /usr/share/icons/hicolor/scalable/apps
icon.files = images/Friendiqa.svg
INSTALLS+=target desktop icon
