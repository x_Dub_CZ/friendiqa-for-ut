//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"
import "qrc:/qml/calendarqml"

Rectangle{
    id:eventItem
    property string status: eventstatus
    property var currEvent: event
    width:parent.width
    height:Math.max(eventNameText.height+eventDetailsText.height,profileImage.height)+mm
    border.color: Material.backgroundDimColor
    color: Material.backgroundColor
    border.width: 1
    radius: 0.5*mm
    Image {
        id:profileImage
        source: ((event.eventOwner.profile_image!="") && (typeof(event.eventOwner.profile_image)=="string"))? "file://"+event.eventOwner.profile_image : event.eventOwner.profile_image_url
        x:1
        y:1
        width: 7*mm
        height: 7*mm
        onStatusChanged: if (profileImage.status == Image.Error) {source="qrc:/images/defaultcontact.jpg"}
    }
    Text {
        id:eventNameText
        x: 8*mm
        width:parent.width-8*mm
        height:contentHeight
        color: Material.primaryTextColor
        textFormat: Text.RichText
        font.pointSize: osSettings.systemFontSize
        text: new Date(event.start).toLocaleString(Qt.locale(),Locale.NarrowFormat)+ " - " +((event.end>0)&&(event.end!=null)?new Date(event.end).toLocaleString(Qt.locale(),Locale.NarrowFormat):"\u221E")+":<br>"+(status=="large"?"<b>"+event.title+"</b>":event.title)
        wrapMode:Text.Wrap
    }

    Text {
        id:eventDetailsText
        x:8*mm
        z:4
        width: parent.width-8*mm
        height: contentHeight
        color: Material.primaryTextColor
        textFormat: Text.RichText
        text: status!="large"?"":Qt.atob(event.desc) + (event.location==""?"":"<br><br>"+qsTr("Location")+": "+event.location)
        anchors.top: eventNameText.bottom
        font.pointSize: osSettings.systemFontSize
        wrapMode:Text.Wrap
        onLinkActivated:{Qt.openUrlExternally(link)}
    }

    MouseArea{
        anchors.fill: parent
        MButton{
            id:  deleteEvent
            anchors.top: parent.top
            anchors.topMargin: 0.5*mm
            anchors.right:parent.right
            anchors.rightMargin:mm
            width: 2*root.fontFactor*osSettings.bigFontSize;
            text:"\uf1f8"
            onClicked: {
                deleteDialog.eventid=event.id
                deleteDialog.open()
            }
        }

        onClicked:{
            if (status==""){
                rootstackView.push("qrc:/qml/calendarqml/EventList.qml",{"dayint": event.startday, "events":[event]});
            } else {rootstackView.pop()}
        }
    }





    Connections{
       target: xhr
       function onSuccess(text,api){
            if (api=="/api/friendica/event_delete"){
                let obj=JSON.parse(text);
                if(obj.status=="deleted"&&obj.id==event.id){
                    Helperjs.deleteData(db,"events",login.username, function(){
                        eventModel.remove(index);
                    },"id",obj.id)
                }
           }
       }
    }
}
