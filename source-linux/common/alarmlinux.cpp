//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2017 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "alarm.h"
#include <QtCore/QDebug>
#include <QtDBus/QtDBus>
#include <QJsonArray>


ALARM *ALARM::instance()
{
    static ALARM alarm;
    return &alarm;
}

ALARM::ALARM(QObject *parent) : QObject(parent){}

void ALARM::setAlarm(int interval)
{
    //qDebug() << interval;
    QVariantMap message;
    message["value"] = interval;
}

void ALARM::notify(QString title, QString text, int id)
{
    //qDebug() << title << text;
//    QVariantMap message;
//    message["title"] = title;
//    message["message"] =  text;
    QStringList actionlist;
    QMap<QString,QVariant> hint;
    QDBusConnection bus = QDBusConnection::sessionBus();
    QDBusInterface dbus_iface("org.freedesktop.Notifications", "/org/freedesktop/Notifications",
                              "org.freedesktop.Notifications", bus);
    QString appname="Friendiqa";
    uint v=12321;
    if (dbus_iface.isValid()){
        dbus_iface.call("Notify",appname,v,"",title,text,actionlist,hint,10000);
        //qDebug() << "Qdebug error " << dbus_iface.lastError();
    }
}
