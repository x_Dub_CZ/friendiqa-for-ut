<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AcceptRules</name>
    <message>
        <location filename="../qml/configqml/AcceptRules.qml" line="41"/>
        <source>Accept instance rules</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountPage</name>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="64"/>
        <location filename="../qml/configqml/AccountPage.qml" line="206"/>
        <location filename="../qml/configqml/AccountPage.qml" line="228"/>
        <location filename="../qml/configqml/AccountPage.qml" line="245"/>
        <location filename="../qml/configqml/AccountPage.qml" line="306"/>
        <location filename="../qml/configqml/AccountPage.qml" line="385"/>
        <location filename="../qml/configqml/AccountPage.qml" line="406"/>
        <source>User</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Servidor</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="229"/>
        <source>Nickname</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="248"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="255"/>
        <source>Image dir.</source>
        <translation>Dir. de imágenes</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Noticias como</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="208"/>
        <source>Instance rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <location filename="../qml/configqml/AccountPage.qml" line="354"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation>No se admiten los apodos que contienen el símbolo @ actualmente</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="304"/>
        <source>Confirm</source>
        <translation>Confirmar</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="311"/>
        <source>No server given! </source>
        <translation>¡Servidor no encontrado!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="312"/>
        <source>No nickname given! </source>
        <translation>¡Usuario incorrecto!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="313"/>
        <source>No password given! </source>
        <translation>¡Contraseña incorrecta!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="314"/>
        <source>No image directory given!</source>
        <translation>¡No se ha encontrado el directorio de imágenes!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <source>Wrong password or 2FA enabled!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">¡Contraseña incorrecta!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Success</source>
        <translation>éxito!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronología</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversaciones</translation>
    </message>
</context>
<context>
    <name>BlockUser</name>
    <message>
        <location filename="../qml/newsqml/BlockUser.qml" line="41"/>
        <source>Block contact?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarTab</name>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="154"/>
        <source>Delete Event?</source>
        <translation>¿Borrar la cita?</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="201"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="206"/>
        <source>Own Calendar</source>
        <translation>Calendario propio</translation>
    </message>
</context>
<context>
    <name>ConfigAppearancePage</name>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="46"/>
        <source>News as</source>
        <translation>Noticias como</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="60"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="78"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="79"/>
        <source>Conversations</source>
        <translation>Conversaciones</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="72"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="73"/>
        <source>Timeline</source>
        <translation>Cronología</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="86"/>
        <source>Max. News</source>
        <translation>Nº Max. de noticias.</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="121"/>
        <source>Hide #nsfw?</source>
        <translation>Ocultar #nsfw?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="159"/>
        <source>Dark Mode</source>
        <translation>Diseño</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="163"/>
        <source>System</source>
        <translation>diseño estándar</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="174"/>
        <source>Dark</source>
        <translation>diseño oscuro</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="185"/>
        <source>Light</source>
        <translation>diseño brillante</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <source>News as</source>
        <translation type="vanished">Noticias como</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversaciones</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronología</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Nº Max. de noticias.</translation>
    </message>
    <message>
        <source>Hide #nsfw?</source>
        <translation type="vanished">Ocultar #nsfw?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="55"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="61"/>
        <source>Sync</source>
        <translation>Sincronización</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="67"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
</context>
<context>
    <name>ConfigStartPage</name>
    <message>
        <location filename="../qml/configqml/ConfigStartPage.qml" line="49"/>
        <source>Autostart</source>
        <translation>Autoarranque</translation>
    </message>
</context>
<context>
    <name>ConfigTab</name>
    <message>
        <source>User</source>
        <translation type="vanished">Usuario</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Servidor</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="vanished">Usuario</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Contraseña</translation>
    </message>
    <message>
        <source>Image dir.</source>
        <translation type="vanished">Dir. de imágenes</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Nº Max. de noticias.</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Noticias como</translation>
    </message>
    <message>
        <source>Interval (0=None)</source>
        <translation type="vanished">Intervalo (0=ningún)</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Confirmar</translation>
    </message>
    <message>
        <source>No server given! </source>
        <translation type="vanished">¡Servidor no encontrado!</translation>
    </message>
    <message>
        <source>No nickname given! </source>
        <translation type="vanished">¡Usuario incorrecto!</translation>
    </message>
    <message>
        <source>Nickname not registered at given server! </source>
        <translation type="vanished">¡Usuario incorrecto!</translation>
    </message>
    <message>
        <source>No username given! </source>
        <translation type="vanished">¡Usuario incorrecto!</translation>
    </message>
    <message>
        <source>No password given! </source>
        <translation type="vanished">¡Contraseña incorrecta!</translation>
    </message>
    <message>
        <source>No image directory given!</source>
        <translation type="vanished">¡No se ha encontrado el directorio de imágenes!</translation>
    </message>
    <message>
        <source>No maximum news number given!</source>
        <translation type="vanished">¡Nº máximo de noticias incorrecto!</translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">¡Contraseña incorrecta!</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">éxito!</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nombre</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronología</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversaciones</translation>
    </message>
</context>
<context>
    <name>ContactComponent</name>
    <message>
        <location filename="../qml/genericqml/ContactComponent.qml" line="42"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
</context>
<context>
    <name>ContactDetailsComponent</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">Conectar</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Descripción</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Localización</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation type="vanished">Mensajes</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="vanished">URL</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation type="vanished">Creado en</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="49"/>
        <source>seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="50"/>
        <source>minute</source>
        <translation>Minuto</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="51"/>
        <source>minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="52"/>
        <source>hour</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="53"/>
        <source>hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="54"/>
        <source>day</source>
        <translation>Dia</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="55"/>
        <source>days</source>
        <translation>Dias</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="56"/>
        <source>month</source>
        <translation>Mes</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="57"/>
        <source>months</source>
        <translation>Meses</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="58"/>
        <source>years</source>
        <translation>Años</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="67"/>
        <source>likes this.</source>
        <translation>le gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="68"/>
        <source>like this.</source>
        <translation>me gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>no de ése.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>no me gusta.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="75"/>
        <source>will attend.</source>
        <translation>asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>Personas que asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="79"/>
        <source>will not attend.</source>
        <translation>no asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>Personas que no asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="83"/>
        <source>may attend.</source>
        <translation>Puede asistir.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>Personas que pueden asistir.</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Conectar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="219"/>
        <source>Approve</source>
        <translation>Aprobar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="234"/>
        <source>Reject</source>
        <translation>Rechazar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="249"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="264"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="281"/>
        <source>Unfollow</source>
        <translation>Dejar de seguir</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="298"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="313"/>
        <source>Unblock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Location</source>
        <translation>Localización</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Posts</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="351"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="352"/>
        <source>Created at</source>
        <translation>Creado en</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="353"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="354"/>
        <source>Following</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="364"/>
        <source>Network Error</source>
        <translation>Fallo de red</translation>
    </message>
</context>
<context>
    <name>ContactsSearchPage</name>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="62"/>
        <source>Network Error</source>
        <translation>Fallo de red</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="91"/>
        <source>Forum</source>
        <translation>Foro</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="93"/>
        <source>Person</source>
        <translation>Persona</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="49"/>
        <source>seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="50"/>
        <source>minute</source>
        <translation>Minuto</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="51"/>
        <source>minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="52"/>
        <source>hour</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="53"/>
        <source>hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="54"/>
        <source>day</source>
        <translation>Dia</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="55"/>
        <source>days</source>
        <translation>Dias</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="56"/>
        <source>month</source>
        <translation>Mes</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="57"/>
        <source>months</source>
        <translation>Meses</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="58"/>
        <source>years</source>
        <translation>Años</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="67"/>
        <source>likes this.</source>
        <translation>le gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="68"/>
        <source>like this.</source>
        <translation>me gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>no de ése.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>no me gusta.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="75"/>
        <source>will attend.</source>
        <translation>asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>Personas que asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="79"/>
        <source>will not attend.</source>
        <translation>no asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>Personas que no asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="83"/>
        <source>may attend.</source>
        <translation>Puede asistir.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>Personas que pueden asistir.</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponent</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="60"/>
        <source>Refresh</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="79"/>
        <source>Timeline</source>
        <translation>Cronología</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="97"/>
        <source>Conversations</source>
        <translation>Conversaciones</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="117"/>
        <source>Replies</source>
        <translation>Respuestas</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="135"/>
        <source>Direct Messages</source>
        <translation>Mensaje directo</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="151"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="169"/>
        <source>Public Timeline</source>
        <translation>Cronología pública</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="187"/>
        <source>Group news</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="205"/>
        <source>Search</source>
        <translation>Busca</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="223"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponentContacts</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="61"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="79"/>
        <source>Friends</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Contactos</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="97"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
</context>
<context>
    <name>EventCreate</name>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="78"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="143"/>
        <source>End</source>
        <translation>Finalizar</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="267"/>
        <source>no end</source>
        <translation>sin fin</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="292"/>
        <source>Title (required)</source>
        <translation>título (obligatorio)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="307"/>
        <source>Event description (optional)</source>
        <translation>Descripción del evento (opcional)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="318"/>
        <source>Location (optional)</source>
        <translation>Ubicación (opcional)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="327"/>
        <source>Publish event?</source>
        <translation>¿Publicar el fecha?</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="342"/>
        <source>Create event</source>
        <translation>crear fecha</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <location filename="../qml/calendarqml/EventCreate.qml" line="385"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <source>No event name supplied</source>
        <translation>No se ha nombre de la fecha</translation>
    </message>
</context>
<context>
    <name>EventList</name>
    <message>
        <source>Location</source>
        <translation type="vanished">Localización</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventList.qml" line="73"/>
        <source>Delete Event?</source>
        <translation type="unfinished">¿Borrar la cita?</translation>
    </message>
</context>
<context>
    <name>EventListItem</name>
    <message>
        <location filename="../qml/calendarqml/EventListItem.qml" line="79"/>
        <source>Location</source>
        <translation>Localización</translation>
    </message>
</context>
<context>
    <name>FriendsListTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="62"/>
        <source>Friend Requests</source>
        <translation>Solicitudes de contacto</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="72"/>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Friends</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>All</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FriendsTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="73"/>
        <source>Me</source>
        <translation>Yo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="78"/>
        <source>Friends</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Contactos</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="83"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
</context>
<context>
    <name>ImageUploadDialog</name>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="133"/>
        <source>Upload to album</source>
        <translation>Subir álbum</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">álbum</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">imagen</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="277"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Upload</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source> No album name given</source>
        <translation>¡Nombre del álbum no encontrado!</translation>
    </message>
</context>
<context>
    <name>LeftDrawerScrollview</name>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="55"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="67"/>
        <source>Accounts</source>
        <translation>Cuentas</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="79"/>
        <source>Quit</source>
        <translation>Salida</translation>
    </message>
</context>
<context>
    <name>MessageImageUploadDialog</name>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="258"/>
        <source>Description</source>
        <translation type="unfinished">Descripción</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="284"/>
        <source>Upload</source>
        <translation type="unfinished">Subir</translation>
    </message>
</context>
<context>
    <name>MessageSend</name>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="219"/>
        <source>to:</source>
        <translation>a:</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="231"/>
        <source>Title (optional)</source>
        <translation>Título (opcional)</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="249"/>
        <source> Drop your Content here.</source>
        <translation> Deje caer su contenido aquí.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="255"/>
        <source>What&apos;s on your mind?</source>
        <translation>¿Qué tienes en mente?</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Only one attachment supported at the moment.
 Remove other attachment first!</source>
        <translation type="vanished">Solo se admite adjuntar un solo archivo en este momento.
 ¡Elimine y deje un archivo adjunto!</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>No receiver supplied!</source>
        <translation>No se ha suministrado ningún receptor!</translation>
    </message>
</context>
<context>
    <name>MoreComments</name>
    <message>
        <location filename="../qml/newsqml/MoreComments.qml" line="53"/>
        <source>Show all comments</source>
        <translation>todos comentarios</translation>
    </message>
</context>
<context>
    <name>NewsStack</name>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Fallo de red</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsStack.qml" line="286"/>
        <source>More</source>
        <translation>Mas</translation>
    </message>
</context>
<context>
    <name>NewsTab</name>
    <message>
        <source>Download profile image for </source>
        <translation type="vanished">Descargar la imagen del perfil para </translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">Mas</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronología</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoritos</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversaciones</translation>
    </message>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Fallo de red</translation>
    </message>
    <message>
        <source>Public timeline</source>
        <translation type="vanished">Cronología pública</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation type="vanished">Mensaje directo</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="vanished">Notificaciones</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Grupos</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Salida</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="75"/>
        <source>seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="76"/>
        <source>minute</source>
        <translation>Minuto</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="77"/>
        <source>minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="78"/>
        <source>hour</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="79"/>
        <source>hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="80"/>
        <source>day</source>
        <translation>Dia</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="81"/>
        <source>days</source>
        <translation>Dias</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="82"/>
        <source>month</source>
        <translation>Mes</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="83"/>
        <source>months</source>
        <translation>Meses</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="84"/>
        <source>years</source>
        <translation>Años</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="93"/>
        <source>likes this.</source>
        <translation>le gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="94"/>
        <source>like this.</source>
        <translation>me gusta esto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="97"/>
        <source>doesn&apos;t like this.</source>
        <translation>no de ése.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="98"/>
        <source>don&apos;t like this.</source>
        <translation>no me gusta.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="101"/>
        <source>will attend.</source>
        <translation>asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="102"/>
        <source>persons will attend.</source>
        <translation>Personas que asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="105"/>
        <source>will not attend.</source>
        <translation>no asistirá.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="106"/>
        <source>persons will not attend.</source>
        <translation>Personas que no asistirán.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="109"/>
        <source>may attend.</source>
        <translation>Puede asistir.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="110"/>
        <source>persons may attend.</source>
        <translation>Personas que pueden asistir.</translation>
    </message>
</context>
<context>
    <name>Newsitem</name>
    <message>
        <source>attending: </source>
        <translation type="vanished">Asistiendo: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Source: </source>
        <translation>Fuente: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Direct Message</source>
        <translation>Mensaje directo</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="133"/>
        <source>In reply to </source>
        <translation>En respuesta a </translation>
    </message>
    <message>
        <source> comments</source>
        <translation type="vanished"> comentarios</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="49"/>
        <source>attending</source>
        <translation>asistencia</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="124"/>
        <source>ago</source>
        <translation>hace</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="269"/>
        <source>Attending: </source>
        <translation>Asistiendo: </translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="vanished">Respuesta</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="463"/>
        <source>DM</source>
        <translation>Mensaje directo</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="428"/>
        <source>Repost</source>
        <translation>Volver a publicar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="431"/>
        <source>Success!</source>
        <translation>éxito!</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="436"/>
        <source>Block contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="449"/>
        <source>Report contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="457"/>
        <source>Conversation</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="469"/>
        <source>Bookmark</source>
        <translation>marca</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="478"/>
        <source>Calendar Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="487"/>
        <source>Attending</source>
        <translation>Asistiendo</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="497"/>
        <source>yes</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="502"/>
        <source>maybe</source>
        <translation>quizás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="507"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="514"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="530"/>
        <source>External</source>
        <translation>sitio web</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="70"/>
        <source>Friends</source>
        <translation>Amigos</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="132"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
</context>
<context>
    <name>PhotoTab</name>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="132"/>
        <source>&apos;s images</source>
        <translation>s Imágenes</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="219"/>
        <source>All Images</source>
        <translation>Todas las imagenes</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="225"/>
        <source>Only new</source>
        <translation>Solo nueva</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="241"/>
        <location filename="../qml/photoqml/PhotoTab.qml" line="246"/>
        <source>Own Images</source>
        <translation>Mis imágenes</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="288"/>
        <source>More</source>
        <translation>Mas</translation>
    </message>
</context>
<context>
    <name>ProfileComponent</name>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="63"/>
        <source>profile name</source>
        <translation>Nombre de perfil</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="64"/>
        <source>is default</source>
        <translation>es por defecto</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="65"/>
        <source>hide friends</source>
        <translation>ocultar amigos</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="66"/>
        <source>profile photo</source>
        <translation>foto de perfil</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="67"/>
        <source>profile thumb</source>
        <translation>foto de perfil pequeña</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="68"/>
        <source>publish</source>
        <translation>publicar</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="69"/>
        <source>publish in network</source>
        <translation>publicar en la red</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="70"/>
        <source>description</source>
        <translation>descripción</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="71"/>
        <source>date of birth</source>
        <translation>fecha de nacimiento</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="72"/>
        <source>address</source>
        <translation>dirección</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="73"/>
        <source>city</source>
        <translation>ciudad</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="74"/>
        <source>region</source>
        <translation>región</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="75"/>
        <source>postal code</source>
        <translation>código postal</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="76"/>
        <source>country</source>
        <translation>país</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="77"/>
        <source>hometown</source>
        <translation>ciudad natal</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="78"/>
        <source>gender</source>
        <translation>género</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="79"/>
        <source>marital status</source>
        <translation>estado civil</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="80"/>
        <source>married with</source>
        <translation>casado con</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="81"/>
        <source>married since</source>
        <translation>casado desde</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="82"/>
        <source>sexual</source>
        <translation>orientación sexual</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="83"/>
        <source>politics</source>
        <translation>política</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="84"/>
        <source>religion</source>
        <translation>religión</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="85"/>
        <source>public keywords</source>
        <translation>palabras clave públicas</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="86"/>
        <source>private keywords</source>
        <translation>palabras clave privadas</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="87"/>
        <source>likes</source>
        <translation>le gusta</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="88"/>
        <source>dislikes</source>
        <translation>no le gusta</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="89"/>
        <source>about</source>
        <translation>sobre</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="90"/>
        <source>music</source>
        <translation>música</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="91"/>
        <source>book</source>
        <translation>libro</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="92"/>
        <source>tv</source>
        <translation>tv</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="93"/>
        <source>film</source>
        <translation>película</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="94"/>
        <source>interest</source>
        <translation>interés</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="95"/>
        <source>romance</source>
        <translation>romance</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="96"/>
        <source>work</source>
        <translation>trabajo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="97"/>
        <source>education</source>
        <translation>educación</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="98"/>
        <source>social networks</source>
        <translation>redes sociales</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="99"/>
        <source>homepage</source>
        <translation>página web</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="100"/>
        <source>other</source>
        <translation>otros</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="193"/>
        <source>Update</source>
        <translation>Actualización</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="227"/>
        <source>profile id</source>
        <translation>profile id</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Location</source>
        <translation>Localización</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Posts</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="254"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="255"/>
        <source>Created at</source>
        <translation>Creado en</translation>
    </message>
</context>
<context>
    <name>ReportUser</name>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="41"/>
        <source>Report contact?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="65"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>illegal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>spam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>violation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmileyDialog</name>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="64"/>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="68"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="72"/>
        <source>Addon</source>
        <translation>Addon</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="77"/>
        <source>Adult</source>
        <translation>XXX</translation>
    </message>
</context>
<context>
    <name>SyncComponent</name>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="57"/>
        <source>sync</source>
        <translation>sync</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="76"/>
        <source>notify</source>
        <translation>notificar</translation>
    </message>
</context>
<context>
    <name>SyncConfig</name>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="46"/>
        <source>Sync Interval (0=None)</source>
        <translation>Intervalo de sincr. (0=Ninguno)</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="86"/>
        <source>Min.</source>
        <translation>min.</translation>
    </message>
</context>
<context>
    <name>friendiqa</name>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Actualizar</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronología</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversaciones</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoritos</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Respuestas</translation>
    </message>
    <message>
        <source>Public Timeline</source>
        <translation type="vanished">Cronología pública</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Grupos</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Busca</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Ajustes</translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation type="vanished">Cuentas</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Salida</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="177"/>
        <source>Background Sync
 Rightclick or Middleclick to Quit</source>
        <translation>Sincronización de fondo
Haga clic con el botón derecho del ratón o con el botón central para salir.</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="294"/>
        <source>Click to open Friendiqa</source>
        <translation>Haga clic para abrir Friendiqa</translation>
    </message>
</context>
<context>
    <name>newsworker</name>
    <message>
        <source>likes this.</source>
        <translation type="vanished">le gusta esto.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation type="vanished">me gusta esto.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation type="vanished">no de ése.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation type="vanished">no me gusta.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation type="vanished">asistirá.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation type="vanished">Personas que asistirán.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation type="vanished">no asistirá.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation type="vanished">Personas que no asistirán..</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation type="vanished">Puede asistir.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation type="vanished">Personas que pueden asistir.</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="53"/>
        <source>yes</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="54"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="55"/>
        <source>maybe</source>
        <translation>quizás</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">Segundos</translation>
    </message>
    <message>
        <source>ago</source>
        <translation type="vanished">hace</translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="vanished">Minuto</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="vanished">Minutos</translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="vanished">Hora</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="vanished">Horas</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">Dia</translation>
    </message>
    <message>
        <source>days</source>
        <translation type="vanished">Dias</translation>
    </message>
    <message>
        <source>month</source>
        <translation type="vanished">Mes</translation>
    </message>
    <message>
        <source>months</source>
        <translation type="vanished">Meses</translation>
    </message>
    <message>
        <source>years</source>
        <translation type="vanished">Años</translation>
    </message>
</context>
<context>
    <name>service</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Setting view type of news has moved from  account page to config page.</source>
        <translation type="vanished">La configuración del tipo de vista de las noticias se ha movido de la página de la cuenta a la página de configuración.</translation>
    </message>
    <message>
        <location filename="../js/service.js" line="460"/>
        <source>Undefined Array Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../js/service.js" line="463"/>
        <source>JSON status Error</source>
        <translation></translation>
    </message>
</context>
</TS>
