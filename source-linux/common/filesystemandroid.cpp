//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "filesystem.h"
#include <QtAndroidExtras>


FILESYSTEM *FILESYSTEM::instance()
{
    static FILESYSTEM filesystem;
    return &filesystem;
}

FILESYSTEM::FILESYSTEM(QObject *parent) : QObject(parent){}

void FILESYSTEM::setDirectory(QString Directory)
{
   if (Directory!=m_Directory) {
       m_Directory = Directory;
       emit directoryChanged();
   }
}

QString FILESYSTEM::Directory() const
{
    return m_Directory;
}
void FILESYSTEM::setVisibility(bool Visibility)
{
   if (Visibility!=m_Visibility) {
       m_Visibility = Visibility;
       emit visibilityChanged();
   }
}

bool FILESYSTEM::Visibility()
{
    return m_Visibility;
}

QString FILESYSTEM::homePath() const
{
    QAndroidJniObject activity =QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative","activity", "()Landroid/app/Activity;");
    QAndroidJniObject dir = activity.callObjectMethod("getExternalFilesDir", "(Ljava/lang/String;)Ljava/io/File;", NULL);
    QString homeDir=dir.toString();
    return homeDir;
}

bool FILESYSTEM::fileexist(QString name)
{ return QFile::exists(name);
}

void FILESYSTEM::makeDir(QString name)
{
    QDir dir(m_Directory);
    if (dir.mkdir(name)){
        emit success(name);
    }
    else {
        emit error(name,1);
    }
}

void FILESYSTEM::makePath(QString name)
{
    QDir dir(m_Directory);
    if (dir.mkpath(name)){
        emit success(name);
    }
    else {
        emit error(name,1);}
}

void FILESYSTEM::rmDir()
{
    QDir dir(m_Directory);
    if (dir.removeRecursively()){
        emit success(m_Directory);
    }
    else {emit error(m_Directory,1);}
}

void FILESYSTEM::rmFile(QString name)
{
    QDir dir(m_Directory);
    if(dir.remove(name)){
    emit success(name);
    }
    else {emit error(name,1);}
}
QFileInfoList FILESYSTEM::fileList()
{
    QDir dir(m_Directory);
    QStringList filters;
    filters << "*.png" <<"*.PNG" << "*.jpg" << "*.JPG" << "*.JPEG";
    dir.setNameFilters(filters);
    dir.setSorting(QDir::Time | QDir::Reversed);
    return dir.entryInfoList();
}

bool FILESYSTEM::isAutostart() {
    QFileInfo check_file(QDir::homePath() + "/.config/autostart/friendiqa.desktop");

    if (check_file.exists() && check_file.isFile()) {
        qDebug()<<"autostart "<<true;
        return true;
    }
    qDebug()<<"autostart "<<false;
    return false;
}

void FILESYSTEM::setAutostart(bool autostart) {
    QString path = QDir::homePath() + "/.config/autostart/";
    QString name ="friendiqa.desktop";
    QFile file(path+name);

    file.remove();

    if(autostart) {
        QDir dir(path);
        if(!dir.exists()) {
            dir.mkpath(path);
        }

        if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            stream << "[Desktop Entry]" << Qt::endl;
            stream << "Exec=friendiqa -background %u"  << Qt::endl;
            stream << "Type=Application" << Qt::endl;
        }
    }
}
