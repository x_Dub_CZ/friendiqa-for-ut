//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import "qrc:/qml/genericqml"

Item {
    id: drawerAccountComponent
    property var account: ({})
    width: parent.width

    Label{

        y:0.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        height: 1.5*osSettings.bigFontSize*root.fontFactor
        verticalAlignment:Text.AlignBottom
        font.pointSize: osSettings.bigFontSize
        text: account.username
    }
    //            Label{
    //                text:login.hasOwnProperty("server")?"@"+login.server:""
    //                font.pixelSize: 5*mm
    //                width: parent.width
    //            }

    Label{
        y:2*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        text: "\uf021 " + qsTr("Refresh")
        MouseArea{
            anchors.fill:parent
            onClicked: {
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
//                newstypeSignal("refresh")
                updatenews.setDatabase();
                updatenews.login();
                updatenews.startsync();
            }
        }
    }

    Label{
        y:3.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Timeline"
        text: "\uf1da " + qsTr("Timeline")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Timeline";
                newstypeSignal("timeline")
            }
        }
    }

    Label{
        y:5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Conversations"
        text: "\uf086 " + qsTr("Conversations")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Conversations";
                newstypeSignal("conversation")
            }
        }
    }



    Label{
        y:6.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Replies"
        text: "\uf0ec " + qsTr("Replies")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=1
                currentnewstabstatus="Replies";
                newstypeSignal("replies")
            }
        }
    }

    Label{
        y:8*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="DirectMessages"
        text: "\uf0e0 " + qsTr("Direct Messages")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=2//newstypeSignal("replies")
                currentnewstabstatus="DirectMessages";
            }
        }
    }
    Label{
        y:9.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Favorites"
        text: "\uf005 " + qsTr("Favorites")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Favorites";
                newstypeSignal("favorites")
            }
        }
    }

    Label{
        y:11*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Public Timeline"
        text: "\uf0ac " + qsTr("Public Timeline")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Public Timeline";
                newstypeSignal("publictimeline")
            }
        }
    }

    Label{
        y:12.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Groupnews"
        text: "\uf0c0 " + qsTr("Group news")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Groupnews";
                newstypeSignal("groupnews")
            }
        }
    }

    Label{
        y:14*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Search"
        text: "\uf002 " + qsTr("Search")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=0;
                currentnewstabstatus="Search";
                newstypeSignal("search")
            }
        }
    }

    Label{
        y:15.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width
        font.pointSize: osSettings.systemFontSize
        font.bold: account.username==login.username && currentnewstabstatus=="Notifications"
        text: "\uf0f3 " + qsTr("Notifications")
        MouseArea{
            anchors.fill:parent
            onClicked:{
                login=account;
                if(!wideScreen){leftDrawerAndroid.close()}
                newsSwipeview.currentIndex=3;
                currentnewstabstatus="Notifications";
                newstypeSignal("notifications")
            }
        }
    }
}
