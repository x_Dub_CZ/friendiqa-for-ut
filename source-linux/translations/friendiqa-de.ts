<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AcceptRules</name>
    <message>
        <source>Accept instance rules</source>
        <translation>Bitte Instanzregeln akzeptieren</translation>
    </message>
</context>
<context>
    <name>AccountPage</name>
    <message>
        <source>User</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Kurzname</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Image dir.</source>
        <translation>Bildverz.</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Anzeige</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation>Kurznamen mit @ Zeichen werden derzeit nicht unterstützt.</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <source>No server given! </source>
        <translation>Kein Server angegeben!</translation>
    </message>
    <message>
        <source>No nickname given! </source>
        <translation>Kein Kurzname angegeben!</translation>
    </message>
    <message>
        <source>No password given! </source>
        <translation>Kein Passwort angegeben!</translation>
    </message>
    <message>
        <source>No image directory given!</source>
        <translation>Kein Verzeichnis für Bilder angegeben!</translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">Falsches Passwort!</translation>
    </message>
    <message>
        <source>Success</source>
        <translation>Bestätigt</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Chronologisch</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Unterhaltungen</translation>
    </message>
    <message>
        <source>Instance rules</source>
        <translation>Server-Verhaltensregeln</translation>
    </message>
    <message>
        <source>Wrong password or 2FA enabled!</source>
        <translation>Falsches Passwort bzw. 2FA eingeschaltet!</translation>
    </message>
</context>
<context>
    <name>BlockUser</name>
    <message>
        <source>Block contact?</source>
        <translation>Kontakt blockieren?</translation>
    </message>
</context>
<context>
    <name>CalendarTab</name>
    <message>
        <source>Events</source>
        <translation>Termine</translation>
    </message>
    <message>
        <source>Own Calendar</source>
        <translation>Eigener Kalender</translation>
    </message>
    <message>
        <source>Delete Event?</source>
        <translation>Termin löschen?</translation>
    </message>
</context>
<context>
    <name>ConfigAppearancePage</name>
    <message>
        <source>News as</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation>Unterhaltungen</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation>Chronologisch</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation>Max. Nachr.</translation>
    </message>
    <message>
        <source>Hide #nsfw?</source>
        <translation>#nsfw minimieren?</translation>
    </message>
    <message>
        <source>Dark Mode</source>
        <translation>Design</translation>
    </message>
    <message>
        <source>System</source>
        <translation>Standard-Design</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Dunkles Design</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Helles Design</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <source>News as</source>
        <translation type="vanished">Anzeige</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Unterhaltungen</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Chronologisch</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Max. Nachr.</translation>
    </message>
    <message>
        <source>Hide #nsfw?</source>
        <translation type="vanished">#nsfw minimieren?</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation>Autom. Aktualisierung</translation>
    </message>
    <message>
        <source>Appearance</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Start</translation>
    </message>
</context>
<context>
    <name>ConfigStartPage</name>
    <message>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
</context>
<context>
    <name>ConfigTab</name>
    <message>
        <source>User</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="vanished">Kurzname</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Passwort</translation>
    </message>
    <message>
        <source>Image dir.</source>
        <translation type="vanished">Bildverz.</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Max. Nachr.</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Anzeige</translation>
    </message>
    <message>
        <source>Interval (0=None)</source>
        <translation type="vanished">Intervall (0=keins)</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>Nickname not registered at given server!</source>
        <translation type="vanished">Name auf der Seite nicht registriert!</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Bestätigen</translation>
    </message>
    <message>
        <source>No server given! </source>
        <translation type="vanished">Kein Server angegeben!</translation>
    </message>
    <message>
        <source>No nickname given! </source>
        <translation type="vanished">Kein Kurzname angegeben!</translation>
    </message>
    <message>
        <source>Nickname not registered at given server! </source>
        <translation type="vanished">Name auf der Seite nicht registriert!</translation>
    </message>
    <message>
        <source>No username given! </source>
        <translation type="vanished">Kein Nutzername angegeben!</translation>
    </message>
    <message>
        <source>Sync Interval (0=None)</source>
        <translation type="vanished">Akt.-intervall (0=keine)</translation>
    </message>
    <message>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation type="vanished">Kurznamen mit @ Zeichen werden derzeit nicht unterstützt.</translation>
    </message>
    <message>
        <source>Min.</source>
        <translation type="vanished">Min.</translation>
    </message>
    <message>
        <source>No password given! </source>
        <translation type="vanished">Kein Passwort angegeben!</translation>
    </message>
    <message>
        <source>No image directory given!</source>
        <translation type="vanished">Kein Verzeichnis für Bilder angegeben!</translation>
    </message>
    <message>
        <source>No maximum news number given!</source>
        <translation type="vanished">Maximale News-Anzahl nicht angegeben!</translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">Falsches Passwort!</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">Bestätigt</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Chronologisch</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Unterhaltungen</translation>
    </message>
</context>
<context>
    <name>ContactComponent</name>
    <message>
        <source>Connect</source>
        <translation>Kontaktanfrage</translation>
    </message>
</context>
<context>
    <name>ContactDetailsComponent</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">Kontaktanfrage</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Beschreibung</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Ort</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation type="vanished">Beiträge</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="vanished">Profilseite</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation type="vanished">Erstellt</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>Minute</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>Stunde</translation>
    </message>
    <message>
        <source>hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <source>day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <source>month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <source>months</source>
        <translation>Monate</translation>
    </message>
    <message>
        <source>years</source>
        <translation>Jahre</translation>
    </message>
    <message>
        <source>likes this.</source>
        <translation>mag das.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation>mögen das.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation>mag das nicht.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation>mögen das nicht.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation>nehmen teil.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation>Personen nehmen teil.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation>nimmt nicht teil.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation>Personen nehmen nicht teil.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation>nimmt vielleicht teil.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation>Personen nehmen vielleicht teil.</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Kontaktanfrage</translation>
    </message>
    <message>
        <source>Approve</source>
        <translation>Erlauben</translation>
    </message>
    <message>
        <source>Reject</source>
        <translation>Ablehnen</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Folgen</translation>
    </message>
    <message>
        <source>Unfollow</source>
        <translation>Entfolgen</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Profilseite</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation>Erstellt</translation>
    </message>
    <message>
        <source>Network Error</source>
        <translation>Netzwerk-Fehler</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Folgende</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Folgt</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Blockieren</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Entblocken</translation>
    </message>
</context>
<context>
    <name>ContactsSearchPage</name>
    <message>
        <source>Network Error</source>
        <translation>Netzwerk-Fehler</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Person</source>
        <translation>Person</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>Minute</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>Stunde</translation>
    </message>
    <message>
        <source>hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <source>day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <source>month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <source>months</source>
        <translation>Monate</translation>
    </message>
    <message>
        <source>years</source>
        <translation>Jahre</translation>
    </message>
    <message>
        <source>likes this.</source>
        <translation>mag das.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation>mögen das.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation>mag das nicht.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation>mögen das nicht.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation>nehmen teil.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation>Personen nehmen teil.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation>nimmt nicht teil.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation>Personen nehmen nicht teil.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation>nimmt vielleicht teil.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation>Personen nehmen vielleicht teil.</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponent</name>
    <message>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation>Chronologisch</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation>Unterhaltungen</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation>Interaktionen</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation>Direktnachrichten</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Markierte News</translation>
    </message>
    <message>
        <source>Public Timeline</source>
        <translation>öff. Timeline</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation>News Gruppe</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Meldungen</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponentContacts</name>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Friends</source>
        <translation>Freunde</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Kontakte</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
</context>
<context>
    <name>EventCreate</name>
    <message>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <source>no end</source>
        <translation>Ende nicht bekannt</translation>
    </message>
    <message>
        <source>Title (required)</source>
        <translation>Titel (notwendig)</translation>
    </message>
    <message>
        <source>Event description (optional)</source>
        <translation>Terminbeschreibung (optional)</translation>
    </message>
    <message>
        <source>Location (optional)</source>
        <translation>Ort (optional)</translation>
    </message>
    <message>
        <source>Publish event?</source>
        <translation>Termin teilen?</translation>
    </message>
    <message>
        <source>Create event</source>
        <translation>Termin erstellen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>No event name supplied</source>
        <translation>Kein Termintitel angegeben</translation>
    </message>
</context>
<context>
    <name>EventList</name>
    <message>
        <source>Location</source>
        <translation type="vanished">Ort</translation>
    </message>
    <message>
        <source>Delete Event?</source>
        <translation>Termin löschen?</translation>
    </message>
</context>
<context>
    <name>EventListItem</name>
    <message>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
</context>
<context>
    <name>FriendsListTab</name>
    <message>
        <source>Friend Requests</source>
        <translation>Kontaktanfragen</translation>
    </message>
    <message>
        <source>Friends</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation>Geblockte</translation>
    </message>
</context>
<context>
    <name>FriendsTab</name>
    <message>
        <source>Me</source>
        <translation>Ich</translation>
    </message>
    <message>
        <source>Friends</source>
        <translation>Freunde</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Kontakte</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
</context>
<context>
    <name>GroupComponent</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>No name given</source>
        <translation type="vanished">Kein Name angegeben</translation>
    </message>
</context>
<context>
    <name>ImageUploadDialog</name>
    <message>
        <source>Upload to album</source>
        <translation>In Album hochladen</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">Album</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Bild</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Hochladen</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source> No album name given</source>
        <translation>Kein Albumname angegeben</translation>
    </message>
</context>
<context>
    <name>LeftDrawerScrollview</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation>Konten</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Schliessen</translation>
    </message>
</context>
<context>
    <name>MessageImageUploadDialog</name>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Hochladen</translation>
    </message>
</context>
<context>
    <name>MessageSend</name>
    <message>
        <source>to:</source>
        <translation>an:</translation>
    </message>
    <message>
        <source>Title (optional)</source>
        <translation>Überschrift (optional)</translation>
    </message>
    <message>
        <source> Drop your Content here.</source>
        <translation> Legen Sie Ihren Inhalt per Drag &amp; Drop hier ab.</translation>
    </message>
    <message>
        <source>What&apos;s on your mind?</source>
        <translation>Woran denkst du gerade?</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Only one attachment supported at the moment.
 Remove other attachment first!</source>
        <translation type="vanished">Nur ein Anhang derzeit unterstützt.
 Lösche zuerst den anderen Anhang!</translation>
    </message>
    <message>
        <source>No receiver supplied!</source>
        <translation>Kein Empfänger angegeben!</translation>
    </message>
</context>
<context>
    <name>MoreComments</name>
    <message>
        <source>Show all comments</source>
        <translation>Alle Kommentare</translation>
    </message>
</context>
<context>
    <name>NewsStack</name>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Netzwerk-Fehler</translation>
    </message>
    <message>
        <source>More</source>
        <translation>Mehr</translation>
    </message>
</context>
<context>
    <name>NewsTab</name>
    <message>
        <source>Download profile image for </source>
        <translation type="vanished">Lade Profilbild für </translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">Mehr</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Chronologisch</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Markierte News</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Unterhaltungen</translation>
    </message>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Netzwerk-Fehler</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Interaktionen</translation>
    </message>
    <message>
        <source>Public timeline</source>
        <translation type="vanished">Gemeinschaft</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation type="vanished">Direktnachrichten</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="vanished">Meldungen</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">News Gruppe</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Schliessen</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>Minute</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>Stunde</translation>
    </message>
    <message>
        <source>hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <source>day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <source>month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <source>months</source>
        <translation>Monate</translation>
    </message>
    <message>
        <source>years</source>
        <translation>Jahre</translation>
    </message>
    <message>
        <source>likes this.</source>
        <translation>mag das.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation>mögen das.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation>mag das nicht.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation>mögen das nicht.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation>nehmen teil.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation>Personen nehmen teil.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation>nimmt nicht teil.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation>Personen nehmen nicht teil.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation>nimmt vielleicht teil.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation>Personen nehmen vielleicht teil.</translation>
    </message>
</context>
<context>
    <name>Newsitem</name>
    <message>
        <source>attending: </source>
        <translation type="vanished">Teilnahme</translation>
    </message>
    <message>
        <source>Source: </source>
        <translation>Quelle: </translation>
    </message>
    <message>
        <source>Direct Message</source>
        <translation>Direktnachricht</translation>
    </message>
    <message>
        <source>In reply to </source>
        <translation>Antwort an </translation>
    </message>
    <message>
        <source> comments</source>
        <translation type="vanished"> Kommentare</translation>
    </message>
    <message>
        <source>attending</source>
        <translation>partecipare</translation>
    </message>
    <message>
        <source>ago</source>
        <translation>her</translation>
    </message>
    <message>
        <source>Attending: </source>
        <translation>Teilnahme: </translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="vanished">Antworten</translation>
    </message>
    <message>
        <source>DM</source>
        <translation>Direktnachricht</translation>
    </message>
    <message>
        <source>Repost</source>
        <translation>Teilen</translation>
    </message>
    <message>
        <source>Success!</source>
        <translation>Erledigt!</translation>
    </message>
    <message>
        <source>Conversation</source>
        <translation>Unterhaltung</translation>
    </message>
    <message>
        <source>Attending</source>
        <translation>Teilnahme</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <source>maybe</source>
        <translation>vielleicht</translation>
    </message>
    <message>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation>Markieren</translation>
    </message>
    <message>
        <source>External</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <source>Block contact</source>
        <translation>Kontakt blockieren</translation>
    </message>
    <message>
        <source>Report contact</source>
        <translation>Kontakt melden</translation>
    </message>
    <message>
        <source>Calendar Entry</source>
        <translation>Kalendereintrag</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <source>Friends</source>
        <translation>Freunde</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
</context>
<context>
    <name>PhotoTab</name>
    <message>
        <source>&apos;s images</source>
        <translation>s Bilder</translation>
    </message>
    <message>
        <source>All Images</source>
        <translation>Alle Bilder</translation>
    </message>
    <message>
        <source>Only new</source>
        <translation>Nur neue</translation>
    </message>
    <message>
        <source>Own Images</source>
        <translation>Eigene Bilder</translation>
    </message>
    <message>
        <source>More</source>
        <translation>Mehr</translation>
    </message>
</context>
<context>
    <name>ProfileComponent</name>
    <message>
        <source>profile name</source>
        <translation>Profilname</translation>
    </message>
    <message>
        <source>is default</source>
        <translation>Hauptprofil</translation>
    </message>
    <message>
        <source>hide friends</source>
        <translation>Verberge Freunde</translation>
    </message>
    <message>
        <source>profile photo</source>
        <translation>Profilbild</translation>
    </message>
    <message>
        <source>profile thumb</source>
        <translation>Mini-Profilbild</translation>
    </message>
    <message>
        <source>publish</source>
        <translation>öffentlich</translation>
    </message>
    <message>
        <source>publish in network</source>
        <translation>Öffentlich im Netzwerk</translation>
    </message>
    <message>
        <source>description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>date of birth</source>
        <translation>Geburtstag</translation>
    </message>
    <message>
        <source>address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>city</source>
        <translation>Stadt</translation>
    </message>
    <message>
        <source>region</source>
        <translation>Region</translation>
    </message>
    <message>
        <source>postal code</source>
        <translation>Postleitzahl</translation>
    </message>
    <message>
        <source>country</source>
        <translation>Land</translation>
    </message>
    <message>
        <source>hometown</source>
        <translation>Heimatstadt</translation>
    </message>
    <message>
        <source>gender</source>
        <translation>Geschlecht</translation>
    </message>
    <message>
        <source>marital status</source>
        <translation>Beziehungsstatus</translation>
    </message>
    <message>
        <source>married with</source>
        <translation>verheiratet mit</translation>
    </message>
    <message>
        <source>married since</source>
        <translation>verheiratet seit</translation>
    </message>
    <message>
        <source>sexual</source>
        <translation>Sex</translation>
    </message>
    <message>
        <source>politics</source>
        <translation>Politik</translation>
    </message>
    <message>
        <source>religion</source>
        <translation>Religion</translation>
    </message>
    <message>
        <source>public keywords</source>
        <translation>öffentliche Schlagwörter</translation>
    </message>
    <message>
        <source>private keywords</source>
        <translation>private Schlagwörter</translation>
    </message>
    <message>
        <source>likes</source>
        <translation>Vorlieben</translation>
    </message>
    <message>
        <source>dislikes</source>
        <translation>Abneigungen</translation>
    </message>
    <message>
        <source>about</source>
        <translation>über</translation>
    </message>
    <message>
        <source>music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <source>book</source>
        <translation>Bücher</translation>
    </message>
    <message>
        <source>tv</source>
        <translation>TV</translation>
    </message>
    <message>
        <source>film</source>
        <translation>Filme</translation>
    </message>
    <message>
        <source>interest</source>
        <translation>Interessen</translation>
    </message>
    <message>
        <source>romance</source>
        <translation>Liebschaften</translation>
    </message>
    <message>
        <source>work</source>
        <translation>Arbeit</translation>
    </message>
    <message>
        <source>education</source>
        <translation>Bildung</translation>
    </message>
    <message>
        <source>social networks</source>
        <translation>Soziale Netzwerke</translation>
    </message>
    <message>
        <source>homepage</source>
        <translation>Homepage</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <source>profile id</source>
        <translation>Profil-Nummer</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>Profilseite</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation>Erstellt</translation>
    </message>
    <message>
        <source>other</source>
        <translation>Sonstige</translation>
    </message>
</context>
<context>
    <name>ReportUser</name>
    <message>
        <source>Report contact?</source>
        <translation>Kontakt melden?</translation>
    </message>
    <message>
        <source>comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <source>illegal</source>
        <translation>illegal</translation>
    </message>
    <message>
        <source>spam</source>
        <translation>Spam</translation>
    </message>
    <message>
        <source>violation</source>
        <translation>Regelverletzung</translation>
    </message>
</context>
<context>
    <name>SmileyDialog</name>
    <message>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Addon</source>
        <translation>Addon</translation>
    </message>
    <message>
        <source>Adult</source>
        <translation>XXX</translation>
    </message>
</context>
<context>
    <name>SyncComponent</name>
    <message>
        <source>sync</source>
        <translation>akt.</translation>
    </message>
    <message>
        <source>notify</source>
        <translation>benachr.</translation>
    </message>
</context>
<context>
    <name>SyncConfig</name>
    <message>
        <source>Sync Interval (0=None)</source>
        <translation>Akt.-intervall (0=keine)</translation>
    </message>
    <message>
        <source>Min.</source>
        <translation>Min.</translation>
    </message>
</context>
<context>
    <name>friendiqa</name>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Aktualisieren</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Chronologisch</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Unterhaltungen</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Markierte News</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Interaktionen</translation>
    </message>
    <message>
        <source>Public Timeline</source>
        <translation type="vanished">öff. Timeline</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">News Gruppe</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Suche</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation type="vanished">Konten</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Schliessen</translation>
    </message>
    <message>
        <source>Background Sync
 Rightclick or Middleclick to Quit</source>
        <translation>Hintergrund-Aktualisierung
Rechtsklick oder Mittelklick zum Schliessen</translation>
    </message>
    <message>
        <source>Click to open Friendiqa</source>
        <translation>Klicken, um Friendiqa zu öffnen</translation>
    </message>
</context>
<context>
    <name>newsworker</name>
    <message>
        <source>likes this.</source>
        <translation type="vanished">mag das.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation type="vanished">mögen das.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation type="vanished">mag das nicht.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation type="vanished">mögen das nicht.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation type="vanished">nehmen teil.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation type="vanished">Personen nehmen teil.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation type="vanished">nimmt nicht teil.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation type="vanished">Personen nehmen nicht teil.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation type="vanished">nimmt vielleicht teil.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation type="vanished">Personen nehmen vielleicht teil.</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <source>maybe</source>
        <translation>vielleicht</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">Sekunden</translation>
    </message>
    <message>
        <source>ago</source>
        <translation type="vanished">her</translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="vanished">Minute</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="vanished">Minuten</translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="vanished">Stunde</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="vanished">Stunden</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">Tag</translation>
    </message>
    <message>
        <source>days</source>
        <translation type="vanished">Tage</translation>
    </message>
    <message>
        <source>month</source>
        <translation type="vanished">Monat</translation>
    </message>
    <message>
        <source>months</source>
        <translation type="vanished">Monate</translation>
    </message>
</context>
<context>
    <name>service</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <source>Changelog</source>
        <translation type="vanished">Änderungen</translation>
    </message>
    <message>
        <source>Setting view type of news has moved from  account page to config page.</source>
        <translation type="vanished">Die Einstellung der Ansichtsart von Nachrichten wurde von der Kontoseite auf die Konfigurationsseite verschoben.</translation>
    </message>
    <message>
        <source>Undefined Array Error</source>
        <translation>Antwort-Array ungültig</translation>
    </message>
    <message>
        <source>JSON status Error</source>
        <translation>Server-Antwort: Fehler</translation>
    </message>
</context>
</TS>
