## v0.6.7

* Upload multiple images for post and add image descriptions
* Block user
* Report user
* Create Calendar entry from post text

## v0.6.6

* Create event
* Delete event
* New sorting of conversations

## v0.6.5

* Fix reply in Friendica 3/2022 version due to API bug
* Image downloads work again
* Some additional emojis

## v0.6.4

* Search for new contacts (hashtags, names)
* Filter contacts
* many new emojis
* Replaced Favorite button with Reply button due to buggy Favourite API
* Rebuild contacts handling due to buggy Contacts API
* Removed permissions for new messages due to buggy Contacts API
* Sync all accounts in background (not only active)


## v0.6.3

* Dark theme
* Account Page: list of largest servers for server field
* Settings page: Tabs for different settings
* Conversation opens on short click on news item
* Linux: Option to Autostart on System start

## v0.6.2

* Follow and Unfollow contacts (Friendica 2021.07 required)
* Linux: App stays in systemtray after close, syncs in background
* bugfix for bulk image upload and download of public images
* bugfix for conversations in timeline

## v0.6.1

* Add requestLegacyExternalStorage in AndroidManifest for attachment permissions
* Refresh timeline on start for Linux
* DropArea bugfix for Flatpak

## v0.6

* New language: Hungarian thank to urbalazs (https://www.urbalazs.hu)
* Multiple photos in post open in separate slideshow
* Layout redesign
* Adaptive sidebar
* Sync Friendship Requests, approve/deny/ignore requests
* Events: show upcoming events below calendar
* Linux: Drag&Drop for new messages and photo upload
* Linux: Remember app size

## v0.5.3

* Implementation of new events API (incl. sync and notification) for Friendica version >= 20.03
* Indentation to see replied to newsitem for conversation view
* News view type config moved from account page to config page
* Simplification of account page on first start
* Bugfixes

## v0.5.2

* Redesign of news item
* Background sync for Android >8.0
* Bugfixes

## v0.5.1

* Videos open in full screen
* Youtube videos open in app
* Option to minimize #nsfw post
* Rotate image in messageSend component

## v0.5

* Redesign due to QML Components 1 being deprecated in Qt 5.12: Slideview for News, left Drawer for Settings, message creation in listview header
* Android Notifications for News, DMs, Replies
* Global app config separated from account config

## v0.4

* Background sync for friends timeline (interval on config page must be > 0) for Android > 5
* Replies timeline
* Bugfix: App asks for storage permission on first start

## v0.3.4

* Direct message creation from profile page works again
* Profile image upload works again
* Viewing private album pictures of contacts works again
* On first start servername from https://dir.friendica.social/servers/surprise selected
* Register button opens webview of registration page on server

## v0.3.3

* Update for OpenSSL and At
* Experimental support for Peertube (links are expanded to video widget)
* Some Unicode emojis
* Redesign of contact details (click on contact opens in new stack and shows last news)

## v0.3.2

* For news containing url ending with mp3, mp4, avi, webm, ogg: media can be played in app
* Pictures can be renamed or moved to another album
* Bugfix: random crashes for conversations
* Bugfix: attach image to message works again
* Bugfix: check for nickname on Server has been removed due to API change

## v0.3.1

* By popular demand: Conversations open in a new stack, like in Twidere
* Conversations open after (long) press on news, like in Twidere
* Image attachments are shown below text and can be enlarged, like in Twidere (solves issue #8)
* New messages are html, line breaks work (solves issue #7)

## v0.3

* Fix for [issue 6](https://github.com/LubuWest/Friendiqa/issues/6)
* Refactoring of news part
* Search button for news
* Click on hashtag in newsitem starts search for news with that word
* Public timeline
* Timeline for selected group
* Small redesign of SendMessage page

## v0.2.2

* Fix for [issue 5](https://github.com/LubuWest/Friendiqa/issues/5)
* Link to list of public server on Config Tab
* Small redesign of SendMessage page
* Intents for texts/urls (Send text or url from everywhere to create message)

## v0.2.1

* Fix for [issue 4](https://github.com/LubuWest/Friendiqa/issues/4)
* Fix for Friendica [issue 4689](https://github.com/friendica/friendica/issues/4689)
* Long posts are automatically truncated
* Intents for pictures (Send one image from gallery: attach to message, send multiple images: upload to album)

## v0.2

# News

* Profile image download completely reworked, resulting in speed improvement

# Contacts

* New profile tab, data of public and private profiles shown
* Change profile picture
* For friends image button shows private images thanks to new remoteAuth API (Friendica 3.6 server required) --> private holiday pictures have finally arrived!
* For friends calendar button shows private events thanks to new remoteAuth API (Friendica 3.6 server required)
* News button for forum accounts shows news published via that forum

# Images

* Complete rework of image download, fixes bug with private images
* Download all or only new images

# Config

* Check if nickname exist on server
* Check if password is correct
* Account deletion now also removes news, image data and events from local db

# Translations

* Italian thanks to Davide de Prisco

## v0.1.2##

* FIX: Include openssl v1.0.2m for SSL connections in Android v7 and above

## v0.1.1##

* FIX: Spanish translation
* FIX: Empty Newsview after deletion of first newsitem

## v0.1##

# News

* Native Android image selector for new message
* Click on contacts shows contact details on news page
* Fix problem with news list after deletion of item

# Contacts

* Clean contacts with no news

# Images

* Upload pictures with description to album (permissions cannot be set due to API problems)
* Delete pictures or albums from client and server (long press on picture in overview)
* Fix problem when enlarging photo

# Translations

* German, Spanish
