# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TEMPLATE = app
TARGET = friendiqa
CONFIG += release
QT += core core-private qml quick gui widgets sql androidextras #webengine  webview

include(androidnative.pri/androidnative.pri)

SOURCES += common/friendiqa.cpp \
        common/uploadableimage.cpp \
	common/xhr.cpp \
        common/filesystemandroid.cpp \
        common/remoteauthasyncimageprovider.cpp \
        common/updatenews.cpp \
        common/alarmandroid.cpp
        lupdate_only{
            SOURCES = qml/friendiqa.qml \
                qml/*.qml
                qml/calendarqml/*.qml
                qml/configqml/*.qml
                qml/genericqml/*.qml
                qml/contactqml/*.qml
                qml/newsqml/*.qml
                qml/photoqml/*.qml
                js/*.js
        }

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android


RESOURCES     = application.qrc

OTHER_FILES += qml/friendiqa.qml \
    translations/*.ts \
    qml/*.qml
    qml/newsqml/*.qml
    qml/contactqml/*.qml
    qml/photoqml/*.qml
    qml/configqml/*.qml
    js/*.js

TRANSLATIONS += translations/friendiqa-de.ts \
                translations/friendiqa-es.ts \
                translations/friendiqa-it.ts \
                translations/friendiqa-hu.ts

HEADERS += \
    common/uploadableimage.h \
    common/xhr.h \
    common/filesystem.h \
    common/remoteauthasyncimageprovider.h \
    common/updatenews.h \
    common/alarm.h

DISTFILES += \
    android/gradle.properties \
    qml/calendarqml/*.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    qml/configqml/ConfigAppearancePage.qml \
    translations/*.ts \
    translations/*.qm \
    qml/*.qml \
    qml/newsqml/*.qml \
    qml/contactqml/*.qml \
    qml/photoqml/*.qml \
    qml/configqml/*.qml \
    js/*.js \
    android/androidnative.gradle \
    android/src/FriendiqaActivity.java \
    android/src/FriendiqaService.java

ANDROID_EXTRA_LIBS = /home/pankraz/android-sdk/android_openssl/latest/arm/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm64/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm64/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86_64/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86_64/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm64/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/arm64/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86/libssl_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86_64/libcrypto_1_1.so /home/pankraz/android-sdk/android_openssl/latest/x86_64/libssl_1_1.so

#ANDROID_ABIS = arm64-v8a
#android: include(/home/pankraz/git/android_openssl/openssl.pri)
android: include(/home/pankraz/android-sdk/android_openssl/openssl.pri)
