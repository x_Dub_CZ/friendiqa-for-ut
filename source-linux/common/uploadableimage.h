//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UPLOADABLEIMAGE_H
#define UPLOADABLEIMAGE_H

#include <QObject>
#include <QQuickItem>
#include <QImage>

class UploadableImage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(int angle READ angle WRITE setAngle NOTIFY angleChanged)
    //Q_PROPERTY(QString base64 READ base64 NOTIFY base64Changed)
    Q_PROPERTY(QString filename READ filename NOTIFY filenameChanged)
    Q_PROPERTY(QString mimetype READ mimetype NOTIFY mimetypeChanged)
    Q_PROPERTY(QByteArray bytes READ bytes)

public:
    void setSource(const QString &a);
    void setAngle(const int &b);
    QString source() const;
    int angle() const;
    //QString base64() const;
    QString filename() const;
    QString mimetype() const;

    QByteArray bytes();
signals:
    void sourceChanged();
    void angleChanged();
    //void base64Changed();
    void filenameChanged();
    void mimetypeChanged();

private:
    QString m_source;
    QImage m_image;
    int m_angle;
    //QString m_base64;
    QString m_filename;
    QString m_mimetype;
};

#endif // UPLOADABLEIMAGE_H
