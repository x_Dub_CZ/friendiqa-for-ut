//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef XHR_H
#define XHR_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QJsonObject>
//#include <QNetworkConfiguration>

class XHR : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString login READ login WRITE setLogin NOTIFY loginChanged)
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(QString imagedir READ imagedir WRITE setImagedir NOTIFY imagedirChanged)
    Q_PROPERTY(QList<QString> contactlist READ contactlist WRITE setContactlist NOTIFY contactlistChanged)
    Q_PROPERTY(QList<QString> filelist READ filelist WRITE setFilelist NOTIFY filelistChanged)
    Q_PROPERTY(QString downloadtype READ downloadtype WRITE setDownloadtype NOTIFY downloadtypeChanged)
    //Q_PROPERTY(QString networktype READ networktype() NOTIFY networktypeChanged)


public:
    static XHR *instance();

    explicit XHR(QObject *parent = 0);
  
    QString url() const;
    QString api() const;
    QString login() const;
    QString filename() const;
    QList<QString> contactlist() const;
    QList<QString> filelist() const;
    QString imagedir() const;
    QString downloadtype() const;
//    QString networktype();

signals:
    void urlChanged();
    void apiChanged();
    void loginChanged();
    void filenameChanged();
    void contactlistChanged();
    void filelistChanged();
    void imagedirChanged();
    void downloadtypeChanged();
    void networktypeChanged();
    void downloaded(QString type, QString url, QString filename, int i);
    void downloadedjson(QString type, QString url, QString filename, int i,QJsonObject jsonObject);
    void success(QByteArray data, QString api);
    void error(QString data, QString url,QString api, int code);

public slots:
    void setUrl(QString url);
    void setApi(QString api);
    void setLogin(QString login);
    void setDownloadtype(QString downloadtype);
    void setFilename(QString filename);
    void setContactlist(QList<QString> filename);
    void setFilelist(QList<QString> filename);
    void setImagedir(QString filename);
    void setParam(QString name, QString value);
    void setImageFileParam(QString name, QString url);
    void clearParams();
    void post();
    void postJSON();
    void get();
    void getlist();
    void download();

//    void networktype();

private slots:
    void onReplyError(QNetworkReply::NetworkError code);
    void onReplySuccess();
    void onRequestFinished();
    void onReadyRead();
    void onSSLError(const QList<QSslError> &errors);
    //void updateDownloadProgress(qint64 bytesRead, qint64 totalBytes);

private:
    QByteArray buffer;
    QString m_url;
    QString m_api;
    QString m_login;
    QString m_filename;
    QString m_downloadtype;
//    QString m_networktype;
    QHash<QString, QString> params;
    QHash<QString, QString> files;
    QList<QString> m_filelist;
    QList<QString> m_contactlist;
    QString m_imagedir;
    int dlindex;

    QNetworkAccessManager manager;
    //QNetworkRequest request;
    QNetworkReply *reply;
    //QNetworkConfiguration nc;
    QString bufferToString();
};

#endif // XHR_H
