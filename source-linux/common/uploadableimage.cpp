//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "uploadableimage.h"


#include <QBuffer>
#include <QDebug>
#include <QFileInfo>
#include <QUrl>
#include <QTransform>

void UploadableImage::setAngle(const int &b) {
    if (b != m_angle) {
        m_angle = b;

        //qDebug() << "UploadableImage::setAngle : " << m_angle;
        if (m_angle==0) {
            emit angleChanged();
            return;
        }
    }
}

void UploadableImage::setSource(const QString &a) {
    if (a != m_source) {
        m_source = a;
        //m_base64 = "";
        m_mimetype = "";
        m_filename = "";

        //qDebug() << "UploadableImage::setSource : " << m_source;

        if (m_source=="") {
            emit sourceChanged();
            //emit base64Changed();
            emit mimetypeChanged();
            emit filenameChanged();
            return;
        }

        QImage fullimage = QImage(QUrl(m_source).toLocalFile());

        if (m_angle!=0){
            QTransform transform;
            transform.rotate(qreal(m_angle));
            fullimage=fullimage.transformed(transform);
        }
        if (fullimage.width() > 800 || fullimage.height() > 800) {
            if (fullimage.width() > fullimage.height()) {
                m_image = fullimage.scaledToWidth(800);
            } else {
                m_image = fullimage.scaledToHeight(800);
            }
        } else {
            m_image = fullimage;
        }
        //qDebug() << "UploadableImage::setSource : " << m_image.width() << "x" << m_image.height();
        emit sourceChanged();

        QFileInfo fi(m_source);

        m_filename = fi.fileName();
        emit filenameChanged();

        QString filetype = fi.suffix().toUpper();
        if (filetype!="PNG" && filetype!="JPG") {
            filetype = "JPG";
        }
        //qDebug() << "UploadableImage::setSource : " << "Saving as " << filetype;

        m_mimetype = "image/"+filetype.toLower();
        emit mimetypeChanged();

        /*
        QByteArray byteArray;
        QBuffer buffer(&byteArray);
        m_image.save(&buffer, filetype.toLatin1().constData());
        QString b64 = QString::fromLatin1(byteArray.toBase64().data());

        for(int k=0; k<b64.length(); k+=76) {
            m_base64 += b64.mid(k,76) + "\n";
        }
        m_base64 = m_base64.trimmed();

        emit base64Changed();
        */
    }
}

QString UploadableImage::source() const {
    return m_source;
}

int UploadableImage::angle() const{
    return m_angle;
}
//QString UploadableImage::base64() const {
//    return m_base64;
//}

QString UploadableImage::filename() const {
    return m_filename;
}

QString UploadableImage::mimetype() const {
    return m_mimetype;
}

QByteArray UploadableImage::bytes() {
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    m_image.save(&buffer, "PNG");
    return byteArray;
}
