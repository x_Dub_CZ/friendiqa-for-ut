//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/qml/genericqml"

Page{
    id:photolistview
    width:root.width;
    height:root.height//-7*mm
    property var photolistarray: []

    Rectangle{
        anchors.fill: newsphotolistView
        color: "black"
    }
    ListView{
        id: newsphotolistView
        anchors.fill: parent
        orientation: Qt.Horizontal
        highlightRangeMode: ListView.StrictlyEnforceRange; snapMode: ListView.SnapOneItem
        model:photolistModel
        delegate: photoWrapper
    }

    ListModel{
        id: photolistModel
    }

    MButton {
            id: closeButton
            z:2
            anchors.top: parent.top
            anchors.topMargin: 1*mm
            anchors.right: parent.right
            anchors.rightMargin: 1*mm
            color: Material.dialogColor
            text: "\uf057"
            onClicked: {
                if (rootstackView.depth>1){
                    //roottoolbar.visible=true;
                    rootstackView.pop()}
            }
    }
    Component {
        id: photoWrapper

        AnimatedImage {
            id: realImage;
            width: photolistview.width; height: photolistview.height
            antialiasing: true;
            asynchronous: true
            autoTransform:true
            cache: false
            fillMode: Image.PreserveAspectFit;
            onStatusChanged: playing = (status == AnimatedImage.Ready);
            source: url
            BusyIndicator{
                running: realImage.status==Image.Loading
                anchors.centerIn: parent
            }
            PinchArea  {
                id:imagePinch
                pinch.target: realImage
                anchors.fill: realImage
                pinch.minimumScale: 0.1
                pinch.maximumScale: 10
                enabled: true
            }
        }


    }


    BlueButton{
        width: 5*mm
        height:photolistview.height
        anchors.left: newsphotolistView.left
        visible: newsphotolistView.currentIndex!=0
        text:"\uf053"
        fontColor:"grey"
        border.color: "transparent"
        color:"transparent"
        radius:0
        onClicked: {newsphotolistView.currentIndex=newsphotolistView.currentIndex-1}
    }

    BlueButton{
        width: 5*mm
        height:photolistview.height
        anchors.right: newsphotolistView.right
        visible: newsphotolistView.currentIndex!=photolistarray.length-1
        text:"\uf054"
        fontColor:"grey"
        border.color: "transparent"
        color:"transparent"
        radius:0
        onClicked: {newsphotolistView.currentIndex=newsphotolistView.currentIndex+1}
    }

    Component.onCompleted: {
        if (photolistarray.length>0){
            photolistarray.forEach(function(photo){
                photolistModel.append(photo)
            })
        }
    }
    //onDestroyed: root.roottoolbar.visible=true
}
