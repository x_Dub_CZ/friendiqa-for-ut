//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick.Window 2.0
import QtQuick 2.0
QtObject{
    property int appWidth: Screen.desktopAvailableWidth
    property int appHeight: Screen.desktopAvailableHeight
    property int backKey: Qt.Key_Back
    //property string attachImageDir:filesystem.cameraPath+"/"
    property string osType: "Android"
    property int systemFontSize: root.font.pointSize*1.1
    property int bigFontSize: systemFontSize*1.3
    property string imagePickQml: "ImagePicker"
    property string imagePicker:'import QtQuick 2.0; import "qrc:/qml/genericqml";'+
                          imagePickQml+'{multiple : true;onReady: {attachImageURLs.push(imageUrl);'+
                          'attachImage(imageUrl)}}'
}
