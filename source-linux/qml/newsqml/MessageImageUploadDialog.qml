﻿//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Page{
    id:imageDialog
    property var attachImageURLs: []
//    property var contacts: []
//    property var groups: []
//    property var contact_allow:login.permissions[0]
//    property var contact_deny:login.permissions[1]
//    property var group_allow:login.permissions[2]
//    property var group_deny:login.permissions[3]
    property int imageNo: 0

    function uploadImage(imageid){
        if(imageUploadModel.get(imageid).uploaded==true){
            imageNo=imageNo+1;
            if(imageNo<imageUploadModel.count){
                uploadImage(imageNo);
            }
        } else{
            xhr.setUrl(login.server);
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setApi("/api/media/upload");
            xhr.clearParams();
            //if (group_allow.length>0) {xhr.setParam("group_allow", Helperjs.cleanArray(group_allow))};
            //if (group_deny.length>0) {xhr.setParam("group_deny",  Helperjs.cleanArray(group_deny))};
            //if (contact_allow.length>0) {xhr.setParam("contact_allow", Helperjs.cleanArray(contact_allow))};
            //if (contact_deny.length>0) {xhr.setParam("contact_deny",  Helperjs.cleanArray(contact_deny))};
            xhr.setImageFileParam("media", imageUploadModel.get(imageid).imageUrl);
            xhr.setImageFileParam("angle", imageUploadModel.get(imageid).imageRotation);
            xhr.post();
        }
    }


    function updateAltText(imageid, media){print("media "+media + " alt_text "+imageUploadModel.get(imageid).description)
        xhr.setUrl(login.server);
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setApi("/api/media/metadata/create");
        xhr.clearParams();
        xhr.setParam("JSON",JSON.stringify({media_id:media,alt_text:{text:imageUploadModel.get(imageid).description}}));
        xhr.postJSON();
    }

    function attach(){
        imagePicking=true;
        var imagePicker = Qt.createQmlObject('import QtQuick 2.0; import "qrc:/qml/genericqml";'+
        osSettings.imagePickQml+'{multiple : false;onReady: {'+
        'attachImage(imageUrl)}}',imageDialog,"imagePicker");
         imagePicker.pickImage()
    }

    function attachImage(url){
        if (url.indexOf(",")>0){
            let urlArray=url.split(",");
            for (let file in urlArray){attachImage(urlArray[file])}
        } else{
            if(url!=""){
                imageUploadModel.append({"imageUrl":url,"description":"","imageUploaded":false,"imageRotation":0})
            }
        }
    }

    y:1
    width: messageColumn.width-1.5*root.fontFactor*osSettings.bigFontSize
    height: 15*root.fontFactor*osSettings.bigFontSize//root.height/2-1.5*root.fontFactor*osSettings.bigFontSize

    onVisibleChanged: {if (visible==false){
            imageUploadModel.clear();
            imageNo=0;
        }}

    Connections{
        target:xhr
        function onError(data,url,api,code){
            print("error "+data);
        }
        function onSuccess(data,api){
            if (api=="/api/media/upload" ){print("data "+data);
                let obj=JSON.parse(data);
                messageSend.media_ids.push(obj.media_id);
                if(imageUploadModel.get(imageNo).description!==""){
                    try{
                        updateAltText(imageNo,obj.media_id_string)
                    }catch(e){}
                }
                else{
                    imageUploadModel.set(imageNo,{"imageUploaded":true});
                    imageNo=imageNo+1;
                    if(imageNo<imageUploadModel.count){
                        uploadImage(imageNo);
                    }
                }

            } else if(api=="/api/media/metadata/create"){
                imageUploadModel.set(imageNo,{"imageUploaded":true});
                imageNo=imageNo+1;
                if(imageNo<imageUploadModel.count){
                    uploadImage(imageNo);
                }
            }
        }
    }

    ListView{
        id: imageUploadView
        x: root.fontFactor*osSettings.bigFontSize
        y: 0.5*root.fontFactor*osSettings.bigFontSize
        width: imageDialog.width-1.5*root.fontFactor*osSettings.bigFontSize
        height: parent.height -(3*root.fontFactor*osSettings.bigFontSize)
        model: imageUploadModel
        delegate: imageDelegate
        footer: imageFooter
        clip:true
        orientation: ListView.Horizontal
        spacing: mm
        DropArea{
            anchors.fill: parent
            onDropped: {
                if (drop.keys.includes('text/uri-list')){
                    var urllist=drop.text.split('\n');
                    for(var i=0;i< urllist.length;i++){
                        var droptext = urllist[i].replace(/(\r\n|\n|\r)/gm, ",");
                        attachImage(droptext)
                    }
                }
            }
        }
    }

    BusyIndicator{
      id: uploadBusy
      running: false
      anchors.horizontalCenter: imageUploadView.horizontalCenter
      anchors.top:imageUploadView.top
      anchors.topMargin: root.fontFactor*osSettings.bigFontSize
      width: 2.5*root.fontFactor*osSettings.bigFontSize
      height: 2.5*root.fontFactor*osSettings.bigFontSize
    }

    ListModel{
        id: imageUploadModel
    }

    Component{
        id: imageDelegate
        Rectangle{
            width: Math.max(10*root.fontFactor*osSettings.bigFontSize,uploadImage.width)
            height:imageUploadView.height-4*root.fontFactor*osSettings.bigFontSize
            color: Material.backgroundColor
            Image{
                id: uploadImage
                width: parent.width //root.width/2-mm
                height: imageUploadView.height-(3*root.fontFactor*osSettings.bigFontSize+2*mm)
                fillMode: Image.PreserveAspectFit
                source:imageUrl
                transform: Rotation {id:rotator; origin.x: uploadImage.width/2; origin.y: uploadImage.height/2; angle: imageRotation}
                onVisibleChanged: descriptionInput.focus=true;
            }

            Rectangle{//rotation
                width: 2*root.fontFactor*osSettings.bigFontSize
                height: 2*root.fontFactor*osSettings.bigFontSize
                visible: uploadImage.source!=""
                anchors.bottom: uploadImage.bottom
                anchors.right: uploadImage.right
                color: "black"
                opacity: 0.5
                Text{anchors.centerIn:parent;text: "\uf01e";color: "white"}
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        rotator.angle+=90;
                        imageRotation+=90
                        imageUploadModel.set(index,{"imageRotation":imageRotation});
                    }
                }
            }

            Rectangle{//remove
                width: 2*root.fontFactor*osSettings.bigFontSize
                height: 2*root.fontFactor*osSettings.bigFontSize
                visible: uploadImage.source!=""
                anchors.bottom: uploadImage.bottom
                anchors.left: uploadImage.left
                color: "black"
                opacity: 0.5
                Text{anchors.centerIn:parent;text: "\uf00d";color: "white"}
                MouseArea{
                    anchors.fill:parent;
                    onClicked:{
                        imageUploadModel.remove(index)
                    }
                }
            }

            Text {
                id:uploadedArrow
                anchors.right:uploadImage.right
                visible: imageUploaded
                z:4
                text: "\u2713"
                width: root.fontFactor*osSettings.bigFontSize
                anchors.top: parent.top
                color: "green"
                font.pointSize: 3*osSettings.bigFontSize
            }

            Rectangle{
                id:descriptionRectangle
                color: Material.backgroundColor
                border.color: "grey"
                anchors.top: uploadImage.bottom
                anchors.topMargin: mm
                width: parent.width //root.width/2-mm  //Math.max(root.width/2-mm, descriptionInput.contentWidth);
                height: 2.5*root.fontFactor*osSettings.bigFontSize //5*mm;
                TextField{
                    id: descriptionInput
                    anchors.fill: parent
                    anchors.margins: 0.5*mm
                    font.pointSize: osSettings.systemFontSize
                    selectByMouse: true
                    placeholderText: qsTr("Description")
                    text:description!=""?description:""
                    onTextChanged: imageUploadModel.set(index,{"description":descriptionInput.text});
                }
            }
        }
    }
    Component{
        id: imageFooter
        BlueButton{
            width: 5*root.fontFactor*osSettings.bigFontSize
            height:imageUploadView.height-3*root.fontFactor*osSettings.bigFontSize
            color: Material.backgroundColor
            text:"\u002b"
            fontSize: 3*osSettings.bigFontSize
            onClicked:{attach()}
        }
    }


    Button{
        id:uploadButton
        height: 2*root.fontFactor*osSettings.bigFontSize
        x: root.fontFactor*osSettings.bigFontSize
        anchors.top:imageUploadView.bottom
        anchors.topMargin: mm
        text: qsTr("Upload")
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            {newimageProgress.visible=true;
                if (imageUploadModel.count>0){
                    uploadImage(imageNo)
            }}
        }
    }
    ProgressBar{
        id: newimageProgress
        width: 5*root.fontFactor*osSettings.bigFontSize
        height: root.fontFactor*osSettings.systemFontSize//buttonRow.height
        anchors.top: parent.top
        visible: false
        value: imageNo/imageUploadModel.count
    }
}
