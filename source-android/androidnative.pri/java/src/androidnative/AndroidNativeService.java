package androidnative;
import android.os.Build;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.util.Log;
import android.app.Notification.Builder;
import android.app.Notification; 
import android.app.job.JobScheduler;
import org.qtproject.qt5.android.bindings.QtService;
import org.qtproject.qt5.android.QtNative;
import org.qtproject.friendiqa.R;
import androidnative.friendiqa.FriendiqaActivity;
   
public class AndroidNativeService extends QtService
    {
        private static String TAG = "AndroidNative";
  
        public void startQtService(Context ctx) {
	    //Log.d(TAG, "QtActivity active "+String.valueOf(QtNative.activity()!=null));
	    if (QtNative.activity()==null){
		//Log.d(TAG,"Friendiqasync Stop existing QtService");
		ctx.stopService(new Intent(ctx, AndroidNativeService.class));

	    }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
		//Log.d(TAG,"Friendiqasync startForegroundService");
		ctx.startForegroundService(new Intent(ctx, AndroidNativeService.class));
	    } else {
		ctx.startService(new Intent(ctx, AndroidNativeService.class));
            }
        }
        
	public static void stopQtService(Context ctx) {
  	   //Log.d(TAG,"Friendiqasync QtServiceStop");
           ctx.stopService(new Intent(ctx, AndroidNativeService.class));
        } 
      
       @Override
	public void onCreate()
	{
	Context context;
	context = this.getApplicationContext();
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Intent intent = new Intent(context,FriendiqaActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            String CHANNEL_ID = "channel_02";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Sync Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null,null);
           ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
	   Log.d(TAG,"Friendiqa onCreate Notification");
	   Notification notification = new Notification.Builder(context,CHANNEL_ID)
           .setSmallIcon(R.drawable.friendiqanotification)
           .setContentTitle("Friendiqa")
           .setContentText("Background Sync")
	   .setContentIntent(pendingIntent).build();
	   startForeground(1, notification);}
	JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
	jobScheduler.cancel(1);
	super.onCreate();
	}
    }
