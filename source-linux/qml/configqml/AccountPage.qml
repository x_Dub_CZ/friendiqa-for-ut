//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.7
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQml.Models 2.12
import "qrc:/js/service.js" as Service
import "qrc:/js/layout.js" as Layoutjs
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/configqml"
import "qrc:/qml/genericqml"

Page{
    id:accountPage
    width: root.width
    height: root.height
    property var users:[]
    property var userdata: ({})
    property string imagestoredir: ""


    function setServericon(server){
        if ((server!=null) && (server!="")){
            try {Helperjs.friendicaWebRequest(server+"/api/statusnet/config",accountPage, function (obj){
               var serverdata = JSON.parse(obj);
               servericon.visible=true;
               servericon.source=serverdata.site.logo})
            } catch(e){print(e)}
        }
     }

    MButton{
        id:userButton
        text:qsTr("User")
        font.pointSize: osSettings.bigFontSize
        x: root.fontFactor*osSettings.bigFontSize
        y: root.fontFactor*osSettings.bigFontSize
        width: root.width/2 - 2*mm
        height: 2*root.fontFactor*osSettings.bigFontSize
        visible: users.length>0
        onClicked:{
            var useritems="";
            for (var i=0;i<accountPage.users.length;i++){
                useritems=useritems+"MenuItem{font.pointSize: osSettings.bigFontSize;width:accountPage.width*2/3; text:'"+accountPage.users[i].username+
                "'; onTriggered: {Service.readConfig(db,function(obj){
                userButton.text=obj.username;
                servername.text=obj.server;
                serverModel.insert(0,{text:obj.server})
                accountPage.setServericon(obj.server);
                username.text= obj.username;
                password.text=Qt.atob(obj.password);
                imagestore.text=obj.imagestore;
                imagestoredir=obj.imagestore;
                if( obj.isActive==0){userButton.font.bold='true'} else {userButton.font.bold='false'}
                },'username','"+ accountPage.users[i].username+"')}}"
            }
            var menuString="import QtQuick.Controls 2.12;import 'qrc:/js/service.js' as Service;"+
                " Menu {width:8*root.fontFactor*osSettings.bigFontSize;"+useritems+"}";
            var userlistObject=Qt.createQmlObject(menuString,accountPage,"usermenuOutput")
            userlistObject.popup()  }
    }

    Image{
        id:servericon
        x:root.fontFactor*osSettings.bigFontSize;y:3*root.fontFactor*osSettings.bigFontSize
        width:2.5*root.fontFactor*osSettings.bigFontSize; height: 2.5*root.fontFactor*osSettings.bigFontSize
        visible: false
        source:""
        MouseArea{
            anchors.fill:parent
            onClicked:{
                Service.showServerConfig(servername.text, accountPage, function(configString){
                var serverconfigObject=Qt.createQmlObject(configString,accountPage,"serverconfigOutput");})
            }
        }
    }
    FontLoader{id: fontAwesome; source: "qrc:/images/fontawesome-webfont.ttf"}
    MButton{
        id:serverSearchButton
        text:"\uf002"
        icon.name: "search"
        font.pointSize: osSettings.bigFontSize
        x:root.fontFactor*osSettings.bigFontSize
        y:3*root.fontFactor*osSettings.bigFontSize
        width: 2*root.fontFactor*osSettings.bigFontSize; height:2*root.fontFactor*osSettings.bigFontSize
        visible: servericon.visible?false:true
        onClicked:{Qt.openUrlExternally(Qt.resolvedUrl("https://dir.friendica.social/servers"))}
    }

//    ComboBox{
//        id: servername
//        x: 4*root.fontFactor*osSettings.bigFontSize
//        y: 3.5*root.fontFactor*osSettings.bigFontSize
//        width: root.width-5*root.fontFactor*osSettings.bigFontSize
//        height: 2.5*root.fontFactor*osSettings.bigFontSize//5*mm;
//        font.pointSize: osSettings.systemFontSize
//        editable:true
//        model: serverModel
//        onAccepted: {
//            let cleanText =currentText;if(currentText==""){cleanText=editText}
//            if((cleanText).substring(0,8) !=="https://"){
//                cleanText="https://"+cleanText
//            }
//            if (find(cleanText) === -1) {
//                serverModel.append({text: cleanText})
//                currentIndex = find(cleanText)
//                displayText=cleanText
//            }
//            if (cleanText!=""){accountPage.setServericon(cleanText)}
//        }
//        onFocusChanged: {
//            if(focus==false){
//                    onAccepted()
//            }
//        }
//    }
    TextField {
        id: servername
        x: 4*root.fontFactor*osSettings.bigFontSize
        y: 3.5*root.fontFactor*osSettings.bigFontSize
        width: root.width-5*root.fontFactor*osSettings.bigFontSize
        height: 2.5*root.fontFactor*osSettings.bigFontSize
        font.pointSize: osSettings.systemFontSize
        text:"https://"
        onFocusChanged:{
             if (focus){servermenu.open()}
             else{
                 if((servername.text).substring(0,11) =="https://http"){
                     servername.text= (servername.text).substring(8)
                 }
                 if (servername.text!="https://"){
                     accountPage.setServericon(servername.text)}
                 }
         }
    }
    Menu {
        id:servermenu
        width: 13*root.fontFactor*osSettings.bigFontSize
        x: 4*root.fontFactor*osSettings.bigFontSize
        y: 5*root.fontFactor*osSettings.bigFontSize
        Instantiator{
            model:serverModel
            MenuItem{
                text: modelData
                onTriggered: {servername.text=modelData}
            }
            onObjectAdded: servermenu.insertItem(index,object)
            onObjectRemoved: servermenu.removeItem(object)
        }
    }

    ListModel{id:serverModel
        ListElement{text:"https://asaps-sm.lafayettegroup.com"}
        ListElement{text:"https://f.freinetz.ch"}
        ListElement{text:"https://friendica.chilemasto.casa"}
        ListElement{text:"https://friendica.utzer.de"}
        ListElement{text:"https://friendica.vrije-mens.org"}
        ListElement{text:"https://friendicarg.nsupdate.info.de"}
        ListElement{text:"https://friends.nogafam.es"}
        ListElement{text:"https://libranet.de"}
        ListElement{text:"https://loma.ml"}
        ListElement{text:"https://social.trom.tf"}
        ListElement{text:"https://motley.club"}
        ListElement{text:"https://nerdica.net"}
        ListElement{text:"https://noovi.org"}
        ListElement{text:"https://nsfw.wnymathguy.com"}
        ListElement{text:"https://opensocial.at"}
        ListElement{text:"https://social.isurf.ca"}
        ListElement{text:"https://squeet.me"}
        ListElement{text:"https://venera.social"}
    }

    MButton {
        id: ruleButton
        x: root.fontFactor*osSettings.bigFontSize; y: 6*root.fontFactor*osSettings.bigFontSize; width: root.width-9*mm;
        visible: (osSettings.osType=="Android") && (userButton.text== qsTr("User"))
        height: 2*root.fontFactor*osSettings.bigFontSize;
        text: qsTr("Instance rules")
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            Helperjs.friendicaWebRequest(servername.text+"/api/v1/instance/rules",root,function(rules){
                let rulestext="";
                let rulesarray=JSON.parse(rules)
                for (let rule in rulesarray){
                    rulestext=rulestext+rulesarray[rule].text+"\n"
                }
                var component = Qt.createComponent("qrc:/qml/configqml/AcceptRules.qml");
                var rulesdialog = component.createObject(root,{"rules": rulestext});
                rulesdialog.open()
            })
        }
    }

    TextField {
        id: username
        x: root.fontFactor*osSettings.bigFontSize; y: 6*root.fontFactor*osSettings.bigFontSize; width: root.width-9*mm; //height: 5*mm;
        font.pointSize: osSettings.systemFontSize
        visible: (osSettings.osType=="Android")?(userButton.text!= qsTr("User")):true
        placeholderText: qsTr("Nickname")
        selectByMouse: true
        onEditingFinished: {
             if (username.text.indexOf('@')>-1){
                    Helperjs.showMessage(qsTr("Error"),qsTr("Nicknames containing @ symbol currently not supported"),accountPage)
              }
              imagestoredir=filesystem.homePath+"/"+username.text+"/"
                    //if (imagestore.text==filesystem.homePath+"/.friendiqa/"){imagestore.text=filesystem.homePath+"/.friendiqa/"+username.text+"/"}
        }
     }


    TextField {
        id: password
        x: root.fontFactor*osSettings.bigFontSize; y: 9*root.fontFactor*osSettings.bigFontSize; width: root.width-9*mm; //height: 5*mm;
        font.pointSize: osSettings.systemFontSize
        visible: (osSettings.osType=="Android")?(userButton.text!= qsTr("User")):true
        selectByMouse: true
        echoMode: TextInput.Password
        placeholderText: qsTr("Password")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
   }

    Label {
        id: imagedirlabel
        visible: imagestore.text!=""
        text: qsTr("Image dir.")
        font.pointSize: osSettings.systemFontSize
        x: root.fontFactor*osSettings.bigFontSize; y: 12*root.fontFactor*osSettings.bigFontSize
     }

    TextField {
        id: imagestore
        x: root.fontFactor*osSettings.bigFontSize; y: 13*root.fontFactor*osSettings.bigFontSize; width: root.width-17*mm; //height: 5*mm;
        visible:imagestore.text!=""
        font.pointSize: osSettings.systemFontSize
        selectByMouse: true
        text: "" //filesystem.homePath+"/.friendiqa/"+username.text+"/" //(osSettings.osType=="Android") && (filesystem.fileexist("/storage/emulated/0/Pictures/"))?"/storage/emulated/0/Pictures/":""
        wrapMode: TextEdit.NoWrap
        onTextChanged: imagestoredir=imagestore.text
     }

    MButton {
           x: root.width-3*root.fontFactor*osSettings.bigFontSize; y: 13*root.fontFactor*osSettings.bigFontSize;
           height: 2*root.fontFactor*osSettings.bigFontSize;
           visible:imagestore.text!=""
           text: "..."
           font.pointSize: osSettings.bigFontSize
           onClicked:{imagestoreDialog.open()}
    }

    FileDialog {
        id: imagestoreDialog
        title: "Please choose a directory"
        folder: shortcuts.pictures
        selectFolder: true
        onAccepted: {
            var imagestoreString=imagestoreDialog.folder.toString();
            imagestoreString=imagestoreString.replace(/^(file:\/{2})/,"")+"/"
            imagestore.text=imagestoreString
        }
    }

    BusyIndicator{
        id: accountBusy
         anchors.horizontalCenter: parent.horizontalCenter
         y: 16*root.fontFactor*osSettings.bigFontSize
         width:10*mm
         height: 10*mm
         running: false
    }

    MButton {
        id:confirmation
        x: root.fontFactor*osSettings.bigFontSize; y: 16*root.fontFactor*osSettings.bigFontSize
        text: qsTr("Confirm")
        font.pointSize: osSettings.bigFontSize
        visible: (osSettings.osType=="Android")?userButton.text!= qsTr("User"):true
        onClicked:{
            accountBusy.running=true;//servername.displayText
            var userconfig={server: servername.displayText, username: username.text, password:Qt.btoa(password.text), imagestore:imagestoredir,interval:  ""};
            var errormessage="";
            if (servername.text==""){errormessage=qsTr("No server given! ")}
            else if (username.text==""){errormessage+=qsTr("No nickname given! ")}
            else if (password.text=="") {errormessage+=qsTr("No password given! ")}
            else if (imagestoredir=="") {errormessage+=qsTr("No image directory given!")}

            else {errormessage=""}
            if (errormessage=="") {
                Helperjs.friendicaRequest(userconfig,"/api/account/verify_credentials.json?skip_status=true",root,function(obj){
                    accountBusy.running=false;
                    try{var credentials=JSON.parse(obj);
                        if (credentials.hasOwnProperty('error')){
                            Helperjs.showMessage(qsTr("Error"),qsTr("Wrong password or 2FA enabled!"),root)
                        }
                        else{
                            if (users.length==0){Service.setDefaultOptions(db);}
                            if(userconfig.imagestore == filesystem.homePath+"/"+username.text+"/")
                                {
                                filesystem.makePath(filesystem.homePath+"/"+username.text);
                               }
                            filesystem.Directory=imagestoredir
                            filesystem.makeDir("contacts");
                            filesystem.makeDir("albums");
                            userconfig.accountId=credentials.id
                            Service.storeConfig(db,userconfig);
                            Service.readConfig(db,function(userconfig){
                                Helperjs.readData(db,"config","",function(storedUsers){
                                   storedUsers.sort(function(obj1, obj2) {
                                       return obj1.isActive - obj2.isActive;
                                   });
                                accountPage.users=storedUsers});
                                 //reset values
                                login=userconfig;
                                news=[];
                                contactlist=[];
                                rootstack.currentIndex=0;
                                newstypeSignal("refresh");
                            },"isActive",0);
                             Helperjs.showMessage(qsTr("Success"),qsTr("Name")+": "+credentials.name+"\nScreen Name: "+credentials.screen_name,root)
                            rootstackView.pop()
                         }
                    }catch(e){Helperjs.showMessage(qsTr("Error"),qsTr("Wrong password or 2FA enabled!"),root)};

        })}
         else {Helperjs.showMessage(qsTr("Error"), errormessage,root)}
    }}


    Row{
        spacing:0.5*mm
        anchors.top: parent.top
        anchors.topMargin: root.fontFactor*osSettings.bigFontSize
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        MButton {
            width: 5*mm;
            visible: users.length>0
            text: "-"
            font.pointSize: osSettings.bigFontSize
            onClicked:{
                var userconfig={server: servername.text, username: username.text, password: Qt.btoa(password.text)};
                Service.deleteConfig(db,userconfig,function(){
                    filesystem.Directory=imagestore.text+"contacts";
                    filesystem.rmDir();
                    filesystem.Directory=imagestore.text+"albums";
                    filesystem.rmDir();
                    servername.text="https://";
                    servericon.visible=false;
                    servericon.source="";
                    username.text="";
                    password.text="";
                    imagestore.text="";
                   //maxNews.value=0;
                   //newsTypeField.text="Conversations";
                   //messageIntervalSlider.value=30;
                    userButton.text=qsTr("User");
                    Helperjs.readData(db,"config","",function(storedUsers){
                        storedUsers.sort(function(obj1, obj2) {
                            return obj1.isActive - obj2.isActive;
                     })
                     accountPage.users=storedUsers;})
               })
            }}

        MButton {
            width: 5*mm;
            visible: users.length>0
            text: "+"
            font.pointSize: osSettings.bigFontSize
            onClicked:{
                servername.text="https://"
                servericon.visible=false;
                servericon.source="";
                username.text=""
                password.text=""
                imagestore.text=""
                userButton.text=qsTr("User")
               }
         }

        MButton {
            width: 5*mm;
            text: "?"
            font.pointSize: osSettings.bigFontSize
            onClicked:{
                rootstackView.push("qrc:/qml/configqml/InfoBox.qml");
            }
        }
        MButton{
            id:closeButton
            width: 5*mm;
            visible: users.length>0
            text:   "\uf057"
            font.pointSize: osSettings.bigFontSize
            onClicked:{rootstackView.pop()}
        }
    }

   Component.onCompleted: {
       try{Helperjs.readData(db,"config","",function(storedUsers){
          storedUsers.sort(function(obj1, obj2) {
              return obj1.isActive - obj2.isActive;
          })
          accountPage.users=storedUsers;
          Service.readConfig(db,function(obj){
            userButton.text=obj.username;
            servername.text=obj.server;
            serverModel.insert(0,{text:obj.server})
            accountPage.setServericon(obj.server);
            username.text= obj.username;
            password.text=Qt.atob(obj.password);
            imagestore.text=obj.imagestore;
            imagestoredir=obj.imagestore;
            if( obj.isActive==0){userButton.font.bold='true'} else {userButton.font.bold='false'}
          },"isActive",0)
        })}
        catch (e){//print("onCompleted" +users.count +e)
      }
    }
}
