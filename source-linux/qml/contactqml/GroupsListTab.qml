//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
//import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/news.js" as Newsjs
import "qrc:/js/service.js" as Service
import "qrc:/qml/contactqml"
import "qrc:/qml/genericqml"

Item{
    id: groupsGridTab

    function showGroups(username){
        try {groupsModel.clear()} catch(e){print(e)};
        Helperjs.readData(db, "groups",root.login.username,function(groupsobject){
          for (var j=0;j<groupsobject.length;j++){
              groupsModel.append({"group":groupsobject[j]});
    }})}

    function updateGroup(login,database,group){
       // update groups
        var api="";
        if (group.new){api="/api/friendica/group_create.json?name="+group.name}else{api="/api/friendica/group_update.json?gid="+group.id}
        xhr.setUrl(login.server);
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setApi(api);
        xhr.clearParams();
        xhr.setParam("gid",group.id);
        xhr.setParam("name",group.name);
        xhr.setParam("user", group.user);
        xhr.setParam("json",group);
        xhr.post();
    }

    Connections{
        target:xhr
        function onError(data,url,api,code){print(data)}//if (data=="image"){Helperjs.showMessage()}}
        function onSuccess(data,api){
            if(api.startsWith("/api/friendica/group")){
                Newsjs.requestGroups(root.login,root.db,root,function(){
                showGroups(root.login.username)});
            }
        }
    }
//    MButton {//requestGroups() not working with Friendica 02/2022
//        id: updateGroupsButton
//        text: "\uf021"
//        anchors.top: parent.top
//        anchors.topMargin: mm
//        anchors.right: parent.right
//        anchors.rightMargin: mm
//        onClicked: {
//            Newsjs.requestGroups(root.login,root.db,root,function(){
//                groupsGridTab.showGroups(root.login.username)})}
//    }
//            BlueButton {
//                id: newGroupButton
//                text: "\uf234"
//                anchors.top: parent.top
//                anchors.topMargin: mm
//                anchors.right: updateGroupsButton.left
//                anchors.rightMargin: mm
//                onClicked: {
//                    groupsModel.append({"group": {"new":true}});
//                }
//            }
    ListView {
        id: groupsView
        x:mm
        y:updateGroupsButton.height+4*mm
        width:groupsGridTab.width-2*mm
        height:groupsGridTab.height-2*mm//-updateGroupsButton.height
        clip: true
//        cellHeight: 16*mm
//        cellWidth: 17*mm
//        add: Transition {
//             NumberAnimation { properties: "x,y"; from: 300; duration: 1000 }
//            }
        model: groupsModel
        delegate: GroupComponent {}
        //footer:groupFooter
        }
    ListModel{
        id: groupsModel
    }
//            Component{
//                id: groupFooter
//                Image{
//                    id: footerImage
//                    width: 15*mm
//                    height: 15*mm
//                    fillMode: Image.PreserveAspectFit
//                    source:"qrc:/images/addImage.png"
//                    MouseArea{
//                        anchors.fill: parent
//                        onClicked:{
//                            print("new group")
//                            var component = Qt.createComponent("qrc:/qml/contactqml/GroupComponent.qml");
//                            var imagedialog = component.createObject(groupsView,{"group": []});}
//                    }
//                }
//            }
    Component.onCompleted: {
        friendsTabView.groupsSignal.connect(showGroups);
        showGroups()
    }
}
