//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// List of people from Friendica Activities
import QtQuick 2.0
import QtQuick.Controls.Material 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Rectangle {
    id:activitiesRectangle
    property var activitymembers
    color: Material.dialogColor
    border.color: Material.frameColor
    radius:0.5*mm
    width:root.width/2
    height:Math.min(root.height/2,(10*mm+6*activitymembers.length*mm))

    ListView {
          id: contactView
          x:mm
          y:8*mm
          width: activitiesRectangle.width-2*mm
          height: activitiesRectangle.height-10*mm
          clip: true
          spacing: 0
          model: activitiesModel
          delegate: activitiesContact
    }

    ListModel{id: activitiesModel}

    Component {
        id:activitiesContact
        Rectangle{
            border.color: Material.frameColor
            color: Material.backgroundColor
            border.width: 1
            radius:0.5*mm
            width:parent.width
            height:6*mm
            Image {
                id: contactImage
                x:1
                y:1
                width: 5*mm
                height:5*mm
                source:(contact.profile_image!="")? "file://"+contact.profile_image : contact.profile_image_url
                onStatusChanged: if (contactImage.status == Image.Error) {source="qrc:/images/defaultcontact.jpg"}
            }
            Text{
                color: Material.primaryTextColor
                font.pointSize: osSettings.bigFontSize
                anchors.left: contactImage.right
                anchors.margins: 1*mm
                text:contact.name
            }

           MouseArea{
                anchors.fill: parent
                onClicked:{showContact(contact)}
            }
        }
    }

    MButton {
        id: closeButton
        anchors.top: parent.top
        anchors.topMargin: 1*mm
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
            //color:"white"
        text: "\uf057"
        onClicked: {
            activitiesRectangle.destroy()
        }
    }

    Component.onCompleted: {
        for (var user in activitymembers){
            activitiesModel.append({"contact":activitymembers[user]})
        }
    }
}
