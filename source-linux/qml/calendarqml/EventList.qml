//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"
import "qrc:/qml/calendarqml"

Rectangle{
    id:eventList
    color: Material.backgroundColor
    property var daylist:[]
    property int dayint: 0
    property var events:[]

    MButton{
       id:closeButton
       anchors.top: parent.top
       anchors.topMargin: 1*mm
       anchors.right: parent.right
       anchors.rightMargin: 1*mm
       text: "\uf057"
       onClicked:{rootstackView.pop()}
    }

    MButton{
        id:  createNewEvent
        anchors.top: parent.top
        anchors.topMargin: 1*mm
        anchors.right:closeButton.left
        anchors.rightMargin:mm
        width: 2*root.fontFactor*osSettings.bigFontSize;
        text:"+"
        onClicked: {
            rootstackView.push("qrc:/qml/calendarqml/EventCreate.qml",{"startDate": new Date(dayint*86400000)})
        }
    }
    Dialog {
        id: deleteDialog
        anchors.centerIn: parent
        property int eventid:0
        title: qsTr("Delete Event?")
        standardButtons: Dialog.Ok | Dialog.Cancel
        modal: true
        onAccepted: {
            xhr.setUrl(login.server);
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setApi("/api/friendica/event_delete");
            xhr.clearParams();
            xhr.setParam("id",eventid);
            xhr.post();
        }
        onRejected: {close()}
    }

    ListView {
        id: eventlistView
        y:closeButton.height+2*mm
        width: eventList.width-4*root.fontFactor*osSettings.bigFontSize
        height: eventList.height-closeButton.height-root.fontFactor*osSettings.bigFontSize
        clip: true
        model: eventModel
        delegate: EventListItem{}
    }
    ListModel{
        id: eventModel
    }
    Component.onCompleted:{
         var currentevents=events.filter(event=>(dayint>=event.startday)&&(dayint<=event.endday));
         for (var i=0; i<currentevents.length;i++){
             var liststate="";if(currentevents.length<2){liststate="large"};
             eventModel.append({"event":currentevents[i],"eventstatus":liststate});
         }
    }
}
