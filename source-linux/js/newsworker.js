//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

function findend (text, startpos) {
    var indexOf = text.substring(startpos || 0).search(/\s/);
    return (indexOf >= 0) ? (indexOf + (startpos || 0)) : text.length;
}

function beautify(newsitemobject,msg){
    var forumname="";
    try{
        if (newsitemobject.messagetype==0&&newsitemobject.hasOwnProperty('friendica_author')&&
                ((newsitemobject.friendica_author.url)!=(newsitemobject.user.url))&&((newsitemobject.friendica_author.url)!=null)){
            //print(" Friendica Author "+JSON.stringify(newsitemobject));
            forumname=" via "+newsitemobject.user.name;
            newsitemobject.user=newsitemobject.friendica_author;
        }
        if (typeof(newsitemobject.friendica_activities_self)=="string"){
            newsitemobject.friendica_activities_self=JSON.parse(newsitemobject.friendica_activities_self);
        }
    }catch(e){print("forum name "+e)}

    var self=({})
    if (newsitemobject.hasOwnProperty("friendica_activities_self")){
        if (newsitemobject.friendica_activities_self.indexOf(3)!=-1){self.attending=qsTr("yes")}
        if (newsitemobject.friendica_activities_self.indexOf(4)!=-1){self.attending=qsTr("no")}
        if (newsitemobject.friendica_activities_self.indexOf(5)!=-1){self.attending=qsTr("maybe")}
        if (newsitemobject.friendica_activities_self.indexOf(1)!=-1){self.liked=1}
        if (newsitemobject.friendica_activities_self.indexOf(2)!=-1){self.disliked=1}
    }
    var friendica_activities={self:self}
    var imageAttachmentList=[];
    var videoAttachmentList=[];
    var videoformats=["mp4", "avi", "webm","ogg","mp3"]
    try{if(newsitemobject.attachments){
            var attachArray=newsitemobject.attachments;
            for (var image in attachArray){
                var attachhelper={mimetype:attachArray[image].mimetype}
                var attachhelperstring="<img"
                var helperstringposition=newsitemobject.statusnet_html.indexOf(attachhelperstring);
                attachhelper.url=attachArray[image].url
                if (helperstringposition>-1){var removeme=newsitemobject.statusnet_html.substring(helperstringposition,newsitemobject.statusnet_html.indexOf('">',helperstringposition)+2);}
                newsitemobject.statusnet_html=newsitemobject.statusnet_html.replace(removeme,"")
                imageAttachmentList.push(attachhelper)
            }
        }
    }catch(e){print("attachment "+e)}
    newsitemobject.statusnet_html=newsitemobject.statusnet_html.replace(/class=\"attachment-image\"/g,"width=\"600\" ");

    for (var format in videoformats){
        if (newsitemobject.text.indexOf("."+videoformats[format])>-1){
            var videohelper={mimetype:"video/"+videoformats[format]}
            var videotext=newsitemobject.text;
            while (videotext.indexOf("."+videoformats[format])>-1){
                var videohelperstringposition=videotext.indexOf("."+videoformats[format]);
                videohelper.url=videotext.substring(videotext.lastIndexOf("http",videohelperstringposition),videohelperstringposition+4);
                videotext=videotext.substring(videohelperstringposition+4,videotext.length)
                if ((videoAttachmentList.length==0) || (videoAttachmentList[videoAttachmentList.length-1].url!=videohelper.url)){videoAttachmentList.push(videohelper)}
            }
        }
    }
    if (newsitemobject.text.indexOf("/videos/watch/")>-1){
        var ptvideohelper={mimetype:"video/mp4"}
        var ptvideotext=newsitemobject.text;
        while (ptvideotext.indexOf("/videos/watch/")>-1){
            var ptvideohelperstringposition=ptvideotext.indexOf("/videos/watch/");
            var ptposend=findend(ptvideotext,ptvideohelperstringposition);
            if(ptposend==-1){ptposend=ptvideotext.length};
            ptvideohelper.url=ptvideotext.substring(ptvideotext.lastIndexOf("http",ptvideohelperstringposition),ptposend)+"-480.mp4";
            ptvideohelper.url=ptvideohelper.url.replace("/videos/watch","/static/webseed");
            ptvideotext=ptvideotext.substring(ptposend,ptvideotext.length)
            if ((videoAttachmentList.length==0) || (videoAttachmentList[videoAttachmentList.length-1].url!=ptvideohelper.url)){videoAttachmentList.push(ptvideohelper)}
        }
    }

    newsitemobject.videoAttachmentList=videoAttachmentList;
    newsitemobject.imageAttachmentList=imageAttachmentList;
    if ((msg.options.hasOwnProperty("hide_nsfw"))&&(msg.options.hide_nsfw==1)&&(newsitemobject.text.indexOf("#nsfw")>-1)){
        newsitemobject.nsfw=true
    } else{newsitemobject.nsfw=false}

    newsitemobject.dateDiff=(msg.currentTime-newsitemobject.created_at)/1000;
    newsitemobject.friendica_activities_view=friendica_activities;
    newsitemobject.forumname=forumname;
    return newsitemobject;
}

WorkerScript.onMessage = function(msg) {
    if(msg.deleteId!==undefined)
    {msg.model.remove(msg.deleteId);
        msg.model.sync()
    }
    else{
        if(msg.method=="refresh" || msg.method=="contact" ||(msg.method=="conversation"&&msg.news.length>0)){msg.model.clear()};
        msg.model.sync()
        for (var j in msg.news){
            let data=({});
            if (typeof(msg.news[j])=='object') {
                var newsitemobject=msg.news[j];
                newsitemobject=beautify(newsitemobject,msg);
                if (!(typeof(newsitemobject.currentconversation)=='undefined') && (newsitemobject.currentconversation.length>0)){
                    let n=1;
                    while ((n<newsitemobject.currentconversation.length)&&(newsitemobject.currentconversation[newsitemobject.currentconversation.length-n].user.statusnet_blocking==true)) {
                      n++;
                    }
                    if (n<newsitemobject.currentconversation.length){
                        newsitemobject.lastcomment=beautify(newsitemobject.currentconversation[newsitemobject.currentconversation.length-n],msg);
                        newsitemobject.lastcomment.indent=1
                        newsitemobject.lastcomment.isLastComment=true
                    }
                }

                if (msg.method=="conversation"){
                    if (j==0){newsitemobject.indent=0}else{
                        var count=0;
                        var firstReply=0;
                        for (var k=msg.model.count-1;k>-1;k--){
                             if (newsitemobject.in_reply_to_status_id==msg.model.get(k).newsitemobject.id){
                                newsitemobject.indent=(msg.model.get(k).newsitemobject.indent||0)+1;
                                if (newsitemobject.indent>6){newsitemobject.indent=6};
                                firstReply=k;
                            }
                            if (newsitemobject.in_reply_to_status_id==msg.model.get(k).newsitemobject.in_reply_to_status_id){
                                count+=1
                            }
                        }
                }}
                data=({"newsitemobject": newsitemobject})
            }
            if(data.newsitemobject.user.statusnet_blocking==true){break}
            if(msg.method=="append") {
                msg.model.insert(j, data)
            } else if (msg.method=="conversation" && firstReply>0){
                msg.model.insert(firstReply+count+1, data)
            }
            else{
                msg.model.append(data)
            }
        }
        msg.model.sync()
    }
}
