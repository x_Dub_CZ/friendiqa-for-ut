package androidnative;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.content.Context;
import android.content.ComponentName;
import android.app.job.JobScheduler;
import android.app.job.JobInfo;
import android.app.PendingIntent;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import org.qtproject.qt5.android.QtNative;
import androidnative.friendiqa.FriendiqaService;
import androidnative.friendiqa.FriendiqaStopService;
import androidnative.friendiqa.FriendiqaActivity;
import androidnative.AndroidNativeActivity;
import androidnative.AndroidNativeService;
import android.content.Intent;
import java.util.Map;
import org.qtproject.friendiqa.R;

public class Util {

    private static final String TAG = "androidnative.Util";

    public static final String SET_TRANSLUCENT_STATUS_BAR = "androidnative.Util.setTranslucentStatusBar";
    public static final String SET_FULL_SCREEN = "androidnative.Util.setFullScreen";
    public static final String SET_SCHEDULE = "androidnative.Util.setSchedule";
    public static final String SET_NOTIFICATION = "androidnative.Util.setNotification";


    static {
        SystemDispatcher.addListener(new SystemDispatcher.Listener() {
            public void onDispatched(String type, Map message) {
                if (type.equals(SET_TRANSLUCENT_STATUS_BAR)) {
                    setTranslucentStatusBar(message);
                } else if (type.equals(SET_FULL_SCREEN)) {
                    setFullScreen(message);
                } else if (type.equals(SET_SCHEDULE)) {
                    setSchedule(message);
                } else if (type.equals(SET_NOTIFICATION)) {
                    setNotification(message);
                }
            }
        });
    }

    static void setTranslucentStatusBar(Map message) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        final Boolean value = (Boolean) message.get("value");
        final Activity activity = QtNative.activity();

        Runnable runnable = new Runnable () {
            public void run() {
                Window w = activity.getWindow(); // in Activity's onCreate() for instance

                if (value) {
//                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                } else {
//                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
            }
        };

        activity.runOnUiThread(runnable);

    }

    static void setFullScreen(Map message) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        final Boolean value = (Boolean) message.get("value");
        final Activity activity = QtNative.activity();

        Runnable runnable = new Runnable () {
            public void run() {
                Window w = activity.getWindow(); // in Activity's onCreate() for instance
                View decorView = w.getDecorView();

                int config = decorView.getSystemUiVisibility();

                if (value) {
                    config &= ~View.SYSTEM_UI_FLAG_FULLSCREEN;
                } else {
                    config |= View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                }
                decorView.setSystemUiVisibility(config);
            }
        };

        activity.runOnUiThread(runnable);

    }

    static void setNotification(Map message) {
        //Log.d(TAG,"setNotification");
        Context context;
        //Context appcontext;
        context = QtNative.service().getApplicationContext();
        //appcontext = QtNative.activity().getApplicationContext();
        Intent intent = new Intent(context,FriendiqaActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

	final String textTitle = (String) message.get("title");
	final String textContent = (String) message.get("message");
	final int  notificationId = (int) message.get("id");
	
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
		final String CHANNEL_ID = "channel_01";
		NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
		            "Channel for Friendiqa News",
		            NotificationManagerCompat.IMPORTANCE_LOW);
		notificationManager.createNotificationChannel(channel);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_ID)
		.setSmallIcon(R.drawable.friendiqanotification)
		.setContentIntent(pendingIntent)
		.setContentTitle(textTitle)
		.setContentText(textContent)
		.setStyle(new NotificationCompat.BigTextStyle()
		        .bigText(textContent))
      		.setAutoCancel(true);
                notificationManager.notify(notificationId, builder.build());
		
	} else {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
		.setSmallIcon(R.drawable.friendiqanotification)
		.setContentIntent(pendingIntent)
		.setContentTitle(textTitle)
		.setContentText(textContent)
		.setStyle(new NotificationCompat.BigTextStyle()
		        .bigText(textContent))
		
		.setPriority(NotificationCompat.PRIORITY_DEFAULT)
		.setAutoCancel(true);
                 notificationManager.notify(notificationId, builder.build());
	}
    }

    static void setSchedule(Map message) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        
        final Integer value = (Integer) message.get("value");
	//final int JOB_ID = 1;
        final int ONE_MIN = 60 * 1000;
        Context context;
        if (QtNative.activity() == null){
		context = QtNative.service().getApplicationContext();
		
	} else {
		context = QtNative.activity().getApplicationContext();
	}
	if (value==0){
		JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        	jobScheduler.cancelAll();
		//Log.d(TAG,"Friendiqasync deleting Androidnative jobscheduler");
	} else {
		//Log.d(TAG,"Friendiqasync schedule Androidnative jobscheduler");
		ComponentName component = new ComponentName(context, FriendiqaService.class);
        	JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
		//Log.d(TAG,"Jobinfolist size " + String.valueOf(jobScheduler.getAllPendingJobs().size()));
    		if (jobScheduler.getAllPendingJobs().size()==0){
			JobInfo.Builder builder = new JobInfo.Builder(2, component)
				.setPeriodic(value * ONE_MIN)
				.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
				.setPersisted(true)
				.setPrefetch(true);
			    //Log.d(TAG,"Friendiqa schedule Androidnative sync schedule"+ (value * ONE_MIN));
		            jobScheduler.schedule(builder.build());
		} else {
			for ( JobInfo jobInfo : jobScheduler.getAllPendingJobs() ) {
				//Log.d(TAG,"Jobinfo current interval " + String.valueOf(jobInfo.getIntervalMillis ()));
	           		if (jobInfo.getIntervalMillis ()!=(value * ONE_MIN)){
				    JobInfo.Builder builder = new JobInfo.Builder(2, component)
					.setPeriodic(value * ONE_MIN)
					.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
					.setPersisted(true)
					.setPrefetch(true);
				    //Log.d(TAG,"Friendiqa schedule Androidnative sync schedule"+ (value * ONE_MIN));
		        	    jobScheduler.schedule(builder.build());
	        		}
    			}
		}
        	//Log.d(TAG,"Active service " + String.valueOf(QtNative.service()!=null));
        	if (QtNative.service() != null){
                	//Log.d(TAG,"Schedule Stopping Friendiqa Androidnative service");
        		ComponentName componentStopper = new ComponentName(context, FriendiqaStopService.class);
        		JobInfo.Builder stopbuilder = new JobInfo.Builder(1, componentStopper)
                		.setMinimumLatency(50)
                		.setOverrideDeadline(100);
 
        		JobScheduler jobStopScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        		jobStopScheduler.schedule(stopbuilder.build());
        	}
        }
	//context.stopService(new Intent(context, AndroidNativeService.class));
    }
}
