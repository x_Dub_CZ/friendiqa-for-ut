package androidnative;
import android.content.Intent;
import android.util.Log;
import android.app.Activity;
import android.os.*;
import java.util.Map;
import java.util.HashMap;
import android.content.pm.PackageManager;
import android.content.Context;
import android.Manifest.permission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/** An alternative Activity class for Qt applicaiton.

  Remarks: It is recommended but not a must to use this class as the main activity.
 */

public class AndroidNativeActivity extends org.qtproject.qt5.android.bindings.QtActivity {
	public static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 0x245285a8;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SystemDispatcher.onActivityResult(requestCode,resultCode,data);
    }


    protected void onResume() {
        super.onResume();
	//if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {

    	// Permission is not granted

        //	ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

        	// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        	// app-defined int constant. The callback method gets the
        	// result of the request.
	//} else {
	
	System.loadLibrary("friendiqa_arm64-v8a");
	//System.loadLibrary("friendiqa_armeabi-v7a");
        if((getIntent().getFlags() == (Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)) || (getIntent().getFlags() == Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) || (getIntent().getFlags() == Intent.FLAG_ACTIVITY_NEW_TASK) || (getIntent().getFlags() == Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED) || (getIntent().getFlags() == (Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED))) {
            SystemDispatcher.onActivityResume();
	} else {

            Intent data = getIntent();
	    if ((data!=null) && (data.getType() != null) && !(data.getBooleanExtra("used",false))){
	       String type = data.getType();
	       String[] parts = type.split("/");
	       String maintype= parts[0];
	       if (maintype.equals("text")){
		   SystemDispatcher.loadClass("androidnative.TextIntent");
		   SystemDispatcher.onActivityResult(0x245285a4,Activity.RESULT_OK,data);

	       } else {
		    SystemDispatcher.loadClass("androidnative.ImagePicker");
		    SystemDispatcher.onActivityResult(0x245285a3,Activity.RESULT_OK,data);         
	       }
                getIntent().replaceExtras(new Bundle());
                getIntent().setAction("");
                getIntent().setData(null);
                getIntent().setFlags(0);
                getIntent().putExtra("used", true);

            } else {
                SystemDispatcher.onActivityResume();
	}}
    //}
    }




	@Override public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
    		switch (requestCode) {
        		case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
            		// If request is cancelled, the result arrays are empty.
            		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        			Map message = new HashMap();
        			message.put("multiple",false);
				SystemDispatcher.dispatch("androidnative.ImagePicker.pickImage",message);
            		} else {
                        }
               return;
        }
    }
}



    protected void onNewIntent(Intent data) {  
      System.loadLibrary("friendiqa_arm64-v8a");  
      //System.loadLibrary("friendiqa_armeabi-v7a");
      super.onNewIntent(data);
	if ((data!=null) && (data.getType() != null) && !(data.getBooleanExtra("used",false))){
	   String type = data.getType();
	   String[] parts = type.split("/");
	   String maintype= parts[0];
	   if (maintype.equals("text")){
	       SystemDispatcher.loadClass("androidnative.TextIntent");
	       SystemDispatcher.onActivityResult(0x245285a4,Activity.RESULT_OK,data);
           } else {
	      SystemDispatcher.loadClass("androidnative.ImagePicker");
	      SystemDispatcher.onActivityResult(0x245285a3,Activity.RESULT_OK,data);         
	   }
               getIntent().replaceExtras(new Bundle());
               getIntent().setAction("");
               getIntent().setData(null);
               getIntent().setFlags(0);
               getIntent().putExtra("used", true);
           } else {
               SystemDispatcher.onActivityResume();
	}

    } // onNewIntent
}

