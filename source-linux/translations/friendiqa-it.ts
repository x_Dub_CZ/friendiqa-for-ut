<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AcceptRules</name>
    <message>
        <location filename="../qml/configqml/AcceptRules.qml" line="41"/>
        <source>Accept instance rules</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountPage</name>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="64"/>
        <location filename="../qml/configqml/AccountPage.qml" line="206"/>
        <location filename="../qml/configqml/AccountPage.qml" line="228"/>
        <location filename="../qml/configqml/AccountPage.qml" line="245"/>
        <location filename="../qml/configqml/AccountPage.qml" line="306"/>
        <location filename="../qml/configqml/AccountPage.qml" line="385"/>
        <location filename="../qml/configqml/AccountPage.qml" line="406"/>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="229"/>
        <source>Nickname</source>
        <translation>Utente</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="248"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="255"/>
        <source>Image dir.</source>
        <translation>Directory immagini</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">News come</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="208"/>
        <source>Instance rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <location filename="../qml/configqml/AccountPage.qml" line="354"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation>I soprannomi contenenti il simbolo @ attualmente non sono supportati</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="304"/>
        <source>Confirm</source>
        <translation>Conferma</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="311"/>
        <source>No server given! </source>
        <translation>Nessun server inserito!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="312"/>
        <source>No nickname given! </source>
        <translation>Nessun utente inserito!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="313"/>
        <source>No password given! </source>
        <translation>Nessuna password inserita!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="314"/>
        <source>No image directory given!</source>
        <translation>Nessuna directory immagini inserita!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <source>Wrong password or 2FA enabled!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">Password sbagliata!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Success</source>
        <translation>Ha funzionato!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronologia</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversazioni</translation>
    </message>
</context>
<context>
    <name>BlockUser</name>
    <message>
        <location filename="../qml/newsqml/BlockUser.qml" line="41"/>
        <source>Block contact?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarTab</name>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="154"/>
        <source>Delete Event?</source>
        <translation>Cancellare la data?</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="201"/>
        <source>Events</source>
        <translation>Eventi</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="206"/>
        <source>Own Calendar</source>
        <translation>Calendario</translation>
    </message>
</context>
<context>
    <name>ConfigAppearancePage</name>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="46"/>
        <source>News as</source>
        <translation>News come</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="60"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="78"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="79"/>
        <source>Conversations</source>
        <translation>Conversazioni</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="72"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="73"/>
        <source>Timeline</source>
        <translation>Cronologia</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="86"/>
        <source>Max. News</source>
        <translation>Nº Max. di notizie</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="121"/>
        <source>Hide #nsfw?</source>
        <translation>Nascondere #nsfw?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="159"/>
        <source>Dark Mode</source>
        <translation>design</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="163"/>
        <source>System</source>
        <translation>design standard</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="174"/>
        <source>Dark</source>
        <translation>design scuro</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="185"/>
        <source>Light</source>
        <translation>design luminoso</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <source>News as</source>
        <translation type="vanished">News come</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversazioni</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronologia</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Nº Max. di notizie</translation>
    </message>
    <message>
        <source>Hide #nsfw?</source>
        <translation type="vanished">Nascondere #nsfw?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="55"/>
        <source>Appearance</source>
        <translation>Visualizzare</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="61"/>
        <source>Sync</source>
        <translation>Sync</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="67"/>
        <source>Start</source>
        <translation>Avviare</translation>
    </message>
</context>
<context>
    <name>ConfigStartPage</name>
    <message>
        <location filename="../qml/configqml/ConfigStartPage.qml" line="49"/>
        <source>Autostart</source>
        <translation>Avvio automatico</translation>
    </message>
</context>
<context>
    <name>ConfigTab</name>
    <message>
        <source>User</source>
        <translation type="vanished">Utente</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Server</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="vanished">Utente</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Password</translation>
    </message>
    <message>
        <source>Image dir.</source>
        <translation type="vanished">Directory immagini</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Nº Max. di notizie</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">News come</translation>
    </message>
    <message>
        <source>Interval (0=None)</source>
        <translation type="vanished">Intervallo (0=nessuno)</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Errore</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Conferma</translation>
    </message>
    <message>
        <source>No server given! </source>
        <translation type="vanished">Nessun server inserito!</translation>
    </message>
    <message>
        <source>No nickname given! </source>
        <translation type="vanished">Nessun utente inserito!</translation>
    </message>
    <message>
        <source>No username given! </source>
        <translation type="vanished">Nessun utente inserito!</translation>
    </message>
    <message>
        <source>No password given! </source>
        <translation type="vanished">Nessuna password inserita!</translation>
    </message>
    <message>
        <source>No image directory given!</source>
        <translation type="vanished">Nessuna directory immagini inserita!</translation>
    </message>
    <message>
        <source>No maximum news number given!</source>
        <translation type="vanished">Nessun numero massimo di news inserito!</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">Ha funzionato!</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronologia</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversazioni</translation>
    </message>
</context>
<context>
    <name>ContactComponent</name>
    <message>
        <location filename="../qml/genericqml/ContactComponent.qml" line="42"/>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
</context>
<context>
    <name>ContactDetailsComponent</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">Connetti</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Descrizione</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Località</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation type="vanished">Messaggi</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="vanished">URL</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation type="vanished">Creato il</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="49"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="50"/>
        <source>minute</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="51"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="52"/>
        <source>hour</source>
        <translation>ora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="53"/>
        <source>hours</source>
        <translation>ore</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="54"/>
        <source>day</source>
        <translation>giorno</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="55"/>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="56"/>
        <source>month</source>
        <translation>mese</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="57"/>
        <source>months</source>
        <translation>mesi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="58"/>
        <source>years</source>
        <translation>anni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="67"/>
        <source>likes this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="68"/>
        <source>like this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="75"/>
        <source>will attend.</source>
        <translation>attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>Persone che attendono.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="79"/>
        <source>will not attend.</source>
        <translation>non aspettare.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>Persone che non aspettano.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="83"/>
        <source>may attend.</source>
        <translation>puoi attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>Persone che possono attendere.</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Connetti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="219"/>
        <source>Approve</source>
        <translation>Approvare</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="234"/>
        <source>Reject</source>
        <translation>Rifiutare</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="249"/>
        <source>Ignore</source>
        <translation>Ignorare</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="264"/>
        <source>Follow</source>
        <translation>Seguire</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="281"/>
        <source>Unfollow</source>
        <translation>Non seguire</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="298"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="313"/>
        <source>Unblock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Location</source>
        <translation>Località</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Posts</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="351"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="352"/>
        <source>Created at</source>
        <translation>Creato il</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="353"/>
        <source>Followers</source>
        <translation>Seguaci</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="354"/>
        <source>Following</source>
        <translation>Seguente</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="364"/>
        <source>Network Error</source>
        <translation>Errore di rete</translation>
    </message>
</context>
<context>
    <name>ContactsSearchPage</name>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="62"/>
        <source>Network Error</source>
        <translation>Errore di rete</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="91"/>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="93"/>
        <source>Person</source>
        <translation>Persona</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="49"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="50"/>
        <source>minute</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="51"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="52"/>
        <source>hour</source>
        <translation>ora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="53"/>
        <source>hours</source>
        <translation>ore</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="54"/>
        <source>day</source>
        <translation>giorno</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="55"/>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="56"/>
        <source>month</source>
        <translation>mese</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="57"/>
        <source>months</source>
        <translation>mesi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="58"/>
        <source>years</source>
        <translation>anni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="67"/>
        <source>likes this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="68"/>
        <source>like this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="75"/>
        <source>will attend.</source>
        <translation>attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>Persone che attendono.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="79"/>
        <source>will not attend.</source>
        <translation>non aspettare.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>Persone che non aspettano.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="83"/>
        <source>may attend.</source>
        <translation>puoi attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>Persone che possono attendere.</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponent</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="60"/>
        <source>Refresh</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="79"/>
        <source>Timeline</source>
        <translation>Cronologia</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="97"/>
        <source>Conversations</source>
        <translation>Conversazioni</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="117"/>
        <source>Replies</source>
        <translation>Risposte</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="135"/>
        <source>Direct Messages</source>
        <translation>Messaggio diretto</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="151"/>
        <source>Favorites</source>
        <translation>Favoriti</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="169"/>
        <source>Public Timeline</source>
        <translation>Cronologia pubblica</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="187"/>
        <source>Group news</source>
        <translation>Notizie del gruppo</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="205"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="223"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponentContacts</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="61"/>
        <source>Profile</source>
        <translation>Profilo</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="79"/>
        <source>Friends</source>
        <translation>Amici</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Contatti</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="97"/>
        <source>Groups</source>
        <translation>Gruppi</translation>
    </message>
</context>
<context>
    <name>EventCreate</name>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="78"/>
        <source>Start</source>
        <translation>Avviare</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="143"/>
        <source>End</source>
        <translation>Fine</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="267"/>
        <source>no end</source>
        <translation>senza fine</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="292"/>
        <source>Title (required)</source>
        <translation>Titolo (obbligatorio)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="307"/>
        <source>Event description (optional)</source>
        <translation>descrizione della data (opzionale)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="318"/>
        <source>Location (optional)</source>
        <translation>Posizione (opzionale)</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="327"/>
        <source>Publish event?</source>
        <translation>Pubblicare l&apos;evento?</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="342"/>
        <source>Create event</source>
        <translation>Creare l&apos;evento</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <location filename="../qml/calendarqml/EventCreate.qml" line="385"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <source>No event name supplied</source>
        <translation>Nessun nome di evento</translation>
    </message>
</context>
<context>
    <name>EventList</name>
    <message>
        <source>Location</source>
        <translation type="vanished">Località</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventList.qml" line="73"/>
        <source>Delete Event?</source>
        <translation type="unfinished">Cancellare la data?</translation>
    </message>
</context>
<context>
    <name>EventListItem</name>
    <message>
        <location filename="../qml/calendarqml/EventListItem.qml" line="79"/>
        <source>Location</source>
        <translation>Località</translation>
    </message>
</context>
<context>
    <name>FriendsListTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="62"/>
        <source>Friend Requests</source>
        <translation>Richieste di contatto</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="72"/>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Friends</source>
        <translation>Amici</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>All</source>
        <translation>Tutti</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FriendsTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="73"/>
        <source>Me</source>
        <translation>Me</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="78"/>
        <source>Friends</source>
        <translation>Amici</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Contatti</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="83"/>
        <source>Groups</source>
        <translation>Gruppi</translation>
    </message>
</context>
<context>
    <name>ImageUploadDialog</name>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="133"/>
        <source>Upload to album</source>
        <translation>Carica su album</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">Album</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Immagine</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="277"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Upload</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Change</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source> No album name given</source>
        <translation>Nessun nome album inserito!</translation>
    </message>
</context>
<context>
    <name>LeftDrawerScrollview</name>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="55"/>
        <source>Settings</source>
        <translation>Configurazione</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="67"/>
        <source>Accounts</source>
        <translation>Conti</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="79"/>
        <source>Quit</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>MessageImageUploadDialog</name>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="258"/>
        <source>Description</source>
        <translation type="unfinished">Descrizione</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="284"/>
        <source>Upload</source>
        <translation type="unfinished">Carica</translation>
    </message>
</context>
<context>
    <name>MessageSend</name>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="219"/>
        <source>to:</source>
        <translation>a:</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="231"/>
        <source>Title (optional)</source>
        <translation>Titolo (opzionale)</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="249"/>
        <source> Drop your Content here.</source>
        <translation> Lascia qui il tuo contenuto.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="255"/>
        <source>What&apos;s on your mind?</source>
        <translation>A cosa stai pensando?</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <source>Only one attachment supported at the moment.
 Remove other attachment first!</source>
        <translation type="vanished">Solo un allegato è attualmente supportato.
 Rimuovere prima gli altri allegati!</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>No receiver supplied!</source>
        <translation>Nessun ricevitore in dotazione!</translation>
    </message>
</context>
<context>
    <name>MoreComments</name>
    <message>
        <location filename="../qml/newsqml/MoreComments.qml" line="53"/>
        <source>Show all comments</source>
        <translation>Tutti commenti</translation>
    </message>
</context>
<context>
    <name>NewsStack</name>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Errore di rete</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsStack.qml" line="286"/>
        <source>More</source>
        <translation>Ancora</translation>
    </message>
</context>
<context>
    <name>NewsTab</name>
    <message>
        <source>Download profile image for </source>
        <translation type="vanished">Download immagine profilo per </translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">Ancora</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronologia</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errore</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoriti</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversazioni</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation type="vanished">Messaggio diretto</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="vanished">Notifiche</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Gruppi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="75"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="76"/>
        <source>minute</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="77"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="78"/>
        <source>hour</source>
        <translation>ora</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="79"/>
        <source>hours</source>
        <translation>ore</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="80"/>
        <source>day</source>
        <translation>giorno</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="81"/>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="82"/>
        <source>month</source>
        <translation>mese</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="83"/>
        <source>months</source>
        <translation>mesi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="84"/>
        <source>years</source>
        <translation>anni</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="93"/>
        <source>likes this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="94"/>
        <source>like this.</source>
        <translation>mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="97"/>
        <source>doesn&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="98"/>
        <source>don&apos;t like this.</source>
        <translation>non mi piace.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="101"/>
        <source>will attend.</source>
        <translation>attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="102"/>
        <source>persons will attend.</source>
        <translation>Persone che attendono.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="105"/>
        <source>will not attend.</source>
        <translation>non aspettare.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="106"/>
        <source>persons will not attend.</source>
        <translation>Persone che non aspettano.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="109"/>
        <source>may attend.</source>
        <translation>puoi attendere.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="110"/>
        <source>persons may attend.</source>
        <translation>Persone che possono attendere.</translation>
    </message>
</context>
<context>
    <name>Newsitem</name>
    <message>
        <source>attending: </source>
        <translation type="vanished">attendere: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Source: </source>
        <translation>Codice: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Direct Message</source>
        <translation>Messaggio diretto</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="133"/>
        <source>In reply to </source>
        <translation>In risposta a </translation>
    </message>
    <message>
        <source> comments</source>
        <translation type="vanished"> commenti</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="49"/>
        <source>attending</source>
        <translation>partecipare</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="124"/>
        <source>ago</source>
        <translation>fa</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="269"/>
        <source>Attending: </source>
        <translation>Attendi: </translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="vanished">Risposta</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="463"/>
        <source>DM</source>
        <translation>Messaggio diretto</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="428"/>
        <source>Repost</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="431"/>
        <source>Success!</source>
        <translation>Ha funzionato!</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="436"/>
        <source>Block contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="449"/>
        <source>Report contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="457"/>
        <source>Conversation</source>
        <translation>Conversazione</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="469"/>
        <source>Bookmark</source>
        <translation>Segnalibro</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="478"/>
        <source>Calendar Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="487"/>
        <source>Attending</source>
        <translation>Attendi</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="497"/>
        <source>yes</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="502"/>
        <source>maybe</source>
        <translation>potrebbe</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="507"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="514"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="530"/>
        <source>External</source>
        <translation>Sito web</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="70"/>
        <source>Friends</source>
        <translation>Amici</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="132"/>
        <source>Groups</source>
        <translation>Gruppi</translation>
    </message>
</context>
<context>
    <name>PhotoTab</name>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="132"/>
        <source>&apos;s images</source>
        <translation> Immagini</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="219"/>
        <source>All Images</source>
        <translation>Tutte immagini</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="225"/>
        <source>Only new</source>
        <translation>Solo nuovo</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="241"/>
        <location filename="../qml/photoqml/PhotoTab.qml" line="246"/>
        <source>Own Images</source>
        <translation>Mie immagini</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="288"/>
        <source>More</source>
        <translation>Ancora</translation>
    </message>
</context>
<context>
    <name>ProfileComponent</name>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="63"/>
        <source>profile name</source>
        <translation>nome del profilo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="64"/>
        <source>is default</source>
        <translation>è predefinito</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="65"/>
        <source>hide friends</source>
        <translation>nascondere gli amici</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="66"/>
        <source>profile photo</source>
        <translation>foto del profilo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="67"/>
        <source>profile thumb</source>
        <translation>piccola foto di profilo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="68"/>
        <source>publish</source>
        <translation>pubblicare</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="69"/>
        <source>publish in network</source>
        <translation>pubblicare in rete</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="70"/>
        <source>description</source>
        <translation>descrizione</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="71"/>
        <source>date of birth</source>
        <translation>data di nascita</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="72"/>
        <source>address</source>
        <translation>indirizzo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="73"/>
        <source>city</source>
        <translation>città</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="74"/>
        <source>region</source>
        <translation>regione</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="75"/>
        <source>postal code</source>
        <translation>codice postale</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="76"/>
        <source>country</source>
        <translation>paese</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="77"/>
        <source>hometown</source>
        <translation>città natale</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="78"/>
        <source>gender</source>
        <translation>genere</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="79"/>
        <source>marital status</source>
        <translation>stato civile</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="80"/>
        <source>married with</source>
        <translation>sposato con</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="81"/>
        <source>married since</source>
        <translation>sposato da quando</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="82"/>
        <source>sexual</source>
        <translation>orientamento sessuale</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="83"/>
        <source>politics</source>
        <translation>politica</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="84"/>
        <source>religion</source>
        <translation>religione</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="85"/>
        <source>public keywords</source>
        <translation>parole chiave pubbliche</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="86"/>
        <source>private keywords</source>
        <translation>parole chiave private</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="87"/>
        <source>likes</source>
        <translation>ama</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="88"/>
        <source>dislikes</source>
        <translation>non piace</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="89"/>
        <source>about</source>
        <translation>su</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="90"/>
        <source>music</source>
        <translation>musica</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="91"/>
        <source>book</source>
        <translation>libro</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="92"/>
        <source>tv</source>
        <translation>tv</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="93"/>
        <source>film</source>
        <translation>film</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="94"/>
        <source>interest</source>
        <translation>interesse</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="95"/>
        <source>romance</source>
        <translation>romanticismo</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="96"/>
        <source>work</source>
        <translation>lavoro</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="97"/>
        <source>education</source>
        <translation>educazione</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="98"/>
        <source>social networks</source>
        <translation>reti sociali</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="99"/>
        <source>homepage</source>
        <translation>homepage</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="100"/>
        <source>other</source>
        <translation>altri</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="193"/>
        <source>Update</source>
        <translation>Aggiornare</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="227"/>
        <source>profile id</source>
        <translation>profilo id</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Location</source>
        <translation>Località</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Posts</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="254"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="255"/>
        <source>Created at</source>
        <translation>Creato il</translation>
    </message>
</context>
<context>
    <name>ReportUser</name>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="41"/>
        <source>Report contact?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="65"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>illegal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>spam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>violation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmileyDialog</name>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="64"/>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="68"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="72"/>
        <source>Addon</source>
        <translation>Addon</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="77"/>
        <source>Adult</source>
        <translation>XXX</translation>
    </message>
</context>
<context>
    <name>SyncComponent</name>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="57"/>
        <source>sync</source>
        <translation>sync</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="76"/>
        <source>notify</source>
        <translation>notificare</translation>
    </message>
</context>
<context>
    <name>SyncConfig</name>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="46"/>
        <source>Sync Interval (0=None)</source>
        <translation>Intervallo (0=nessuno)</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="86"/>
        <source>Min.</source>
        <translation>Min.</translation>
    </message>
</context>
<context>
    <name>friendiqa</name>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Aggiorna</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Cronologia</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Conversazioni</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Favoriti</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Risposte</translation>
    </message>
    <message>
        <source>Public Timeline</source>
        <translation type="vanished">Cronologia pubblica</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Notizie del gruppo</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Cerca</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Configurazione</translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation type="vanished">Conti</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Chiudi</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="177"/>
        <source>Background Sync
 Rightclick or Middleclick to Quit</source>
        <translation>Sincronizzazione dello sfondo
Fare clic con il tasto destro del mouse o con il tasto centrale per uscire</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="294"/>
        <source>Click to open Friendiqa</source>
        <translation>Clicca per aprire Friendiqa</translation>
    </message>
</context>
<context>
    <name>newsworker</name>
    <message>
        <source>likes this.</source>
        <translation type="vanished">mi piace.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation type="vanished">mi piace.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation type="vanished">non mi piace.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation type="vanished">non mi piace.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation type="vanished">attendere.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation type="vanished">Persone che attendono.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation type="vanished">non aspettare.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation type="vanished">Persone che non aspettano.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation type="vanished">puoi attendere.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation type="vanished">Persone che possono attendere.</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="53"/>
        <source>yes</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="54"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="55"/>
        <source>maybe</source>
        <translation>potrebbe</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">secondi</translation>
    </message>
    <message>
        <source>ago</source>
        <translation type="vanished">fa</translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="vanished">minuti</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="vanished">minuti</translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="vanished">ora</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="vanished">ore</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">giorno</translation>
    </message>
    <message>
        <source>days</source>
        <translation type="vanished">giorni</translation>
    </message>
    <message>
        <source>month</source>
        <translation type="vanished">mese</translation>
    </message>
    <message>
        <source>months</source>
        <translation type="vanished">mesi</translation>
    </message>
    <message>
        <source>years</source>
        <translation type="vanished">anni</translation>
    </message>
</context>
<context>
    <name>service</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errore</translation>
    </message>
    <message>
        <source>Changelog</source>
        <translation type="vanished">Changelog</translation>
    </message>
    <message>
        <source>Setting view type of news has moved from  account page to config page.</source>
        <translation type="vanished">L&apos;impostazione del tipo di visualizzazione delle notizie è stata spostata dalla pagina del conto alla pagina di configurazione.</translation>
    </message>
    <message>
        <location filename="../js/service.js" line="460"/>
        <source>Undefined Array Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../js/service.js" line="463"/>
        <source>JSON status Error</source>
        <translation></translation>
    </message>
</context>
</TS>
