## Privacy Policy##


You have probably selected Friendica as your preferred social network because you have doubts about privacy on the well-known global networks. I as the developer of Friendiqa try to take privacy very seriously.

Storing some pieces of user data on the device is necessary for some app features, other apps on your device may not access this data (keeping app’s own database storage protected from other apps is guaranteed by Android). The app has been programmed so that stored information contain any personal data (only login data and id number of last seen post will be transferred to servers). Photos from the albums and contact images are stored in a public directory on the device. Other apps may access these files and the Friendiqa needs access to this directory.
To send images from the Android gallery the app needs access to picture databases on the device.
This app don’t collect any usage statistics. In addition, no information is sent from your Smartphone to me as a developer or to a website associated with me.
On first start, the website https://dir.friendica.social/servers/surprise is contacted and the selected server shown in the app.
