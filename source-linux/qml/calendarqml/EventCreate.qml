//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls 1.4 as Oldcontrols
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"
import "qrc:/qml/calendarqml"

Flickable{
    id:eventCreateBox
    property date startDate: new Date()
    property var eventInformation: ({})
    anchors.fill: parent

    contentWidth: eventRect.width; contentHeight: eventRect.height
    function formatText(count, modelData) {
        var data = count === 12 ? modelData + 1 : modelData;
        return data.toString().length < 2 ? "0" + data : data;
    }

    boundsBehavior:Flickable.StopAtBounds
    ScrollBar.vertical: ScrollBar { }
    Rectangle{
        id: eventRect
        width: root.width
        height: textColumn.height + 6*root.fontFactor*osSettings.bigFontSize
        color: Material.backgroundColor
        MButton{
            id:closeButton
            anchors.top: parent.top
            anchors.topMargin: 1*mm
            anchors.right: parent.right
            anchors.rightMargin: 1*mm
            text: "\uf057"
            onClicked:{rootstackView.pop()}
        }

        Label{
            x: 0.5*root.fontFactor*osSettings.bigFontSize
            y: 2*root.fontFactor*osSettings.bigFontSize
            width: 3*root.fontFactor*osSettings.bigFontSize
            height: root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            //verticalAlignment: TextInput.AlignBottom
            color: Material.primaryTextColor
            text:qsTr("Start")
        }

        TextField {
            id: textStartDate
            property string dateDay:(startDate.getDate()).toString().length<2?"0"+(startDate.getDate()):(startDate.getDate())
            property string dateMonth: (startDate.getMonth()+1).toString().length<2?"0"+(startDate.getMonth()+1):(startDate.getMonth()+1)
            x: 4*root.fontFactor*osSettings.bigFontSize
            y: root.fontFactor*osSettings.bigFontSize
            width: 5*root.fontFactor*osSettings.bigFontSize
            height: 2.5*root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            horizontalAlignment: TextInput.AlignRight
            text: dateDay+"-"+dateMonth+"-"+startDate.getFullYear()
            inputMask: "99-99-9999"
            validator: RegExpValidator{regExp: /^([0-2\s]?[0-9\s]|3[0-1\s])-(0[0-9\s]|1[0-2\s])-([0-9\s][0-9\s][0-9\s][0-9\s])$ / }
            font.bold: true
        }

        MButton {
            id: textStartDateDropdown
            x: 9.5*root.fontFactor*osSettings.bigFontSize
            y: root.fontFactor*osSettings.bigFontSize
            width: 2*root.fontFactor*osSettings.bigFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            text:"\uf0d7"
            onClicked:{
                cal.visible=true;
                cal.curSelection="start"
            }
        }

        TextField {
            id: textStartTime
            x: 13*root.fontFactor*osSettings.bigFontSize
            y: root.fontFactor*osSettings.bigFontSize
            width: 3*root.fontFactor*osSettings.bigFontSize
            height: 2.5*root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            inputMask: "99:99"
            text: "00:00"
            horizontalAlignment: TextInput.AlignRight
            validator: RegExpValidator{regExp: /^([0-1\s]?[0-9\s]|2[0-3\s]):([0-5\s][0-9\s])$ / }
            font.bold: true
        }

        MButton {
            id: textStartTimeDropdown
            x: 16.5*root.fontFactor*osSettings.bigFontSize
            y: root.fontFactor*osSettings.bigFontSize
            width: 2*root.fontFactor*osSettings.bigFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            text:"\uf0d7"
            onClicked:{
                onClicked: {timeTumbler.visible=true;timeTumbler.curSelection="start"}
            }
        }

        Label{
            x: 0.5*root.fontFactor*osSettings.bigFontSize
            y: 4*root.fontFactor*osSettings.bigFontSize
            width: 3*root.fontFactor*osSettings.bigFontSize
            height: root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            color: Material.primaryTextColor
            text:qsTr("End")
        }
        TextField {
            id: textEndDate
            x: 4*root.fontFactor*osSettings.bigFontSize
            y: 3*root.fontFactor*osSettings.bigFontSize
            width: 5*root.fontFactor*osSettings.bigFontSize
            height: 2.5*root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            horizontalAlignment: TextInput.AlignRight
            inputMask: "99-99-9999"
            validator: RegExpValidator{regExp: /^([0-2\s]?[0-9\s]|3[0-1\s])-(0[0-9\s]|1[0-2\s])-([0-9\s][0-9\s][0-9\s][0-9\s])$ / }
            enabled: false
            font.bold: true
        }

        MButton {
            id: textEndDateDropdown
            x: 9.5*root.fontFactor*osSettings.bigFontSize
            y: 3*root.fontFactor*osSettings.bigFontSize
            width: 2*root.fontFactor*osSettings.bigFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            enabled: false
            text:"\uf0d7"
            onClicked:{
                cal.visible=true;
                cal.curSelection="end"
            }
        }

        TextField {
            id: textEndTime
            x: 13*root.fontFactor*osSettings.bigFontSize
            y: 3*root.fontFactor*osSettings.bigFontSize
            width: 3*root.fontFactor*osSettings.bigFontSize
            height: 2.5*root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.systemFontSize
            enabled: false
            horizontalAlignment: TextInput.AlignRight
            inputMask: "99:99"
            validator: RegExpValidator{regExp: /^([0-1\s]?[0-9\s]|2[0-3\s]):([0-5\s][0-9\s])$ / }
            font.bold: true
        }

        MButton {
            id: textEndTimeDropdown
            x: 16.5*root.fontFactor*osSettings.bigFontSize
            y: 3*root.fontFactor*osSettings.bigFontSize
            width: 2*root.fontFactor*osSettings.bigFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            enabled: false
            text:"\uf0d7"
            onClicked:{
                onClicked: {timeTumbler.visible=true;timeTumbler.curSelection="end"}
            }
        }

        Column{
            id: textColumn
            x: 4*root.fontFactor*osSettings.bigFontSize
            y: 6*root.fontFactor*osSettings.bigFontSize
            width: parent.width-7*root.fontFactor*osSettings.bigFontSize
            Oldcontrols.Calendar{
                id:cal
                property string curSelection: "start"
                width: 12*root.fontFactor*osSettings.bigFontSize
                height: 15*root.fontFactor*osSettings.bigFontSize
                visible: false
                selectedDate: new Date()
                onClicked:  {
                    if (curSelection=="start"){
                        textStartDate.text=Qt.formatDate(cal.selectedDate, "dd-MM-yyyy");
                    }else{
                        textEndDate.text=Qt.formatDate(cal.selectedDate, "dd-MM-yyyy");
                    }
                    cal.visible=false
                }
            }

            Frame {
                id: timeTumbler
                width: 12*root.fontFactor*osSettings.bigFontSize
                height: 10*root.fontFactor*osSettings.bigFontSize
                visible: false
                property string curSelection: "start"

                Row {
                    Tumbler {
                        id: hoursTumbler
                        model: 24
                        delegate: tumblerDelegateComponent
                        currentIndex: 12
                    }
                    Tumbler {
                        id: minutesTumbler
                        model: 60
                        delegate: tumblerDelegateComponent
                    }
                }
                MButton {
                    id: timeInputfinished
                    width: 2*root.fontFactor*osSettings.bigFontSize
                    height: 2*root.fontFactor*osSettings.bigFontSize
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter

                    text:"\uf00c"
                    onClicked:{
                        if (timeTumbler.curSelection=="start"){
                            textStartTime.text=formatText(24,hoursTumbler.currentIndex)+":"+formatText(60,minutesTumbler.currentIndex);
                        }else{
                            textEndTime.text=formatText(24,hoursTumbler.currentIndex)+":"+formatText(60,minutesTumbler.currentIndex);
                        }
                        timeTumbler.visible=false
                    }
                }
            }

            CheckBox{
                id: checkNoEndTime
                width: 12*root.fontFactor*osSettings.bigFontSize
                height: 2.5*root.fontFactor*osSettings.bigFontSize
                checked: true
                font.pointSize: osSettings.systemFontSize
                text: qsTr("no end")
                onCheckedChanged: {
                    if(checked==true){
                        textEndDate.enabled=false;
                        textEndDateDropdown.enabled=false;
                        textEndTime.enabled=false;
                        textEndTimeDropdown.enabled=false;
                        textEndDate.text="";
                        textEndTime.text=""
                    }else{
                        textEndDate.enabled=true;
                        textEndDateDropdown.enabled=true;
                        textEndTime.enabled=true;
                        textEndTimeDropdown.enabled=true;
                        textEndDate.text=textStartDate.text;
                        textEndTime.text=textStartTime.text
                    }
                }
            }

            TextField {
                id: titleField
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                font.pointSize: osSettings.systemFontSize
                font.bold: true
                placeholderText: qsTr("Title (required)")
            }

            Rectangle{
                color: Material.backgroundColor
                radius: 0.5*mm
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                height:Math.max(bodyField.contentHeight+root.fontFactor*osSettings.bigFontSize,2.5*root.fontFactor*osSettings.bigFontSize)
                TextArea {
                    id: bodyField
                    anchors.fill: parent
                    font.pointSize: osSettings.systemFontSize
                    font.family: "Noto Sans"
                    wrapMode: Text.Wrap
                    selectByMouse: true
                    placeholderText: qsTr("Event description (optional)")
                    textFormat: TextEdit.PlainText
                    text: eventInformation.hasOwnProperty("text")?eventInformation.text:""
                    onLinkActivated:{Qt.openUrlExternally(link)}
                }
            }

            TextField {
                id: locationField
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                font.pointSize: osSettings.systemFontSize
                placeholderText: qsTr("Location (optional)")
            }

            CheckBox{
                id: chkbxPublish
                width: 10*root.fontFactor*osSettings.bigFontSize
                height: 2.5*root.fontFactor*osSettings.bigFontSize
                checked: true
                font.pointSize: osSettings.systemFontSize
                text: qsTr("Publish event?")
            }

            BusyIndicator{
                id: eventCreateBusy
                anchors.horizontalCenter: eventCreateBox.horizontalCenter
                anchors.top:eventCreateBox.top
                anchors.topMargin: 2*root.fontFactor*osSettings.bigFontSize
                width:3*root.fontFactor*osSettings.bigFontSize
                height: 3*root.fontFactor*osSettings.bigFontSize
                running: false
            }

            MButton{
                id:createEventButton
                text: qsTr("Create event")
                onClicked:{
                    let startdatetext=textStartDate.getText(0,textStartDate.length);
                    let startdate=new Date(startdatetext.substring(6,10)+"-"+startdatetext.substring(3,5)+"-"+startdatetext.substring(0,2)+"T"+textStartTime.text)

                    if (titleField.text==""){
                        Helperjs.showMessage(qsTr("Error"),qsTr("No event name supplied"),eventCreateBox)
                    }else{
                        let startdatetext=textStartDate.getText(0,textStartDate.length);
                        let startdate=new Date(startdatetext.substring(6,10)+"-"+startdatetext.substring(3,5)+"-"+startdatetext.substring(0,2)+"T"+textStartTime.text)
                        eventCreateBusy.running=true;
                        xhr.setLogin(login.username+":"+Qt.atob(login.password));
                        xhr.setUrl(login.server);
                        xhr.setApi("/api/friendica/event_create");
                        xhr.clearParams();
                        xhr.setParam("name", titleField.text);
                        xhr.setParam("start_time",startdate.toISOString())
                        if(!checkNoEndTime.checked){
                            let enddatetext=textEndDate.getText(0,textEndDate.length);
                            let enddate=new Date(enddatetext.substring(6,10)+"-"+enddatetext.substring(3,5)+"-"+enddatetext.substring(0,2)+"T"+textEndTime.text)
                            xhr.setParam("end_time",enddate.toISOString())
                        }
                        xhr.setParam("name",titleField.text)
                        if (bodyField.text!=""){xhr.setParam("desc",bodyField.text)}
                        if (locationField.text!=""){xhr.setParam("place",locationField.text)}
                        xhr.setParam("publish",chkbxPublish.checked)
                        xhr.post();
                    }
                }
            }
            Connections{
                target: xhr
                function onSuccess(text,api){
                    if (api=="/api/friendica/event_create"){
                        updatenews.setDatabase();
                        updatenews.login();
                        updatenews.setSyncAll(false);
                        updatenews.events();
                        try{while(rootstackView.depth>1){rootstackView.pop()}}catch(e){}
                    }
                }
                function onError(text,api){
                    if (api=="/api/friendica/event_create"){
                        Helperjs.showMessage(qsTr("Error"),text,root)
                    }
                }
            }
        }

        Component {
            id: tumblerDelegateComponent
            Label {
                text: formatText(Tumbler.tumbler.count, modelData)
                opacity: 1.0 - Math.abs(Tumbler.displacement) / (Tumbler.tumbler.visibleItemCount / 2)
                color:Material.primaryTextColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: osSettings.systemFontSize
            }
        }
    }
}
