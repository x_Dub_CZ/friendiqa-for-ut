//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

function showFriends(db) {
    Service.readActiveConfig(db,function(login){
        Service.requestFriends(login.url,login.user,login.password,displayFriends);
    });
}

function displayFriends(obj){
     for (var i=0; i<obj.length; i++){
                //print(obj[i]);
                if (obj[i]) {friendsModel.append({"friendName": obj[i]});
                };
         
}}

function ensureVisibility(c,f)
{
    if (f.contentX >= c.x)
        f.contentX = c.x;
    else if (f.contentX+f.width <= c.x+c.width)
        f.contentX = c.x+c.width-f.width;
    if (f.contentY >= c.y)
        f.contentY = c.y;
    else if (f.contentY+f.height <= c.y+c.height)
        f.contentY = c.y+c.height-f.height;
}

function createObject(objectQml,qmlParameters,parentitem,callback) {
   var component = Qt.createComponent(objectQml);
    if (component.status === Component.Ready || component.status === Component.Error)
       finishCreation(component,qmlParameters,parentitem,callback);
    else
       component.statusChanged.connect(finishCreation(qmlParameters)); 
} 

function finishCreation(component,qmlParameters,parentitem,callback) {
    if (component.status === Component.Ready) {
       var createdObject = component.createObject(parentitem, qmlParameters);
   if (createdObject === null)
    print("Error creating image"); }
  else if (component.status === Component.Error)
    print("Error loading component:"+component.errorString());
    else {print("created")}
    //callback(createdObject);
}

