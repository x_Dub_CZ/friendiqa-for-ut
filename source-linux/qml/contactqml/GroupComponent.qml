//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/news.js" as Newsjs
import "qrc:/qml/genericqml"

Item {
    id: groupComponent
    height: 5*root.fontFactor*osSettings.bigFontSize//8*mm
    width: groupComponent.ListView.view.width
    property var groupmembers:[]
    //property bool newGroup: false
    function groupModelAppend(groupcontacts,callback){
       for (var n in groupcontacts){
        groupModel.append({"groupmember":groupcontacts[n]});}
        callback()
    }

    Rectangle {
        id: wrapper
        x:0.5*mm
        y:0.5*mm
        width:parent.width-mm
        height: parent.height-mm
        radius: 0.5*mm
        border.color: "grey"
        color:Material.backgroundColor

        Rectangle{
            id:namelabelRect
            x: 1
            y: 1
            width: wrapper.width-2
            height: 3*root.fontFactor*osSettings.bigFontSize
            //border.color: "light grey"
            color:Material.backgroundColor
                TextInput {
                    id: namelabel
                    anchors.fill: parent
                    readOnly: true
                    text: group.new?"":group.groupname
                    color: Material.secondaryTextColor//"#303030"
                    font.pointSize: osSettings.bigFontSize
            }
        }
        
       MButton{
            id: closeButton
            visible: false
            anchors.right: parent.right
            anchors.margins: mm
            anchors.top: parent.top
            text: "\uf057"
            font.pointSize: osSettings.bigFontSize
            onClicked:{groupComponent.state="";
                if (group.new){groupsModel.remove(index)}
            }
        }
        
        MButton{
            id:infobutton
            text:"?"
            font.pointSize: osSettings.bigFontSize
            anchors.right: parent.right
            anchors.rightMargin: mm
            anchors.topMargin: mm
            anchors.top: parent.top
            onClicked:{
                groupComponent.state="large";
                //if(group.new){
                    Helperjs.readField("members",root.db,"groups",root.login.username,function(groups){
                    try {groupModel.clear()}catch (e){print(e)}
                    groupmembers=JSON.parse(groups);
                    for (var user in groupmembers){
                        Helperjs.readData(root.db,"contacts",root.login.username,function(userdata){
                            if (userdata[0]){
                                userdata[0].name=Qt.atob(userdata[0].name);
                                userdata[0].description=Qt.atob(userdata[0].description)
                                //groupModel.append({"groupmember":userdata[0]
                                groupModel.append({"contact":userdata[0]
                            })}
                        },"id",groupmembers[user])
                    } //catch(e){}
                },"groupname",group.groupname);
                //}

            }
        }

        Rectangle{
            id: detailsrectangle
            anchors.top: namelabelRect.bottom
            color: Material.backgroundColor
            //anchors.topMargin: mm
            x:mm
            width: parent.width-2*mm
            height:parent.height -(3*root.fontFactor*osSettings.bigFontSize+mm)
            visible: false

            ListView{
                id: groupListView
                anchors.fill: parent
                anchors.margins: mm
                clip: true
                spacing: 2
                model: groupModel
                delegate: ContactComponent { }// groupMember
                function processContactSelection(contactobject){showContactdetails(contactobject)}
            }
            ListModel{id: groupModel}
        }
    Component.onCompleted:{if(group.new){groupComponent.state="large"}}
    }

    states: [
        State {
            name: "large"
            PropertyChanges { target: groupComponent; height:  groupsView.height - 6*root.fontFactor*osSettings.bigFontSize }
            PropertyChanges { target: namelabel; font.pointSize: 1.2*osSettings.bigFontSize; readOnly:false}
            PropertyChanges { target: closeButton; visible: true}
            PropertyChanges { target: detailsrectangle; visible:true  }
            PropertyChanges { target: infobutton; visible: false}
            }
    ]
}
