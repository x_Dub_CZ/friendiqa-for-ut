//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtMultimedia 5.15
import QtQuick 2.11
import QtQuick.Controls 2.12
import "qrc:/qml/genericqml"

Page{
    id:newsvideofullscreen
    //color:"black"
    //border.color: "light grey"
    width:root.width;
    height:root.height-3*root.fontFactor*osSettings.bigFontSize
    property alias source:video.source
    Text{
        id:noticeText
        text:""
        color:"light grey"
        width:parent.width/2
        wrapMode: Text.Wrap
        font.pointSize: osSettings.bigFontSize
        x:parent.width/2-parent.height/4
        y:parent.height/5
        visible: video.playbackState!=MediaPlayer.PlayingState
    }

    Video {id:video;
        anchors.fill:parent
        property string mimetype:""
        onErrorChanged:{noticeText.font.pointSize=osSettings.bigFontSize;noticeText.text=errorString;}
        fillMode: Image.PreserveAspectFit;
        autoLoad: true
        autoPlay: true
        audioRole: MediaPlayer.VideoRole
//        MouseArea {
//            anchors.fill:parent;
//            onClicked:{
//                rootstackView.pop()
//            }
//        }
    }

    ProgressBar{
        id: videoProgress
        width: parent.width
        height: 2*mm
        anchors.bottom: video.bottom
        z:2
        visible:video.playbackState!=MediaPlayer.StoppedState
        value: video.position/video.duration

        MouseArea {
            anchors.fill:parent;
            onClicked:{
                if(video.playbackState!=MediaPlayer.PlayingState){
                    video.play()} else{video.pause()
                }
            }
        }
    }

    ProgressBar{
        id: videoBuffer
        width: parent.width
        height: 2*mm
        anchors.bottom: video.bottom
        visible:video.playbackState!=MediaPlayer.StoppedState
        value: video.bufferProgress
    }

    MButton {
            id: closeButton
            z:2
            anchors.top: parent.top
            anchors.topMargin: 1*mm
            anchors.right: parent.right
            anchors.rightMargin: 1*mm
            text: "\uf057"
            onClicked: {if (rootstackView.depth>1){ rootstackView.pop()}
            }
    }


//            Slider{ id: videoSlider
//                width: parent.width
//                height: 3*mm
//                anchors.top: video.bottom
//                visible:video.playbackState!=MediaPlayer.StoppedState && video.seekable
//                value: video.position/video.duration
//                onPressed:video.seek(value*video.duration)
//    }
}
