//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import QtQuick.Controls 2.12
import "qrc:/qml/photoqml"

Package {
   Item { id: stackItem; Package.name: 'stack'; z: stackItem.PathView.z;width:16.5*mm;height:16.5*mm}
    Item { id: listItem; Package.name: 'list'; width: root.width-1*mm; height: root.height-8*mm; }
    Item { id: gridItem; Package.name: 'grid';}

    Item {
      id: photoWrapper
      width: 16.5*mm; height: 16.5*mm
      z: stackItem.PathView.z
      property string hqphotolink: photoLink
      property string imageId:""

        Rectangle {
           id: placeHolder
           color: 'lightblue'; antialiasing: true
           anchors.fill:parent
        }
        BusyIndicator { anchors.centerIn: parent; running: realImage.status != Image.Ready }
        Image {
            id: realImage;
            visible: (albumWrapper.state != '')||(index==0)
            width: photoWrapper.width; height: photoWrapper.height
            antialiasing: true;
            asynchronous: true
            autoTransform:true
            cache: false
            fillMode: Image.PreserveAspectFit;
            source: (albumWrapper.state == '')&&(index>0)?"":imageLocation
        }
        Rectangle{
            id:phototextRectangle
            color:"black"
            z:3
            opacity: 0.5
            width:phototext.contentWidth
            height: phototext.contentHeight
            anchors.bottom: photoWrapper.bottom
        }
        Text {
            id:phototext
            z:4
            text: photoDescription.trim()
            width:15*mm
            anchors.bottom: photoWrapper.bottom
            color: "white"
            font.pointSize: osSettings.systemFontSize
            wrapMode:Text.Wrap
        }
        MouseArea {
            width: realImage.paintedWidth; height: realImage.paintedHeight; anchors.centerIn: realImage
             onPressAndHold:{
                 var menuString="import QtQuick 2.5;import QtQuick.Controls 2.12; "+
                     "Menu {MenuItem {text:qsTr('Delete on client and server'); onTriggered: {"+
                             "changeimage('delete','image','"+imageLocation+"');photoModel.remove(index)}}"+
                         "MenuItem {text:qsTr('Move to album'); onTriggered: {"+
                             "changeimage('update','image','"+imageId+"');}}"+
                         "}";
                 var imagemenuObject=Qt.createQmlObject(menuString,photoWrapper,"imagemenuOutput")
                        imagemenuObject.popup()  
             }
             onClicked: {
                if (albumWrapper.state == 'inGrid') {
                    gridItem.GridView.view.currentIndex = index;
                    albumWrapper.state = 'fullscreen';
                    listItem.ListView.view.currentIndex=index
                } else {
                    gridItem.GridView.view.currentIndex = index;
                    albumWrapper.state = 'inGrid'
                }
            }
        }
        PinchArea  {
            id:imagePinch
            pinch.target: realImage
            anchors.fill: realImage
            pinch.minimumScale: 0.1
            pinch.maximumScale: 10
            enabled: false
        }

        // onStateChanged: print("State"+photoWrapper.state+index)
        states: [
            State {
                name: 'stacked'; when: albumWrapper.state == ''
                ParentChange { target: photoWrapper; parent: stackItem; }//x: 1*mm; y: 1*mm }
                PropertyChanges { target: photoWrapper; opacity: stackItem.PathView.onPath ? 1.0 : 0.0 }
                PropertyChanges { target: phototext; opacity: 0.0 }
                PropertyChanges { target: phototextRectangle; opacity: 0.0 }
            },
            State {
                name: 'inGrid'; when: albumWrapper.state == 'inGrid'
                ParentChange { target: photoWrapper; parent: gridItem; x: 1*mm; y: 1*mm;}
                PropertyChanges { target: phototext; opacity: 1.0 }
                PropertyChanges { target: phototextRectangle; opacity: 0.5 }
                PropertyChanges { target: placeHolder; opacity: 1.0 }
            },
            State {
                name: 'fullscreen'; when: albumWrapper.state == 'fullscreen'
                ParentChange {
                    target: photoWrapper; parent: listItem; x: 1; y: 1;
                    width: root.width-mm; height: root.height-8*mm
                }
                PropertyChanges { target: placeHolder; opacity: 0.0 }
                PropertyChanges { target: realImage; source: photoWrapper.hqphotolink}
                PropertyChanges { target: phototext; anchors.bottom: realImage.bottom}
                PropertyChanges { target: phototext; width:realImage.width }
                PropertyChanges { target: phototextRectangle; anchors.bottom: realImage.bottom }
                PropertyChanges { target: imagePinch; enabled:true}
               // PropertyChanges { target: realImage; width: Math.min(listItem.width,sourceSize.width);height: Math.min(listItem.height,sourceSize.height) }
            }
        ]
    }
}
