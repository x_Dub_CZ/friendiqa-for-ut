<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu">
<context>
    <name>AcceptRules</name>
    <message>
        <location filename="../qml/configqml/AcceptRules.qml" line="41"/>
        <source>Accept instance rules</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountPage</name>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="64"/>
        <location filename="../qml/configqml/AccountPage.qml" line="206"/>
        <location filename="../qml/configqml/AccountPage.qml" line="228"/>
        <location filename="../qml/configqml/AccountPage.qml" line="245"/>
        <location filename="../qml/configqml/AccountPage.qml" line="306"/>
        <location filename="../qml/configqml/AccountPage.qml" line="385"/>
        <location filename="../qml/configqml/AccountPage.qml" line="406"/>
        <source>User</source>
        <translation>Felhasználó</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Kiszolgáló</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="229"/>
        <source>Nickname</source>
        <translation>Becenév</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="248"/>
        <source>Password</source>
        <translation>Jelszó</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="255"/>
        <source>Image dir.</source>
        <translation>Képkönyvtár</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Hírek mint</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="208"/>
        <source>Instance rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <location filename="../qml/configqml/AccountPage.qml" line="354"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="233"/>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation>A @ szimbólumot tartalmazó becenevek jelenleg nem támogatottak</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="304"/>
        <source>Confirm</source>
        <translation>Megerősítés</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="311"/>
        <source>No server given! </source>
        <translation>Nincs kiszolgáló megadva! </translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="312"/>
        <source>No nickname given! </source>
        <translation>Nincs becenév megadva! </translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="313"/>
        <source>No password given! </source>
        <translation>Nincs jelszó megadva! </translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="314"/>
        <source>No image directory given!</source>
        <translation>Nincs képkönyvtár megadva!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="322"/>
        <location filename="../qml/configqml/AccountPage.qml" line="351"/>
        <source>Wrong password or 2FA enabled!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">Hibás jelszó!</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Success</source>
        <translation>Sikeres</translation>
    </message>
    <message>
        <location filename="../qml/configqml/AccountPage.qml" line="348"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Idővonal</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Beszélgetések</translation>
    </message>
</context>
<context>
    <name>BlockUser</name>
    <message>
        <location filename="../qml/newsqml/BlockUser.qml" line="41"/>
        <source>Block contact?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarTab</name>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="154"/>
        <source>Delete Event?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="201"/>
        <source>Events</source>
        <translation>Események</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/CalendarTab.qml" line="206"/>
        <source>Own Calendar</source>
        <translation>Saját naptár</translation>
    </message>
</context>
<context>
    <name>ConfigAppearancePage</name>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="46"/>
        <source>News as</source>
        <translation>Hírek mint</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="60"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="78"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="79"/>
        <source>Conversations</source>
        <translation>Beszélgetések</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="72"/>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="73"/>
        <source>Timeline</source>
        <translation>Idővonal</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="86"/>
        <source>Max. News</source>
        <translation>Legtöbb hír</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="121"/>
        <source>Hide #nsfw?</source>
        <translation>A #NSFW elrejtése?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="159"/>
        <source>Dark Mode</source>
        <translation>Sötét mód</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="163"/>
        <source>System</source>
        <translation>Rendszer</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="174"/>
        <source>Dark</source>
        <translation>Sötét</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigAppearancePage.qml" line="185"/>
        <source>Light</source>
        <translation>Világos</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <source>News as</source>
        <translation type="vanished">Hírek mint</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Beszélgetések</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Idővonal</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Legtöbb hír</translation>
    </message>
    <message>
        <source>Hide #nsfw?</source>
        <translation type="vanished">A #NSFW elrejtése?</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="55"/>
        <source>Appearance</source>
        <translation>Megjelenés</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="61"/>
        <source>Sync</source>
        <translation>Szinkronizálás</translation>
    </message>
    <message>
        <location filename="../qml/configqml/ConfigPage.qml" line="67"/>
        <source>Start</source>
        <translation>Indítás</translation>
    </message>
</context>
<context>
    <name>ConfigStartPage</name>
    <message>
        <location filename="../qml/configqml/ConfigStartPage.qml" line="49"/>
        <source>Autostart</source>
        <translation>Automatikus indítás</translation>
    </message>
</context>
<context>
    <name>ConfigTab</name>
    <message>
        <source>User</source>
        <translation type="vanished">Felhasználó</translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="vanished">Kiszolgáló</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="vanished">Becenév</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Jelszó</translation>
    </message>
    <message>
        <source>Image dir.</source>
        <translation type="vanished">Képkönyvtár</translation>
    </message>
    <message>
        <source>Max. News</source>
        <translation type="vanished">Legtöbb hír</translation>
    </message>
    <message>
        <source>News as</source>
        <translation type="vanished">Hírek mint</translation>
    </message>
    <message>
        <source>Interval (0=None)</source>
        <translation type="vanished">Időköz (0 = nincs)</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Hiba</translation>
    </message>
    <message>
        <source>Nickname not registered at given server!</source>
        <translation type="vanished">A becenév nincs regisztrálva a megadott kiszolgálón!</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Megerősítés</translation>
    </message>
    <message>
        <source>No server given! </source>
        <translation type="vanished">Nincs kiszolgáló megadva! </translation>
    </message>
    <message>
        <source>No nickname given! </source>
        <translation type="vanished">Nincs becenév megadva! </translation>
    </message>
    <message>
        <source>Nickname not registered at given server! </source>
        <translation type="vanished">A becenév nincs regisztrálva a megadott kiszolgálón! </translation>
    </message>
    <message>
        <source>No username given! </source>
        <translation type="vanished">Nincs felhasználónév megadva! </translation>
    </message>
    <message>
        <source>Sync Interval (0=None)</source>
        <translation type="vanished">Szinkronizálási időköz (0 = nincs)</translation>
    </message>
    <message>
        <source>Nicknames containing @ symbol currently not supported</source>
        <translation type="vanished">A @ szimbólumot tartalmazó becenevek jelenleg nem támogatottak</translation>
    </message>
    <message>
        <source>Min.</source>
        <translation type="vanished">Legkisebb</translation>
    </message>
    <message>
        <source>No password given! </source>
        <translation type="vanished">Nincs jelszó megadva! </translation>
    </message>
    <message>
        <source>No image directory given!</source>
        <translation type="vanished">Nincs képkönyvtár megadva!</translation>
    </message>
    <message>
        <source>No maximum news number given!</source>
        <translation type="vanished">Nincs legtöbb hír szám megadva!</translation>
    </message>
    <message>
        <source>Wrong password!</source>
        <translation type="vanished">Hibás jelszó!</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">Sikeres</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Név</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Idővonal</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Beszélgetések</translation>
    </message>
</context>
<context>
    <name>ContactComponent</name>
    <message>
        <location filename="../qml/genericqml/ContactComponent.qml" line="42"/>
        <source>Connect</source>
        <translation>Kapcsolódás</translation>
    </message>
</context>
<context>
    <name>ContactDetailsComponent</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">Kapcsolódás</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Leírás</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">Hely</translation>
    </message>
    <message>
        <source>Posts</source>
        <translation type="vanished">Bejegyzések</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="vanished">URL</translation>
    </message>
    <message>
        <source>Created at</source>
        <translation type="vanished">Létrehozva</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="49"/>
        <source>seconds</source>
        <translation>másodperc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="50"/>
        <source>minute</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="51"/>
        <source>minutes</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="52"/>
        <source>hour</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="53"/>
        <source>hours</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="54"/>
        <source>day</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="55"/>
        <source>days</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="56"/>
        <source>month</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="57"/>
        <source>months</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="58"/>
        <source>years</source>
        <translation>év</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="67"/>
        <source>likes this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="68"/>
        <source>like this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="75"/>
        <source>will attend.</source>
        <translation>részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>személy részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="79"/>
        <source>will not attend.</source>
        <translation>nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>személy nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="83"/>
        <source>may attend.</source>
        <translation>talán részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>személy talán részt vesz.</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Kapcsolódás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="219"/>
        <source>Approve</source>
        <translation>Jóváhagyás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="234"/>
        <source>Reject</source>
        <translation>Visszautasítás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="249"/>
        <source>Ignore</source>
        <translation>Mellőzés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="264"/>
        <source>Follow</source>
        <translation>Követés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="281"/>
        <source>Unfollow</source>
        <translation>Követés megszüntetése</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="298"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="313"/>
        <source>Unblock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="350"/>
        <source>Posts</source>
        <translation>Bejegyzések</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="351"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="352"/>
        <source>Created at</source>
        <translation>Létrehozva</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="353"/>
        <source>Followers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="354"/>
        <source>Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ContactPage.qml" line="364"/>
        <source>Network Error</source>
        <translation>Hálózati hiba</translation>
    </message>
</context>
<context>
    <name>ContactsSearchPage</name>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="62"/>
        <source>Network Error</source>
        <translation>Hálózati hiba</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="91"/>
        <source>Forum</source>
        <translation>fórum</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ContactsSearchPage.qml" line="93"/>
        <source>Person</source>
        <translation>személy</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="49"/>
        <source>seconds</source>
        <translation>másodperc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="50"/>
        <source>minute</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="51"/>
        <source>minutes</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="52"/>
        <source>hour</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="53"/>
        <source>hours</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="54"/>
        <source>day</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="55"/>
        <source>days</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="56"/>
        <source>month</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="57"/>
        <source>months</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="58"/>
        <source>years</source>
        <translation>év</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="67"/>
        <source>likes this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="68"/>
        <source>like this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="71"/>
        <source>doesn&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="72"/>
        <source>don&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="75"/>
        <source>will attend.</source>
        <translation>részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="76"/>
        <source>persons will attend.</source>
        <translation>személy részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="79"/>
        <source>will not attend.</source>
        <translation>nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="80"/>
        <source>persons will not attend.</source>
        <translation>személy nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="83"/>
        <source>may attend.</source>
        <translation>talán részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Conversation.qml" line="84"/>
        <source>persons may attend.</source>
        <translation>személy talán részt vesz.</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponent</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="60"/>
        <source>Refresh</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="79"/>
        <source>Timeline</source>
        <translation>Idővonal</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="97"/>
        <source>Conversations</source>
        <translation>Beszélgetések</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="117"/>
        <source>Replies</source>
        <translation>Válaszok</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="135"/>
        <source>Direct Messages</source>
        <translation>Közvetlen üzenetek</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="151"/>
        <source>Favorites</source>
        <translation>Kedvencek</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="169"/>
        <source>Public Timeline</source>
        <translation>Nyilvános idővonal</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="187"/>
        <source>Group news</source>
        <translation>Csoporthírek</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="205"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponent.qml" line="223"/>
        <source>Notifications</source>
        <translation>Értesítések</translation>
    </message>
</context>
<context>
    <name>DrawerAccountComponentContacts</name>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="61"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="79"/>
        <source>Friends</source>
        <translation>Ismerősök</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Partnerek</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/DrawerAccountComponentContacts.qml" line="97"/>
        <source>Groups</source>
        <translation>Csoportok</translation>
    </message>
</context>
<context>
    <name>EventCreate</name>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="78"/>
        <source>Start</source>
        <translation type="unfinished">Indítás</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="143"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="267"/>
        <source>no end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="292"/>
        <source>Title (required)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="307"/>
        <source>Event description (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="318"/>
        <source>Location (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="327"/>
        <source>Publish event?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="342"/>
        <source>Create event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <location filename="../qml/calendarqml/EventCreate.qml" line="385"/>
        <source>Error</source>
        <translation type="unfinished">Hiba</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventCreate.qml" line="348"/>
        <source>No event name supplied</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventList</name>
    <message>
        <source>Location</source>
        <translation type="vanished">Hely</translation>
    </message>
    <message>
        <location filename="../qml/calendarqml/EventList.qml" line="73"/>
        <source>Delete Event?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventListItem</name>
    <message>
        <location filename="../qml/calendarqml/EventListItem.qml" line="79"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
</context>
<context>
    <name>FriendsListTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="62"/>
        <source>Friend Requests</source>
        <translation>Barátkérések</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="72"/>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Friends</source>
        <translation>Ismerősök</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsListTab.qml" line="176"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FriendsTab</name>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="73"/>
        <source>Me</source>
        <translation>Én</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="78"/>
        <source>Friends</source>
        <translation>Ismerősök</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="vanished">Partnerek</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/FriendsTab.qml" line="83"/>
        <source>Groups</source>
        <translation>Csoportok</translation>
    </message>
</context>
<context>
    <name>GroupComponent</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Hiba</translation>
    </message>
    <message>
        <source>No name given</source>
        <translation type="vanished">Nincs név megadva</translation>
    </message>
</context>
<context>
    <name>ImageUploadDialog</name>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="133"/>
        <source>Upload to album</source>
        <translation>Feltöltés albumba</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">Album</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Kép</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="277"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Upload</source>
        <translation>Feltöltés</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="309"/>
        <source>Change</source>
        <translation>Változtatás</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/ImageUploadDialog.qml" line="312"/>
        <source> No album name given</source>
        <translation> Nincs albumnév megadva</translation>
    </message>
</context>
<context>
    <name>LeftDrawerScrollview</name>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="55"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="67"/>
        <source>Accounts</source>
        <translation>Fiókok</translation>
    </message>
    <message>
        <location filename="../qml/configqml/LeftDrawerScrollview.qml" line="79"/>
        <source>Quit</source>
        <translation>Kilépés</translation>
    </message>
</context>
<context>
    <name>MessageImageUploadDialog</name>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="258"/>
        <source>Description</source>
        <translation type="unfinished">Leírás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageImageUploadDialog.qml" line="284"/>
        <source>Upload</source>
        <translation type="unfinished">Feltöltés</translation>
    </message>
</context>
<context>
    <name>MessageSend</name>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="219"/>
        <source>to:</source>
        <translation>címzett:</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="231"/>
        <source>Title (optional)</source>
        <translation>Cím (elhagyható)</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="249"/>
        <source> Drop your Content here.</source>
        <translation> Ejtse ide a tartalmat.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="255"/>
        <source>What&apos;s on your mind?</source>
        <translation>Mire gondol?</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <source>Only one attachment supported at the moment.
 Remove other attachment first!</source>
        <translation type="vanished">Csak egyetlen melléklet támogatott jelenleg.
 Először távolítsa el a másik mellékletet.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/MessageSend.qml" line="431"/>
        <source>No receiver supplied!</source>
        <translation>Nincs fogadó megadva!</translation>
    </message>
</context>
<context>
    <name>MoreComments</name>
    <message>
        <location filename="../qml/newsqml/MoreComments.qml" line="53"/>
        <source>Show all comments</source>
        <translation>Összes hozzászólás megjelenítése</translation>
    </message>
</context>
<context>
    <name>NewsStack</name>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Hálózati hiba</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsStack.qml" line="286"/>
        <source>More</source>
        <translation>Több</translation>
    </message>
</context>
<context>
    <name>NewsTab</name>
    <message>
        <source>Download profile image for </source>
        <translation type="vanished">Profilkép letöltése ennél: </translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">Több</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Idővonal</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Hiba</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Kedvencek</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Beszélgetések</translation>
    </message>
    <message>
        <source>Network Error</source>
        <translation type="vanished">Hálózati hiba</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Válaszok</translation>
    </message>
    <message>
        <source>Public timeline</source>
        <translation type="vanished">Nyilvános idővonal</translation>
    </message>
    <message>
        <source>Direct Messages</source>
        <translation type="vanished">Közvetlen üzenetek</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="vanished">Értesítések</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Csoporthírek</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Kilépés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="75"/>
        <source>seconds</source>
        <translation>másodperc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="76"/>
        <source>minute</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="77"/>
        <source>minutes</source>
        <translation>perc</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="78"/>
        <source>hour</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="79"/>
        <source>hours</source>
        <translation>óra</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="80"/>
        <source>day</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="81"/>
        <source>days</source>
        <translation>nap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="82"/>
        <source>month</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="83"/>
        <source>months</source>
        <translation>hónap</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="84"/>
        <source>years</source>
        <translation>év</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="93"/>
        <source>likes this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="94"/>
        <source>like this.</source>
        <translation>kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="97"/>
        <source>doesn&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="98"/>
        <source>don&apos;t like this.</source>
        <translation>nem kedveli ezt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="101"/>
        <source>will attend.</source>
        <translation>részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="102"/>
        <source>persons will attend.</source>
        <translation>személy részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="105"/>
        <source>will not attend.</source>
        <translation>nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="106"/>
        <source>persons will not attend.</source>
        <translation>személy nem vesz részt.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="109"/>
        <source>may attend.</source>
        <translation>talán részt vesz.</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/NewsTab.qml" line="110"/>
        <source>persons may attend.</source>
        <translation>személy talán részt vesz.</translation>
    </message>
</context>
<context>
    <name>Newsitem</name>
    <message>
        <source>attending: </source>
        <translation type="vanished">részvétel: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Source: </source>
        <translation>Forrás: </translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="116"/>
        <source>Direct Message</source>
        <translation>Közvetlen üzenet</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="133"/>
        <source>In reply to </source>
        <translation>Válaszul erre: </translation>
    </message>
    <message>
        <source> comments</source>
        <translation type="vanished"> hozzászólás</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="49"/>
        <source>attending</source>
        <translation>részvétel</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="124"/>
        <source>ago</source>
        <translation>óta</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="269"/>
        <source>Attending: </source>
        <translation>Részvétel: </translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="vanished">Válasz</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="463"/>
        <source>DM</source>
        <translation>DM</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="428"/>
        <source>Repost</source>
        <translation>Újraküldés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="431"/>
        <source>Success!</source>
        <translation>Sikeres!</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="436"/>
        <source>Block contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="449"/>
        <source>Report contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="457"/>
        <source>Conversation</source>
        <translation>Beszélgetés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="469"/>
        <source>Bookmark</source>
        <translation>könyvjelző</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="478"/>
        <source>Calendar Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="487"/>
        <source>Attending</source>
        <translation>Részvétel</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="497"/>
        <source>yes</source>
        <translation>igen</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="502"/>
        <source>maybe</source>
        <translation>talán</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="507"/>
        <source>no</source>
        <translation>nem</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="514"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/Newsitem.qml" line="530"/>
        <source>External</source>
        <translation>weboldal</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="70"/>
        <source>Friends</source>
        <translation>Ismerősök</translation>
    </message>
    <message>
        <location filename="../qml/genericqml/PermissionDialog.qml" line="132"/>
        <source>Groups</source>
        <translation>Csoportok</translation>
    </message>
</context>
<context>
    <name>PhotoTab</name>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="132"/>
        <source>&apos;s images</source>
        <translation> képei</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="219"/>
        <source>All Images</source>
        <translation>Összes kép</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="225"/>
        <source>Only new</source>
        <translation>Csak újak</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="241"/>
        <location filename="../qml/photoqml/PhotoTab.qml" line="246"/>
        <source>Own Images</source>
        <translation>Saját képek</translation>
    </message>
    <message>
        <location filename="../qml/photoqml/PhotoTab.qml" line="288"/>
        <source>More</source>
        <translation>Több</translation>
    </message>
</context>
<context>
    <name>ProfileComponent</name>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="63"/>
        <source>profile name</source>
        <translation>profilnév</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="64"/>
        <source>is default</source>
        <translation>alapértelmezett</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="65"/>
        <source>hide friends</source>
        <translation>ismerősök elrejtése</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="66"/>
        <source>profile photo</source>
        <translation>profilfénykép</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="67"/>
        <source>profile thumb</source>
        <translation>profilbélyegkép</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="68"/>
        <source>publish</source>
        <translation>közzététel</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="69"/>
        <source>publish in network</source>
        <translation>közzététel hálózaton</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="70"/>
        <source>description</source>
        <translation>leírás</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="71"/>
        <source>date of birth</source>
        <translation>születési dátum</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="72"/>
        <source>address</source>
        <translation>cím</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="73"/>
        <source>city</source>
        <translation>település</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="74"/>
        <source>region</source>
        <translation>régió</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="75"/>
        <source>postal code</source>
        <translation>irányítószám</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="76"/>
        <source>country</source>
        <translation>ország</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="77"/>
        <source>hometown</source>
        <translation>szülőváros</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="78"/>
        <source>gender</source>
        <translation>nem</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="79"/>
        <source>marital status</source>
        <translation>családi állapot</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="80"/>
        <source>married with</source>
        <translation>házas vele</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="81"/>
        <source>married since</source>
        <translation>házas ekkortól</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="82"/>
        <source>sexual</source>
        <translation>szexuális</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="83"/>
        <source>politics</source>
        <translation>politika</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="84"/>
        <source>religion</source>
        <translation>vallás</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="85"/>
        <source>public keywords</source>
        <translation>nyilvános kulcsszavak</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="86"/>
        <source>private keywords</source>
        <translation>személyes kulcsszavak</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="87"/>
        <source>likes</source>
        <translation>kedvelések</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="88"/>
        <source>dislikes</source>
        <translation>nem kedvelések</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="89"/>
        <source>about</source>
        <translation>névjegy</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="90"/>
        <source>music</source>
        <translation>zene</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="91"/>
        <source>book</source>
        <translation>könyv</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="92"/>
        <source>tv</source>
        <translation>TV</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="93"/>
        <source>film</source>
        <translation>film</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="94"/>
        <source>interest</source>
        <translation>érdeklődés</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="95"/>
        <source>romance</source>
        <translation>romantika</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="96"/>
        <source>work</source>
        <translation>munka</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="97"/>
        <source>education</source>
        <translation>oktatás</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="98"/>
        <source>social networks</source>
        <translation>közösségi hálózatok</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="99"/>
        <source>homepage</source>
        <translation>honlap</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="100"/>
        <source>other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="193"/>
        <source>Update</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="227"/>
        <source>profile id</source>
        <translation>profilazonosító</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="253"/>
        <source>Posts</source>
        <translation>Bejegyzések</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="254"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/contactqml/ProfileComponent.qml" line="255"/>
        <source>Created at</source>
        <translation>Létrehozva</translation>
    </message>
</context>
<context>
    <name>ReportUser</name>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="41"/>
        <source>Report contact?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="65"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>illegal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>spam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/newsqml/ReportUser.qml" line="72"/>
        <source>violation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmileyDialog</name>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="64"/>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="68"/>
        <source>Standard</source>
        <translation>Szabványos</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="72"/>
        <source>Addon</source>
        <translation>Bővítmény</translation>
    </message>
    <message>
        <location filename="../qml/newsqml/SmileyDialog.qml" line="77"/>
        <source>Adult</source>
        <translation>Felnőtt</translation>
    </message>
</context>
<context>
    <name>SyncComponent</name>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="57"/>
        <source>sync</source>
        <translation>szinkronizálás</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncComponent.qml" line="76"/>
        <source>notify</source>
        <translation>értesítés</translation>
    </message>
</context>
<context>
    <name>SyncConfig</name>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="46"/>
        <source>Sync Interval (0=None)</source>
        <translation>Szinkronizálási időköz (0 = nincs)</translation>
    </message>
    <message>
        <location filename="../qml/configqml/SyncConfig.qml" line="86"/>
        <source>Min.</source>
        <translation>Legkisebb</translation>
    </message>
</context>
<context>
    <name>friendiqa</name>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Frissítés</translation>
    </message>
    <message>
        <source>Timeline</source>
        <translation type="vanished">Idővonal</translation>
    </message>
    <message>
        <source>Conversations</source>
        <translation type="vanished">Beszélgetések</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Kedvencek</translation>
    </message>
    <message>
        <source>Replies</source>
        <translation type="vanished">Válaszok</translation>
    </message>
    <message>
        <source>Public Timeline</source>
        <translation type="vanished">Nyilvános idővonal</translation>
    </message>
    <message>
        <source>Group news</source>
        <translation type="vanished">Csoporthírek</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Keresés</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Beállítások</translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation type="vanished">Fiókok</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Kilépés</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="177"/>
        <source>Background Sync
 Rightclick or Middleclick to Quit</source>
        <translation>Háttérszinkronizálás
Kilépéshez kattintson a jobb gombbal vagy középső gombbal</translation>
    </message>
    <message>
        <location filename="../qml/friendiqa.qml" line="294"/>
        <source>Click to open Friendiqa</source>
        <translation>Kattintson a Friendiqa megnyitásához</translation>
    </message>
</context>
<context>
    <name>newsworker</name>
    <message>
        <source>likes this.</source>
        <translation type="vanished">kedveli ezt.</translation>
    </message>
    <message>
        <source>like this.</source>
        <translation type="vanished">kedveli ezt.</translation>
    </message>
    <message>
        <source>doesn&apos;t like this.</source>
        <translation type="vanished">nem kedveli ezt.</translation>
    </message>
    <message>
        <source>don&apos;t like this.</source>
        <translation type="vanished">nem kedveli ezt.</translation>
    </message>
    <message>
        <source>will attend.</source>
        <translation type="vanished">részt vesz.</translation>
    </message>
    <message>
        <source>persons will attend.</source>
        <translation type="vanished">személy részt vesz.</translation>
    </message>
    <message>
        <source>will not attend.</source>
        <translation type="vanished">nem vesz részt.</translation>
    </message>
    <message>
        <source>persons will not attend.</source>
        <translation type="vanished">személy nem vesz részt.</translation>
    </message>
    <message>
        <source>may attend.</source>
        <translation type="vanished">talán részt vesz.</translation>
    </message>
    <message>
        <source>persons may attend.</source>
        <translation type="vanished">személy talán részt vesz.</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="53"/>
        <source>yes</source>
        <translation>igen</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="54"/>
        <source>no</source>
        <translation>nem</translation>
    </message>
    <message>
        <location filename="../js/newsworker.js" line="55"/>
        <source>maybe</source>
        <translation>talán</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="vanished">másodperc</translation>
    </message>
    <message>
        <source>ago</source>
        <translation type="vanished">óta</translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="vanished">perc</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="vanished">perc</translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="vanished">óra</translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="vanished">óra</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">nap</translation>
    </message>
    <message>
        <source>days</source>
        <translation type="vanished">nap</translation>
    </message>
    <message>
        <source>month</source>
        <translation type="vanished">hónap</translation>
    </message>
    <message>
        <source>months</source>
        <translation type="vanished">hónap</translation>
    </message>
</context>
<context>
    <name>service</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Hiba</translation>
    </message>
    <message>
        <source>Changelog</source>
        <translation type="vanished">Változásnapló</translation>
    </message>
    <message>
        <source>Setting view type of news has moved from  account page to config page.</source>
        <translation type="vanished">A hírek nézettípusának beállítása át lett helyezve a fiókoldalról a beállítási oldalra.</translation>
    </message>
    <message>
        <location filename="../js/service.js" line="460"/>
        <source>Undefined Array Error</source>
        <translation>Meghatározatlan tömbhiba</translation>
    </message>
    <message>
        <location filename="../js/service.js" line="463"/>
        <source>JSON status Error</source>
        <translation>JSON-állapothiba</translation>
    </message>
</context>
</TS>
