//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls.Material 2.12
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Rectangle{
    id:permissionDialog
    color: Material.backgroundColor
    //    x: mm
    width: parent.width-5*mm
    height:root.height/3
    function updatePerms(){
        for (var i=0;i<groupModel.count;i++)
        {if (groupModel.get(i).groupstatus=="positive"){
                group_allow.push(groupModel.get(i).group.gid)
            }
            if (groupModel.get(i).groupstatus=="negative"){
                group_deny.push(groupModel.get(i).group.gid)
            }
        }
        for (var j=0;j<contactModel.count;j++){
            if (contactModel.get(j).contactstatus=="positive"){
                contact_allow.push(contactModel.get(j).contact.cid)
            }
            if (contactModel.get(j).contactstatus=="negative"){
                contact_deny.push(contactModel.get(j).contact.cid)
            }
            if ((contact_allow.length==0)&&(contact_deny.length==0)&&(group_allow.length==0)&&(group_deny.length==0))
            {permButton.text="\uf09c"}
            else{permButton.text="\uf023"}
        }
    }

    Text{ //cid not working in Friendica 02/2022
        x:0.5*mm
        y:0.5*mm
        color: Material.primaryTextColor
        text: qsTr("Friends")
    }
    ListView {
        id: contactView
        x:0.5*mm
        y:5.5*mm
        width: permissionDialog.width/2-2*mm
        height: permissionDialog.height-14*mm
        clip: true
        spacing: 1
        model: contactModel
        delegate: contactItem
    }

    ListModel{id: contactModel}
    Component{
        id:contactItem
        Rectangle{
            id:contactitemRect
            color: Material.backgroundColor
            width:contactView.width
            height: 5*mm
            radius: 0.5*mm
            property string contactstatus
            onContactstatusChanged:{
                if(contactstatus=="positive"){contactitemRect.color="light green"}
                else if (contactstatus=="negative"){contactitemRect.color= "red"}
                else{contactitemRect.color= Material.backgroundColor}}
            border.color:Material.frameColor
            Text{
                color: Material.primaryTextColor
                text:contact.screen_name
            }
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    if(contactModel.get(index).contactstatus=="neutral"){
                        contactModel.set(index,{"contactstatus":"positive"});
                        contactstatus="positive"
                    }
                    else if (contactModel.get(index).contactstatus=="positive"){
                        contactModel.set(index,{"contactstatus":"negative"})
                        contactstatus="negative"
                    }
                    else{contactModel.set(index,{"contactstatus":"neutral"});
                        contactstatus="neutral";
                    }
                }}
            Component.onCompleted:{
                if (contactModel.get(index).contactstatus=="positive"){
                    contactstatus="positive"
                }
                else if (contactModel.get(index).contactstatus=="negative"){
                    contactstatus="negative"
                }
                else {contactstatus="neutral"}  }
        }
    }
    Text{
        color: Material.primaryTextColor
        x:contactView.width+2*mm
        y:0.5*mm
        text: qsTr("Groups")
    }
    ListView {
        id: groupView
        x:contactView.width+2*mm
        y:5.5*mm
        width: permissionDialog.width/2-2*mm
        height: permissionDialog.height-14*mm
        clip: true
        spacing: 1
        model: groupModel
        delegate: groupItem
    }

    ListModel{id: groupModel}
    Component{
        id:groupItem
        Rectangle{
            id:groupitemRect
            width:groupView.width
            radius: 0.5*mm
            height: 5*mm
            property string groupstatus:"neutral"
            onGroupstatusChanged:
            {if(groupstatus=="positive"){groupitemRect.color="light green"}
                else if (groupstatus=="negative"){groupitemRect.color= "red"}
                else{groupitemRect.color= Material.backgroundColor}}
            color: Material.backgroundColor
            border.color: Material.frameColor
            Text{
                color: Material.primaryTextColor
                text:group.groupname
            }
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    if(groupModel.get(index).groupstatus=="neutral"){
                        groupModel.set(index,{"groupstatus":"positive"});
                        groupstatus="positive"}
                    else if (groupModel.get(index).groupstatus=="positive"){
                        groupModel.set(index,{"groupstatus":"negative"});
                        groupstatus="negative"}
                    else{groupModel.set(index,{"groupstatus":"neutral"})
                        groupstatus="neutral"}
                }}
            Component.onCompleted:{ if (groupModel.get(index).groupstatus=="positive"){
                    groupstatus="positive"
                }
                else if (groupModel.get(index).groupstatus=="negative"){
                    groupstatus="negative"
                }
                else {groupstatus="neutral"}  }
        }
    }
    MButton{
        x:0.5*mm
        anchors.bottom: parent.bottom
        anchors.bottomMargin:1
        text:"\uf0c7"
        onClicked:{
            updatePerms();
            var perms=[];
            perms.push(contact_allow,contact_deny,group_allow,group_deny);
            Service.savePermissions(db,perms)
        }
    }
    MButton{
        x:contactView.width+2*mm
        anchors.bottom: parent.bottom
        anchors.bottomMargin:1
        text:"\u2713"
        onClicked:{
            updatePerms();
            permissionDialog.visible=false;
        }
    }

    Component.onCompleted:{
        Helperjs.readData(db,"contacts",login.username,function(contacts){
            for (var name in contacts){
                var contactstatus="neutral";
                if (contact_allow.indexOf(contacts[name].cid)>-1){contactstatus="positive"}
                else if (contact_deny.indexOf(contacts[name].cid)>-1){contactstatus="negative"}
                contactModel.append({"contact":contacts[name],"contactstatus":contactstatus})
            }},"isFriend",1,"name");

        Helperjs.readData(db,"groups",login.username,function(owngroups){
            for (var number in owngroups){
                var groupstatus= "neutral";
                if (group_allow.indexOf(owngroups[number].gid)>-1){groupstatus="positive"}
                else if (group_deny.indexOf(owngroups[number].gid)>-1){groupstatus="negative"}
                groupModel.append({"group":owngroups[number],"groupstatus":groupstatus})
            }});
    }
}
