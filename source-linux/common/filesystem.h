//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <QDir>
#include <QObject>
#include <QStandardPaths>

//#include <QtAndroidExtras>
//#include <QAndroidActivityResultReceiver>

class FILESYSTEM : public QObject//, public QAndroidActivityResultReceiver
{
    Q_OBJECT
    Q_PROPERTY(QString Directory READ Directory WRITE setDirectory NOTIFY directoryChanged)
    Q_PROPERTY(bool Visibility READ Visibility WRITE setVisibility NOTIFY visibilityChanged)
    //Q_PROPERTY(bool direxist READ direxist)
    Q_PROPERTY(QString homePath READ homePath)
    Q_PROPERTY(bool isAutostart READ isAutostart)
    //Q_PROPERTY(QString cameraPath READ cameraPath)


public:
    static FILESYSTEM *instance();
    explicit FILESYSTEM(QObject *parent = 0);
    void setDirectory(QString Directory);
    void setVisibility(bool Visibility);
    QString Directory() const;
    QFileInfoList fileList();
    //bool direxist(QString Directory);
    QString homePath() const;
    bool Visibility();
    bool isAutostart();

    //QString cameraPath() const;
   // virtual void handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data);

signals:
    //void imageselected(QString);
    void directoryChanged();
    void visibilityChanged();
    //void fileListContent(QList data);
    void success(QString data);
    void error(QString data, int code);

public slots:
    bool fileexist(QString name);
    void makeDir(QString name);
    void makePath(QString name);
    void rmDir();
    void rmFile(QString name);
    void setAutostart(bool autostart);
    //void searchImage();
    //void fileList();

private:
    QString m_Directory;
    QString homeDir;
    bool m_Visibility;
    //QList m_Filelist;
};

#endif // FILSYSTEM_H
