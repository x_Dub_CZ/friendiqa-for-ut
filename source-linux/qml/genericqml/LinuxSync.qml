import QtQuick 2.4


Item {
    Timer{
        id:syncTimer
        repeat: true
        onTriggered: {
            updatenews.setDatabase();
            updatenews.login();
            updatenews.setSyncAll(true)
            updatenews.startsync();
        }
    }

    function startSyncTimer(interval){
        syncTimer.interval=interval*60000;
        syncTimer.start()
    }

    Component.onCompleted: {
        if (root.globaloptions.hasOwnProperty("syncinterval") && root.globaloptions.syncinterval !=null && root.globaloptions.syncinterval !=0){
            startSyncTimer(root.globaloptions.syncinterval)
        }
        root.updateSyncinterval.connect(startSyncTimer)
    }
}

