//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2017 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import QtQuick.LocalStorage 2.0
import QtQuick.Window 2.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.11
import QSystemTrayIcon 1.0
import "qrc:/js/news.js" as Newsjs
import "qrc:/js/service.js" as Service
import "qrc:/qml/genericqml"
import "qrc:/qml/configqml"

ApplicationWindow{
    id:root
    title: "Friendiqa"
    property var globaloptions: Service.readGO(db)
    property QtObject osSettings: {var tmp=Qt.createComponent("qrc:/qml/configqml/OSSettingsLinux.qml");return tmp.createObject(root)}
    width: globaloptions.hasOwnProperty("appWidth")?globaloptions.appWidth:osSettings.appWidth
    height:globaloptions.hasOwnProperty("appHeight")?globaloptions.appHeight:osSettings.appHeight
    visible: filesystem.Visibility// true
    property var db: ["Friendiqa", "1.0", "Stores Friendica data", 100000000]
    property var login: Service.readActiveConfig(db)
    property real fontFactor: root.font.pixelSize/root.font.pointSize
    property var contactlist: []
    property real mm:  osSettings.osType=="Android"?Screen.pixelDensity:Screen.pixelDensity*1.5
    property bool wideScreen : width>height
    signal fotoSignal(var username, var friend)
    signal directmessageSignal(var friend)
    signal newsSignal(var news)
    signal newstypeSignal(var type)
    signal friendsSignal(var username)
    signal contactdetailsSignal(var contact)
    signal contactRefreshSignal()
    signal searchSignal (var searchterm)
    signal eventSignal(var contact)
    signal eventcreateSignal(var event)
    signal uploadSignal(var urls)
    signal sendtextSignal(var intenttext)
    signal changeimage(var method, var type, var id)
    signal updateSyncinterval(int interval)
    signal replySignal(var newsobject)
    property var news:[]
    property var newContacts:[]
    property var contactposts:[]
    property bool imagePicking: false


    Material.theme: Material.System
    color: Material.backgroundColor

    function onLoginChanged(login){
        if(login==""){rootstackView.push("qrc:/qml/configqml/AccountPage.qml")}
        else{
            if (login.newsViewType!="" || login.newsViewType!=null){
                newstab.newstabstatus=login.newsViewType;}
            Newsjs.getCurrentContacts(login,db,function(contacts){
                contactlist=contacts})
        }
    }
    function onNewContactsChanged(newContacts){
        if(newContacts.length>0){// download contact images and update db
           var contacturls=[];
           var contactnames=[];
            for (var link in newContacts){//print("new contact " +newContacts[link].screen_name);
                contacturls.push(newContacts[link].profile_image_url);
                contactnames.push(newContacts[link].screen_name);
                Service.updateContactInDB(login,db,newContacts[link].isFriend,newContacts[link])
                contactlist.push(newContacts[link].url);
            }
            xhr.setDownloadtype("contactlist");
            xhr.setFilelist(contacturls);
            xhr.setContactlist(contactnames);
            xhr.setImagedir(login.imagestore);
            xhr.getlist();
        }
    }

    function showContact(contact){
        rootstackView.push("qrc:/qml/newsqml/ContactPage.qml",{"contact": contact})
    }

    Connections {
        target: root
        function onWidthChanged(appWidth) {
            if(osSettings.osType=="Linux" && Math.abs(appWidth-(globaloptions.appWidth||0))>50){
                Service.updateglobaloptions(db,"appWidth",appWidth)
            }
        }
    }

    Connections {
        target: root
        function onHeightChanged(appHeight) {
            if(osSettings.osType=="Linux" && Math.abs(appHeight-(globaloptions.appHeight||0))>50){
                Service.updateglobaloptions(db,"appHeight",appHeight)
            }
        }
    }

    Connections{
        target:xhr
        function onDownloaded(type,url,filename,i){
            if(type=="contactlist"){
                var database=LocalStorage.openDatabaseSync(root.db[0],root.db[1],root.db[2],root.db[3]);
                var result;
                database.transaction( function(tx) {
                    result = tx.executeSql('UPDATE contacts SET profile_image="'+filename+'" where profile_image_url="'+url+'"');
                })
            }
        }
    }

    FontLoader{id: fontAwesome; source: "qrc:/images/fontawesome-webfont.ttf"}

    onClosing: {
            if (rootstack.currentIndex==0){
                newstab.active=true;
                if (newstab.newstabstatus!=globaloptions.newsViewType){
                    newstab.newstabstatus=globaloptions.newsViewType;
                    if(globaloptions.newsViewType=="Timeline"){Newsjs.newsfromdb(db,login.username,0,function(dbnews){
                        newsSignal(dbnews)
                    })}
                    else{
                         Newsjs.chatsfromdb(db,login.username,0,[],function(dbnews){
                        newsSignal(dbnews)
                    })}
                    close.accepted=false;
                 }

                else if (newstab.conversation.length>0){
                    newstab.conversation=[];
                    close.accepted=false
                }
                else if (root.depth>1){
                    root.pop();
                    close.accepted=false
                }
                else{
                    Service.cleanNews(root.db,function(){
                        Service.cleanHashtags(root.db,function(){
                            Service.cleanContacts(root.login,root.db,function(){
                                if (osSettings.osType=="Android" || !iconTrayAvailable){
                                     Qt.quit()
                                }
                                else{
                                    systemTray.show();
                                    systemTray.showMessage("",qsTr("Background Sync\n Rightclick or Middleclick to Quit"),"",5000)
                                    root.hide()
                                }
                            })
                     })})
                    close.accepted=true
                }
             }
         else if (rootstack.currentIndex==2){fotoSignal(login.username,"backButton");close.accepted=false}
         else {rootstack.currentIndex=0;close.accepted=false}
    }


    Rectangle{
        anchors.fill: parent
        color: Material.backgroundColor
    }

    footer: ToolBar{
        background: Rectangle{
            anchors.fill: parent
            color: Material.backgroundDimColor
        }
        RowLayout{
            anchors.fill: parent

            TabBar {
                id: bar
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    rootstack.currentIndex=bar.currentIndex;
                    try{while(rootstackView.depth>1){rootstackView.pop()}}catch(e){}
                }
                TabButton {
                    text: "\uf03a"
                    background:Rectangle{
                        anchors.fill: parent
                        color: Material.backgroundDimColor
                    }
                    onDoubleClicked: {newstypeSignal("refresh")}
                }
                TabButton {
                    text:  "\uf0c0"
                    background:Rectangle{
                        anchors.fill: parent
                        color: Material.backgroundDimColor
                    }
                }
                TabButton {
                    text:   "\uf03e"
                    background:Rectangle{
                        anchors.fill: parent
                        color: Material.backgroundDimColor
                    }
                }
                TabButton {
                    text:    "\uf073"
                    background:Rectangle{
                        anchors.fill: parent
                        color: Material.backgroundDimColor
                    }
                }
            }
        }
    }


StackView{id:rootstackView
    width:root.width
    height: root.height
    initialItem: StackLayout{
            id:rootstack
            width:rootstackView.width
            height: rootstackView.height
            currentIndex:bar.currentIndex

            Loader{
                id: newstab
                property string newstabstatus
                property var conversation:[]

                source:(rootstack.currentIndex==0)? "qrc:/qml/newsqml/NewsTab.qml":""
            }
            Loader{
                id: friendstab
                source: (rootstack.currentIndex==1)?"qrc:/qml/contactqml/FriendsTab.qml":""
            }
            Loader{
                id: fotostab
                property string phototabstatus:"Images"
                source: (rootstack.currentIndex==2)?"qrc:/qml/photoqml/PhotoTab.qml":""
            }
            Loader{
                id: calendartab
                property string calendartabstatus:"Events"
                source: (rootstack.currentIndex==3)?"qrc:/qml/calendarqml/CalendarTab.qml":""
            }
        }
    }


    QSystemTrayIcon {
        id: systemTray
        visible: false
        onActivated: {
            if(reason === 3){
                root.show();
                bar.currentIndex=0;
                root.globaloptions.hasOwnProperty("newsViewType")?newstab.newstabstatus=root.globaloptions.newsViewType:newstab.newstabstatus="Conversations";
                newstypeSignal(newstab.newstabstatus)
                systemTray.hide()
            } else {
                Qt.quit()
            }
        }
        Component.onCompleted: {
            icon = iconTray
            toolTip = qsTr("Click to open Friendiqa")
            //&hide()
        }
    }


    Component.onCompleted: {
        onLoginChanged(login);
        globaloptions=Service.readGO(db);
        if(globaloptions.view_darkmode==1){Material.theme=Material.Dark}
        else if (globaloptions.view_darkmode==2){Material.theme=Material.Light}
        else {Material.theme=Material.System}
        if(!filesystem.Visibility){
            systemTray.icon = iconTray;
//            systemTray.toolTip = qsTr("Click to open Friendiqa");
            systemTray.visible=true
            systemTray.show();
        }

        if(osSettings.osType=="Android"){
            var component = Qt.createComponent("qrc:/qml/genericqml/IntentReceiver.qml");
            var IntentReceiverQml = component.createObject(root);
        }
        else if (osSettings.osType=="Linux"){
            if (login!=""){newstypeSignal("refresh")}
            var component = Qt.createComponent("qrc:/qml/genericqml/LinuxSync.qml");
            var LinuxSyncQml = component.createObject(root);
        }
    }
}
