package androidnative.friendiqa;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.app.job.JobService;
import android.app.job.JobParameters;
import androidnative.AndroidNativeService;
import org.qtproject.qt5.android.bindings.QtService;
import org.qtproject.qt5.android.QtNative;
//import androidnative.friendiqa.FriendiqaQtService;

public class FriendiqaService extends JobService{
	private static String TAG = "AndroidNative";
	//Log.e(TAG,"Service");


    @Override
    public boolean onStartJob(JobParameters params) {
        //Log.d(TAG,"Friendiqa JobService");
	Context context = this.getApplicationContext();
        AndroidNativeService fs = new AndroidNativeService();
	fs.startQtService(context);
	jobFinished(params,false);
	//Intent serviceIntent = new Intent(this, AndroidNativeService.class);
        //startService(serviceIntent);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        // whether or not you would like JobScheduler to automatically retry your failed job.
        return false;
    }
}

