## Friendiqa

QML based client for the Friendica Social Network. Tabs for news (incl. Direct Messages), friends, photos and events. OS: currently Linux and Android (4.3 Jelly Bean, 5.1 for background sync). Source code is a QtCreator project.

## Screenshots

![Newstab](Screenshots/NewsTab.jpg) ![Friendstab](Screenshots/FriendsTab.jpg) ![Photostab](Screenshots/PhotoTab.jpg) ![Eventstab](Screenshots/EventsTab.jpg) ![Configtab](Screenshots/ConfigTab.jpg)

## Features

# News

Currently supported:

* Shows Posts from friends, replies, Direct Messages and notifications (in swipe view), selected group, replies, favorited messages, public timeline
* Background sync with configurable interval of 15 min to 2h for active user for friends timeline, replies and DMs (Android 5.1 required)
* Android notifications or Dbus notification (Linux) for new items in friends timeline, replies and DMs
* Search for news
* Click on hashtag in newsitem starts search for news with that word
* Only first photo attachment in post is shown, click for more images and fullscreen image
* Click on video shows video fullscreen
* For news containing urls ending with mp3, mp4, avi, webm, ogg or to a Peertube instance: media can be played in the app
* Open links in external browser
* Click on contact photo for contact details and last news
* Click on like text for additional contact info
* Click on post text opens conversation
* Deletion, Reposting, Answering of Posts
* Expand truncated news items
* Liking, disliking
* Attending for event posts
* Update fetches new posts (up to last 50) since last in local DB
* More shows older posts from local DB
* Create new Message with images or direct messages,smileys
* Send image(s) from Android gallery
* Send text or urls from other apps to Friendiqa
* Native Android image dialog

ToDo:

* Videos and other binary data as attachment (sending, not supported in API)
* Attachments for Direct messages (currently not supported in API)

# Friends

Currently supported:

* Tabs for own profiles, friends, other contacts and groups
* Show profile(s) of user and change profile picture
* List of all known contacts with locally downloaded pictures
* Follow/unfollow or block/unblock contacts
* Search for new contacts according to topic
* Show follow requests; approve, deny, ignore requests
* Additional information, last messages and other functionality shown in news tab
* Show public and private pictures of contact (screenscraping of contact's website, works only with certain theme)
* Show public and private events of contact
* Show members of groups
* Open website of contact
* Clean other contacts with no news

ToDo:

* More information for contact from description page, possibly private information for friends (needs API change)
* Groups: create, change, delete (needs API change)

# Images

Currently supported:

* Download public own images to local directory (API is currently broken)
* Upload public picture to album with descriptions, send from gallery (API is currently broken)
* Delete own pictures and albums on client and server
* Change name or album of existing picture
* Show albums in grid, show images in album in grid and fullscreen
* Show public albums and images of contacts
* Pinch to zoom, swipe to scroll

ToDo:

* Upload private images

# Events

Currently supported:

* Show own events
* Show public events of Friendica contacts
* List view of events of selected date
* Click on event to show details
* Create event
* Delete event

ToDo

* Show more details and attendance of events (needs API)

# Config/Accounts

Currently supported:

* Multiple accounts
* Dark Mode
* View mode for news (conversation tree or timeline)
* Maximum news (deleted after use of Quit button)
* Sync home timeline, replies, DM, Events, friend requests; Notify yes/no
* Hide #nsfw

ToDo

* OAuth

# Other

* on Linux: App stays in systemtray and syncs in background
* Autostart minimized in systray possible

ToDo

* Video tab
* Translation
* Blingbling

# Translations

* German, Spanish, Italian, Hungarian
* To contribute translations: Have a look at linux-sources/translations/friendiqa-de.ts and open it with an editor. It's an xml file. Change values and send me the file to thomasschmidt45 at gmx.net / do pull request.

# Install

* F-Droid binary repository: <https://www.ma-nic.de/fdroid/repo> or for those who get the "error getting index file" from F-Droid client due to outdated crypto libraries the url without encryption: <http://www.ma-nic.de/fdroid/repo> To include repo in Fdroid: Open config --> package sources --> plus symbol --> paste url
* [Google Playstore](https://play.google.com/store/apps/details?id=org.qtproject.friendiqa)
* Arch User Repository: <https://aur.archlinux.org/packages/friendiqa/>
* Flatpak: <https://friendiqa.ma-nic.de/friendiqa.flatpakref>

## License

Pubished under the [GPL v3](http://gplv3.fsf.org) with the exception of the Openssl library, which is published under OpenSSL License.
