//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import "qrc:/qml/genericqml"
import "qrc:/js/service.js" as Service

ScrollView{
    id:leftDrawerScrollviewId
    clip: true
    width:parent.width-mm
    height: parent.height
    contentHeight: leftDrawerColumn.height
    property string currentnewstabstatus:root.globaloptions.hasOwnProperty("newsViewType")?root.globaloptions.newsViewType:"Conversations";

    Column{
        id:leftDrawerColumn
        x:mm
        y:0.5*root.fontFactor*osSettings.bigFontSize
        width:parent.width-2*mm
        height: 4*root.fontFactor*osSettings.bigFontSize
        spacing: 0.7*root.fontFactor*osSettings.bigFontSize
        Label{
            width:implicitWidth
            font.pointSize: osSettings.systemFontSize
            text: "\uf085 "+ qsTr("Settings")
            MouseArea{
                anchors.fill:parent
                onClicked:{rootstackView.push("qrc:qml/configqml/ConfigPage.qml");
                    if(!wideScreen){leftDrawerAndroid.close()}
                }
            }
        }

        Label{y: 2*root.fontFactor*osSettings.bigFontSize
            width:implicitWidth
            font.pointSize: osSettings.systemFontSize
            text: "\uf2bb " + qsTr("Accounts")
            MouseArea{
                anchors.fill:parent
                onClicked:{rootstackView.push("qrc:qml/configqml/AccountPage.qml");
                    if(!wideScreen){leftDrawerAndroid.close()}
                }
            }
        }

        Label{y: 4*root.fontFactor*osSettings.bigFontSize
            width:implicitWidth
            font.pointSize: osSettings.systemFontSize
            text: "\uf08b  " +qsTr("Quit")
            MouseArea{
                anchors.fill:parent
                onClicked:{
                    Service.cleanNews(root.db,function(){
                        Service.cleanHashtags(root.db,function(){
                            Service.cleanContacts(root.login,root.db,function(){
                                Qt.quit()})
                        })})
                }
            }
        }
    }
    Component.onCompleted:{
        Service.readAllLogins(db,function(accounts){
            if (accounts.length>0 && bar.currentIndex==0){
                leftDrawerColumn.height=4.5*root.fontFactor*osSettings.bigFontSize+accounts.length*17*root.fontFactor*osSettings.bigFontSize
                for(var i = 0; i < accounts.length; i++) {
                    var accountComponent = Qt.createComponent("qrc:/qml/genericqml/DrawerAccountComponent.qml");
                    var accountQml = accountComponent.createObject(leftDrawerColumn,{
                                                                       "y":4.5*root.fontFactor*osSettings.bigFontSize+i*17*root.fontFactor*osSettings.bigFontSize,
                                                                       "currentnewstabstatus":currentnewstabstatus,
                                                                       "account":accounts[i]});
                }
            }else if(accounts.length>0 && bar.currentIndex==1){
                leftDrawerColumn.height=4.5*root.fontFactor*osSettings.bigFontSize+accounts.length*8*root.fontFactor*osSettings.bigFontSize
                for(var i = 0; i < accounts.length; i++) {
                    var accountComponent = Qt.createComponent("qrc:/qml/genericqml/DrawerAccountComponentContacts.qml");
                    var accountQml = accountComponent.createObject(leftDrawerColumn,{
                                                                       "y":4.5*root.fontFactor*osSettings.bigFontSize+i*8*root.fontFactor*osSettings.bigFontSize,
                                                                       "account":accounts[i]});
                }
            }
        })}
}
