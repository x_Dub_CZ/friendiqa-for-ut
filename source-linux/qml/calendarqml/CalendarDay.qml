//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls.Material 2.12
//import QtQuick.Controls 2.4

Item {
    id: calendarDay
    width: root.fontFactor*osSettings.bigFontSize*2
    height: root.fontFactor*osSettings.bigFontSize*2
    property int dateInt: Math.floor(model.date.valueOf()/86400000)
    Rectangle {
        id: placeHolder
        color: model.today?'lightblue':'transparent';
        border.color: 'lightblue'
        border.width: 2
        antialiasing: true
        anchors.fill:parent
        radius: 0.5*mm
    }
    Text {
        id:daytext
        anchors.right: parent.right
        anchors.margins: 0.5*mm
        color:(model.month==monthgrid.month)?Material.primaryTextColor:Material.secondaryTextColor
        wrapMode: Text.WrapAnywhere
        text: model.day
        font.bold: model.today
        font.pointSize: 1.2*osSettings.systemFontSize
    }
    Rectangle {
        id:eventRect
        color:"grey"
        anchors.margins: 0.5*mm
        anchors.bottom: calendarDay.bottom
        width: parent.width-mm
        height: 0.5*osSettings.systemFontSize//mm
        visible: eventdays.indexOf(dateInt)>-1
    }
    MouseArea {
        anchors.fill: calendarDay
        onClicked: {
            rootstackView.push("qrc:/qml/calendarqml/EventList.qml",{"dayint": dateInt,"events":events});
        }
    }
}
