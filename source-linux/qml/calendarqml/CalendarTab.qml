//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQml 2.2
import Qt.labs.calendar 1.0
//import QtQuick.Layouts 1.3
import "qrc:/js/service.js" as Service
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/calendarqml"
import "qrc:/qml/genericqml"

Rectangle {
    id:calendarrectangle
    //    y:1
    width:parent.width
    height:parent.height
    color: Material.backgroundColor
    property date currentTime: new Date()
    property int offsetTime: currentTime.getTimezoneOffset() * 60 * 1000
    property var events:[]
    property var eventdays:[]

    function showEvents(friend){
        if(friend=="backButton"){Service.eventsfromdb(db,login.username,function(eventArray,dayArray){
            events=cleanEvents(eventArray);
            eventdays=dayArray})
        }
        else if (friend!=""){
            calendartab.calendartabstatus=friend.url.substring(friend.url.lastIndexOf("/")+1,friend.url.length)
            Service.newRequestFriendsEvents(login,friend,calendartab,function(eventArray,dayArray){
                events=cleanEvents(eventArray);
                eventdays=dayArray})
        }
        else {calendartab.calendartabstatus="Events";
            Service.eventsfromdb(db,login.username,function(eventArray,dayArray){
                events=cleanEvents(eventArray);
                eventdays=dayArray;
                calBusy.running=false
                var currentevents=events;
                var currentevents=events.filter(event=>(currentTime<=event.end));
                for (var i=0; i<Math.min(5,currentevents.length);i++){
                    var liststate="";
                    eventModel.append({"event":currentevents[i],"eventstatus":liststate});
                }
            })
        }
    }

    function cleanEvents(events){
        for (var item in events){
            events[item].start=events[item].start-offsetTime;
            if(events[item].end>0){events[item].end=events[item].end-offsetTime};
        }
        return events
    }

    function createEvent(event){
        rootstackView.push("qrc:/qml/calendarqml/EventCreate.qml",{"eventInformation": event})
    }

    BusyIndicator{
        id: calBusy
        anchors.horizontalCenter: calendarView.horizontalCenter
        anchors.top:calendarView.top
        anchors.topMargin: 2*mm
        width:10*mm
        height: 10*mm
        running: false
    }

    BlueButton{
        z:2
        visible: !wideScreen
        fontColor: "grey"
        border.color: "transparent"
        text: "\uf0c9"
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            leftDrawerAndroid.visible?leftDrawerAndroid.close():leftDrawerAndroid.open()}
    }

    LeftDrawerLinux{
        id:leftDrawer
        property var newstabstatus: newstab.newstabstatus
        visible: wideScreen&&rootstackView.depth<2
        width: visible?root.fontFactor*osSettings.systemFontSize*15:0
        height: root.height-bar.height
    }

    LeftDrawerAndroid{
        id: leftDrawerAndroid
    }

    MButton{
        id:  updateEvents
        anchors.top: parent.top
        anchors.topMargin: 0.5*mm
        anchors.right:calendartabstatusButton.left
        anchors.rightMargin:mm
        width: 2*root.fontFactor*osSettings.bigFontSize;
        text:"\uf021"
        onClicked: {
            calBusy.running=true;
            updatenews.setDatabase();
            updatenews.login();
            updatenews.setSyncAll(false);
            updatenews.events();
        }}
    
    Connections{
        target: updatenews

        function onSuccess(api){
            calBusy.running=false;
            showEvents("")
        }
    }

    Dialog {
        id: deleteDialog
        anchors.centerIn: parent
        property int eventid:0
        title: qsTr("Delete Event?")
        standardButtons: Dialog.Ok | Dialog.Cancel
        modal: true
        onAccepted: {
            xhr.setUrl(login.server);
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setApi("/api/friendica/event_delete");
            xhr.clearParams();
            xhr.setParam("id",eventid);
            xhr.post();
        }
        onRejected: {close()}
    }

    MButton{
        id:  createNewEvent
        anchors.top: parent.top
        anchors.topMargin: 0.5*mm
        anchors.right:updateEvents.left
        anchors.rightMargin:mm
        width: 2*root.fontFactor*osSettings.bigFontSize;
        text:"+"
        onClicked: {
            rootstackView.push("qrc:/qml/calendarqml/EventCreate.qml")
        }
    }

    Connections{
        target: xhr
        function onSuccess(text,api){
            if(api=="/api/friendica/event_create"){
                calBusy.running=true;
                updatenews.setDatabase();
                updatenews.login();
                updatenews.setSyncAll(false);
                updatenews.events();
            }
        }
    }

    MButton{
        id:  calendartabstatusButton
        anchors.top: parent.top
        anchors.topMargin: 0.5*mm
        anchors.right: parent.right
        anchors.rightMargin:2*mm
        width: Math.max(6*root.fontFactor*osSettings.bigFontSize,implicitWidth)
        text: calendartab.calendartabstatus=="Events"?qsTr("Events"):calendartabstatus
        Menu {
            id:calendartabmenu
            width: 20*root.fontFactor*osSettings.systemFontSize
            MenuItem {
                text: qsTr("Own Calendar")
                font.pointSize: osSettings.systemFontSize
                onTriggered: {
                    calendartab.calendartabstatus="Events";
                    // calendartabstatusButton.text=qsTr("own Calendar");
                    showEvents("")}
            }
        }
        onClicked: {calendartabmenu.popup()}
    }

    ListView{
        id: calendarView
        y:2*root.fontFactor*osSettings.bigFontSize//8*mm
        width: wideScreen&&rootstackView.depth<2?parent.width-leftDrawer.width-mm:parent.width-mm//newstabitem.width/3*2:newstabitem.width
        x: leftDrawer.width
        height: 18*root.fontFactor*osSettings.bigFontSize //parent.height-9*mm
        clip: true
        snapMode: ListView.SnapOneItem
        orientation: ListView.Horizontal
        highlightRangeMode: ListView.StrictlyEnforceRange

        model:     CalendarModel {id:calendarModel
            from: new Date()
            to: new Date(new Date().valueOf()+93312000000)
        }
        delegate:
            Item{
            width:Math.min(23*root.fontFactor*osSettings.bigFontSize,calendarView.width)
            height: parent.height
            Text{
                font.bold: true
                //Layout.fillWidth: true
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                horizontalAlignment:Text.AlignHCenter
                color: Material.primaryTextColor
                text: model.year
                font.pointSize: osSettings.systemFontSize
            }
            Text{y:1.5*root.fontFactor*osSettings.bigFontSize
                width: parent.width-osSettings.bigFontSize
                text: Qt.locale().standaloneMonthName(model.month)
                //Layout.fillWidth: true
                color: Material.primaryTextColor
                horizontalAlignment:Text.AlignHCenter
                font.pointSize: osSettings.systemFontSize
            }
            DayOfWeekRow{y:3*root.fontFactor*osSettings.bigFontSize
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                locale: monthgrid.locale
                //Layout.fillWidth: true
                font.pointSize: osSettings.systemFontSize
            }

            MonthGrid {y:5*root.fontFactor*osSettings.bigFontSize
                id: monthgrid
                height: parent.height-5*root.fontFactor*osSettings.bigFontSize
                width: parent.width-root.fontFactor*osSettings.bigFontSize
                month: model.month
                year: model.year
                locale: Qt.locale()
                delegate: CalendarDay{}
            }
        }
        ScrollIndicator.horizontal: ScrollIndicator { }
        Component.onCompleted: positionViewAtBeginning()
    }
    
    ListView {
        id: eventlistView
        y:20*root.fontFactor*osSettings.bigFontSize
        x: leftDrawer.width
        height: parent.height-20*root.fontFactor*osSettings.bigFontSize
        width: wideScreen&&rootstackView.depth<2?parent.width-leftDrawer.width-mm:parent.width-mm
        clip: true
        model: eventModel
        delegate: EventListItem{}
    }

    ListModel{
        id: eventModel
    }

    Component.onCompleted: {
        root.eventcreateSignal.connect(createEvent);
        root.eventSignal.connect(showEvents);
        if (calendartab.calendartabstatus=="Events"){showEvents("")}
    }
}
