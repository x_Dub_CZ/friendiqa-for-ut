//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/news.js" as Newsjs
import "qrc:/qml/contactqml"
import "qrc:/qml/genericqml"

Item{
    id: friendsGridTab
    Layout.fillWidth:true
    Layout.fillHeight: true
    property int currentContact: 0
    signal contactRefreshSignal()

    function showFriends(username){
        try {friendsModel.clear()} catch(e){};
        Helperjs.readData(db,"friendshiprequests",username,function(friendrequestsobject){
            for (var i=0;i<friendrequestsobject.length;i++){
                if (friendrequestsobject[i].note!=null){
                     friendrequestsobject[i].description=Qt.atob(friendrequestsobject[i].note);}
                else{friendrequestsobject[i].description=""}
                friendrequestsobject[i].name=Qt.atob(friendrequestsobject[i].display_name);
                friendrequestsobject[i].screen_name=friendrequestsobject[i].usernamef;
                friendrequestsobject[i].location="";
                friendrequestsobject[i].profile_image=friendrequestsobject[i].avatar_static;
                friendrequestsobject[i].profile_image_url=friendrequestsobject[i].avatar;
                friendrequestsobject[i].curIndex=friendsModel.count;
                friendsModel.append({"contact":friendrequestsobject[i],"contactType":qsTr("Friend Requests")});
                }
         });
         Newsjs.listFriends(login,db,function(friendsobject){
             for (var i=0;i<friendsobject.length;i++){
                 if(friendsobject[i].description!=""){
                     friendsobject[i].description=Qt.atob(friendsobject[i].description);}
                     if(Helperjs.getCount(db,login,"contacts","screen_name",friendsobject[i].screen_name)>1){
                         friendsobject[i].screen_name=friendsobject[i].screen_name+"+"+friendsobject[i].cid
                     }
                     friendsModel.append({"contact":friendsobject[i],"contactType":qsTr("Friends")});
              }
          },(searchText.text==""?searchText.preeditText:searchText.text));
    }

    function showContacts(contact){
        try {friendsModel.clear()} catch(e){};
        Newsjs.listFriends(login,db,function(contactsobject){
            for (var j=0;j<contactsobject.length;j++){
                contactsobject[j].description=Qt.atob(contactsobject[j].description);
                if(Helperjs.getCount(db,login,"contacts","screen_name",contactsobject[j].screen_name)>1){
                    contactsobject[j].screen_name=contactsobject[j].screen_name+"+"+contactsobject[j].cid
                }
                 friendsModel.append({"contact":contactsobject[j]});
                }
          },searchText.text,-1);
    }

    function showBlocked(contact){
        try {friendsModel.clear()} catch(e){};
        Newsjs.listBlocked(login,db,function(contactsobject){
            for (var j=0;j<contactsobject.length;j++){
                contactsobject[j].description=Qt.atob(contactsobject[j].description);
                if(Helperjs.getCount(db,login,"contacts","screen_name",contactsobject[j].screen_name)>1){
                    contactsobject[j].screen_name=contactsobject[j].screen_name+"+"+contactsobject[j].cid
                }
                 friendsModel.append({"contact":contactsobject[j]});
                }
          });
    }

    Connections{
        target:xhr
        function onDownloaded(type,url,filename,i){
            if(type=="contactlist"){
                friendsGridTab.currentContact=i+1;
                if(friendsGridTab.currentContact==root.newContacts.length){
                   friendsGridTab.showFriends(root.login.username)
                }
            }
        }
    }

    MButton {
        id: updateFriendsButton
        text:  "\uf021"
        anchors.top: parent.top
        anchors.topMargin: mm
        anchors.right: parent.right
        onClicked: {
            try {friendsModel.clear()} catch(e){print(e)};
            Helperjs.deleteData(root.db,"friendshiprequests",root.login.username,function(){});
            updatenews.setDatabase();
            updatenews.login();
            updatenews.setSyncAll(false);
            updatenews.friendrequests();
            //root.contactLoadType="friends";
            Newsjs.requestFriends(root.login,db,root,function(nc){
                 root.newContacts=nc
                 root.onNewContactsChanged(nc);
            })
        }
    }

    ProgressBar{
        id: newContactsProgress
        width: friendsView.width
        height: 2*mm
        x: mm
        y: updateFriendsButton.height+mm
        visible: (friendsGridTab.currentContact!=(root.newContacts.length))?true:false
        value: friendsGridTab.currentContact/root.newContacts.length
    }

    Rectangle {
        id:searchComponent
        x: mm; y:mm
        color: Material.backgroundColor
        radius:0.5*mm
        width: 10*root.fontFactor*osSettings.bigFontSize
        height: 2*root.fontFactor*osSettings.bigFontSize
        TextField {
            id: searchText
            color: Material.primaryTextColor
            focus: true
            font.pointSize: osSettings.systemFontSize
            wrapMode: Text.Wrap
            anchors.fill:parent
            selectByMouse: true
            cursorVisible: false
            placeholderText: "\uf0b0"
            onTextChanged: {showFriends(root.login.username)}//if (text.length>0)
            onPreeditTextChanged: {{showFriends(root.login.username)}}//if (preeditText.length>0)
        }
    }

    ComboBox{
        id: friendsCombo
        anchors.left: searchComponent.right
        anchors.leftMargin: root.fontFactor*osSettings.bigFontSize
        y: mm
        width: 6*root.fontFactor*osSettings.bigFontSize
        height: 1.5*root.fontFactor*osSettings.bigFontSize
        font.pointSize: osSettings.systemFontSize
        model: [qsTr("Friends"), qsTr("All"), qsTr("Blocked")]
        onCurrentIndexChanged:{
            if (currentIndex === 0) {
                showFriends(root.login.username);
            } else
            if (currentIndex===1){
                showContacts()
            } else if (currentIndex===2){
                showBlocked()
            }
        }
        Component.onCompleted: {root.contactRefreshSignal.connect(onCurrentIndexChanged)}
    }


    Component {
        id: sectionHeading
        Rectangle {
            width: friendsView.width
            height: childrenRect.height
            color: Material.backgroundColor
            required property string section
            Text {
                color: Material.secondaryTextColor
                text: parent.section
                font.bold: true
                font.pointSize: osSettings.bigFontSize
            }
        }
    }
            //GridView {


    Component {  id:headerComponent
        Rectangle{
            color: Material.dialogColor
            width:friendsView.width
            height:6*mm
            Text{
                color: Material.primaryTextColor
                font.pointSize: osSettings.bigFontSize
                anchors.centerIn: parent
                text:"\uf234"
            }
            MouseArea{
                anchors.fill:parent
                onClicked:{
                    rootstackView.push("qrc:/qml/contactqml/ContactsSearchPage.qml")
                }
            }
        }
    }

    ListView{
        id: friendsView
        x:mm
        y:updateFriendsButton.height+2*mm
        width:friendsGridTab.width-2*mm
        height:friendsGridTab.height-(updateFriendsButton.height+10*mm)
        clip: true
        spacing: 2
              function processContactSelection(contactobject){showContactdetails(contactobject)}
              //add: Transition {
               //        NumberAnimation { properties: "x,y"; from: 300; duration: 1000 }
               //    }
        model: friendsModel
        delegate: ContactComponent { }
        header:headerComponent
        section.property: "contactType"
        section.criteria: ViewSection.FullString
        section.delegate: sectionHeading
    }
    ListModel{id:friendsModel}

    Component.onCompleted: {
        root.friendsSignal.connect(showFriends);
        friendsTabView.contactsSignal.connect(showFriends);
        showFriends(root.login.username);
        root.newContacts=[]
    }
}
