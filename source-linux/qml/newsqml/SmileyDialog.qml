//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.12
import "qrc:/js/smiley.js" as Smileyjs
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"

Rectangle{
    id:smileyDialog
    x: mm
    width: messageColumn.width-5*mm
    height:root.height/2
    color: Material.backgroundColor
    
    MButton{
       id:closeButton
       anchors.top: parent.top
       anchors.topMargin: 1*mm
       anchors.right: parent.right
       anchors.rightMargin: 1*mm
       text:   "\uf057"
       onClicked:{smileyDialog.visible=false}
    }


    TabBar {
      id: smileybar
      width: parent.width
      height: 9*mm
      position:TabBar.Header
      TabButton {
          text:qsTr("Unicode")
          font.pointSize: osSettings.systemFontSize
      }
      TabButton {
          text:  qsTr("Standard")
          font.pointSize: osSettings.systemFontSize
      }
      TabButton {
          text:  qsTr("Addon")
          font.pointSize: osSettings.systemFontSize
      }
    
      TabButton {
          text:  qsTr("Adult")
          font.pointSize: osSettings.systemFontSize
      }
  }


    StackLayout{
        id:smileyTabView
        currentIndex: smileybar.currentIndex
        anchors.top: closeButton.bottom
        anchors.topMargin: 1*mm
        width: smileyDialog.width-2*mm
        height: smileyDialog.height-4*root.fontFactor*osSettings.bigFontSize
        Rectangle{
               id: htmlGridTab
               color: Material.backgroundColor
               GridView {
                     id:htmlView
                     anchors.fill: parent
                     cellWidth: 5*mm
                     cellHeight: 5*mm
                     clip: true
                     model: htmlModel
                     delegate: htmlItem
                }

                ListModel{
                    id:htmlModel
                }
           Component.onCompleted:{
                   for (var icon in Smileyjs.html){
                       htmlModel.append({"emoji":Smileyjs.html[icon]})
                   }
               }
           }

        Rectangle{
            id: coreGridTab
            color: Material.backgroundColor
            GridView {
                  id: coreSmileyView
                  anchors.fill: parent
                  cellWidth: 5*mm
                  cellHeight: 5*mm
                  clip: true
                  model: coreSmileyModel
                  delegate: smileyItem
             }

             ListModel{
                 id: coreSmileyModel
             }

             Component.onCompleted:{
                var smileyarray=Smileyjs.core
                for (var icon in smileyarray){
                    coreSmileyModel.append({"emoji":smileyarray[icon]})
                }
             }
         }

        Rectangle{
            id: addonGridTab
            color: Material.backgroundColor
            GridView {
                  id: addonView
                  anchors.fill: parent
                  cellWidth: 5*mm
                  cellHeight: 5*mm
                  clip: true
                  model: addonModel
                  delegate: smileyItem
             }

             ListModel{
                 id: addonModel
             }
        Component.onCompleted:{
                for (var icon in Smileyjs.addon){
                    addonModel.append({"emoji":Smileyjs.addon[icon]})
                }
            }
        }


        Rectangle{
            id: adultGridTab
            color: Material.backgroundColor
            GridView {
                  id: adultView
                  anchors.fill: parent
                  cellWidth: 5*mm
                  cellHeight: 5*mm
                  clip: true
                  model: adultModel
                  delegate: smileyItem
             }

             ListModel{
                 id: adultModel
             }
        Component.onCompleted:{
                for (var icon in Smileyjs.adult){
                    adultModel.append({"emoji":Smileyjs.adult[icon]})
                }
            }
        }
    }
    Component{
         id:smileyItem
         AnimatedImage{id:smileyImage
             width:4.5*mm
             height: 4.5*mm
             fillMode:Image.PreserveAspectFit
             source:emoji.url
          
            MouseArea{
                anchors.fill: parent
                onClicked:{
                     //bodyField.append(emoji.name+" ")
                    bodyField.insert(bodyField.cursorPosition,emoji.name+" ");
                     smileyDialog.visible=false
                    }
                }
            }
         }
         
    Component{
         id:htmlItem
        Label{id:smileText
             width:4.5*mm
             height: 4.5*mm
             textFormat:Text.RichText
             font.pointSize: osSettings.osType=="Linux"?1.7*osSettings.systemFontSize:1.2*osSettings.systemFontSize
             text: emoji
          
            MouseArea{
                anchors.fill: parent
                onClicked:{
                     //bodyField.append(emoji.name+" ")
                    bodyField.insert(bodyField.cursorPosition,emoji+" ");
                     smileyDialog.visible=false
                    }
                }
            }
         }
    }
