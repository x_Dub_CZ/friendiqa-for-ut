//  This file is part of Friendiqa
//  https://github.com/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "remoteauthasyncimageprovider.h"
#include <QQuickAsyncImageProvider>
#include <QImage>

 AsyncImageResponse::AsyncImageResponse(QNetworkRequest req, QSize reqSize)
 {
     m_reply = m_imageLoader.get(req);
     m_requestedSize = reqSize;
     connect(m_reply, &QNetworkReply::finished, this, &AsyncImageResponse::onResponseFinished);
}


 void AsyncImageResponse::onResponseFinished()
 {
     QByteArray myImageData = m_reply->readAll();
     m_resultImage = QImage::fromData(myImageData);
     if (m_requestedSize.isValid())
     {
         m_resultImage = m_resultImage.scaled(m_requestedSize);
     }
     emit finished();
 }


 QQuickTextureFactory *AsyncImageResponse::textureFactory() const
 {
     return QQuickTextureFactory::textureFactoryForImage(m_resultImage);
 }


 RemoteAuthAsyncImageProvider::RemoteAuthAsyncImageProvider()
 {
 }


 QQuickImageResponse* RemoteAuthAsyncImageProvider::requestImageResponse(const QString &id, const QSize &requestedSize)
 {
     QUrl iUrl=url()+"/api/friendica/remoteauth?c_url="+contacturl()+"&url="+id;
     QByteArray loginData = m_login.toLocal8Bit().toBase64();
     QString headerData = "Basic " + loginData;

     QNetworkRequest request(iUrl);
     request.setRawHeader("Authorization", headerData.toLocal8Bit());
     request.setAttribute(QNetworkRequest::FollowRedirectsAttribute,true);
     request.setUrl(iUrl);
     return new AsyncImageResponse(request, requestedSize);
 }


 void RemoteAuthAsyncImageProvider::setContacturl(QString contacturl)
 {
    if (contacturl!=m_contacturl) {
        m_contacturl = contacturl;
        emit contacturlChanged();
    }
 }


 void RemoteAuthAsyncImageProvider::setUrl(QString url)
 {
    if (url!=m_url) {
        m_url = url;
        emit urlChanged();
    }
 }


 void RemoteAuthAsyncImageProvider::setLogin(QString login)
 {
    if (login!=m_login) {
        m_login = login;
        emit loginChanged();
    }
 }


 QString RemoteAuthAsyncImageProvider::contacturl() const
 {
     return m_contacturl;
 }


 QString RemoteAuthAsyncImageProvider::url() const
 {
     return m_url;
 }


 QString RemoteAuthAsyncImageProvider::login() const
 {
     return m_login;
 }
