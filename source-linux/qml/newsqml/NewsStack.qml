//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/news.js" as Newsjs
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/service.js" as Service

Rectangle{
    id: newsStack
    //anchors.fill: parent
    width: parent.width
    height: parent.height
    color: Material.backgroundColor
    property string updateMethodNews: "refresh"
    property var allchats: ({})
    property int lastnewsid:0
    property string newstabstatus: ""

    BusyIndicator{
        id: newsBusy
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top:parent.top
        anchors.topMargin: mm
        width: 2*root.fontFactor*osSettings.bigFontSize
        height: 2*root.fontFactor*osSettings.bigFontSize
        z:2
        running: false
    }

    function newstypeHandling(newstype){
        try{newsBusy.running=true}catch(e){print(e)};
        replySignal("");
        switch(newstype){
        case "timeline":
            newstab.newstabstatus="Timeline";
            try{ Newsjs.newsfromdb(root.db,root.login,0, function(dbnews,lastid){
                lastnewsid=lastid;
                showNews(dbnews)
            })}catch(e){//Helperjs.showMessage("Error",e,root)
            };
            break;
        case "conversation":
            newsStack.updateMethodNews="conversation";
            newstab.newstabstatus="Conversations";
            Newsjs.chatsfromdb(root.db,root.login,0,[],function(news,lastid){
                lastnewsid=lastid;
                showNews(news)});
            break;
        case "favorites":
            newsStack.updateMethodNews="refresh";
            newstab.newstabstatus="Favorites";
            Service.updateView("Favorites");
            break;
        case "replies":
            newsStack.updateMethodNews="refresh";
            newstab.newstabstatus="Replies";
            Service.updateView("Replies");
            break;
        case "publictimeline":
            newsStack.updateMethodNews="refresh";
            newstab.newstabstatus="Public Timeline";
            Service.updateView("Public Timeline");
            break;
        case "groupnews":
            newsStack.updateMethodNews="refresh";
            newstab.newstabstatus="Groupnews";
            Service.showGroups();
            break;
        case "search":
            newsView.anchors.topMargin=7*mm;
            newstab.newstabstatus="Search";
            newsBusy.running=false;
            var leftoffset=(osSettings.osType=="Android")?2*root.fontFactor*osSettings.bigFontSize:0
            var component = Qt.createComponent("qrc:/qml/genericqml/Search.qml");
            var searchItem = component.createObject(newsStack,{y:mm,x:leftoffset,width:root.width-(leftoffset+mm),height: 1.5*root.fontFactor*osSettings.systemFontSize,selfdestroying:true});
            break;
        case "refresh":
            if (newstab.newstabstatus=="Timeline" || newstabstatus=="Timeline"){
                newsStack.updateMethodNews="append"
            } else if (newstab.newstabstatus=="Conversations" || newstabstatus=="Conversations"){
                newsStack.updateMethodNews="conversation"}
            else {newsStack.updateMethodNews="refresh"}

            if (newsSwipeview.stacktype=="Home"){
                Service.updateView(newstab.newstabstatus)
            }
            else if (newsSwipeview.stacktype=="DirectMessage"){
                Service.updateView("Direct Messages")
            }
            else if (newsSwipeview.stacktype=="Notifications"){
                Service.updateView("Notifications")
            }
            break;
        default:
            if (newstab.newstabstatus=="Timeline" || newstabstatus=="Timeline"){
                newsStack.updateMethodNews="append"
            } else if (newstab.newstabstatus=="Conversations" || newstabstatus=="Conversations"){
                newsStack.updateMethodNews="conversation"}
            else {newsStack.updateMethodNews="refresh"}
            //root.contactLoadType="news";
            if (newsSwipeview.stacktype=="Home"){
                Service.updateView(newstab.newstabstatus)
            }
            else if (newsSwipeview.stacktype=="Directmessage"){
                Service.updateView("Direct Messages")
            }
            else if (newsSwipeview.stacktype=="Notifications"){
                Service.updateView("Notifications")
            }
        }
    }

    function showNews(newsToShow){
        try{if (rootstackView.depth>1){rootstackView.pop()}}catch(e){}
        newsBusy.running=false;
        if(newsToShow.length>0){
            var currentTime= new Date()
            // downloadNotice.text=downloadNotice.text + "\n shownews start "+ Date.now();
            if (newsToShow.length==1){var method="refresh"}else{var method = newsStack.updateMethodNews}
            var msg = {'currentTime': currentTime, 'model': newsModel,'news':newsToShow,'method':method, 'options':globaloptions};
            newsWorker.sendMessage(msg);
        }
    }

    function search(term){
        if (term!=""){
            newstabstatus="Search";
            newsBusy.running=true;
            newsStack.updateMethodNews="refresh";
            xhr.setLogin(login.username+":"+Qt.atob(login.password));
            xhr.setUrl(login.server);
            xhr.setApi("/api/search");
            xhr.clearParams();
            xhr.setParam("q",term)
            xhr.get();}
        newsView.anchors.topMargin=mm
    }

    function getOldNews(){
        var currentTime= new Date();

        try{var oldnewsitemobject=newsModel.get(newsModel.count-1).newsitemobject;
            if(oldnewsitemobject.hasOwnProperty("lastcomment")){
                var lastnews_id=oldnewsitemobject.lastcomment.created_at;
            }else{
                var lastnews_id=oldnewsitemobject.created_at;
            }
        } catch(e){print(e);var lastnews_id=99999999999999}
        var messagetype=0;
        switch(newsSwipeview.stacktype){
        case "Home":messagetype=0;break;
        case "DirectMessages": messagetype=1;break;
        case "Notifications":messagetype=2;break;
        case "Replies":messagetype=3;break;
        default:messagetype=0;
        }

        if(newsSwipeview.stacktype!="Home"|| newstab.newstabstatus=="Timeline" || newstabstatus=="Timeline" ){
            Newsjs.newsfromdb(root.db,root.login, messagetype,function(news){
                var msg = {'currentTime': currentTime, 'model': newsModel,'news':news,'method':"", 'options':globaloptions};
                newsWorker.sendMessage(msg);
            },false,lastnews_id)}

        else if(newstab.newstabstatus=="Conversations" || newstabstatus=="Conversations"){
            var currentconversations=[];
            for (let c=0;c<newsModel.count;c++){
                currentconversations.push(parseInt(newsModel.get(c).newsitemobject.statusnet_conversation_id))
            }

            Newsjs.chatsfromdb(root.db,root.login, messagetype,currentconversations,function(news){
                var msg = {'currentTime': currentTime, 'model': newsModel,'news':news,'method':"", 'options':globaloptions};
                newsWorker.sendMessage(msg);
            },lastnews_id)}

        else if (newstab.newstabstatus=="Notifications" || newstabstatus=="Notifications"){}
        else{
            switch(newsSwipeview.stacktype){
            case "Home":messagetype=0;break;
            case "DirectMessages": messagetype=1;break;
            case "Notifications":messagetype=2;break;
            case "Replies":messagetype=3;break;
            default:messagetype=0;
            }

            try{xhr.setParam("max_id",newsModel.get(newsModel.count-1).newsitemobject.id-1);}catch(e){}
            xhr.get()
        }
    }
    Connections{
        target:xhr
        function onError(data,url,api,code){
            //if (data !="contactlist"){Helperjs.showMessage(qsTr("Network Error"),"API:\n" +login.server+api+"\n Return: \n"+data,root);}
            newsBusy.running=false;
        }
        function onSuccess(data,api){
            // downloadNotice.text=downloadNotice.text+ "\n xhr finished "+Date.now();
            const newsApiArray=["/api/statuses/friends_timeline",
                                "/api/direct_messages/all",
                                "/api/direct_messages/conversation",
                                "/api/direct_messages/new",
                                "/api/friendica/notification",
                                "/api/statuses/user_timeline",
                                "/api/conversation/show",
                                "/api/search",
                                "/api/statuses/public_timeline",
                                "/api/favorites",
                                "/api/statuses/replies",
                                "/api/lists/statuses",
                                "/api/statuses/update",
                                "/api/direct_messages/new"
                ];
            if(newsApiArray.includes(api)){
                Service.processNews(api,data)
                replySignal("")
            }
        }
    }

    Timer {id:replytimer; interval: 1000; running: false; repeat: false
        onTriggered: {
            newsBusy.running=true;
            if(newstab.newstabstatus=="Conversation"){
                showConversation(newsStack.timelineIndex-1,newsModel.get(0).newsitemobject)}
            else{
                if (newstab.newstabstatus=="Timeline" || newstabstatus=="Timeline"){
                    newsStack.updateMethodNews="append"
                } else if (newstab.newstabstatus=="Conversations" || newstabstatus=="Conversations"){
                    newsStack.updateMethodNews="conversation"}
                else {newsStack.updateMethodNews="refresh"}
                if (newsSwipeview.stacktype=="Home"){
                    Service.updateView(newstab.newstabstatus)
                }
                else if (newsSwipeview.stacktype=="DirectMessages"){
                    Service.updateView("Direct Messages")
                }
                else if (newsSwipeview.stacktype=="Replies"){
                    Service.updateView("Replies")
                }
                replySignal("")
            }
        }
    }

    Component {  id:footerComponent
        Rectangle{
            border.color: "#EEEEEE"
            color: Material.dialogColor
            border.width: 1
            width:newsView.width
            height:6*mm
            Text{
                color: Material.primaryTextColor
                font.pointSize: osSettings.systemFontSize
                anchors.centerIn: parent
                text:qsTr("More")
            }
            MouseArea{
                anchors.fill:parent
                onClicked:{
                    if (newsModel.count==0){
                        if (newsSwipeview.stacktype=="Home"){
                            Service.updateView(newstab.newstabstatus)
                        }
                        else if (newsSwipeview.stacktype=="DirectMessages"){
                            Service.updateView("Direct Messages")
                        }
                        else if (newsSwipeview.stacktype=="Notifications"){
                            Service.updateView("Notifications")
                        }
                        else if (newsSwipeview.stacktype=="Replies"){
                            Service.updateView("Replies")
                        }
                    }
                    else {getOldNews();}
                }
            }
        }
    }

    ListView {
        id: newsView
        property real oldContentY:0
        property bool viewdragged: false
        property bool viewdraggedpositive: false
        property string viewtype: "news"
        width: parent.width
        height: parent.height-3*mm
        anchors.margins: 0.5*mm
        clip: true
        spacing: 0
        header:MessageSend{id:messagesend;onHeightChanged: {if(state=="active"){newsView.positionViewAtBeginning()}}}
        footer: footerComponent
        model: newsModel
        delegate: Newsitem{}
        onDragStarted: oldContentY=contentY
        onDragEnded: {
            if(verticalOvershoot<-5*mm){
                viewdragged=true
            }
            else if (verticalOvershoot>5*mm){
                viewdraggedpositive=true
            }
            else{
                if((contentY-oldContentY)>15*mm){
                    //swipeIndicator.visible=false;
                    //newsSwipeview.height=rootstack.height;
                    //newsSwipeview.y=0;
                    //rootStackItem.state="fullscreen"
                }
                else if ((contentY-oldContentY)<-15*mm){
                    //swipeIndicator.visible=true;
                    //newsSwipeview.height=rootstack.height-12*mm;
                    //newsSwipeview.y=5*mm;
                    //rootStackItem.state=""
                }
            }
        }
        onViewdraggedChanged: {
            if (viewdragged){
                var onlynew=true;
                newsBusy.running=true;
                viewdragged=false
                if (newstab.newstabstatus=="Timeline" || newstabstatus=="Timeline"){
                    newsStack.updateMethodNews="append"
                } else if (newstab.newstabstatus=="Conversations" || newstabstatus=="Conversations"){
                    newsStack.updateMethodNews="conversation"}
                else {newsStack.updateMethodNews="refresh"}
                let currentnewstabstatus="Conversations";
                if (newstab.newstabstatus!=""){
                    currentnewstabstatus=newstab.newstabstatus
                }else if (newstabstatus!=""){
                    currentnewstabstatus=newstabstatus
                }

                if (newsSwipeview.stacktype=="Home"){
                    Newsjs.getLastNews(root.login,root.db,function(currentlastnews){
                        //print("currentlastnews "+currentlastnews+ " lastnewsid "+lastnewsid)
                        if (currentlastnews>lastnewsid){
                            if(currentnewstabstatus=="Timeline"){
                                try{ Newsjs.newsfromdb(root.db,root.login,0, function(dbnews,lastid){
                                    lastnewsid=lastid;
                                    showNews(dbnews)
                                })}catch(e){Helperjs.showMessage("Error",e,root)};
                            }
                            if(currentnewstabstatus=="Conversations"){
                                Newsjs.chatsfromdb(db,root.login,0,[],function(news,lastid){
                                    lastnewsid=lastid;
                                    showNews(news)});
                            }
                        } else {
                            Service.updateView(currentnewstabstatus)
                        }
                    });
                }
                else if (newsSwipeview.stacktype=="DirectMessages"){
                    Service.updateView("Direct Messages")
                }
                else if (newsSwipeview.stacktype=="Notifications"){
                    Service.updateView("Notifications")
                }
                else if (newsSwipeview.stacktype=="Replies"){
                    Service.updateView("Replies")
                }

            }
        }
        onViewdraggedpositiveChanged: {
            if (viewdraggedpositive){
                getOldNews();
                viewdraggedpositive=false
            }
        }
        ScrollBar.vertical: ScrollBar { }
    }

    ListModel{id: newsModel}

    WorkerScript {
        id: newsWorker
        source: "qrc:/js/newsworker.js"
    }


    Rectangle{
        id:downloadNotice
        property alias text: noticeText.text
        color:"white"
        border.color:"grey"
        z:1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom:parent.bottom
        anchors.bottomMargin: 2*mm
        width: noticeText.width+2*mm
        height: noticeText.height+2*mm
        visible: (downloadNotice.text!="")

        Text{
            id:noticeText
            color: "grey"
            anchors.centerIn: parent
            width: contentWidth
            height: contentHeight
            font.pointSize: osSettings.systemFontSize
            text:""
        }
    }

    Component.onCompleted: {
        root.newstypeSignal.connect(newstypeHandling);
        root.contactdetailsSignal.connect(showContact);
        root.newsSignal.connect(showNews);
        root.searchSignal.connect(search);
        try{newsModel.clear()} catch(e){}
        swipeIndicator.visible=true;
        //newsSwipeview.height=rootstack.height
        //newsSwipeview.y=5*mm;
        //rootStackItem.state=""
        root.globaloptions.hasOwnProperty("newsViewType")?newstab.newstabstatus=root.globaloptions.newsViewType:newstab.newstabstatus="Conversations";
        newstabstatus=newstab.newstabstatus;
        var messagetype=0;
        switch(newsSwipeview.stacktype){
        case "Home":messagetype=0;break;
        case "DirectMessages": messagetype=1;break;
        case "Notifications":messagetype=2;break;
        case "Replies":messagetype=3;break;
        default:messagetype=0;
        }

        if((newstabstatus=="Conversations")&&(newsSwipeview.stacktype=="Home")){
            Newsjs.chatsfromdb(db,login,messagetype,[],function(dbnews,lastid){
                lastnewsid=lastid;
                showNews(dbnews);
            })
        }
        else{
            Newsjs.newsfromdb(db,login,messagetype,function(dbnews,lastid){
                lastnewsid=lastid;
                showNews(dbnews)
            })}
    }
}
