//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


var html=[//Smileys
          '\u263A',
          '\ud83d\ude00',
          '\ud83d\ude02',
          '\ud83d\ude06',
          '\ud83d\ude07',
          '\ud83d\ude08',
          '\ud83d\ude09',
          '\ud83d\ude0B',
          '\ud83d\ude0D',
          '\ud83d\ude0E',
          '\ud83d\ude0F',
          '\ud83d\ude10',
          '\ud83d\ude15',
          '\ud83d\ude18',
          '\ud83d\ude1B',
          '\ud83d\ude1C',
          '\ud83d\ude1D',
          '\ud83d\ude1E',
          '\ud83d\ude1F',
          '\ud83d\ude20',
          '\ud83d\ude21',
          '\ud83d\ude22',
          '\ud83d\ude23',
          '\ud83d\ude24',
          '\ud83d\ude26',
          '\ud83d\ude27',
          '\ud83d\ude2C',
          '\ud83d\ude2D',
          '\ud83d\ude2E',
          '\ud83d\ude2F',
          '\ud83d\ude31',
          '\ud83d\ude32',
          '\ud83d\ude33',
          '\ud83d\ude31',
          '\ud83d\ude32',
          '\ud83d\ude33',
          '\ud83d\ude34',
          '\ud83d\ude37',
          '\ud83d\ude41',
          '\ud83d\ude42',
          '\ud83d\ude43',
          '\ud83d\ude44',
          '\ud83d\ude45',
          '\ud83d\ude46',
          '\ud83d\ude47',
          '\ud83d\ude4B',
          '\ud83d\ude4C',
          '\ud83d\ude4D',
          '\ud83d\ude4E',
          '\ud83d\ude4F',
          '\ud83e\udd2F',
          '\uD83D\uDC4D',
          '\u2764\uFE0F',
          '\uD83D\uDCA9',
          '\uD83D\uDC2D',
          '\uD83D\uDC2E',
          '\uD83D\uDC31',
          '\u2639',
          '\u263B',
          //Weather
          '\u2600',
          '\u2601',
          '\u263C',
          '\u2614',
          '\u2602',
          '\u2603',
          '\u2604',
          '\u26C4',
          '\u26C5',
          '\u26C8',
          //Leisure
          '\u2615',
          '\u26BD',
          '\u26BE',
          '\u26F1',
          '\u26F2',
          '\u26F3',
          '\u26F4',
          '\u26F5',
          '\u26F7',
          '\u26F8',
          '\u26F9',
          '\u26FA',
          '\u26FD',
          //Hand
          '\u261C',
          '\u261D',
          '\u261E',
          '\u261F',
          '\u2620',
          '\u2622',
          '\u2623',
          //Religion
          '\u2626',
          '\u262A',
          '\u262C',
          '\u262E',
          '\u262F',
          '\u26EA',
          '\u26E9'
        ]

var core=[
            {name:'&lt;3',url:
                'qrc:///images/smileys/core/smiley-heart.gif'},

            {name:'&lt;/3',url:
                'qrc:///images/smileys/core/smiley-brokenheart.gif'},

            {name:':-)',url:
                'qrc:///images/smileys/core/smiley-smile.gif'},

            {name:';-)',url:
                'qrc:///images/smileys/core/smiley-wink.gif'},

            {name:':-(',url:
                'qrc:///images/smileys/core/smiley-frown.gif'},

            {name:':-P',url:
                'qrc:///images/smileys/core/smiley-tongue-out.gif'},

            {name:':-X',url:
                'qrc:///images/smileys/core/smiley-kiss.gif'},

            {name:':-D',url:
                'qrc:///images/smileys/core/smiley-laughing.gif'},

            {name:':-O',url:
                'qrc:///images/smileys/core/smiley-surprised.gif'},

            {name:'\\o/',url:
                'qrc:///images/smileys/core/smiley-thumbsup.gif'},

            {name:'o.O',url:
                'qrc:///images/smileys/core/smiley-Oo.gif'},

            {name:":'(",url:
                'qrc:///images/smileys/core/smiley-cry.gif'},

            {name:":-!",url:
                'qrc:///images/smileys/core/smiley-foot-in-mouth.gif'},

            {name:":-/",url:
                'qrc:///images/smileys/core/smiley-undecided.gif'},

            {name:":-[",url:
                'qrc:///images/smileys/core/smiley-embarassed.gif'},

            {name:"8-)",url:
                'qrc:///images/smileys/core/smiley-cool.gif'},

            {name:':beer',url:
                'qrc:///images/smileys/core/beer_mug.gif'},

            {name:':coffee',url:
                'qrc:///images/smileys/core/coffee.gif'},

            {name:':facepalm',url:
                'qrc:///images/smileys/core/smiley-facepalm.gif'},

            {name:':like',url:
                'qrc:///images/smileys/core/like.gif'},

            {name:':dislike',url:
                'qrc:///images/smileys/core/dislike.gif'},

            {name:'~friendica',url:
                'qrc:///images/smileys/core/friendica-16.png'},

            {name:'red#',url:
                'qrc:///images/smileys/core/rm-16.png'}
        ]


var addon=[
            {name:':bunnyflowers',url:
                'qrc:///images/smileys/animals/bunnyflowers.gif'},

            {name:':chick',url:
                'qrc:///images/smileys/animals/chick.gif'},

            {name:':bumblebee',url:
                'qrc:///images/smileys/animals/bee.gif'},

            {name:':ladybird',url:
                'qrc:///images/smileys/animals/ladybird.gif'},

            {name:':bigspider',url:
                'qrc:///images/smileys/animals/bigspider.gif' },

            {name:':cat',url:
                'qrc:///images/smileys/animals/cat.gif'},

            {name:':bunny',url:
                'qrc:///images/smileys/animals/bunny.gif' },

            {name:':cow',url:
                'qrc:///images/smileys/animals/cow.gif' },

            {name:':crab',url:
                'qrc:///images/smileys/animals/crab.gif' },

            {name:':dolphin',url:
                'qrc:///images/smileys/animals/dolphin.gif' },

            {name:':dragonfly',url:
                'qrc:///images/smileys/animals/dragonfly.gif' },

            {name:':frog',url:
                'qrc:///images/smileys/animals/frog.gif'},

            {name:':hamster',url:
                'qrc:///images/smileys/animals/hamster.gif' },

            {name:':monkey',url:
                'qrc:///images/smileys/animals/monkey.gif' },

            {name:':horse',url:
                'qrc:///images/smileys/animals/horse.gif' },

            {name:':parrot',url:
                'qrc:///images/smileys/animals/parrot.gif' },

            {name:':tux',url:
                'qrc:///images/smileys/animals/tux.gif' },

            {name:':snail',url:
                'qrc:///images/smileys/animals/snail.gif' },

            {name:':sheep',url:
                'qrc:///images/smileys/animals/sheep.gif' },

            {name:':dog',url:
                'qrc:///images/smileys/animals/dog.gif'},

            {name:':elephant',url:
                'qrc:///images/smileys/animals/elephant.gif' },

            {name:':fish',url:
                'qrc:///images/smileys/animals/fish.gif' },

            {name:':giraffe',url:
                'qrc:///images/smileys/animals/giraffe.gif' },

            {name:':pig',url:
                'qrc:///images/smileys/animals/pig.gif'},

            //Baby

            {name:':baby',url:
                'qrc:///images/smileys/babies/baby.gif' },

            {name:':babycot',url:
                'qrc:///images/smileys/babies/babycot.gif' },


            {name:':pregnant',url:
                'qrc:///images/smileys/babies/pregnant.gif' },

            {name:':stork',url:
                'qrc:///images/smileys/babies/stork.gif' },


            //Confused
            {name:':confused',url:
                'qrc:///images/smileys/confused/confused.gif' },

            {name:':shrug',url:
                'qrc:///images/smileys/confused/shrug.gif' },

            {name:':stupid',url:
                'qrc:///images/smileys/confused/stupid.gif' },

            {name:':dazed',url:
                'qrc:///images/smileys/confused/dazed.gif' },
            //Cool 'qrc:///images/smileys

            {name:':affro',url:
                'qrc:///images/smileys/cool/affro.gif'},

            //Devil/Angel

            {name:':angel',url:
                'qrc:///images/smileys/devilangel/angel.gif'},

            {name:':cherub',url:
                'qrc:///images/smileys/devilangel/cherub.gif'},

            {name:':devilangel',url:
                'qrc:///images/smileys/devilangel/blondedevil.gif' },
            {name:':catdevil',url:
                'qrc:///images/smileys/devilangel/catdevil.gif'},

            {name:':devillish',url:
                'qrc:///images/smileys/devilangel/devil.gif'},

            {name:':daseesaw',url:
                'qrc:///images/smileys/devilangel/daseesaw.gif'},

            {name:':turnevil',url:
                'qrc:///images/smileys/devilangel/turnevil.gif' },

            {name:':saint',url:
                'qrc:///images/smileys/devilangel/saint.gif'},

            {name:':graveside',url:
                'qrc:///images/smileys/devilangel/graveside.gif'},

            //Unpleasent

            {name:':toilet',url:
                'qrc:///images/smileys/disgust/toilet.gif'},

            {name:':fartinbed',url:
                'qrc:///images/smileys/disgust/fartinbed.gif' },

            {name:':fartblush',url:
                'qrc:///images/smileys/disgust/fartblush.gif' },

            //Drinks

            {name:':tea',url:
                'qrc:///images/smileys/drink/tea.gif' },

            {name:':drool',url:
                'qrc:///images/smileys/drool/drool.gif'},

            //Sad

            {name:':crying',url:
                'qrc:///images/smileys/sad/crying.png'},

            {name:':prisoner',url:
                'qrc:///images/smileys/sad/prisoner.gif' },

            {name:':sigh',url:
                'qrc:///images/smileys/sad/sigh.gif'},

            //Smoking - only one smiley in here, maybe it needs moving elsewhere?

            {name:':smoking',url:
                'qrc:///images/smileys/smoking/smoking.gif'},

            //Sport

            {name:':basketball',url:
                'qrc:///images/smileys/sport/basketball.gif'},

            {name:':bowling',url:
                'qrc:///images/smileys/sport/bowling.gif'},

            {name:':cycling',url:
                'qrc:///images/smileys/sport/cycling.gif'},

            {name:':darts',url:
                'qrc:///images/smileys/sport/darts.gif'},

            {name:':fencing',url:
                'qrc:///images/smileys/sport/fencing.gif' },

            {name:':juggling',url:
                'qrc:///images/smileys/sport/juggling.gif'},

            {name:':skipping',url:
                'qrc:///images/smileys/sport/skipping.gif'},

            {name:':archery',url:
                'qrc:///images/smileys/sport/archery.gif'},

            {name:':surfing',url:
                'qrc:///images/smileys/sport/surfing.gif' },

            {name:':snooker',url:
                'qrc:///images/smileys/sport/snooker.gif' },

            {name:':horseriding',url:
                'qrc:///images/smileys/sport/horseriding.gif'},

            //Love

            {name:':iloveyou',url:
                'qrc:///images/smileys/love/iloveyou.gif'},

            {name:':inlove',url:
                'qrc:///images/smileys/love/inlove.gif'},

            {name:':~love',url:
                'qrc:///images/smileys/love/love.gif' },

            {name:':lovebear',url:
                'qrc:///images/smileys/love/lovebear.gif'},

            {name:':lovebed',url:
                'qrc:///images/smileys/love/lovebed.gif' },

            {name:':loveheart',url:
                'qrc:///images/smileys/love/loveheart.gif' },

            //Tired/Sleep

            {name:':countsheep',url:
                'qrc:///images/smileys/tired/countsheep.gif' },

            {name:':hammock',url:
                'qrc:///images/smileys/tired/hammock.gif'},

            {name:':pillow',url:
                'qrc:///images/smileys/tired/pillow.gif' },

            {name:':yawn',url:
                'qrc:///images/smileys/tired/yawn.gif'},

            //Fight/Flame/Violent

            {name:':2guns',url:
                'qrc:///images/smileys/fight/2guns.gif' },

            {name:':alienfight',url:
                'qrc:///images/smileys/fight/alienfight.gif' },

            {name:':army',url:
                'qrc:///images/smileys/fight/army.gif'},

            {name:':arrowhead',url:
                'qrc:///images/smileys/fight/arrowhead.gif'},

            {name:':bfg',url:
                'qrc:///images/smileys/fight/bfg.gif' },

            {name:':bowman',url:
                'qrc:///images/smileys/fight/bowman.gif' },

            {name:':chainsaw',url:
                'qrc:///images/smileys/fight/chainsaw.gif'},

            {name:':crossbow',url:
                'qrc:///images/smileys/fight/crossbow.gif'},

            {name:':crusader',url:
                'qrc:///images/smileys/fight/crusader.gif' },

            {name:':dead',url:
                'qrc:///images/smileys/fight/dead.gif' },

            {name:':hammersplat',url:
                'qrc:///images/smileys/fight/hammersplat.gif' },

            {name:':lasergun',url:
                'qrc:///images/smileys/fight/lasergun.gif' },

            {name:':machinegun',url:
                'qrc:///images/smileys/fight/machinegun.gif' },

            {name:':acid',url:
                'qrc:///images/smileys/fight/acid.gif' },

            //Fantasy - monsters and dragons fantasy.  The other type of fantasy belongs in adult

            {name:':alienmonster',url:
                'qrc:///images/smileys/fantasy/alienmonster.gif' },

            {name:':barbarian',url:
                'qrc:///images/smileys/fantasy/barbarian.gif' },

            {name:':dinosaur',url:
                'qrc:///images/smileys/fantasy/dinosaur.gif'},

            {name:':dragon',url:
                'qrc:///images/smileys/fantasy/dragon.gif'},

            {name:':draco',url:
                'qrc:///images/smileys/fantasy/dragonwhelp.gif'},

            {name:':ghost',url:
                'qrc:///images/smileys/fantasy/ghost.gif'},

            {name:':mummy',url:
                'qrc:///images/smileys/fantasy/mummy.gif'},

            //Food

            {name:':apple',url:
                'qrc:///images/smileys/food/apple.gif' },

            {name:':broccoli',url:
                'qrc:///images/smileys/food/broccoli.gif' },

            {name:':cake',url:
                'qrc:///images/smileys/food/cake.gif'},

            {name:':carrot',url:
                'qrc:///images/smileys/food/carrot.gif' },

            {name:':popcorn',url:
                'qrc:///images/smileys/food/popcorn.gif'},

            {name:':tomato',url:
                'qrc:///images/smileys/food/tomato.gif'},

            {name:':banana',url:
                'qrc:///images/smileys/food/banana.gif'},

            {name:':cooking',url:
                'qrc:///images/smileys/food/cooking.gif'},

            {name:':fryegg',url:
                'qrc:///images/smileys/food/fryegg.gif'},

            {name:':birthdaycake',url:
                'qrc:///images/smileys/food/birthdaycake.gif'},

            //Happy

            {name:':cloud9',url:
                'qrc:///images/smileys/happy/cloud9.gif'},

            {name:':tearsofjoy',url:
                'qrc:///images/smileys/happy/tearsofjoy.gif' },

            //Repsect

            {name:':bow',url:
                'qrc:///images/smileys/respect/bow.gif'},

            {name:':bravo',url:
                'qrc:///images/smileys/respect/bravo.gif'},

            {name:':hailking',url:
                'qrc:///images/smileys/respect/hailking.gif'},

            {name:':number1',url:
                'qrc:///images/smileys/respect/number1.gif' },

            //Laugh

            {name:':hahaha',url:
                'qrc:///images/smileys/laugh/hahaha.gif'},

            {name:':loltv',url:
                'qrc:///images/smileys/laugh/loltv.gif' },

            {name:':rofl',url:
                'qrc:///images/smileys/laugh/rofl.gif'},

            //Music

            {name:':drums',url:
                'qrc:///images/smileys/music/drums.gif'},


            {name:':guitar',url:
                'qrc:///images/smileys/music/guitar.gif'},

            {name:':trumpet',url:
                'qrc:///images/smileys/music/trumpet.gif' },

            //smileys that used to be in core

            {name:':headbang',url:
                'qrc:///images/smileys/oldcore/headbang.gif'},

            {name:':beard',url:
                'qrc:///images/smileys/oldcore/beard.png'},

            {name:':whitebeard',url:
                'qrc:///images/smileys/oldcore/whitebeard.png'},

            {name:':shaka',url:
                'qrc:///images/smileys/oldcore/shaka.gif'},

            {name:':\\.../',url:
                'qrc:///images/smileys/oldcore/shaka.gif'},

            {name:':\\ooo/',url:
                'qrc:///images/smileys/oldcore/shaka.gif' },

            {name:':headdesk',url:
                'qrc:///images/smileys/oldcore/headbang.gif' },

            //These two are still in core, so oldcore isn't strictly right, but we don't want too many directories

            {name:':-d',url:
                'qrc:///images/smileys/oldcore/laughing.gif'},

            {name:':-o',url:
                'qrc:///images/smileys/oldcore/surprised.gif' },

            // Regex killers - stick these at the bottom so they appear at the end of the English and
            // at the start of $OtherLanguage.

            {name:':cool',url:
                'qrc:///images/smileys/cool/cool.gif' },

            {name:':vomit',url:
                'qrc:///images/smileys/disgust/vomit.gif' },

            {name:':golf',url:
                'qrc:///images/smileys/sport/golf.gif' },

            {name:':football',url:
                'qrc:///images/smileys/sport/football.gif'},

            {name:':tennis',url:
                'qrc:///images/smileys/sport/tennis.gif' },

            {name:':alpha',url:
                'qrc:///images/smileys/fight/alpha.png' },

            {name:':marine',url:
                'qrc:///images/smileys/fight/marine.gif' },

            {name:':sabre',url:
                'qrc:///images/smileys/fight/sabre.gif' },

            {name:':tank',url:
                'qrc:///images/smileys/fight/tank.gif' },

            {name:':viking',url:
                'qrc:///images/smileys/fight/viking.gif' },

            {name:':gangs',url:
                'qrc:///images/smileys/fight/gangs.gif' },

            {name:':dj',url:
                'qrc:///images/smileys/music/dj.gif'},

            {name:':elvis',url:
                'qrc:///images/smileys/music/elvis.gif'},

            {name:':violin',url:
                'qrc:///images/smileys/music/violin.gif'},
        ]


var adult=[
            {
                name:'(o)(o) ',url:
                'qrc:///images/smileys/adult/tits.gif'},

            {name:'(.)(.) ',url:
                'qrc:///images/smileys/adult/tits.gif'},

            {name:':bong',url:
                'qrc:///images/smileys/adult/bong.gif'},

            {name:':sperm',url:
                'qrc:///images/smileys/adult/sperm.gif'},

            {name:':drunk',url:
                'qrc:///images/smileys/adult/drunk.gif'},

            {name:':finger',url:
                'qrc:///images/smileys/adult/finger.gif'}
        ]
