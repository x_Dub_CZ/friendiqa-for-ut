//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/service.js" as Service
import "qrc:/qml/configqml"
import "qrc:/qml/genericqml"

Page{
    //anchors.fill: parent
    width:root.width
    height:root.height

    Label {
        text: qsTr("News as")
        font.pointSize:osSettings.systemFontSize
        x: root.fontFactor*osSettings.bigFontSize; y: 2*root.fontFactor*osSettings.bigFontSize
    }

    Rectangle{
        x: root.fontFactor*osSettings.bigFontSize; y: 4*root.fontFactor*osSettings.bigFontSize;
        width: newsTypeField.contentWidth+2*mm; height: 2*root.fontFactor*osSettings.bigFontSize
        color: Material.dialogColor//"#F3F3F3"
        radius: 0.5*mm
        Label{
            id: newsTypeField
            anchors.fill: parent
            font.pointSize:osSettings.bigFontSize
            text:qsTr("Conversations")
        }
        MouseArea{
            anchors.fill:parent
            onClicked:newstypemenu.popup()
        }
    }
    Menu {
        id:newstypemenu
        width:12*root.fontFactor*osSettings.bigFontSize
        MenuItem {
            font.pointSize: osSettings.bigFontSize
            text: qsTr("Timeline")
            onTriggered: {newsTypeField.text=qsTr("Timeline");
                Service.updateglobaloptions(root.db,"newsViewType","Timeline");}
        }
        MenuItem {
            font.pointSize: osSettings.bigFontSize
            text: qsTr("Conversations")
            onTriggered: {newsTypeField.text=qsTr("Conversations");
                Service.updateglobaloptions(root.db,"newsViewType","Conversations");}
        }
    }


    Label {
        text: qsTr("Max. News")
        font.pointSize: osSettings.systemFontSize
        x: root.fontFactor*osSettings.bigFontSize; y:8*root.fontFactor*osSettings.bigFontSize
    }

    Slider{ id: maxNews
        x:6*root.fontFactor*osSettings.bigFontSize; y: 10*root.fontFactor*osSettings.bigFontSize;
        width: root.width/2;height:2*root.fontFactor*osSettings.bigFontSize
        from: 0;to:2000; stepSize: 100
        value:  root.globaloptions.hasOwnProperty("max_news")?root.globaloptions.max_news:1000
    }

    Rectangle{
        color: Material.dialogColor
        x: root.fontFactor*osSettings.bigFontSize; y: 10*root.fontFactor*osSettings.bigFontSize;
        width: 4*root.fontFactor*osSettings.bigFontSize; height: 2*root.fontFactor*osSettings.bigFontSize;
        radius: 0.5*mm
        TextEdit{id:maxNewsText;
            anchors.fill: parent
            font.pointSize: osSettings.bigFontSize
            verticalAlignment:TextEdit.AlignRight
            color: Material.primaryTextColor
            text:maxNews.value
            selectByMouse: true
            onTextChanged: {
                Service.updateglobaloptions(root.db,"max_news",text);
            }
        }
    }

    CheckBox{
        id: nsfwCheckbox
        x: root.fontFactor*osSettings.bigFontSize
        y: 14*root.fontFactor*osSettings.bigFontSize
        font.pointSize: osSettings.bigFontSize
        text: qsTr("Hide #nsfw?")
        checked:(globaloptions["hide_nsfw"]==1)?true:false
        onClicked: {
            toggle();
            if(nsfwCheckbox.checked==true){
                Service.updateglobaloptions(root.db,"hide_nsfw",0);nsfwCheckbox.checked=false;
            }
            else{
                Service.updateglobaloptions(root.db,"hide_nsfw",1);nsfwCheckbox.checked=true;
            }
        }
    }


//    CheckBox{
//        id: darkmodeCheckbox
//        tristate:true
//        x: root.fontFactor*osSettings.bigFontSize
//        y: 24*root.fontFactor*osSettings.bigFontSize
//        font.pointSize: osSettings.bigFontSize
//        text: qsTr("Dark Mode")
//        checked:(globaloptions["view_darkmode"]==1)?true:false
//        onClicked: {
//            toggle();
//            if(darkmodeCheckbox.checked==true){
//                Service.updateglobaloptions(root.db,"view_darkmode",0);darkmodeCheckbox.checked=false;
//                root.Material.theme=Material.Light
//            }
//            else{
//                Service.updateglobaloptions(root.db,"view_darkmode",1);darkmodeCheckbox.checked=true;
//                root.Material.theme=Material.Dark
//            }
//        }
//    }
    Column{
        x: root.fontFactor*osSettings.bigFontSize
        y: 18*root.fontFactor*osSettings.bigFontSize
        Label{
            text: qsTr("Dark Mode")
            font.pointSize: osSettings.systemFontSize}

        RadioButton{
            text: qsTr("System")
            checked: (globaloptions["view_darkmode"]==0 || globaloptions["view_darkmode"]==undefined)?true:false
            font.pointSize: osSettings.bigFontSize
            onClicked: {
                if(checked==true){
                    Service.updateglobaloptions(root.db,"view_darkmode",0);
                    root.Material.theme=Material.System
                    }
            }
        }
        RadioButton{
            text: qsTr("Dark")
            checked: (globaloptions["view_darkmode"]==1)?true:false
            font.pointSize: osSettings.bigFontSize
            onClicked: {
                if(checked==true){
                    Service.updateglobaloptions(root.db,"view_darkmode",1);
                    root.Material.theme=Material.Dark
                    }
            }
        }
        RadioButton{
            text: qsTr("Light")
            checked: (globaloptions["view_darkmode"]==2)?true:false
            font.pointSize: osSettings.bigFontSize
            onClicked: {
                if(checked==true){
                    Service.updateglobaloptions(root.db,"view_darkmode",2);
                    root.Material.theme=Material.Light
                    }
            }
        }
    }

    MButton {
        anchors.right: parent.right; //anchors.rightMargin: mm;
        anchors.top: parent.top
        anchors.topMargin: 2*root.fontFactor*osSettings.bigFontSize
        width: 2*root.fontFactor*osSettings.bigFontSize;
        text: "?"
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            rootstackView.push("qrc:/qml/configqml/InfoBox.qml");
        }
    }
//    MButton{
//        id:closeButton
//        //         height: 2*root.fontFactor*osSettings.bigFontSize
//        width: 2*root.fontFactor*osSettings.bigFontSize;
//        anchors.top: parent.top
//        anchors.topMargin:root.fontFactor*osSettings.bigFontSize
//        anchors.right: parent.right
//        anchors.rightMargin: 1*mm
//        text:   "\uf057"
//        font.pointSize: osSettings.bigFontSize
//        onClicked:{rootstackView.pop()}
//    }
}
