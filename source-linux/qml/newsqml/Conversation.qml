//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// ConversationView with button
import QtQuick 2.0
import QtQuick.Controls 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/qml/genericqml"
import "qrc:/qml/newsqml"

Page {
    id:conversationList
    property var news:[]
//    color: "white"
    //function backRequested(){pageStack.pop()}
//    width:root.width
//    height: root.height-7*mm

    function getDateDiffString (seconds){
        var timestring="";
        if (seconds<60) {timestring= Math.round(seconds) +  " " +qsTr("seconds");}
        else if (seconds<90){timestring= Math.round(seconds/60) +  " " +qsTr("minute") ;}
        else if (seconds<3600){timestring= Math.round(seconds/60) +  " " +qsTr("minutes");}
        else if (seconds<5400){timestring= Math.round(seconds/3600) +  " " +qsTr("hour");}
        else if (seconds<86400){timestring= Math.round(seconds/3600) +  " " +qsTr("hours");}
        else if (seconds<129600){timestring= Math.round(seconds/86400) +  " " +qsTr("day");}
        else if (seconds<3888000){timestring= Math.round(seconds/86400) +  " " +qsTr("days");}
        else if (seconds<5832000){timestring= Math.round(seconds/3888000) +  " " +qsTr("month");}
        else if (seconds<69984000){timestring= Math.round(seconds/3888000) +  " " +qsTr("months");}
        else {timestring= Math.round(seconds/46656000) +  " " + qsTr("years");}

        return timestring;
    }

    function getActivitiesView(newsitemobject){
        var likeText="";var dislikeText="";var attendyesText="";var attendnoText="";var attendmaybeText="";  var self={};
        try{if (newsitemobject.messagetype==0&&newsitemobject.hasOwnProperty('friendica_activities')){
           if (newsitemobject.friendica_activities.like.length>0){
              if (newsitemobject.friendica_activities.like.length==1){likeText= newsitemobject.friendica_activities.like[0].name+" "+ qsTr("likes this.")}
              else {likeText= newsitemobject.friendica_activities.like.length+" "+ qsTr("like this.")}
           }
           if (newsitemobject.friendica_activities.dislike.length>0){
              if (newsitemobject.friendica_activities.dislike.length==1){dislikeText= newsitemobject.friendica_activities.dislike[0].name+" "+ qsTr("doesn't like this.")}
              else {dislikeText= newsitemobject.friendica_activities.dislike.length+" "+ qsTr("don't like this.")}
           }
           if (newsitemobject.friendica_activities.attendyes.length>0){
              if (newsitemobject.friendica_activities.attendyes.length==1){attendyesText=newsitemobject.friendica_activities.attendyes[0].name+" "+ qsTr("will attend.")}
              else {attendyesText= newsitemobject.friendica_activities.attendyes.length+" "+ qsTr("persons will attend.")}
           }
           if (newsitemobject.friendica_activities.attendno.length>0){
              if (newsitemobject.friendica_activities.attendno.length==1){attendnoText= newsitemobject.friendica_activities.attendno[0].name+" "+ qsTr("will not attend.")}
              else {attendnoText= newsitemobject.friendica_activities.attendno.length+" "+ qsTr("persons will not attend.")}
           }
           if (newsitemobject.friendica_activities.attendmaybe.length>0){
              if (newsitemobject.friendica_activities.attendmaybe.length==1){attendmaybeText= newsitemobject.friendica_activities.attendmaybe[0].name+" "+ qsTr("may attend.")}
              else {attendmaybeText= newsitemobject.friendica_activities.attendmaybe.length+" "+ qsTr("persons may attend.")}
           }
           //var friendica_activities_self=JSON.parse(newsitemobject.friendica_activities_self);
            }} catch(e){print("Activities "+e+ " "+JSON.stringify(newsitemobject.friendica_activities))}
        return {likeText:likeText,dislikeText:dislikeText,attendyesText:attendyesText,attendnoText:attendnoText,attendmaybeText:attendmaybeText}
    }


   ListView {
          id: conversationView
          property string viewtype: "conversation"
          width: conversationList.width//-4*mm
          height:conversationList.height-2*root.fontFactor*osSettings.bigFontSize//-20*mm
          clip: true
          spacing: 0
          //footer: MessageSend{conversation:true}
          model: conversationModel
          delegate: Newsitem{}
          }
    BusyIndicator{
         id: conversationBusy
         anchors.horizontalCenter: conversationView.horizontalCenter
         anchors.top:conversationView.top
         anchors.topMargin: mm
         width: 2*root.fontFactor*osSettings.bigFontSize
         height: 2*root.fontFactor*osSettings.bigFontSize
         running: true
       }

    Connections{
        target:newstab
        function onConversationChanged(){
            if(newstab.conversation.length==0){
                rootstackView.pop()
            } else { conversationBusy.running=false;
                conversationModel.clear();
                var currentTime= new Date();
                var msg = {'currentTime': currentTime, 'model': conversationModel,'news':newstab.conversation, 'method':'conversation', 'options':globaloptions};
                conversationWorker.sendMessage(msg)
            }
        }
    }

    ListModel{id: conversationModel}

    WorkerScript {
        id: conversationWorker
        source: "qrc:/js/newsworker.js"
    }

    MButton {
            id: closeButton
            anchors.top: parent.top
            anchors.topMargin: 1*mm
            anchors.right: parent.right
            anchors.rightMargin: 1*mm
            width: 2*root.fontFactor*osSettings.bigFontSize;
            text: "\uf057"
            onClicked: {
                //newsView.positionViewAtIndex(newsStack.conversationIndex,ListView.Beginning);
                newstab.conversation=[];
                if (rootstackView.depth>1){ rootstackView.pop()}
            }
    }

    Component.onCompleted: {
        if (news.length>0){var currentTime= new Date();
        var msg = {'currentTime': currentTime, 'model': conversationModel,'news':news,'method':'conversation', 'options':globaloptions};
        conversationWorker.sendMessage(msg)}
    }
}
