//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.11
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/service.js" as Service
import "qrc:/qml/configqml"
import "qrc:/qml/genericqml"

Page{
    //anchors.fill: parent
    width:root.width
    height:root.height

    TabBar {
        id: configbar
        width: parent.width-3*root.fontFactor*osSettings.bigFontSize//osSettings.osType=="Android"?parent.width-3*root.fontFactor*osSettings.bigFontSize:parent.width
        height: 2*root.fontFactor*osSettings.bigFontSize
        x: osSettings.osType=="Android"?2*osSettings.bigFontSize:0
          //visible: !wideScreen
        position:TabBar.Header
        currentIndex: 0
        TabButton {
            text: qsTr("Appearance")
            font.pointSize: osSettings.systemFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            width:6*root.fontFactor*osSettings.bigFontSize
        }
        TabButton {
            text:  qsTr("Sync")
            font.pointSize: osSettings.systemFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            width:10*root.fontFactor*osSettings.bigFontSize
        }
        TabButton {
            text:  qsTr("Start")
            visible:osSettings.osType=="Linux"
            font.pointSize: osSettings.systemFontSize
            height: 2*root.fontFactor*osSettings.bigFontSize
            width:10*root.fontFactor*osSettings.bigFontSize
        }
    }

    LeftDrawerLinux{
        id:leftDrawer
        visible: wideScreen&&rootstackView.depth<2
        width: visible?osSettings.systemFontSize*15:0
        height: root.height-bar.height
    }

    LeftDrawerAndroid{
        id: leftDrawerAndroid
    }

    StackLayout{
        id:configTabView
        //anchors.fill: parent
        width: wideScreen&&rootstackView.depth<2?parent.width-leftDrawer.width-mm:parent.width-mm//newstabitem.width/3*2:newstabitem.width
        x: leftDrawer.width
        y: configbar.height
        height: parent.height-configbar.height-mm
        currentIndex: configbar.currentIndex
//        onCurrentIndexChanged:{
//            if (currentIndex==1){
//                contactsSignal("")
//            }
//            else if (currentIndex==2){
//                contactsSignal("")
//            }
//            else if (currentIndex==3){groupsSignal(root.login.username)}
//        }

        Loader{
            id: appearanceLoader
            source:(configTabView.currentIndex==0)? "qrc:/qml/configqml/ConfigAppearancePage.qml":""
        }

        Loader{
            id: syncLoader
            source:(configTabView.currentIndex==1)? "qrc:/qml/configqml/SyncConfig.qml":""
        }

        Loader{
            id: startLoader
            source:(configTabView.currentIndex==2)? "qrc:/qml/configqml/ConfigStartPage.qml":""
        }
    }
    MButton{
        id:closeButton
        //         height: 2*root.fontFactor*osSettings.bigFontSize
        width: 2*root.fontFactor*osSettings.bigFontSize;
        anchors.top: parent.top
        anchors.topMargin:2*root.fontFactor*osSettings.bigFontSize
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        text:   "\uf057"
        font.pointSize: osSettings.bigFontSize
        onClicked:{rootstackView.pop()}
    }
}
