//  This file is part of Friendiqa
//  https://git.friendi.ca/lubuwest/Friendiqa
//  Copyright (C) 2020 Marco R. <thomasschmidt45@gmx.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  In addition, as a special exception, the copyright holders give
//  permission to link the code of portions of this program with the
//  OpenSSL library under certain conditions as described in each
//  individual source file, and distribute linked combinations including
//  the two.
//
//  You must obey the GNU General Public License in all respects for all
//  of the code used other than OpenSSL. If you modify file(s) with this
//  exception, you may extend this exception to your version of the
//  file(s), but you are not obligated to do so. If you do not wish to do
//  so, delete this exception statement from your version. If you delete
//  this exception statement from all source files in the program, then
//  also delete it here.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.5
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "qrc:/js/helper.js" as Helperjs
import "qrc:/js/image.js" as Imagejs
import "qrc:/qml/genericqml"

Page{
    id:imageDialog
    property var attachImageURLs: []
    property string imageId: ""
    property string currentAlbum:""
    property var contacts: []
    property var groups: []
    property var contact_allow:login.permissions[0]
    property var contact_deny:login.permissions[1]
    property var group_allow:login.permissions[2]
    property var group_deny:login.permissions[3]
    property int imageNo: 0

    function uploadSelectedImage(inumber){
        xhr.setUrl(login.server);
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setApi("/api/friendica/photo/create.json");
        xhr.clearParams();
        xhr.setParam("desc",imageUploadModel.get(inumber).description);
        if(album.editText!=""){xhr.setParam("album", album.editText)}else{xhr.setParam("album", album.currentText)};
        //if (group_allow.length>0) {xhr.setParam("group_allow", Helperjs.cleanArray(group_allow))};
        //if (group_deny.length>0) {xhr.setParam("group_deny",  Helperjs.cleanArray(group_deny))};
        //if (contact_allow.length>0) {xhr.setParam("contact_allow", Helperjs.cleanArray(contact_allow))};
        //if (contact_deny.length>0) {xhr.setParam("contact_deny",  Helperjs.cleanArray(contact_deny))};
        xhr.setImageFileParam("media", imageUploadModel.get(inumber).imageUrl );
        xhr.post();
     }


    function updateImage(){
        xhr.setUrl(login.server);
        xhr.setLogin(login.username+":"+Qt.atob(login.password));
        xhr.setApi("/api/friendica/photo/update.json");
        xhr.clearParams();
        xhr.setParam("desc",imageUploadModel.get(0).description);
        xhr.setParam("album", currentAlbum);
        xhr.setParam("album_new", album.currentText);
        xhr.setParam("photo_id", imageId);
        xhr.post();
     }


    function attachImage(url){
        if (url.indexOf(",")>0){
            let urlArray=url.split(",");
            for (let file in urlArray){attachImage(urlArray[file])}
        } else{
            if(url!=""){
                imageUploadModel.append({"imageUrl":url,"description":"","imageUploaded":false,"imageRotation":0})
            }
        }
    }
    //border.color: "grey"
    y:1
    width:root.width-mm
    height:root.height-5*mm
    property string directory: ""

    Connections{
        target:xhr
        //onError:{print(data)}//if (data=="image"){Helperjs.showMessage()}}
        onSuccess:{
            if (imageId==""){
                imageNo=imageNo+1;
                if(imageNo<imageUploadModel.count){
                    uploadSelectedImage(imageNo);
                }else{
                    Imagejs.requestList(login,db, true,root,function(obj){
                        fotorectangle.newimages=obj;
                        photoStack.pop()
                    })

                }} else{
                Imagejs.updateImage(db,login,"image",filesystem,imageId,root,function(){
                    Imagejs.requestList(login,db, true,root,function(obj){
                        fotorectangle.newimages=obj;
                        fotoSignal(login,"backButton");
                        //photoStack.pop()
                    })
                })
            }
        }
    }

    Row{
        id:buttonRow
        anchors.top: parent.top
        anchors.topMargin: 1*mm
        anchors.right: parent.right
        anchors.rightMargin: 1*mm
        spacing:5*mm
        Label{
            font.pointSize: osSettings.bigFontSize
            font.bold: true
            text:qsTr("Upload to album")
        }
//        BlueButton{
//            id:permButton
//            text: ((contact_allow.length==0)&&(contact_deny.length==0)&&(group_allow.length==0)&&(group_deny.length==0))?"\uf09c":"\uf023"
//            onClicked: {
//                var component = Qt.createComponent("qrc:/qml/genericqml/PermissionDialog.qml");
//                var permissions = component.createObject(imageDialog,{"x":mm,"y":40*mm});
//            }
//        }
        
     MButton{
            id:closeButton
            text:   "\uf057"
            width: 2.5*root.fontFactor*osSettings.bigFontSize
            font.pointSize: osSettings.bigFontSize
            onClicked:{photoStack.pop()}
        }
    }

    ComboBox{
        id: album
        x: 3*mm
        y: 3*root.fontFactor*osSettings.bigFontSize
        width: root.width/2;
        height: 2.5*root.fontFactor*osSettings.bigFontSize//5*mm;
        font.pointSize: osSettings.bigFontSize
        editable:true
        model: albumModel

//        delegate: ItemDelegate {
//            width: album.width
//            contentItem: Text {
//                //text: modelData
//                color: "white"
//                font: album.font
//                font.pixelSize: album.font.pixelSize
//                elide: Text.ElideRight
//                verticalAlignment: Text.AlignVCenter
//            }
//            //highlighted: album.highlightedIndex === index
//        }
        onAccepted: {
            if (find(currentText) === -1) {
                albumModel.append({text: editText})
                currentIndex = find(editText)
            }
        }
    }

    ListModel{id:albumModel}
//    Text {
//      text: qsTr("Album")
//     x: 4*mm; y: 10*mm
//    }

//    Text {
//      text: qsTr("Image")
//     x: 4*mm; y: 17*mm
//    }

//    Text {
//      text: qsTr("Description")
//     x: 4*mm; y: 33*mm
//    }

    ListView{
        id: imageUploadView
        x:3*mm //23*mm
        y: 11*mm + album.height
        width: imageDialog.width-5*mm //25*mm
        height: parent.height -(12*root.fontFactor*osSettings.bigFontSize)//root.width/2 //25*mm
        model: imageUploadModel
        delegate: imageDelegate
        footer: imageId==""?imageFooter:null
        clip:true
        orientation: ListView.Horizontal
        spacing: mm
        DropArea{
            anchors.fill: parent
            onDropped: {
                if (drop.keys.includes('text/uri-list')){
                    var urllist=drop.text.split('\n');
                    for(var i=0;i< urllist.length;i++){
                        var droptext = urllist[i].replace(/(\r\n|\n|\r)/gm, "");
                        attachImage(droptext)
                    }
                }
            }
        }
    }

    BusyIndicator{
      id: uploadBusy
      running: false
      anchors.horizontalCenter: imageUploadView.horizontalCenter
      anchors.top:imageUploadView.top
      anchors.topMargin: 2*mm
      width:10*mm
      height: 10*mm
    }

    ListModel{
        id: imageUploadModel
    }

    Component{
        id: imageDelegate
        Rectangle{
            width:root.width/2 //Math.max(20*mm,descriptionInput.contentWidth)
            height:imageUploadView.height-3*root.fontFactor*osSettings.bigFontSize// 20*mm
            color: Material.backgroundColor
            Image{
                id: uploadImage
                width: root.width/2-mm //20*mm
                height: imageUploadView.height-(3*root.fontFactor*osSettings.bigFontSize+2*mm)
                fillMode: Image.PreserveAspectFit
                source:imageUrl
                onVisibleChanged: descriptionInput.focus=true;
                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        imageUploadModel.remove(index)
//                        attachImageURLs.splice(attachImageURLs.indexOf(imageUrl,1))
//                        attachImage(imageUrl)
                    }
                }
            }

            Rectangle{
                color: Material.backgroundColor
                border.color: "grey"
                anchors.top: uploadImage.bottom
                anchors.topMargin: mm
                 //x: mm; y: 15*mm;
                 width: root.width/2-mm  //Math.max(root.width/2-mm, descriptionInput.contentWidth);
                 height: 2.5*root.fontFactor*osSettings.bigFontSize +2*mm //5*mm;
                 TextField{
                 //TextInput {
                     id: descriptionInput
                     anchors.fill: parent
                     anchors.margins: mm
                     font.pointSize: osSettings.systemFontSize
                     selectByMouse: true
                     placeholderText: qsTr("Description")
                     text:description!=""?description:""
                     onTextChanged: imageUploadModel.set(index,{"description":descriptionInput.text});
                }
            }
        }
    }
    Component{
        id: imageFooter
        BlueButton{
            width: 5*root.fontFactor*osSettings.bigFontSize
            height:imageUploadView.height-3*root.fontFactor*osSettings.bigFontSize
            color: Material.backgroundColor
            text:"\u002b"
            fontSize: 3*osSettings.bigFontSize
            onClicked:{
                imagePicking=true;
                var imagePicker = Qt.createQmlObject('import QtQuick 2.0; import "qrc:/qml/genericqml";'+
                osSettings.imagePickQml+'{multiple : false;onReady: {'+
                'attachImage(imageUrl)}}',imageDialog,"imagePicker");
                 imagePicker.pickImage()}
        }
    }


    Button{
        id:uploadButton
        height: 2*root.fontFactor*osSettings.bigFontSize
        x:4*mm;
        anchors.top:imageUploadView.bottom
        anchors.topMargin: mm
        //y:root.width/2+18*mm //40*mm
        text:   imageId==""?qsTr("Upload"):qsTr("Change")
        font.pointSize: osSettings.bigFontSize
        onClicked:{
            if(album.currentText==""&&album.editText==""){Helperjs.showMessage(qsTr("Error"),qsTr(" No album name given"), imageDialog)}
            else if (imageId!=""){uploadBusy.running=true; updateImage()}
            else{newimageProgress.visible=true;
                if (imageUploadModel.count>0){
                    uploadSelectedImage(0)
            }}
        }
    }
    ProgressBar{
        id: newimageProgress
        width: 15*mm
        height: buttonRow.height
        anchors.top: parent.top
        anchors.right:buttonRow.left
        anchors.rightMargin:mm
        visible: false
        value: imageNo/imageUploadModel.count
    }

    Component.onCompleted:{
        albumModel.append({"text":""});
        try{Helperjs.readField("album",db,"imageData",login.username,function(storedAlbums){
          for (var n in storedAlbums){
              albumModel.append({"text":storedAlbums[n]})}
        })}
        catch (e){print(e)}
        if(attachImageURLs.length>0){
            for (var n in attachImageURLs){attachImage(attachImageURLs[n])}
        }
    }
}
